# Argument types
## imm
An immediate primitive, that is more closely defined on a per instruction basis.
## ref
A reference to a register.
## const
A static constant.
## lbl
A label reference.

# Instructions
## Branch
 - op\_1/imm:op\_2/imm = signed relative offset  
    Jump to relative 32-bit signed offset.
## Call
 - op\_1/ref, op\_2/ref, ..., op\_n/ref = closure, arguments...  
    Call closure with arguments.
## TailCall
 - op\_1/ref, op\_2/ref, ..., op\_n/ref = closure, arguments...  
    Tail call closure with arguments.
## Return
 - *none*  
    Return with Nil.
 - op\_1 = return value  
    Return with value.

## Equal
 - op\_1/ref:op\_n/ref = registers to compare  
    Compare any two values for equality.
## Nequal
 - op\_1/ref:op\_n/ref = registers to compare  
    Compare any two values for inequality.
## Less
 - op\_1/ref:op\_n/ref = registers to compare  
    Compare any two values for ordering (less).
## Greater
 - op\_1/ref:op\_n/ref = registers to compare  
    Compare any two values for ordering (greater).
## LessEq
 - op\_1/ref:op\_n/ref = registers to compare  
    Compare any two values for ordering (less or equal).
## GreaterEq
 - op\_1/ref:op\_n/ref = registers to compare  
    Compare any two values for ordering (greater or equal).
## Nan
 - op\_1/ref = register to check  
    Check floating point number for NaN.
## Inf
 - op\_1/ref = register to check  
    Check floating point number for infinity.
## Ninf
 - op\_1/ref = register to check  
    Check floating point number for negative infinity.
## Normal
 - op\_1/ref = register to check  
    Check floating point number for normality.
## Subnormal
 - op\_1/ref = register to check  
    Check floating point number for subnormality.
## Match
 - op\_1/ref, op\_2/ref, ..., op\_n/ref = closure, arguments  
    Check if closure can be called with arguments.
## Not
 - *none*  
    Negate last comparison.
## Trueish
 - op\_1/ref = register to check  
    Check if value is trueish.  Nil and false are falseish, everything else is trueish.

## Add
 - op\_1/ref:op\_n/ref = registers/numbers to add  
    Add n registers.
## Sub
 - op\_1/ref:op\_n/ref = registers/numbers to subtract  
    Subtract n registers.
## Mul
 - op\_1/ref:op\_n/ref = registers/numbers to multiply  
    Multiply n registers.
## Div
 - op\_1/ref, op\_2/ref = registers/numbers to divide  
    Divide two registers.
## Mod
 - op\_1/ref, op\_2/ref = registers/numbers to modulo  
    Modulo two registers.
## Neg
 - op\_1 = register/number to negate  
    Negate number.
## BitAnd
 - op\_1/ref:op\_n/ref = registers/integers to bitwise and  
    Bitwise and n integers.
## BitOr
 - op\_1/ref:op\_n/ref = registers/integers to bitwise or  
    Bitwise or n integers.
## BitXor
 - op\_1/ref:op\_n/ref = registers/integers to bitwise xor  
    Bitwise xor n integers.
## BitNot
 - op\_1 = register/integer to bitwise negate  
    Bitwise not an integer.
## ShiftL
 - op\_1/ref, op\_2/ref = integer, shift
    Bitwise op\_1 by op\_2 bits to the left
## ShiftR
 - op\_1/ref, op\_2/ref = integer, shift
    Bitwise op\_1 by op\_2 bits to the right

## Closure
 - op\_1/label, 0/imm, op\_3/imm, op\_4/ref, ..., op\_n/ref = label to function, 0/dynamic, capture, parameters
    Create a lexical closure with the specified label, op_3 last registers will be capture, op\_4 through op\_n will be
    the patterns.
 - op\_1/label, 1/imm, op\_3/imm, op\_4/ref, ..., op\_n/ref = label to function, 1/dynamic, mutable, parameters  
    Create a dynamic closure with the specified label, op_3 defines whether or not its scope is mutable, op\_4 through
    op\_n will be the patterns.
## Load
 - op\_1/const:op\_2/const = reference to a static constant  
    Loads a static constant from the static program memory.
## Ref
 - op\_1/ref = reference to a register  
    Loads a register to a new register.
## Pop
 - op\_1/imm
    Pops the last op\_1 registers.

## Cons
 - op\_1/ref, ..., op\_n-1 op\_n = value, ..., value, List  
    Construct a list with n-1 values and a list.
## Head
 - op\_1/ref = List  
    Take the head of a list.
## Tail
 - op\_1/ref = List  
    Take the tail of a list.
## Assoc
 - op\_1/ref, op\_2/ref, op\_3/ref = key, value, *Map*  
    Associate a key with a map.
## Find
 - op\_1/ref, op\_2/ref = key, *Map*  
    Find a key within a map.
## List
 - *none*
    Create a new empty list.
## Map
 - *none*
    Create a new empty map.

## Promote
 - op\_1/ref:op\_n/ref = numbers to promote to the *highest kinded*  
    op\_1 through op\_n will be promoted to the most general numeric type.
## Cast
 - op\_1/ref, op\_2/ref = number to cast, number type name to cast to  
    op\_1 will be cast to the numeric type that op\_2 represents.

## CallC
 - op\_1/ref, op\_2/ref, ..., op\_n/ref = the c function to call, arguments  
    Call a foreign c function with arguments arguments op\_2/ref through op\_n/ref.
