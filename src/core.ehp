def fun + $(a :: Int) $(b :: Int) = intrinsic.add a b;
def fun + $(a :: Ratio) $(b :: Ratio) = intrinsic.add a b;
def fun + $(a :: Real) $(b :: Real) = intrinsic.add a b;

def fun - $(a :: Int) $(b :: Int) = intrinsic.sub a b;
def fun - $(a :: Ratio) $(b :: Ratio) = intrinsic.sub a b;
def fun - $(a :: Real) $(b :: Real) = intrinsic.sub a b;

def fun * $(a :: Int) $(b :: Int) = intrinsic.mul a b;
def fun * $(a :: Ratio) $(b :: Ratio) = intrinsic.mul a b;
def fun * $(a :: Real) $(b :: Real) = intrinsic.mul a b;

def fun / $(a :: Int) $(b :: Int) = intrinsic.div a b;
def fun / $(a :: Ratio) $(b :: Ratio) = intrinsic.div a b;
def fun / $(a :: Real) $(b :: Real) = intrinsic.div a b;

def fun % $(a :: Int) $(b :: Int) = intrinsic.mod a b;

def fun not (True) = ^False;
def fun not (False) = ^True;

def fun and (True) (True) = ^True;
def fun and $(_ :: Bool) $(_ :: Bool) = ^False;

def fun or (False) (False) = ^False;
def fun or $(_ :: Bool) $(_ :: Bool) = ^True;

def fun xor (True) (False) = ^True;
def fun xor (False) (True) = ^True;
def fun xor $(_ :: Bool) $(_ :: Bool) = ^False;

def fun = () () = ^True;
def fun = (True) (True) = ^True;
def fun = (False) (False) = ^True;
def fun = $(_ :: Bool) $(_ :: Bool) = ^False;
def fun = $(a :: Int) $(b :: Int) = intrinsic.eq a b;
def fun = $(a :: Ratio) $(b :: Ratio) = intrinsic.eq a b;
def fun = $(a :: Real) $(b :: Real) = intrinsic.eq a b;
def fun = $(a :: Char) $(b :: Char) = intrinsic.eq a b;
def fun = $(a :: List) $(b :: List) = intrinsic.eq a b;

def fun /= () () = ^False;
def fun /= (True) (True) = ^False;
def fun /= (False) (False) = ^False;
def fun /= $(_ :: Bool) $(_ :: Bool) = ^True;
def fun /= $(a :: Int) $(b :: Int) = intrinsic.ne a b;
def fun /= $(a :: Ratio) $(b :: Ratio) = intrinsic.ne a b;
def fun /= $(a :: Real) $(b :: Real) = intrinsic.ne a b;
def fun /= $(a :: Char) $(b :: Char) = intrinsic.ne a b;
def fun /= $(a :: List) $(b :: List) = intrinsic.ne a b;

def fun < $(a :: Int) $(b :: Int) = intrinsic.lt a b;
def fun < $(a :: Ratio) $(b :: Ratio) = intrinsic.lt a b;
def fun < $(a :: Real) $(b :: Real) = intrinsic.lt a b;
def fun < $(a :: Char) $(b :: Char) = intrinsic.lt a b;

def fun > $(a :: Int) $(b :: Int) = intrinsic.gt a b;
def fun > $(a :: Ratio) $(b :: Ratio) = intrinsic.gt a b;
def fun > $(a :: Real) $(b :: Real) = intrinsic.gt a b;
def fun > $(a :: Char) $(b :: Char) = intrinsic.gt a b;

def fun <= $(a :: Int) $(b :: Int) = intrinsic.le a b;
def fun <= $(a :: Ratio) $(b :: Ratio) = intrinsic.le a b;
def fun <= $(a :: Real) $(b :: Real) = intrinsic.le a b;
def fun <= $(a :: Char) $(b :: Char) = intrinsic.le a b;

def fun >= $(a :: Int) $(b :: Int) = intrinsic.ge a b;
def fun >= $(a :: Ratio) $(b :: Ratio) = intrinsic.ge a b;
def fun >= $(a :: Real) $(b :: Real) = intrinsic.ge a b;
def fun >= $(a :: Char) $(b :: Char) = intrinsic.ge a b;

def fun rev $(lst :: List) =
    let rec inner $acc `() = acc;
    let rec inner $acc (Cons $fst $rest) = inner (^Cons fst acc) rest;
    inner `() lst;;

def fun map $(f :: Closure Any) $(lst :: List) =
    let rec inner $(f :: Closure Any) `() = `();
    let rec inner $(f :: Closure Any) (Cons $fst $rest) = ^Cons (f fst) (inner f rest);
    inner f lst;;

def fun filter $(f :: Closure Any) $(lst :: List) =
    let rec inner $(f :: Closure Any) `() = `();
    let rec inner $(f :: Closure Any) (Cons $fst $rest) = inner (f fst) f (^Cons fst rest);
    let rec inner (True) $(f :: Closure Any) (Cons $fst $rest) = ^Cons fst (inner f rest);
    let rec inner (False) $(f :: Closure Any) (Cons $fst $rest) = inner f rest;
    inner f lst;;

def fun fold $acc $(f :: Closure Any) $(lst :: List) =
    let rec inner $acc $(f :: Closure Any) `() = acc;
    let rec inner $acc $(f :: Closure Any) (Cons $fst $rest) = inner (f acc fst) rest;
    inner acc f lst;;

def fun exists? $(f :: Closure Any) `() = ^False;
def fun exists? $(f :: Closure Any) (Cons $fst $rest) =
    let rec inner (True) $(f :: Closure Any) $_ = ^True;
    let rec inner $acc $(f :: Closure Any) `() = acc;
    let rec inner $_ $(f :: Closure Any) (Cons $fst $rest) = inner (f fst) f rest;
    inner (f fst) f rest;;

def fun for-all? $(f :: Closure Any) `() = ^True;
def fun for-all? $(f :: Closure Any) (Cons $fst $rest) =
    let rec inner (False) $(f :: Closure Any) $_ = ^False;
    let rec inner $acc $(f :: Closure Any) `() = acc;
    let rec inner $_ $(f :: Closure Any) (Cons $fst $rest) = inner (f fst) f rest;
    inner (f fst) f rest;;

def fun ++ `() `() = `();
def fun ++ `() $(b :: List) = b;
def fun ++ $(a :: List) `() = a;
def fun ++ $(a :: List) $(b :: List) =
    let fun rev* $(lst :: List) =
        let rec inner $acc `() = acc;
        let rec inner $acc (Cons $fst $rest) = inner (^Cons fst acc) rest;
        inner `() lst;;
    let rec inner `() $a = a;
    let rec inner $acc `() = acc;
    let rec inner $acc (Cons $fst $rest) = inner (^Cons fst acc) rest;
    inner b (rev* a);;

def fun len $(lst :: List) =
    let rec inner $acc `() = acc;
    let rec inner $acc (Cons $_ $rest) = inner (intrinsic.add acc 1) rest;
    inner 0 lst;;

def fun @ $(var :: Var) = intrinsic.getvar var;
def fun <- $(var :: Var) $(new :: Any) = intrinsic.updatevar var new;

def fun *> $x $(f :: Closure Any) = f x;

def fun promote $(a :: Int) $(b :: Int) = `(~a ~b);
def fun promote $(a :: Int) $(b :: Ratio) = `(~(intrinsic.int->ratio a) ~b);
def fun promote $(a :: Int) $(b :: Real) = `(~(intrinsic.int->real a) ~b);
def fun promote $(a :: Ratio) $(b :: Int) = `(~a ~(intrinsic.int->ratio b));
def fun promote $(a :: Ratio) $(b :: Ratio) = `(~a ~b);
def fun promote $(a :: Ratio) $(b :: Real) = `(~(intrinsic.ratio->real a) ~b);
def fun promote $(a :: Real) $(b :: Int) = `(~a ~(intrinsic.int->real b));
def fun promote $(a :: Real) $(b :: Ratio) = `(~a ~(intrinsic.ratio->real b));
def fun promote $(a :: Real) $(b :: Real) = `(~a ~b);

def fun ord $(char :: Char) = intrinsic.char->int char;
def fun chr $(int :: Int) = intrinsic.int->char int;
