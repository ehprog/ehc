use std::str::FromStr;
use std::cell::RefCell;

use crate::error::{Error, ErrorKind};
use crate::collections::Boxed;
use crate::lex::{Span, TokenKind, Lexer};
use crate::pp::Preprocessor;
use crate::parse::Parser;
use crate::parse::ast::{self, Meta, AsMeta};

pub fn parse_program(parser: &mut Parser) -> Result<ast::Program, Error> {
    let block = parse_block(parser)?;
    let eof = parse_eof(parser)?;
    let span = if let Some(span) = eof {
        block.span().clone().join(span)
    } else {
        block.span().clone()
    };
    Ok(ast::Program {
        meta: Meta::new(span),
        block,
    })
}

fn parse_eof(parser: &mut Parser) -> Result<Option<Span>, Error> {
    match parser.advance() {
        Some(Ok(ref tok)) if tok.kind() == TokenKind::EOF => Ok(Some(tok.span().clone())),
        Some(Ok(tok)) => Err(Error::new(ErrorKind::ExpectedEof).with_span(tok.span())),
        Some(Err(err)) => Err(err),
        None => Ok(None),
    }
}

fn parse_block(parser: &mut Parser) -> Result<ast::Block, Error> {
    let mut error = false;
    let mut block_span = vec![];
    let mut stmts = vec![];
    let mut expr = None;
    'outer: loop {
        let tmp = match parse_expr(parser) {
            Ok(ok) => ok,
            Err(err) => {
                parser.push_err(err);
                error = true;
                let mut tok = match parser.advance() {
                    Some(Ok(ok)) => ok,
                    Some(Err(err)) => return Err(err),
                    None => break,
                };
                while tok.kind() != TokenKind::Semi && tok.kind() != TokenKind::EOF {
                    tok = match parser.advance() {
                        Some(Ok(ok)) => ok,
                        Some(Err(err)) => return Err(err),
                        None => break 'outer,
                    };
                }
                continue;
            }
        };
        block_span.push(tmp.span().clone());
        let tok = parser.peek().ok_or_else(|| Error::new(ErrorKind::UnexpectedEof))??;
        match tok.kind() {
            TokenKind::Semi => {
                block_span.push(tok.span().clone());
                let span = tmp.span().clone().join(tok.span().clone());
                parser.advance();
                stmts.push(ast::Stmt {
                    meta: Meta::new(span),
                    expr: tmp,
                });
                let tok = parser.peek().ok_or_else(|| Error::new(ErrorKind::UnexpectedEof))??;
                match tok.kind() {
                    TokenKind::EOF => break,
                    TokenKind::ParEnd => {
                        parser.advance();
                        break;
                    }
                    _ => {}
                }
            }
            TokenKind::ParEnd => {
                parser.advance();
                expr = Some(tmp);
                break;
            }
            TokenKind::EOF => {
                expr = Some(tmp);
                break;
            }
            found => {
                return Err(Error::new(ErrorKind::UnexpectedToken)
                    .describe(format!("found {:?}, expected {:?}", found, TokenKind::Semi))
                    .with_span(tok.span()));
            }
        }
    }
    if block_span.is_empty() {
        debug_assert!(error);
        return Err(Error::new(ErrorKind::EmptyBlock));
    } else {
        Ok(ast::Block {
            meta: Meta::new(block_span.into_iter().collect()),
            stmts,
            expr,
        })
    }
}

fn parse_expr(parser: &mut Parser) -> Result<ast::Expr, Error> {
    let mut span = vec![];
    let first = parse_word(parser)?;
    span.push(first.span().clone());
    let mut rest = vec![];
    loop {
        let tok = parser.peek().ok_or_else(|| Error::new(ErrorKind::UnexpectedEof))??;
        match tok.kind() {
            TokenKind::EOF => break,
            TokenKind::ParEnd => break,
            TokenKind::Semi => break,
            TokenKind::ParenR => break,
            TokenKind::SquareR => break,
            TokenKind::CurlyR => break,
            TokenKind::Pipe => break,
            TokenKind::ParBegin => {
                let tok = parser.advance().unwrap().unwrap();
                if let Some(next) = parser.peek() {
                    match next?.kind() {
                        TokenKind::Pipe => {
                            parser.push(tok);
                            break;
                        }
                        _ => {}
                    }
                }
                parser.push(tok);
                let word = parse_word(parser)?;
                span.push(word.span().clone());
                rest.push(word);
            }
            _ => {
                let word = parse_word(parser)?;
                span.push(word.span().clone());
                rest.push(word);
            }
        }
    }
    Ok(ast::Expr {
        meta: Meta::new(span.into_iter().collect()),
        first,
        rest,
    })
}

fn parse_word(parser: &mut Parser) -> Result<ast::Word, Error> {
    let tok = parser.peek().ok_or_else(|| Error::new(ErrorKind::UnexpectedEof))??;
    match tok.kind() {
        TokenKind::Atom => {
            let tok = parser.advance().unwrap().unwrap();
            match parser.peek() {
                Some(Ok(next)) => {
                    match next.kind() {
                        TokenKind::Dot => {
                            parser.push(tok);
                            parse_path(parser).map(|atom| {
                                ast::Word {
                                    meta: Meta::new(atom.span().clone()),
                                    kind: ast::WordKind::Path(atom),
                                }
                            })
                        }
                        _ => {
                            parser.push(tok);
                            parse_atom(parser).map(|atom| {
                                ast::Word {
                                    meta: Meta::new(atom.span().clone()),
                                    kind: ast::WordKind::Atom(atom),
                                }
                            })
                        }
                    }
                }
                Some(Err(err)) => Err(err),
                None => {
                    parse_atom(parser).map(|atom| {
                        ast::Word {
                            meta: Meta::new(atom.span().clone()),
                            kind: ast::WordKind::Atom(atom),
                        }
                    })
                }
            }
        }
        TokenKind::Integer => parse_integer(parser),
        TokenKind::Real => parse_real(parser),
        TokenKind::Ratio => parse_ratio(parser),
        TokenKind::Char => parse_char(parser),
        TokenKind::Str => parse_str(parser),
        TokenKind::Caret => {
            let span = tok.span().clone();
            parser.advance();
            let mut typeatom = parse_typeatom(parser)?;
            typeatom.meta.span = span.join(typeatom.span().clone());
            Ok(typeatom)
        }
        TokenKind::Backslash => {
            let span = tok.span().clone();
            parser.advance();
            let mut quote = parse_quote(parser)?;
            quote.meta.span = span.join(quote.span().clone());
            Ok(quote)
        }
        TokenKind::Backtick => {
            let span = tok.span().clone();
            parser.advance();
            let mut quasiquote = parse_quasiquote(parser)?;
            quasiquote.meta.span = span.join(quasiquote.span().clone());
            Ok(quasiquote)
        }
        TokenKind::Tilde => {
            let span = tok.span().clone();
            parser.advance();
            let mut interpolate = parse_interpolate(parser)?;
            interpolate.meta.span = span.join(interpolate.span().clone());
            Ok(interpolate)
        }
        TokenKind::ParenL => {
            let span = tok.span().clone();
            parser.advance();
            let tok = parser.peek().ok_or_else(|| Error::new(ErrorKind::UnexpectedEof))??;
            match tok.kind() {
                TokenKind::Type => {
                    let mut destructure = parse_destructure(parser)?;
                    destructure.meta.span = span.join(destructure.span().clone());
                    Ok(destructure)
                }
                TokenKind::ParenR => {
                    let span = span.join(tok.span().clone());
                    parser.advance();
                    Ok(ast::Word {
                        meta: Meta::new(span),
                        kind: ast::WordKind::Empty,
                    })
                }
                _ => {
                    let mut parenth = parse_parenth(parser)?;
                    parenth.meta.span = span.join(parenth.span().clone());
                    Ok(parenth)
                }
            }
        }
        TokenKind::Dollar => parse_bind(parser),
        TokenKind::DollarParen => parse_bind(parser),
        TokenKind::SquareL => {
            let span = tok.span().clone();
            parser.advance();
            let mut closure = parse_multi(parser)?;
            closure.meta.span = span.join(closure.span().clone());
            Ok(closure)
        }
        TokenKind::ParBegin => {
            let span = tok.span().clone();
            parser.advance();
            let block = Boxed::new(RefCell::new(parse_block(parser)?));
            let span = span.join(block.borrow().span().clone());
            Ok(ast::Word {
                meta: Meta::new(span),
                kind: ast::WordKind::Compound(block),
            })
        }
        found => {
            Err(Error::new(ErrorKind::UnexpectedToken)
                .describe(format!(
                    "found {:?}, expected any of: {:?}, {:?}, {:?}, {:?}, {:?}, {:?}, {:?}, {:?}, {:?}, {:?}, {:?}, \
                     {:?}, {:?}, {:?}, {:?}",
                    found,
                    TokenKind::Atom,
                    TokenKind::Integer,
                    TokenKind::Real,
                    TokenKind::Ratio,
                    TokenKind::Char,
                    TokenKind::Str,
                    TokenKind::Caret,
                    TokenKind::Backslash,
                    TokenKind::Backtick,
                    TokenKind::Tilde,
                    TokenKind::ParenL,
                    TokenKind::Dollar,
                    TokenKind::DollarParen,
                    TokenKind::SquareL,
                    TokenKind::ParBegin,
                ))
                .with_span(tok.span()))
        }
    }
}

fn parse_atom(parser: &mut Parser) -> Result<ast::Atom, Error> {
    let tok = parser.advance().ok_or_else(|| Error::new(ErrorKind::UnexpectedEof))??;
    match tok.kind() {
        TokenKind::Atom => {
            Ok(ast::Atom {
                meta: Meta::new(tok.span().clone()),
                atom: tok.span().src().clone(),
            })
        }
        found => {
            Err(Error::new(ErrorKind::UnexpectedToken)
                .describe(format!("found {:?}, expected {:?}", found, TokenKind::Atom))
                .with_span(tok.span()))
        }
    }
}

fn parse_path(parser: &mut Parser) -> Result<ast::Path, Error> {
    let mut span = vec![];
    let mut path = vec![];
    loop {
        let tok = parser.advance().ok_or_else(|| Error::new(ErrorKind::UnexpectedEof))??;
        match tok.kind() {
            TokenKind::Atom => {
                span.push(tok.span().clone());
                path.push(tok.span().src().clone());
                let next = parser.peek();
                match next {
                    Some(Ok(ok)) => {
                        match ok.kind() {
                            TokenKind::Dot => {
                                parser.advance();
                            }
                            _ => break,
                        }
                    }
                    Some(Err(err)) => return Err(err),
                    None => break,
                }
            }
            found => {
                return Err(Error::new(ErrorKind::UnexpectedToken)
                    .describe(format!("found {:?}, expected {:?}", found, TokenKind::Atom))
                    .with_span(tok.span()));
            }
        }
    }
    let span = span.into_iter().collect();
    Ok(ast::Path {
        meta: Meta::new(span),
        path,
    })
}

fn parse_integer(parser: &mut Parser) -> Result<ast::Word, Error> {
    let tok = parser.advance().ok_or_else(|| Error::new(ErrorKind::UnexpectedEof))??;
    match tok.kind() {
        TokenKind::Integer => {
            let string = tok.span().to_string().chars().filter(|&c| c != '_').collect::<String>();
            let int = if let Some(idx) = string.find(':') {
                let (base, rest) = string.split_at(idx);
                let int = &rest[1..];
                let base = base.parse().unwrap();
                if base < 2 || base > 36 {
                    return Err(Error::new(ErrorKind::InvalidBase).with_span(tok.span()));
                }
                i64::from_str_radix(int, base).unwrap()
            } else {
                i64::from_str(&string).unwrap()
            };
            Ok(ast::Word {
                meta: Meta::new(tok.span().clone()),
                kind: ast::WordKind::Integer(int),
            })
        }
        found => {
            Err(Error::new(ErrorKind::UnexpectedToken)
                .describe(format!("found {:?}, expected {:?}", found, TokenKind::Integer))
                .with_span(tok.span()))
        }
    }
}

fn parse_real(parser: &mut Parser) -> Result<ast::Word, Error> {
    let tok = parser.advance().ok_or_else(|| Error::new(ErrorKind::UnexpectedEof))??;
    match tok.kind() {
        TokenKind::Real => {
            let string = tok.span().to_string().chars().filter(|&c| c != '_').collect::<String>();
            let real = f64::from_str(&string).unwrap();
            Ok(ast::Word {
                meta: Meta::new(tok.span().clone()),
                kind: ast::WordKind::Real(real),
            })
        }
        found => {
            Err(Error::new(ErrorKind::UnexpectedToken)
                .describe(format!("found {:?}, expected {:?}", found, TokenKind::Real))
                .with_span(tok.span()))
        }
    }
}

fn parse_ratio(parser: &mut Parser) -> Result<ast::Word, Error> {
    let tok = parser.advance().ok_or_else(|| Error::new(ErrorKind::UnexpectedEof))??;
    match tok.kind() {
        TokenKind::Ratio => {
            let string = tok.span().to_string().chars().filter(|&c| c != '_').collect::<String>();
            let (sign, string) = if string.starts_with('-') {
                (-1, &string[1..])
            } else {
                (1, &string[..])
            };
            let idx = string.find('/').unwrap();
            let (p, rest) = string.split_at(idx);
            let q = &rest[1..];
            let p = p.parse().unwrap();
            let q = q.parse().unwrap();
            Ok(ast::Word {
                meta: Meta::new(tok.span().clone()),
                kind: ast::WordKind::Ratio(sign, p, q),
            })
        }
        found => {
            Err(Error::new(ErrorKind::UnexpectedToken)
                .describe(format!("found {:?}, expected {:?}", found, TokenKind::Ratio))
                .with_span(tok.span()))
        }
    }
}

fn parse_char(parser: &mut Parser) -> Result<ast::Word, Error> {
    let tok = parser.advance().ok_or_else(|| Error::new(ErrorKind::UnexpectedEof))??;
    match tok.kind() {
        TokenKind::Char => {
            let mut chars = tok.span().src()[1..].chars();
            let c = chars.next().unwrap();
            let c = match c {
                '\\' => {
                    match chars.next().unwrap() {
                        't' => '\t',
                        'n' => '\n',
                        'e' => '\x1b',
                        '\\' => '\\',
                        '\'' => '\n',
                        _ => unreachable!(),
                    }
                }
                c => c,
            };
            Ok(ast::Word {
                meta: Meta::new(tok.span().clone()),
                kind: ast::WordKind::Char(c),
            })
        }
        found => {
            Err(Error::new(ErrorKind::UnexpectedToken)
                .describe(format!("found {:?}, expected {:?}", found, TokenKind::Char))
                .with_span(tok.span()))
        }
    }
}

fn parse_str(parser: &mut Parser) -> Result<ast::Word, Error> {
    let tok = parser.advance().ok_or_else(|| Error::new(ErrorKind::UnexpectedEof))??;
    match tok.kind() {
        TokenKind::Str => {
            enum State {
                Normal,
                Interp,
                Backslash,
            }
            let span = tok.span().clone();
            let src = span.src().substring(1..span.src().len() - 2);
            let mut state = State::Normal;
            let mut paren = 0;
            let mut parts = vec![];
            let mut string = String::new();
            for c in src.chars() {
                match state {
                    State::Normal => {
                        match c {
                            '\\' => state = State::Backslash,
                            '~' => {
                                if string.len() > 0 {
                                    parts.push(ast::InterpolatedPart::Str(From::from(string)));
                                    string = String::new();
                                }
                                state = State::Interp;
                            }
                            _ => string.push(c),
                        }
                    }
                    State::Interp => {
                        match c {
                            '(' => {
                                string.push(c);
                                paren += 1;
                            }
                            ')' => {
                                string.push(c);
                                paren -= 1;
                                if paren == 0 {
                                    let lexer = Lexer::new(parser.pp.lexer().file().clone(), From::from(string));
                                    let pp = Preprocessor::new(lexer);
                                    let mut parser = Parser::new(pp);
                                    let expr = parse_expr(&mut parser)?;
                                    parts.push(ast::InterpolatedPart::Interpolate(Boxed::new(RefCell::new(expr))));
                                    string = String::new();
                                    state = State::Normal;
                                }
                            }
                            _ => string.push(c),
                        }
                    }
                    State::Backslash => {
                        let c = match c {
                            't' => '\t',
                            'n' => '\n',
                            'e' => '\x1b',
                            '\\' => '\\',
                            '\"' => '\n',
                            '~' => '~',
                            _ => unreachable!(),
                        };
                        string.push(c);
                        state = State::Normal;
                    }
                }
            }
            match state {
                State::Normal => {
                    if string.len() > 0 {
                        parts.push(ast::InterpolatedPart::Str(From::from(string)));
                    }
                }
                State::Interp => unreachable!(),
                State::Backslash => unreachable!(),
            }
            Ok(ast::Word {
                meta: Meta::new(span.clone()),
                kind: ast::WordKind::Str(ast::Interpolated {
                    meta: Meta::new(span),
                    parts,
                }),
            })
        }
        found => {
            Err(Error::new(ErrorKind::UnexpectedToken)
                .describe(format!("found {:?}, expected {:?}", found, TokenKind::Str))
                .with_span(tok.span()))
        }
    }
}

fn parse_typeatom(parser: &mut Parser) -> Result<ast::Word, Error> {
    parse_typeword(parser).map(|word| {
        ast::Word {
            meta: Meta::new(word.span().clone()),
            kind: ast::WordKind::TypeAtom(word),
        }
    })
}

fn parse_quote(parser: &mut Parser) -> Result<ast::Word, Error> {
    parse_word(parser).map(RefCell::new).map(Boxed::new).map(|word| {
        let span = word.borrow().span().clone();
        ast::Word {
            meta: Meta::new(span),
            kind: ast::WordKind::Quote(word),
        }
    })
}

fn parse_quasiquote(parser: &mut Parser) -> Result<ast::Word, Error> {
    parse_word(parser).map(RefCell::new).map(Boxed::new).map(|word| {
        let span = word.borrow().span().clone();
        ast::Word {
            meta: Meta::new(span),
            kind: ast::WordKind::Quasiquote(word),
        }
    })
}

fn parse_interpolate(parser: &mut Parser) -> Result<ast::Word, Error> {
    parse_word(parser).map(RefCell::new).map(Boxed::new).map(|word| {
        let span = word.borrow().span().clone();
        ast::Word {
            meta: Meta::new(span),
            kind: ast::WordKind::Interpolate(word),
        }
    })
}

fn parse_parenth(parser: &mut Parser) -> Result<ast::Word, Error> {
    let word = parse_expr(parser).map(RefCell::new).map(Boxed::new).map(|word| {
        let span = word.borrow().span().clone();
        ast::Word {
            meta: Meta::new(span),
            kind: ast::WordKind::Parenth(word),
        }
    })?;
    let tok = parser.advance().ok_or_else(|| Error::new(ErrorKind::UnexpectedEof))??;
    match tok.kind() {
        TokenKind::ParenR => {}
        found => {
            return Err(Error::new(ErrorKind::UnexpectedToken)
                .describe(format!("found {:?}, expected {:?}", found, TokenKind::ParenR))
                .with_span(tok.span()));
        }
    }
    Ok(word)
}

fn parse_bind(parser: &mut Parser) -> Result<ast::Word, Error> {
    let tok = parser.advance().ok_or_else(|| Error::new(ErrorKind::UnexpectedEof))??;
    match tok.kind() {
        TokenKind::Dollar => {
            let atom = parse_atom(parser)?;
            let variadic = (|| {
                let tok = match parser.peek() {
                    Some(Ok(tok)) => tok,
                    _ => return None,
                };
                match tok.kind() {
                    TokenKind::DoubleDot => {
                        let span = tok.span().clone();
                        parser.advance();
                        Some(span)
                    }
                    _ => None,
                }
            })();
            let span = tok.span().clone().join(atom.span().clone());
            let (span, variadic) = if let Some(variadic) = variadic {
                (span.join(variadic), true)
            } else {
                (span, false)
            };
            Ok(ast::Word {
                meta: Meta::new(span),
                kind: ast::WordKind::Bind(atom, None, None, variadic),
            })
        }
        TokenKind::DollarParen => {
            let mut span = vec![tok.span().clone()];
            let atom = parse_atom(parser)?;
            span.push(atom.span().clone());
            let variadic = (|| {
                let tok = match parser.peek() {
                    Some(Ok(tok)) => tok,
                    _ => return false,
                };
                match tok.kind() {
                    TokenKind::DoubleDot => {
                        let tokspan = tok.span().clone();
                        parser.advance();
                        span.push(tokspan);
                        true
                    }
                    _ => false,
                }
            })();
            let ty = (|| {
                let tok = match parser.peek() {
                    Some(Ok(tok)) => tok,
                    Some(Err(err)) => return Some(Err(err)),
                    _ => return None,
                };
                match tok.kind() {
                    TokenKind::DoubleColon => {
                        let tokspan = tok.span().clone();
                        parser.advance();
                        let expr = parse_typeexpr(parser);
                        span.push(tokspan);
                        let _ = expr.as_ref().map(|expr| span.push(expr.span().clone()));
                        Some(expr)
                    }
                    _ => None,
                }
            })();
            let ty = match ty {
                Some(Ok(ty)) => Some(ty),
                Some(Err(err)) => return Err(err),
                None => None,
            };
            let pred = (|| {
                let tok = match parser.peek() {
                    Some(Ok(tok)) => tok,
                    Some(Err(err)) => return Some(Err(err)),
                    _ => return None,
                };
                match tok.kind() {
                    TokenKind::Colon => {
                        let tokspan = tok.span().clone();
                        parser.advance();
                        let expr = parse_expr(parser);
                        span.push(tokspan);
                        let _ = expr.as_ref().map(|expr| span.push(expr.span().clone()));
                        Some(expr)
                    }
                    _ => None,
                }
            })();
            let pred = match pred {
                Some(Ok(pred)) => Some(pred),
                Some(Err(err)) => return Err(err),
                None => None,
            };
            let tok = parser.advance().ok_or_else(|| Error::new(ErrorKind::UnexpectedEof))??;
            let _paren = match tok.kind() {
                TokenKind::ParenR => span.push(tok.span().clone()),
                found => {
                    return Err(Error::new(ErrorKind::UnexpectedToken)
                        .describe(format!("found {:?}, expected {:?}", found, TokenKind::ParenR))
                        .with_span(tok.span()));
                }
            };
            Ok(ast::Word {
                meta: Meta::new(span.into_iter().collect()),
                kind: ast::WordKind::Bind(atom, ty, pred.map(RefCell::new).map(Boxed::new), variadic),
            })
        }
        found => {
            return Err(Error::new(ErrorKind::UnexpectedToken)
                .describe(format!(
                    "found {:?}, expected {:?} or {:?}",
                    found,
                    TokenKind::Dollar,
                    TokenKind::DollarParen
                ))
                .with_span(tok.span()));
        }
    }
}

fn parse_destructure(parser: &mut Parser) -> Result<ast::Word, Error> {
    let mut span = vec![];
    let ty = parse_typeexpr(parser)?;
    span.push(ty.span().clone());
    let mut pats = vec![];
    loop {
        let tok = parser.peek().ok_or_else(|| Error::new(ErrorKind::UnexpectedEof))??;
        match tok.kind() {
            TokenKind::ParenR => {
                span.push(tok.span().clone());
                parser.advance();
                break;
            }
            _ => {
                let word = parse_word(parser)?;
                span.push(word.span().clone());
                pats.push(word);
            }
        }
    }
    Ok(ast::Word {
        meta: Meta::new(span.into_iter().collect()),
        kind: ast::WordKind::Destructure(ty, pats),
    })
}

fn parse_multi(parser: &mut Parser) -> Result<ast::Word, Error> {
    let mut span = vec![];
    let mut closures = vec![];
    loop {
        let closure = parse_closure(parser)?;
        span.push(closure.span().clone());
        closures.push(closure);
        let tok = parser.peek().ok_or_else(|| Error::new(ErrorKind::UnexpectedEof))??;
        match tok.kind() {
            TokenKind::Pipe => {}
            TokenKind::SquareR => {
                span.push(tok.span().clone());
                parser.advance();
                break;
            }
            TokenKind::ParBegin => {
                let tok = parser.advance().unwrap().unwrap();
                if let Some(next) = parser.peek() {
                    match next?.kind() {
                        TokenKind::Pipe => continue,
                        _ => {}
                    }
                }
                let found = tok;
                return Err(Error::new(ErrorKind::UnexpectedToken)
                    .describe(format!(
                        "found {:?}, expected {:?}, {:?}, {:?} or {:?}",
                        found,
                        TokenKind::SquareR,
                        TokenKind::Pipe,
                        TokenKind::ParBegin,
                        TokenKind::ParEnd,
                    ))
                    .with_span(found.span()));
            }
            TokenKind::ParEnd => {
                let tok = parser.advance().unwrap().unwrap();
                if let Some(next) = parser.peek() {
                    match next?.kind() {
                        TokenKind::SquareR => {
                            span.push(tok.span().clone());
                            parser.advance();
                            break;
                        }
                        _ => {}
                    }
                }
                let found = tok;
                return Err(Error::new(ErrorKind::UnexpectedToken)
                    .describe(format!(
                        "found {:?}, expected {:?}, {:?}, {:?} or {:?}",
                        found,
                        TokenKind::SquareR,
                        TokenKind::Pipe,
                        TokenKind::ParBegin,
                        TokenKind::ParEnd,
                    ))
                    .with_span(found.span()));
            }
            found => {
                return Err(Error::new(ErrorKind::UnexpectedToken)
                    .describe(format!(
                        "found {:?}, expected {:?}, {:?}, {:?} or {:?}",
                        found,
                        TokenKind::SquareR,
                        TokenKind::Pipe,
                        TokenKind::ParBegin,
                        TokenKind::ParEnd,
                    ))
                    .with_span(tok.span()));
            }
        }
    }
    let span = span.into_iter().collect();
    Ok(ast::Word {
        meta: Meta::new(span),
        kind: ast::WordKind::Closure(closures),
    })
}

fn parse_closure(parser: &mut Parser) -> Result<ast::Closure<ast::Expr>, Error> {
    let mut span = vec![];
    let mut attr = false;
    let mut dynamic = false;
    let mut mutable = false;
    let mut macroeh = false;
    let mut params = vec![];
    loop {
        let tok = parser.peek().ok_or_else(|| Error::new(ErrorKind::UnexpectedEof))??;
        match tok.kind() {
            TokenKind::Atom if tok.span().src().as_str() == "dyn" => {
                span.push(tok.span().clone());
                parser.advance();
                dynamic = true;
                attr = true;
            }
            TokenKind::Atom if tok.span().src().as_str() == "mut" => {
                span.push(tok.span().clone());
                parser.advance();
                mutable = true;
                attr = true;
            }
            TokenKind::Atom if tok.span().src().as_str() == "macro" => {
                span.push(tok.span().clone());
                parser.advance();
                macroeh = true;
                attr = true;
            }
            TokenKind::Pipe => {
                span.push(tok.span().clone());
                parser.advance();
                attr = true;
                break;
            }
            _ if !attr => break,
            found => {
                return Err(Error::new(ErrorKind::InvalidAttr)
                    .describe(format!(
                        "found {:?}, valid attributes are: `dyn`, `mut`, `macro`",
                        found
                    ))
                    .with_span(tok.span()));
            }
        }
    }
    if attr {
        loop {
            let tok = parser.peek().ok_or_else(|| Error::new(ErrorKind::UnexpectedEof))??;
            match tok.kind() {
                TokenKind::Pipe => {
                    span.push(tok.span().clone());
                    parser.advance();
                    break;
                }
                _ => {
                    let word = parse_word(parser)?;
                    span.push(word.span().clone());
                    params.push(word);
                }
            }
        }
    }
    let body = parse_expr(parser)?;
    span.push(body.span().clone());
    let span = span.into_iter().collect::<Span>();
    Ok(ast::Closure {
        meta: Meta::new(span),
        dynamic,
        mutable,
        macroeh,
        params,
        body: Boxed::new(RefCell::new(body)),
    })
}

fn parse_type(parser: &mut Parser) -> Result<ast::Type, Error> {
    let tok = parser.advance().ok_or_else(|| Error::new(ErrorKind::UnexpectedEof))??;
    match tok.kind() {
        TokenKind::Type => {
            Ok(ast::Type {
                meta: Meta::new(tok.span().clone()),
                atom: tok.span().src().clone(),
            })
        }
        found => {
            Err(Error::new(ErrorKind::UnexpectedToken)
                .describe(format!("found {:?}, expected {:?}", found, TokenKind::Type))
                .with_span(tok.span()))
        }
    }
}

fn parse_typeexpr(parser: &mut Parser) -> Result<ast::TypeExpr, Error> {
    let mut span = vec![];
    let first = parse_type(parser)?;
    span.push(first.span().clone());
    let mut rest = vec![];
    loop {
        let tok = parser.peek().ok_or_else(|| Error::new(ErrorKind::UnexpectedEof))??;
        match tok.kind() {
            TokenKind::Type | TokenKind::ParenL => {
                let word = parse_typeword(parser)?;
                span.push(word.span().clone());
                rest.push(word);
            }
            _ => break,
        }
    }
    Ok(ast::TypeExpr {
        meta: Meta::new(span.into_iter().collect()),
        first,
        rest,
    })
}

fn parse_typeword(parser: &mut Parser) -> Result<ast::TypeWord, Error> {
    let tok = parser.peek().ok_or_else(|| Error::new(ErrorKind::UnexpectedEof))??;
    match tok.kind() {
        TokenKind::Type => {
            parse_type(parser).map(|ty| {
                ast::TypeWord {
                    meta: Meta::new(ty.span().clone()),
                    kind: ast::TypeWordKind::Type(ty),
                }
            })
        }
        TokenKind::ParenL => {
            let mut span = tok.span().clone();
            parser.advance();
            let expr = parse_typeexpr(parser).map(Boxed::new)?;
            span = span.join(expr.span().clone());
            let tok = parser.advance().ok_or_else(|| Error::new(ErrorKind::UnexpectedEof))??;
            match tok.kind() {
                TokenKind::ParenR => span = span.join(tok.span().clone()),
                found => {
                    return Err(Error::new(ErrorKind::UnexpectedToken)
                        .describe(format!("found {:?}, expected {:?}", found, TokenKind::ParenR))
                        .with_span(tok.span()));
                }
            }
            Ok(ast::TypeWord {
                meta: Meta::new(span),
                kind: ast::TypeWordKind::Parenth(expr),
            })
        }
        found => {
            return Err(Error::new(ErrorKind::UnexpectedToken)
                .describe(format!(
                    "found {:?}, expected {:?} or {:?}",
                    found,
                    TokenKind::Type,
                    TokenKind::ParenL
                ))
                .with_span(tok.span()));
        }
    }
}

#[cfg(test)]
mod tests {
    use crate::collections::RcStr;
    use crate::lex::Lexer;
    use crate::pp::Preprocessor;
    use super::*;

    #[test]
    fn program() {
        static INPUTS: &[&str] = &[
            "let x = 5;",
            "let y = \n  5;",
            "let z = \n  let a = 5;\n  a",
            "let a = 1;\nlet b = 2;\nlet c = 3;",
        ];
        for &input in INPUTS {
            let input = RcStr::from(input);
            let lexer = Lexer::new(RcStr::from("<test>"), input);
            let pp = Preprocessor::new(lexer);
            let mut parser = Parser::new(pp);
            let result = parse_program(&mut parser);
            let errs = parser.errors();
            if errs.len() > 0 {
                for err in errs {
                    eprintln!("{}", err);
                }
            }
            match result {
                Ok(ok) => println!("{}", ok),
                Err(err) => {
                    eprintln!("{}", err);
                }
            }
        }
    }

    #[test]
    fn block() {
        static INPUTS: &[&str] = &[
            "let x = 5;",
            "let y = \n  5;",
            "let z = \n  let a = 5;\n  a",
            "let a = 1;\nlet b = 2;\nlet c = 3;",
        ];
        for &input in INPUTS {
            let input = RcStr::from(input);
            let lexer = Lexer::new(RcStr::from("<test>"), input);
            let pp = Preprocessor::new(lexer);
            let mut parser = Parser::new(pp);
            let result = parse_block(&mut parser);
            let errs = parser.errors();
            if errs.len() > 0 {
                for err in errs {
                    eprintln!("{}", err);
                }
            }
            match result {
                Ok(ok) => println!("{}", ok),
                Err(err) => {
                    eprintln!("{}", err);
                }
            }
        }
    }
}
