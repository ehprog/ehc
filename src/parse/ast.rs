use std::fmt::{self, Write, Display};
use std::cell::RefCell;

use crate::collections::{Boxed, RcStr};
use crate::lex::Span;
use crate::ty;

#[derive(Debug, Clone)]
pub struct Meta {
    pub span: Span,
    pub ty: Option<Boxed<ty::Type>>,
    pub eval: Option<Boxed<ty::Type>>,
    pub matches: Option<Boxed<ty::Type>>,
    pub params: Option<Boxed<RefCell<Vec<Word>>>>,
    pub def: Option<Boxed<RefCell<Expr>>>,
}

impl Meta {
    pub fn new(span: Span) -> Self {
        Meta {
            span,
            ty: None,
            eval: None,
            matches: None,
            params: None,
            def: None,
        }
    }
}

impl PartialEq for Meta {
    fn eq(&self, _other: &Self) -> bool {
        true
    }
}

impl Eq for Meta {}

pub trait AsMeta {
    fn as_meta(&self) -> &Meta;
    fn as_meta_mut(&mut self) -> &mut Meta;

    fn span(&self) -> &Span {
        &self.as_meta().span
    }
}

pub trait ToIdent {
    fn to_ident(&self) -> RcStr;

    fn with_prefix(&self, prefix: &str) -> RcStr {
        From::from(format!("__{}{}", prefix, self.to_ident()))
    }
}

pub(crate) mod seal {
    use std::fmt::Debug;
    pub trait Sealed<T>: Debug + Clone + super::ToIdent + super::AsMeta {}
}

pub struct BlockSeal;
pub struct StmtSeal;
pub struct ExprSeal;

pub trait BlockLike: seal::Sealed<BlockSeal> {
    type Stmt: StmtLike<Expr = Self::Expr>;
    type Expr: ExprLike;

    fn new(span: Span, stmts: Vec<Self::Stmt>, expr: Option<Self::Expr>) -> Self;
    fn stmts(&self) -> &[Self::Stmt];
    fn stmts_mut(&mut self) -> &mut [Self::Stmt];
    fn expr(&self) -> Option<&Self::Expr>;
    fn expr_mut(&mut self) -> Option<&mut Self::Expr>;
}

pub trait StmtLike: seal::Sealed<StmtSeal> {
    type Expr: ExprLike;

    fn new(span: Span, expr: Self::Expr) -> Self;
    fn expr(&self) -> &Self::Expr;
    fn expr_mut(&mut self) -> &mut Self::Expr;
}

pub trait ExprLike: seal::Sealed<ExprSeal> + Into<Expr> + Display {
    fn new(span: Span, first: Word, rest: Vec<Word>) -> Self;
    fn first(&self) -> &Word;
    fn first_mut(&mut self) -> &mut Word;
    fn rest(&self) -> &[Word];
    fn rest_mut(&mut self) -> &mut [Word];
}

#[derive(Debug, Clone, PartialEq)]
pub struct Program {
    pub meta: Meta,
    pub block: Block,
}

impl AsMeta for Program {
    fn as_meta(&self) -> &Meta {
        &self.meta
    }

    fn as_meta_mut(&mut self) -> &mut Meta {
        &mut self.meta
    }
}

impl ToIdent for Program {
    fn to_ident(&self) -> RcStr {
        RcStr::from(format!(
            "__Program_{}_{}_{}",
            normalize_string(&self.span().file()),
            self.span().line(),
            self.span().col()
        ))
    }
}

impl Display for Program {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        f.pretty_print(0).program(self).finish()
    }
}

#[derive(Debug, Clone, PartialEq)]
pub struct Block {
    pub meta: Meta,
    pub stmts: Vec<Stmt>,
    pub expr: Option<Expr>,
}

impl AsMeta for Block {
    fn as_meta(&self) -> &Meta {
        &self.meta
    }

    fn as_meta_mut(&mut self) -> &mut Meta {
        &mut self.meta
    }
}

impl ToIdent for Block {
    fn to_ident(&self) -> RcStr {
        RcStr::from(format!(
            "__Block_{}_{}_{}",
            normalize_string(&self.span().file()),
            self.span().line(),
            self.span().col()
        ))
    }
}

impl seal::Sealed<BlockSeal> for Block {}

impl BlockLike for Block {
    type Expr = Expr;
    type Stmt = Stmt;

    fn new(span: Span, stmts: Vec<Self::Stmt>, expr: Option<Self::Expr>) -> Self {
        Block {
            meta: Meta::new(span),
            stmts,
            expr,
        }
    }

    fn stmts(&self) -> &[Self::Stmt] {
        &self.stmts
    }

    fn stmts_mut(&mut self) -> &mut [Self::Stmt] {
        &mut self.stmts
    }

    fn expr(&self) -> Option<&Self::Expr> {
        self.expr.as_ref()
    }

    fn expr_mut(&mut self) -> Option<&mut Self::Expr> {
        self.expr.as_mut()
    }
}

impl Display for Block {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        f.pretty_print(0).block(self).finish()
    }
}

#[derive(Debug, Clone, PartialEq)]
pub struct Stmt {
    pub meta: Meta,
    pub expr: Expr,
}

impl AsMeta for Stmt {
    fn as_meta(&self) -> &Meta {
        &self.meta
    }

    fn as_meta_mut(&mut self) -> &mut Meta {
        &mut self.meta
    }
}

impl ToIdent for Stmt {
    fn to_ident(&self) -> RcStr {
        RcStr::from(format!(
            "__Stmt_{}_{}_{}",
            normalize_string(&self.span().file()),
            self.span().line(),
            self.span().col()
        ))
    }
}

impl seal::Sealed<StmtSeal> for Stmt {}

impl StmtLike for Stmt {
    type Expr = Expr;

    fn new(span: Span, expr: Self::Expr) -> Self {
        Stmt {
            meta: Meta::new(span),
            expr,
        }
    }

    fn expr(&self) -> &Self::Expr {
        &self.expr
    }

    fn expr_mut(&mut self) -> &mut Self::Expr {
        &mut self.expr
    }
}

impl Display for Stmt {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        f.pretty_print(0).stmt(self).finish()
    }
}

#[derive(Debug, Clone, PartialEq)]
pub struct Expr {
    pub meta: Meta,
    pub first: Word,
    pub rest: Vec<Word>,
}

impl AsMeta for Expr {
    fn as_meta(&self) -> &Meta {
        &self.meta
    }

    fn as_meta_mut(&mut self) -> &mut Meta {
        &mut self.meta
    }
}

impl ToIdent for Expr {
    fn to_ident(&self) -> RcStr {
        RcStr::from(format!(
            "__Expr_{}_{}_{}",
            normalize_string(&self.span().file()),
            self.span().line(),
            self.span().col()
        ))
    }
}

impl seal::Sealed<ExprSeal> for Expr {}

impl ExprLike for Expr {
    fn new(span: Span, first: Word, rest: Vec<Word>) -> Self {
        Expr {
            meta: Meta::new(span),
            first,
            rest,
        }
    }

    fn first(&self) -> &Word {
        &self.first
    }

    fn first_mut(&mut self) -> &mut Word {
        &mut self.first
    }

    fn rest(&self) -> &[Word] {
        &self.rest
    }

    fn rest_mut(&mut self) -> &mut [Word] {
        &mut self.rest
    }
}

impl Display for Expr {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        f.pretty_print(0).expr(self).finish()
    }
}

#[derive(Debug, Clone, PartialEq)]
pub struct Word {
    pub meta: Meta,
    pub kind: WordKind,
}

impl AsMeta for Word {
    fn as_meta(&self) -> &Meta {
        &self.meta
    }

    fn as_meta_mut(&mut self) -> &mut Meta {
        &mut self.meta
    }
}

impl ToIdent for Word {
    fn to_ident(&self) -> RcStr {
        RcStr::from(format!(
            "__Word_{}_{}_{}",
            normalize_string(&self.span().file()),
            self.span().line(),
            self.span().col()
        ))
    }
}

#[derive(Debug, Clone, PartialEq)]
pub enum WordKind {
    Atom(Atom),
    Path(Path),
    Integer(i64),
    Real(f64),
    Ratio(i32, u64, u64),
    Char(char),
    Str(Interpolated),
    TypeAtom(TypeWord),
    Quote(Boxed<RefCell<Word>>),
    Quasiquote(Boxed<RefCell<Word>>),
    Interpolate(Boxed<RefCell<Word>>),
    Empty,
    Parenth(Boxed<RefCell<Expr>>),
    Bind(Atom, Option<TypeExpr>, Option<Boxed<RefCell<Expr>>>, bool),
    Destructure(TypeExpr, Vec<Word>),
    Closure(Vec<Closure<Expr>>),
    Compound(Boxed<RefCell<Block>>),
}

impl Display for Word {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        f.pretty_print(0).word(self).finish()
    }
}

#[derive(Debug, Clone, PartialEq)]
pub struct Closure<T: ExprLike> {
    pub meta: Meta,
    pub mutable: bool,
    pub dynamic: bool,
    pub macroeh: bool,
    pub params: Vec<Word>,
    pub body: Boxed<RefCell<T>>,
}

impl<T: ExprLike> AsMeta for Closure<T> {
    fn as_meta(&self) -> &Meta {
        &self.meta
    }

    fn as_meta_mut(&mut self) -> &mut Meta {
        &mut self.meta
    }
}

impl<T: ExprLike> ToIdent for Closure<T> {
    fn to_ident(&self) -> RcStr {
        RcStr::from(format!(
            "__Closure_{}_{}_{}",
            normalize_string(&self.span().file()),
            self.span().line(),
            self.span().col()
        ))
    }
}

impl Display for Closure<Expr> {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        f.pretty_print(0).closure(self).finish()
    }
}

#[derive(Debug, Clone, PartialEq)]
pub struct Atom {
    pub meta: Meta,
    pub atom: RcStr,
}

impl AsMeta for Atom {
    fn as_meta(&self) -> &Meta {
        &self.meta
    }

    fn as_meta_mut(&mut self) -> &mut Meta {
        &mut self.meta
    }
}

impl ToIdent for Atom {
    fn to_ident(&self) -> RcStr {
        RcStr::from(format!(
            "__Atom_{}_{}_{}",
            normalize_string(&self.span().file()),
            self.span().line(),
            self.span().col()
        ))
    }
}

#[derive(Debug, Clone, PartialEq)]
pub struct Path {
    pub meta: Meta,
    pub path: Vec<RcStr>,
}

impl AsMeta for Path {
    fn as_meta(&self) -> &Meta {
        &self.meta
    }

    fn as_meta_mut(&mut self) -> &mut Meta {
        &mut self.meta
    }
}

impl ToIdent for Path {
    fn to_ident(&self) -> RcStr {
        RcStr::from(format!(
            "__Path_{}_{}_{}",
            normalize_string(&self.span().file()),
            self.span().line(),
            self.span().col()
        ))
    }
}

#[derive(Debug, Clone, PartialEq)]
pub struct Interpolated {
    pub meta: Meta,
    pub parts: Vec<InterpolatedPart>,
}

impl AsMeta for Interpolated {
    fn as_meta(&self) -> &Meta {
        &self.meta
    }

    fn as_meta_mut(&mut self) -> &mut Meta {
        &mut self.meta
    }
}

impl ToIdent for Interpolated {
    fn to_ident(&self) -> RcStr {
        RcStr::from(format!(
            "__Interpolated_{}_{}_{}",
            normalize_string(&self.span().file()),
            self.span().line(),
            self.span().col()
        ))
    }
}

#[derive(Debug, Clone, PartialEq)]
pub enum InterpolatedPart {
    Interpolate(Boxed<RefCell<Expr>>),
    Str(RcStr),
}

#[derive(Debug, Clone, PartialEq)]
pub struct Type {
    pub meta: Meta,
    pub atom: RcStr,
}

impl AsMeta for Type {
    fn as_meta(&self) -> &Meta {
        &self.meta
    }

    fn as_meta_mut(&mut self) -> &mut Meta {
        &mut self.meta
    }
}

impl ToIdent for Type {
    fn to_ident(&self) -> RcStr {
        RcStr::from(format!(
            "__Type_{}_{}_{}",
            normalize_string(&self.span().file()),
            self.span().line(),
            self.span().col()
        ))
    }
}

#[derive(Debug, Clone, PartialEq)]
pub struct TypeExpr {
    pub meta: Meta,
    pub first: Type,
    pub rest: Vec<TypeWord>,
}

impl AsMeta for TypeExpr {
    fn as_meta(&self) -> &Meta {
        &self.meta
    }

    fn as_meta_mut(&mut self) -> &mut Meta {
        &mut self.meta
    }
}

impl ToIdent for TypeExpr {
    fn to_ident(&self) -> RcStr {
        RcStr::from(format!(
            "__TypeExpr_{}_{}_{}",
            normalize_string(&self.span().file()),
            self.span().line(),
            self.span().col()
        ))
    }
}

impl Display for TypeExpr {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        f.pretty_print(0).type_expr(self).finish()
    }
}

#[derive(Debug, Clone, PartialEq)]
pub struct TypeWord {
    pub meta: Meta,
    pub kind: TypeWordKind,
}

impl AsMeta for TypeWord {
    fn as_meta(&self) -> &Meta {
        &self.meta
    }

    fn as_meta_mut(&mut self) -> &mut Meta {
        &mut self.meta
    }
}

impl ToIdent for TypeWord {
    fn to_ident(&self) -> RcStr {
        RcStr::from(format!(
            "__TypeWord_{}_{}_{}",
            normalize_string(&self.span().file()),
            self.span().line(),
            self.span().col()
        ))
    }
}

#[derive(Debug, Clone, PartialEq)]
pub enum TypeWordKind {
    Type(Type),
    Parenth(Boxed<TypeExpr>),
}

impl Display for TypeWord {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        f.pretty_print(0).type_word(self).finish()
    }
}

pub trait PrettyEscape {
    type Escape: Display;

    fn pretty_escape(&self) -> Self::Escape;
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, Hash)]
pub enum EscapeChar {
    Char(char),
    Tab,
    Newline,
    Esc,
    Backslash,
    Quote,
}

impl Display for EscapeChar {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            EscapeChar::Char(c) => write!(f, "{}", c),
            EscapeChar::Tab => write!(f, "\\t"),
            EscapeChar::Newline => write!(f, "\\n"),
            EscapeChar::Esc => write!(f, "\\e"),
            EscapeChar::Backslash => write!(f, "\\\\"),
            EscapeChar::Quote => write!(f, "\\'"),
        }
    }
}

impl PrettyEscape for char {
    type Escape = EscapeChar;

    fn pretty_escape(&self) -> Self::Escape {
        match self {
            '\t' => EscapeChar::Tab,
            '\n' => EscapeChar::Newline,
            '\x1b' => EscapeChar::Esc,
            '\\' => EscapeChar::Backslash,
            &c => EscapeChar::Char(c),
        }
    }
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, Hash)]
pub enum EscapeStrChar {
    Char(char),
    Tab,
    Newline,
    Esc,
    Backslash,
    Quote,
    Interp,
}

impl From<char> for EscapeStrChar {
    fn from(c: char) -> Self {
        match c {
            '\t' => EscapeStrChar::Tab,
            '\n' => EscapeStrChar::Newline,
            '\x1b' => EscapeStrChar::Esc,
            '\\' => EscapeStrChar::Backslash,
            '~' => EscapeStrChar::Interp,
            c => EscapeStrChar::Char(c),
        }
    }
}

impl Display for EscapeStrChar {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            EscapeStrChar::Char(c) => write!(f, "{}", c),
            EscapeStrChar::Tab => write!(f, "\\t"),
            EscapeStrChar::Newline => write!(f, "\\n"),
            EscapeStrChar::Esc => write!(f, "\\e"),
            EscapeStrChar::Backslash => write!(f, "\\\\"),
            EscapeStrChar::Quote => write!(f, "\\\""),
            EscapeStrChar::Interp => write!(f, "\\~"),
        }
    }
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, Hash)]
pub struct EscapeStr<'a> {
    string: &'a str,
}

impl<'a> Display for EscapeStr<'a> {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        for c in self.string.chars() {
            write!(f, "{}", EscapeStrChar::from(c))?;
        }
        Ok(())
    }
}

impl<'a> PrettyEscape for &'a str {
    type Escape = EscapeStr<'a>;

    fn pretty_escape(&self) -> Self::Escape {
        EscapeStr { string: *self }
    }
}

pub trait PrettyPrint: Sized {
    fn pretty_print(&mut self, indent: usize) -> PrettyPrinter<'_, Self>;
}

impl<T: Write> PrettyPrint for T {
    fn pretty_print(&mut self, indent: usize) -> PrettyPrinter<'_, Self> {
        PrettyPrinter {
            fmt: self,
            result: Ok(()),
            lines: String::new(),
            indent,
            newline: true,
        }
    }
}

pub struct PrettyPrinter<'a, T> {
    fmt: &'a mut T,
    result: fmt::Result,
    lines: String,
    indent: usize,
    newline: bool,
}

impl<'a, T: Write> PrettyPrinter<'a, T> {
    #[inline]
    pub fn append<U: ToString>(&mut self, a: U) -> &mut Self {
        let a = a.to_string();
        self.lines.push_str(&a.to_string());
        if a.chars().last() == Some('\n') {
            self.newline = true;
        } else {
            self.newline = false;
        }
        self
    }

    #[inline]
    pub fn indent(&mut self) -> &mut Self {
        self.lines.push_str(&" ".repeat(self.indent));
        self.newline = false;
        self
    }

    #[inline]
    pub fn newline(&mut self) -> &mut Self {
        self.lines.push('\n');
        self.indent();
        self.newline = true;
        self
    }

    #[inline]
    pub fn endline(&mut self) -> &mut Self {
        self.lines.push('\n');
        self.lines.push_str(&" ".repeat(self.indent - 4));
        self.newline = true;
        self
    }

    pub fn program(&mut self, program: &Program) -> &mut Self {
        self.result = self.result.and_then(|_| {
            let mut line = String::new();
            let mut printer = line.pretty_print(self.indent);
            printer.block(&program.block);
            printer.finish()?;
            self.append(line);
            Ok(())
        });
        self
    }

    pub fn block(&mut self, block: &Block) -> &mut Self {
        self.result = self.result.and_then(|_| {
            let mut line = String::new();
            let mut printer = line.pretty_print(self.indent);
            if let Some(stmt) = block.stmts.first() {
                printer.stmt(stmt);
            }
            for stmt in block.stmts.iter().skip(1) {
                printer.newline();
                printer.stmt(stmt);
            }
            if let Some(ref expr) = block.expr {
                if block.stmts.len() > 0 {
                    printer.newline();
                }
                printer.expr(expr);
            }
            printer.finish()?;
            self.append(line);
            Ok(())
        });
        self
    }

    pub fn stmt(&mut self, stmt: &Stmt) -> &mut Self {
        self.result = self.result.and_then(|_| {
            let mut line = String::new();
            let mut printer = line.pretty_print(self.indent);
            printer.expr(&stmt.expr);
            printer.append(";");
            printer.finish()?;
            self.append(line);
            Ok(())
        });
        self
    }

    pub fn expr(&mut self, expr: &Expr) -> &mut Self {
        self.result = self.result.and_then(|_| {
            let mut line = String::new();
            let mut printer = line.pretty_print(self.indent);
            printer.word(&expr.first);
            for word in &expr.rest {
                if !printer.newline {
                    printer.append(" ");
                }
                printer.word(word);
            }
            printer.finish()?;
            self.append(line);
            Ok(())
        });
        self
    }

    pub fn type_expr(&mut self, expr: &TypeExpr) -> &mut Self {
        self.result = self.result.and_then(|_| {
            let mut line = String::new();
            let mut printer = line.pretty_print(self.indent);
            printer.append(&expr.first.atom);
            for word in &expr.rest {
                printer.append(" ");
                printer.type_word(word);
            }
            printer.finish()?;
            self.append(line);
            Ok(())
        });
        self
    }

    pub fn type_word(&mut self, word: &TypeWord) -> &mut Self {
        self.result = self.result.and_then(|_| {
            let mut line = String::new();
            let mut printer = line.pretty_print(self.indent);
            match &word.kind {
                TypeWordKind::Type(ty) => {
                    printer.append(&ty.atom);
                }
                TypeWordKind::Parenth(expr) => {
                    printer.append("(");
                    printer.type_expr(expr);
                    printer.append(")");
                }
            }
            printer.finish()?;
            self.append(line);
            Ok(())
        });
        self
    }

    pub fn word(&mut self, word: &Word) -> &mut Self {
        self.result = self.result.and_then(|_| {
            let mut line = String::new();
            let mut printer = line.pretty_print(self.indent);
            match &word.kind {
                WordKind::Atom(atom) => {
                    printer.append(&atom.atom);
                }
                WordKind::Path(path) => {
                    if let Some(first) = path.path.first() {
                        printer.append(first);
                    }
                    for path in &path.path[1..] {
                        printer.append('.');
                        printer.append(path);
                    }
                }
                WordKind::Integer(int) => {
                    printer.append(int);
                }
                WordKind::Real(real) => {
                    printer.append(format!("{:#?}", real));
                }
                WordKind::Ratio(sign, p, q) => {
                    let sign = if *sign < 0 { "-" } else { "" };
                    printer.append(format!("{}{}/{}", sign, p, q));
                }
                WordKind::Char(c) => {
                    printer.append(format!("'{}'", c.pretty_escape()));
                }
                WordKind::Str(string) => {
                    printer.append("\"");
                    for part in &string.parts {
                        match part {
                            InterpolatedPart::Str(ref string) => {
                                printer.append(string.as_str().pretty_escape().to_string());
                            }
                            InterpolatedPart::Interpolate(ref expr) => {
                                printer.append("~");
                                printer.expr(&expr.borrow());
                            }
                        }
                    }
                    printer.append("\"");
                }
                WordKind::TypeAtom(ty) => {
                    printer.append("^");
                    printer.type_word(ty);
                }
                WordKind::Quote(word) => {
                    printer.append("\\");
                    printer.word(&word.borrow());
                }
                WordKind::Quasiquote(word) => {
                    printer.append("`");
                    printer.word(&word.borrow());
                }
                WordKind::Interpolate(word) => {
                    printer.append("~");
                    printer.word(&word.borrow());
                }
                WordKind::Empty => {
                    printer.append("()");
                }
                WordKind::Parenth(expr) => {
                    printer.append("(");
                    printer.expr(&expr.borrow());
                    printer.append(")");
                }
                WordKind::Bind(atom, ty, pred, variadic) => {
                    if ty.is_some() || pred.is_some() {
                        printer.append("$(");
                    } else {
                        printer.append("$");
                    }
                    printer.append(&atom.atom);
                    if let Some(ty) = ty {
                        printer.append(" :: ");
                        printer.type_expr(ty);
                    }
                    if let Some(pred) = pred {
                        printer.append(" : ");
                        printer.expr(&pred.borrow());
                    }
                    if ty.is_some() || pred.is_some() {
                        printer.append(")");
                    }
                    if *variadic {
                        printer.append("..");
                    }
                }
                WordKind::Destructure(ty, pats) => {
                    printer.append("(");
                    printer.type_expr(ty);
                    if let Some(pat) = pats.first() {
                        printer.append(" ");
                        printer.word(pat);
                    }
                    for pat in pats.iter().skip(1) {
                        printer.append(" ");
                        printer.word(pat);
                    }
                    printer.append(")");
                }
                WordKind::Closure(closure) => {
                    printer.multi(closure);
                }
                WordKind::Compound(block) => {
                    let mut block_line = String::new();
                    let mut block_printer = block_line.pretty_print(self.indent + 4);
                    block_printer.append("\n");
                    block_printer.append(" ".repeat(self.indent + 4));
                    block_printer.block(&block.borrow());
                    block_printer.finish()?;
                    printer.append(block_line);
                }
            }
            printer.finish()?;
            self.append(line);
            Ok(())
        });
        self
    }

    pub fn multi(&mut self, closure: &[Closure<Expr>]) -> &mut Self {
        self.result = self.result.and_then(|_| {
            let mut line = String::new();
            let mut printer = line.pretty_print(self.indent);
            printer.append("[");
            for closure in closure {
                printer.closure(closure);
            }
            printer.append(" ]");
            printer.finish()?;
            self.append(line);
            Ok(())
        });
        self
    }

    pub fn closure(&mut self, closure: &Closure<Expr>) -> &mut Self {
        self.result = self.result.and_then(|_| {
            let mut line = String::new();
            let mut printer = line.pretty_print(self.indent);
            if closure.dynamic {
                printer.append(" dyn");
            }
            if closure.mutable {
                printer.append(" mut");
            }
            if closure.macroeh {
                printer.append(" macro");
            }
            printer.append(" | ");
            if let Some(param) = closure.params.first() {
                printer.word(param);
            }
            for param in closure.params.iter().skip(1) {
                printer.append(" ");
                printer.word(param);
            }
            printer.append(" | ");
            printer.expr(&closure.body.borrow());
            printer.finish()?;
            self.append(line);
            Ok(())
        });
        self
    }

    pub fn finish(&mut self) -> fmt::Result {
        if self.result.is_err() {
            return self.result;
        }
        self.fmt.write_str(&self.lines)?;
        Ok(())
    }
}

pub(crate) fn normalize_string(string: &str) -> String {
    let mut output = String::new();
    let mut chars = string.chars();
    if let Some(ch) = chars.next() {
        output.push_str(normalize_ascii_char(ch));
    }
    let mut last = 0;
    for ch in chars {
        let ch = normalize_ascii_char(ch);
        if ch.len() > 1 || last > 1 {
            output.push('_');
        }
        output.push_str(ch);
        last = ch.len();
    }
    output
}

pub(crate) fn normalize_ascii_char(ch: char) -> &'static str {
    match ch {
        '\x00' => "NUL",
        '\x01' => "SOH",
        '\x02' => "STX",
        '\x03' => "ETX",
        '\x04' => "EOT",
        '\x05' => "ENQ",
        '\x06' => "ACK",
        '\x07' => "BEL",
        '\x08' => "BS",
        '\x09' => "TAB",
        '\x0a' => "LF",
        '\x0b' => "VT",
        '\x0c' => "FF",
        '\x0d' => "CR",
        '\x0e' => "SO",
        '\x0f' => "SI",
        '\x10' => "DLE",
        '\x11' => "DC1",
        '\x12' => "DC2",
        '\x13' => "DC3",
        '\x14' => "DC4",
        '\x15' => "NAK",
        '\x16' => "SYN",
        '\x17' => "ETB",
        '\x18' => "CAN",
        '\x19' => "EM",
        '\x1a' => "SUB",
        '\x1b' => "ESC",
        '\x1c' => "FS",
        '\x1d' => "GS",
        '\x1e' => "RS",
        '\x1f' => "US",
        ' ' => "SPACE",
        '!' => "EXCLAM",
        '"' => "DQUOTE",
        '#' => "SHARP",
        '$' => "DOLLAR",
        '%' => "PERCENT",
        '&' => "ET",
        '\'' => "QUOTE",
        '(' => "PARENL",
        ')' => "PARENR",
        '*' => "ASTERISK",
        '+' => "PLUS",
        ',' => "COMMA",
        '-' => "MINUS",
        '.' => "DOT",
        '/' => "SLASH",
        ':' => "COLON",
        ';' => "SEMICOLON",
        '<' => "LT",
        '=' => "EQ",
        '>' => "GT",
        '?' => "QUESTION",
        '@' => "AT",
        '[' => "BRACKETL",
        '\\' => "BACKSLASH",
        ']' => "BRACKETR",
        '^' => "CARET",
        '_' => "_",
        '`' => "BACKTICK",
        '{' => "CURLYL",
        '|' => "PIPE",
        '}' => "CURLYR",
        '~' => "TILDE",
        '\x7f' => "DEL",
        '0' => "0",
        '1' => "1",
        '2' => "2",
        '3' => "3",
        '4' => "4",
        '5' => "5",
        '6' => "6",
        '7' => "7",
        '8' => "8",
        '9' => "9",
        'A' => "A",
        'B' => "B",
        'C' => "C",
        'D' => "D",
        'E' => "E",
        'F' => "F",
        'G' => "G",
        'H' => "H",
        'I' => "I",
        'J' => "J",
        'K' => "K",
        'L' => "L",
        'M' => "M",
        'N' => "N",
        'O' => "O",
        'P' => "P",
        'Q' => "Q",
        'R' => "R",
        'S' => "S",
        'T' => "T",
        'U' => "U",
        'V' => "V",
        'W' => "W",
        'X' => "X",
        'Y' => "Y",
        'Z' => "Z",
        'a' => "a",
        'b' => "b",
        'c' => "c",
        'd' => "d",
        'e' => "e",
        'f' => "f",
        'g' => "g",
        'h' => "h",
        'i' => "i",
        'j' => "j",
        'k' => "k",
        'l' => "l",
        'm' => "m",
        'n' => "n",
        'o' => "o",
        'p' => "p",
        'q' => "q",
        'r' => "r",
        's' => "s",
        't' => "t",
        'u' => "u",
        'v' => "v",
        'w' => "w",
        'x' => "x",
        'y' => "y",
        'z' => "z",
        _ => panic!("not an ascii character"),
    }
}
