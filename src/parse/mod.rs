use crate::error::Error;
use crate::lex::Token;
use crate::pp::Preprocessor;

pub mod ast;
pub mod grammar;

pub struct Parser {
    pp: Preprocessor,
    push_back: Vec<Token>,
    errors: Vec<Error>,
}

impl Parser {
    pub fn new(pp: Preprocessor) -> Parser {
        Parser {
            pp,
            push_back: Vec::new(),
            errors: vec![],
        }
    }

    pub fn parse(mut self) -> Result<ast::Program, Vec<Error>> {
        let program = match grammar::parse_program(&mut self) {
            Ok(ok) => ok,
            Err(err) => {
                self.errors.push(err);
                return Err(self.errors);
            }
        };
        if self.errors.len() > 0 {
            return Err(self.errors);
        }
        Ok(program)
    }

    pub fn errors(&self) -> &[Error] {
        &self.errors
    }

    pub(crate) fn push_err(&mut self, err: Error) {
        self.errors.push(err);
    }

    pub(crate) fn peek(&mut self) -> Option<Result<&Token, Error>> {
        if !self.push_back.is_empty() {
            return self.push_back.last().map(Ok);
        }
        match self.pp.next() {
            Some(Ok(peek)) => {
                self.push_back.push(peek);
                self.push_back.last().map(Ok)
            }
            Some(Err(err)) => return Some(Err(err)),
            None => None,
        }
    }

    pub(crate) fn advance(&mut self) -> Option<Result<Token, Error>> {
        self.push_back.pop().map(Ok).or_else(|| self.pp.next())
    }

    pub(crate) fn push(&mut self, tok: Token) {
        self.push_back.push(tok);
    }
}

#[cfg(test)]
mod tests {
    use crate::collections::RcStr;
    use crate::lex::Lexer;
    use crate::pp::Preprocessor;
    use super::*;

    #[test]
    fn sanity() {
        let input = "x; y (1 + 2) [ dyn | (Type $x) | x ]; z";
        let input = RcStr::from(input);
        let lexer = Lexer::new(RcStr::from("<test>"), input);
        let pp = Preprocessor::new(lexer);
        let parser = Parser::new(pp);
        match parser.parse() {
            Ok(ok) => println!("{}", ok),
            Err(errs) => {
                for err in errs {
                    eprintln!("{}", err);
                }
            }
        }

        let input = "let a = \n  let x = \n    let y = 42;\n    y\n  x\n";
        let input = RcStr::from(input);
        let lexer = Lexer::new(RcStr::from("<test>"), input);
        let pp = Preprocessor::new(lexer);
        let parser = Parser::new(pp);
        match parser.parse() {
            Ok(ok) => println!("{}", ok),
            Err(errs) => {
                for err in errs {
                    eprintln!("{}", err);
                }
            }
        }

        let input = "let a = \"a\";\nlet b = \"~(b)\";\nlet c = \"c = ~(c)\";\nlet d = \"~(d) = d\";";
        let input = RcStr::from(input);
        let lexer = Lexer::new(RcStr::from("<test>"), input);
        let pp = Preprocessor::new(lexer);
        let parser = Parser::new(pp);
        match parser.parse() {
            Ok(ok) => println!("{}", ok),
            Err(errs) => {
                for err in errs {
                    eprintln!("{}", err);
                }
            }
        }

        let input = "let x = ^Foo ^Bar 42;";
        let input = RcStr::from(input);
        let lexer = Lexer::new(RcStr::from("<test>"), input);
        let pp = Preprocessor::new(lexer);
        let parser = Parser::new(pp);
        match parser.parse() {
            Ok(ok) => println!("{}", ok),
            Err(errs) => {
                for err in errs {
                    eprintln!("{}", err);
                }
            }
        }

        let input = "
let qsort = [| (List $head $tail) |
    let left = filter [| $x | x < head ] tail;
    let right = filter [| $x | x >= head ] tail;
    let mid = `(~head);
    ++ (qsort left) mid (qsort right)
];";
        let input = RcStr::from(input);
        let lexer = Lexer::new(RcStr::from("<test>"), input);
        let pp = Preprocessor::new(lexer);
        let parser = Parser::new(pp);
        match parser.parse() {
            Ok(ok) => println!("{}", ok),
            Err(errs) => {
                for err in errs {
                    eprintln!("{}", err);
                }
            }
        }
    }
}
