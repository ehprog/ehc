use std::iter;
use std::fmt::{self, Display};
use std::collections::HashMap;
use std::cell::RefCell;

use bitflags::bitflags;

use ehrt_sys::{
    TypeId_EH_NIL as EH_NIL, TypeId_EH_ANY as EH_ANY, TypeId_EH_ATOM as EH_ATOM, TypeId_EH_BOOL as EH_BOOL,
    TypeId_EH_INT as EH_INT, TypeId_EH_PTR as EH_PTR, TypeId_EH_RATIO as EH_RATIO, TypeId_EH_REAL as EH_REAL, TypeId_EH_CHAR as EH_CHAR,
    TypeId_EH_LIST as EH_LIST, TypeId_EH_BIND as EH_BIND, TypeId_EH_HNODE as EH_HNODE, TypeId_EH_HTRIE as EH_HTRIE,
    TypeId_EH_FUNCTION as EH_FUNCTION, TypeId_EH_IFUNC as EH_IFUNC, TypeId_EH_CLOSURE as EH_CLOSURE,
    TypeId_EH_VAR as EH_VAR, TypeId_EH_LAST as EH_LAST, EHTYPE_VARIADIC, EHTYPE_UNION, EHTYPE_UNIONALL, EHTYPE_CLOSURE,
    EHTYPE_DYNAMIC, EHTYPE_MUTABLE, EHTYPE_MACRO, EhBoolTag_EH_BOOL_TRUE as EH_BOOL_TRUE,
    EhBoolTag_EH_BOOL_FALSE as EH_BOOL_FALSE, EhListTag_EH_LIST_EMPTY as EH_LIST_EMPTY,
    EhListTag_EH_LIST_CONS as EH_LIST_CONS,
};

use crate::error::{Error, ErrorKind};
use crate::collections::{Boxed, RcStr};
use crate::lex::Span;
use crate::parse::ast::{self, normalize_string, ToIdent, Meta, AsMeta, BlockLike, StmtLike, ExprLike};
use crate::module::{ModuleHandler, Modules, Module};

pub struct TypeInferrer {
    modh: ModuleHandler,
    modname: Vec<RcStr>,
    ctx: Option<TypeContext>,
}

impl TypeInferrer {
    pub fn new(modh: ModuleHandler) -> Self {
        let modname = modh.modname().to_vec();
        TypeInferrer {
            modh,
            modname,
            ctx: None,
        }
    }

    pub fn with_ctx(modh: ModuleHandler, ctx: TypeContext) -> Self {
        let modname = modh.modname().to_vec();
        TypeInferrer {
            modh,
            modname,
            ctx: Some(ctx),
        }
    }

    pub fn modname(&self) -> &[RcStr] {
        &self.modname
    }

    pub fn is_core(&self) -> bool {
        self.modname == ["core"]
    }

    pub fn evaluate(self) -> Result<(Modules, TypeContext), Vec<Error>> {
        let is_core = self.is_core();
        let mut modules = self.modh.handle()?;
        let mut types = self.ctx.unwrap_or_else(|| TypeContext::new(modules.main.clone()));
        let name = modules.main.clone();
        let main = modules.modules.remove(&modules.main).unwrap();
        types.evaluate(&mut modules, name, main, !is_core)?;

        Ok((modules, types))
    }
}

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub enum Vis {
    Pub,
    Priv,
}

#[derive(Debug, Clone, PartialEq, Eq)]
pub struct TypeContext {
    type_id: u32,
    types: HashMap<Type, u32>,
    variants: HashMap<Type, (u32, u32)>,
    ptr: Vec<RcStr>,
    bindings: HashMap<RcStr, HashMap<Vec<RcStr>, (Boxed<Type>, Vis)>>,
    macros: HashMap<Vec<RcStr>, Boxed<Type>>,
    constructors: HashMap<Vec<RcStr>, Boxed<Type>>,
}

impl Default for TypeContext {
    fn default() -> Self {
        let types = HashMap::new();
        let variants = HashMap::new();
        let bindings = HashMap::new();
        let macros = HashMap::new();
        let constructors = HashMap::new();
        TypeContext {
            type_id: 2 * EH_LAST,
            types,
            variants,
            ptr: vec![From::from("main")],
            bindings,
            macros,
            constructors,
        }
    }
}

impl TypeContext {
    pub fn new(module: Vec<RcStr>) -> Self {
        let mut types = HashMap::new();
        types.insert(Type::from("Nil"), EH_NIL);
        types.insert(Type::from("Any"), EH_ANY);
        types.insert(Type::from("Atom"), EH_ATOM);
        types.insert(Type::from("Bool"), EH_BOOL);
        types.insert(Type::from("Int"), EH_INT);
        types.insert(Type::from("Ptr"), EH_PTR);
        types.insert(Type::from("Ratio"), EH_RATIO);
        types.insert(Type::from("Real"), EH_REAL);
        types.insert(Type::from("Char"), EH_CHAR);
        types.insert(Type::from("List"), EH_LIST);
        types.insert(Type::from("Bind"), EH_BIND);
        types.insert(Type::from("Hnode"), EH_HNODE);
        types.insert(Type::from("Htrie"), EH_HTRIE);
        types.insert(Type::from("Function"), EH_FUNCTION);
        types.insert(Type::from("Ifunc"), EH_IFUNC);
        types.insert(
            Type {
                flags: TypeFlags::CLOSURE,
                first: TypeAtom::from("Closure"),
                rest: vec![TypeExpr::from(TypeAtom::from("Any"))],
            },
            EH_CLOSURE,
        );
        types.insert(Type::from("Var"), EH_VAR);

        let mut variants = HashMap::new();
        variants.insert(Type::from("Nil"), (EH_NIL, 0));
        variants.insert(Type::from("Any"), (EH_ANY, 0));
        variants.insert(Type::from("Atom"), (EH_ATOM, 0));
        variants.insert(Type::from("False"), (EH_BOOL, EH_BOOL_FALSE));
        variants.insert(Type::from("True"), (EH_BOOL, EH_BOOL_TRUE));
        variants.insert(Type::from("Int"), (EH_INT, 0));
        variants.insert(Type::from("Ptr"), (EH_PTR, 0));
        variants.insert(Type::from("Ratio"), (EH_RATIO, 0));
        variants.insert(Type::from("Real"), (EH_REAL, 0));
        variants.insert(Type::from("Char"), (EH_CHAR, 0));
        variants.insert(Type::from("Empty"), (EH_LIST, EH_LIST_EMPTY));
        variants.insert(Type::from("Cons"), (EH_LIST, EH_LIST_CONS));
        variants.insert(Type::from("Bind"), (EH_BIND, 0));
        variants.insert(Type::from("Hnode"), (EH_HNODE, 0));
        variants.insert(Type::from("Htrie"), (EH_HTRIE, 0));
        variants.insert(Type::from("Function"), (EH_FUNCTION, 0));
        variants.insert(Type::from("Ifunc"), (EH_IFUNC, 0));
        variants.insert(
            Type {
                flags: TypeFlags::CLOSURE,
                first: TypeAtom::from("Closure"),
                rest: vec![TypeExpr::from(TypeAtom::from("Any"))],
            },
            (EH_CLOSURE, 0),
        );
        variants.insert(Type::from("Var"), (EH_VAR, 0));

        let mut bindings = HashMap::new();
        bindings.insert(
            vec![From::from("let")],
            (
                Boxed::new(Type {
                    flags: TypeFlags::CLOSURE,
                    first: TypeAtom::from("Closure"),
                    rest: vec![
                        TypeExpr::from(Type::from("Nil")),
                        TypeExpr::from(Type::from("Atom")),
                        TypeExpr::from(Type::from("Atom")),
                        TypeExpr::from(Type::from("Any").with_flags(TypeFlags::UNIONALL | TypeFlags::VARIADIC)),
                    ],
                }),
                Vis::Priv,
            ),
        );
        bindings.insert(
            vec![From::from("def")],
            (
                Boxed::new(Type {
                    flags: TypeFlags::CLOSURE,
                    first: TypeAtom::from("Closure"),
                    rest: vec![
                        TypeExpr::from(Type::from("Nil")),
                        TypeExpr::from(Type::from("Atom")),
                        TypeExpr::from(Type::from("Atom")),
                        TypeExpr::from(Type::from("Any").with_flags(TypeFlags::UNIONALL | TypeFlags::VARIADIC)),
                    ],
                }),
                Vis::Priv,
            ),
        );
        bindings.insert(
            vec![From::from("use")],
            (
                Boxed::new(Type {
                    flags: TypeFlags::CLOSURE,
                    first: TypeAtom::from("Closure"),
                    rest: vec![
                        TypeExpr::from(Type::from("Nil")),
                        TypeExpr::from(TypeAtom::from("Atom")),
                    ],
                }),
                Vis::Priv,
            ),
        );
        bindings.insert(
            vec![From::from("from")],
            (
                Boxed::new(Type {
                    flags: TypeFlags::CLOSURE,
                    first: TypeAtom::from("Closure"),
                    rest: vec![
                        TypeExpr::from(Type::from("Nil")),
                        TypeExpr::from(TypeAtom::from("Atom")),
                        TypeExpr::from(TypeAtom::from("Atom")),
                        TypeExpr::from(Type::from("Atom").with_flags(TypeFlags::VARIADIC)),
                    ],
                }),
                Vis::Priv,
            ),
        );
        bindings.insert(
            vec![From::from("type")],
            (
                Boxed::new(Type {
                    flags: TypeFlags::CLOSURE,
                    first: TypeAtom::from("Closure"),
                    rest: vec![
                        TypeExpr::from(Type::from("Nil")),
                        TypeExpr::from(Type::from("Type")),
                        TypeExpr::from(Type::from("Any")),
                    ],
                }),
                Vis::Priv,
            ),
        );
        bindings.insert(
            vec![From::from("intrinsic"), From::from("add")],
            (
                Boxed::new(Type {
                    flags: TypeFlags::CLOSURE,
                    first: TypeAtom::from("Closure"),
                    rest: vec![
                        TypeExpr::from(TypeAtom::from("Any")),
                        TypeExpr::from(TypeAtom::from("Any")),
                        TypeExpr::from(TypeAtom::from("Any")),
                    ],
                }),
                Vis::Priv,
            ),
        );
        bindings.insert(
            vec![From::from("intrinsic"), From::from("sub")],
            (
                Boxed::new(Type {
                    flags: TypeFlags::CLOSURE,
                    first: TypeAtom::from("Closure"),
                    rest: vec![
                        TypeExpr::from(TypeAtom::from("Any")),
                        TypeExpr::from(TypeAtom::from("Any")),
                        TypeExpr::from(TypeAtom::from("Any")),
                    ],
                }),
                Vis::Priv,
            ),
        );
        bindings.insert(
            vec![From::from("intrinsic"), From::from("mul")],
            (
                Boxed::new(Type {
                    flags: TypeFlags::CLOSURE,
                    first: TypeAtom::from("Closure"),
                    rest: vec![
                        TypeExpr::from(TypeAtom::from("Any")),
                        TypeExpr::from(TypeAtom::from("Any")),
                        TypeExpr::from(TypeAtom::from("Any")),
                    ],
                }),
                Vis::Priv,
            ),
        );
        bindings.insert(
            vec![From::from("intrinsic"), From::from("div")],
            (
                Boxed::new(Type {
                    flags: TypeFlags::CLOSURE,
                    first: TypeAtom::from("Closure"),
                    rest: vec![
                        TypeExpr::from(TypeAtom::from("Any")),
                        TypeExpr::from(TypeAtom::from("Any")),
                        TypeExpr::from(TypeAtom::from("Any")),
                    ],
                }),
                Vis::Priv,
            ),
        );
        bindings.insert(
            vec![From::from("intrinsic"), From::from("mod")],
            (
                Boxed::new(Type {
                    flags: TypeFlags::CLOSURE,
                    first: TypeAtom::from("Closure"),
                    rest: vec![
                        TypeExpr::from(TypeAtom::from("Any")),
                        TypeExpr::from(TypeAtom::from("Any")),
                        TypeExpr::from(TypeAtom::from("Any")),
                    ],
                }),
                Vis::Priv,
            ),
        );
        bindings.insert(
            vec![From::from("intrinsic"), From::from("head")],
            (
                Boxed::new(Type {
                    flags: TypeFlags::CLOSURE,
                    first: TypeAtom::from("Closure"),
                    rest: vec![
                        TypeExpr::from(TypeAtom::from("Any")),
                        TypeExpr::from(TypeAtom::from("List")),
                    ],
                }),
                Vis::Priv,
            ),
        );
        bindings.insert(
            vec![From::from("intrinsic"), From::from("tail")],
            (
                Boxed::new(Type {
                    flags: TypeFlags::CLOSURE,
                    first: TypeAtom::from("Closure"),
                    rest: vec![
                        TypeExpr::from(TypeAtom::from("List")),
                        TypeExpr::from(TypeAtom::from("List")),
                    ],
                }),
                Vis::Priv,
            ),
        );
        bindings.insert(
            vec![From::from("intrinsic"), From::from("getvar")],
            (
                Boxed::new(Type {
                    flags: TypeFlags::CLOSURE,
                    first: TypeAtom::from("Closure"),
                    rest: vec![
                        TypeExpr::from(TypeAtom::from("Any")),
                        TypeExpr::from(TypeAtom::from("Var")),
                    ],
                }),
                Vis::Priv,
            ),
        );
        bindings.insert(
            vec![From::from("intrinsic"), From::from("updatevar")],
            (
                Boxed::new(Type {
                    flags: TypeFlags::CLOSURE,
                    first: TypeAtom::from("Closure"),
                    rest: vec![
                        TypeExpr::from(TypeAtom::from("Nil")),
                        TypeExpr::from(TypeAtom::from("Var")),
                        TypeExpr::from(TypeAtom::from("Any")),
                    ],
                }),
                Vis::Priv,
            ),
        );
        bindings.insert(
            vec![From::from("intrinsic"), From::from("eq")],
            (
                Boxed::new(Type {
                    flags: TypeFlags::CLOSURE,
                    first: TypeAtom::from("Closure"),
                    rest: vec![
                        TypeExpr::from(TypeAtom::from("Bool")),
                        TypeExpr::from(TypeAtom::from("Any")),
                        TypeExpr::from(TypeAtom::from("Any")),
                    ],
                }),
                Vis::Priv,
            ),
        );
        bindings.insert(
            vec![From::from("intrinsic"), From::from("ne")],
            (
                Boxed::new(Type {
                    flags: TypeFlags::CLOSURE,
                    first: TypeAtom::from("Closure"),
                    rest: vec![
                        TypeExpr::from(TypeAtom::from("Bool")),
                        TypeExpr::from(TypeAtom::from("Any")),
                        TypeExpr::from(TypeAtom::from("Any")),
                    ],
                }),
                Vis::Priv,
            ),
        );
        bindings.insert(
            vec![From::from("intrinsic"), From::from("lt")],
            (
                Boxed::new(Type {
                    flags: TypeFlags::CLOSURE,
                    first: TypeAtom::from("Closure"),
                    rest: vec![
                        TypeExpr::from(TypeAtom::from("Bool")),
                        TypeExpr::from(TypeAtom::from("Any")),
                        TypeExpr::from(TypeAtom::from("Any")),
                    ],
                }),
                Vis::Priv,
            ),
        );
        bindings.insert(
            vec![From::from("intrinsic"), From::from("gt")],
            (
                Boxed::new(Type {
                    flags: TypeFlags::CLOSURE,
                    first: TypeAtom::from("Closure"),
                    rest: vec![
                        TypeExpr::from(TypeAtom::from("Bool")),
                        TypeExpr::from(TypeAtom::from("Any")),
                        TypeExpr::from(TypeAtom::from("Any")),
                    ],
                }),
                Vis::Priv,
            ),
        );
        bindings.insert(
            vec![From::from("intrinsic"), From::from("le")],
            (
                Boxed::new(Type {
                    flags: TypeFlags::CLOSURE,
                    first: TypeAtom::from("Closure"),
                    rest: vec![
                        TypeExpr::from(TypeAtom::from("Bool")),
                        TypeExpr::from(TypeAtom::from("Any")),
                        TypeExpr::from(TypeAtom::from("Any")),
                    ],
                }),
                Vis::Priv,
            ),
        );
        bindings.insert(
            vec![From::from("intrinsic"), From::from("ge")],
            (
                Boxed::new(Type {
                    flags: TypeFlags::CLOSURE,
                    first: TypeAtom::from("Closure"),
                    rest: vec![
                        TypeExpr::from(TypeAtom::from("Bool")),
                        TypeExpr::from(TypeAtom::from("Any")),
                        TypeExpr::from(TypeAtom::from("Any")),
                    ],
                }),
                Vis::Priv,
            ),
        );
        bindings.insert(
            vec![From::from("intrinsic"), From::from("repr")],
            (
                Boxed::new(Type {
                    flags: TypeFlags::CLOSURE,
                    first: TypeAtom::from("Closure"),
                    rest: vec![
                        TypeExpr::from(TypeAtom::from("List")),
                        TypeExpr::from(TypeAtom::from("Any")),
                    ],
                }),
                Vis::Priv,
            ),
        );
        bindings.insert(
            vec![From::from("intrinsic"), From::from("display")],
            (
                Boxed::new(Type {
                    flags: TypeFlags::CLOSURE,
                    first: TypeAtom::from("Closure"),
                    rest: vec![
                        TypeExpr::from(TypeAtom::from("List")),
                        TypeExpr::from(TypeAtom::from("Any")),
                    ],
                }),
                Vis::Priv,
            ),
        );
        bindings.insert(
            vec![From::from("intrinsic"), From::from("int->ratio")],
            (
                Boxed::new(Type {
                    flags: TypeFlags::CLOSURE,
                    first: TypeAtom::from("Closure"),
                    rest: vec![
                        TypeExpr::from(TypeAtom::from("Ratio")),
                        TypeExpr::from(TypeAtom::from("Int")),
                    ],
                }),
                Vis::Priv,
            ),
        );
        bindings.insert(
            vec![From::from("intrinsic"), From::from("int->real")],
            (
                Boxed::new(Type {
                    flags: TypeFlags::CLOSURE,
                    first: TypeAtom::from("Closure"),
                    rest: vec![
                        TypeExpr::from(TypeAtom::from("Real")),
                        TypeExpr::from(TypeAtom::from("Int")),
                    ],
                }),
                Vis::Priv,
            ),
        );
        bindings.insert(
            vec![From::from("intrinsic"), From::from("int->char")],
            (
                Boxed::new(Type {
                    flags: TypeFlags::CLOSURE,
                    first: TypeAtom::from("Closure"),
                    rest: vec![
                        TypeExpr::from(TypeAtom::from("Char")),
                        TypeExpr::from(TypeAtom::from("Int")),
                    ],
                }),
                Vis::Priv,
            ),
        );
        bindings.insert(
            vec![From::from("intrinsic"), From::from("ratio->int")],
            (
                Boxed::new(Type {
                    flags: TypeFlags::CLOSURE,
                    first: TypeAtom::from("Closure"),
                    rest: vec![
                        TypeExpr::from(TypeAtom::from("Int")),
                        TypeExpr::from(TypeAtom::from("Ratio")),
                    ],
                }),
                Vis::Priv,
            ),
        );
        bindings.insert(
            vec![From::from("intrinsic"), From::from("ratio->real")],
            (
                Boxed::new(Type {
                    flags: TypeFlags::CLOSURE,
                    first: TypeAtom::from("Closure"),
                    rest: vec![
                        TypeExpr::from(TypeAtom::from("Real")),
                        TypeExpr::from(TypeAtom::from("Ratio")),
                    ],
                }),
                Vis::Priv,
            ),
        );
        bindings.insert(
            vec![From::from("intrinsic"), From::from("real->int")],
            (
                Boxed::new(Type {
                    flags: TypeFlags::CLOSURE,
                    first: TypeAtom::from("Closure"),
                    rest: vec![
                        TypeExpr::from(TypeAtom::from("Int")),
                        TypeExpr::from(TypeAtom::from("Real")),
                    ],
                }),
                Vis::Priv,
            ),
        );
        bindings.insert(
            vec![From::from("intrinsic"), From::from("real->ratio")],
            (
                Boxed::new(Type {
                    flags: TypeFlags::CLOSURE,
                    first: TypeAtom::from("Closure"),
                    rest: vec![
                        TypeExpr::from(TypeAtom::from("Ratio")),
                        TypeExpr::from(TypeAtom::from("Real")),
                    ],
                }),
                Vis::Priv,
            ),
        );
        bindings.insert(
            vec![From::from("intrinsic"), From::from("char->int")],
            (
                Boxed::new(Type {
                    flags: TypeFlags::CLOSURE,
                    first: TypeAtom::from("Closure"),
                    rest: vec![
                        TypeExpr::from(TypeAtom::from("Int")),
                        TypeExpr::from(TypeAtom::from("Char")),
                    ],
                }),
                Vis::Priv,
            ),
        );

        init_lib_cmath(&mut bindings);
        init_lib_sys(&mut bindings);

        let mut macros = HashMap::new();
        macros.insert(
            vec![From::from("macro")],
            Boxed::new(Type {
                flags: TypeFlags::CLOSURE | TypeFlags::MACRO,
                first: TypeAtom::from("Closure"),
                rest: vec![
                    TypeExpr::from(Type::from("Nil")),
                    TypeExpr::from(Type::from("Atom")),
                    TypeExpr::from(Type::from("Atom")),
                    TypeExpr::from(Type {
                        flags: TypeFlags::CLOSURE,
                        first: TypeAtom::from("Closure"),
                        rest: vec![TypeExpr::from(TypeAtom::from("Any"))],
                    }),
                ],
            }),
        );
        macros.insert(
            vec![From::from("macro"), From::from("gensym")],
            Boxed::new(Type {
                flags: TypeFlags::CLOSURE,
                first: TypeAtom::from("Closure"),
                rest: vec![TypeExpr::from(Type::from("Atom"))],
            }),
        );

        let mut constructors = HashMap::new();
        constructors.insert(
            vec![From::from("Nil")],
            Boxed::new(Type {
                flags: TypeFlags::CLOSURE,
                first: TypeAtom::from("Closure"),
                rest: vec![TypeExpr::from(TypeAtom::from("Nil"))],
            }),
        );
        constructors.insert(
            vec![From::from("Atom")],
            Boxed::new(Type {
                flags: TypeFlags::CLOSURE,
                first: TypeAtom::from("Closure"),
                rest: vec![
                    TypeExpr::from(TypeAtom::from("Atom")),
                    TypeExpr::from(TypeAtom::from("Atom")),
                ],
            }),
        );
        constructors.insert(
            vec![From::from("True")],
            Boxed::new(Type {
                flags: TypeFlags::CLOSURE,
                first: TypeAtom::from("Closure"),
                rest: vec![TypeExpr::from(TypeAtom::from("Bool"))],
            }),
        );
        constructors.insert(
            vec![From::from("False")],
            Boxed::new(Type {
                flags: TypeFlags::CLOSURE,
                first: TypeAtom::from("Closure"),
                rest: vec![TypeExpr::from(TypeAtom::from("Bool"))],
            }),
        );
        constructors.insert(
            vec![From::from("Int")],
            Boxed::new(Type {
                flags: TypeFlags::CLOSURE,
                first: TypeAtom::from("Closure"),
                rest: vec![
                    TypeExpr::from(TypeAtom::from("Int")),
                    TypeExpr::from(TypeAtom::from("Int")),
                ],
            }),
        );
        constructors.insert(
            vec![From::from("Real")],
            Boxed::new(Type {
                flags: TypeFlags::CLOSURE,
                first: TypeAtom::from("Closure"),
                rest: vec![
                    TypeExpr::from(TypeAtom::from("Real")),
                    TypeExpr::from(TypeAtom::from("Real")),
                ],
            }),
        );
        constructors.insert(
            vec![From::from("Char")],
            Boxed::new(Type {
                flags: TypeFlags::CLOSURE,
                first: TypeAtom::from("Closure"),
                rest: vec![
                    TypeExpr::from(TypeAtom::from("Char")),
                    TypeExpr::from(TypeAtom::from("Char")),
                ],
            }),
        );
        constructors.insert(
            vec![From::from("Empty")],
            Boxed::new(Type {
                flags: TypeFlags::CLOSURE,
                first: TypeAtom::from("Closure"),
                rest: vec![TypeExpr::from(TypeAtom::from("List"))],
            }),
        );
        constructors.insert(
            vec![From::from("Cons")],
            Boxed::new(Type {
                flags: TypeFlags::CLOSURE,
                first: TypeAtom::from("Closure"),
                rest: vec![
                    TypeExpr::from(TypeAtom::from("List")),
                    TypeExpr::from(TypeAtom::from("Any")),
                    TypeExpr::from(TypeAtom::from("List")),
                ],
            }),
        );
        constructors.insert(
            vec![From::from("Var")],
            Boxed::new(Type {
                flags: TypeFlags::CLOSURE,
                first: TypeAtom::from("Closure"),
                rest: vec![
                    TypeExpr::from(TypeAtom::from("Var")),
                    TypeExpr::from(TypeAtom::from("Any")),
                ],
            }),
        );
        let name = RcStr::from(module[1..].iter().fold(module[0].to_string(), |mut acc, path| {
            acc.push('.');
            acc.push_str(path);
            acc
        }));
        let name = RcStr::from(normalize_string(&name));
        TypeContext {
            type_id: 2 * EH_LAST,
            types,
            variants,
            ptr: vec![name.clone()],
            bindings: iter::once((name.clone(), bindings)).collect(),
            macros,
            constructors,
        }
    }

    pub fn evaluate(
        &mut self,
        mods: &mut Modules,
        name: Vec<RcStr>,
        mut module: Module,
        with_core: bool,
    ) -> Result<(), Vec<Error>> {
        let mut errors = vec![];
        match &module {
            Module::Source { dependencies, .. } => {
                for name in dependencies {
                    let with_core = with_core && name != &["core"];
                    if let Some(dep) = mods.modules.remove(name) {
                        let mut types = TypeContext::new(name.clone());
                        types
                            .evaluate(mods, name.clone(), dep, with_core)
                            .unwrap_or_else(|err| errors.extend(err));
                        self.import(&name, &types);
                    }
                }
            }
            Module::Ehlib(..) => {}
        }
        if errors.is_empty() {
            match &mut module {
                Module::Source { program, .. } => {
                    if with_core {
                        evaluate_from(
                            &ast::Expr::new(
                                Span::new(From::from(file!()), From::from(""), line!() as _, 0),
                                ast::Word {
                                    meta: Meta::new(Span::new(From::from(file!()), From::from(""), line!() as _, 0)),
                                    kind: ast::WordKind::Empty,
                                },
                                vec![],
                            ),
                            self,
                            vec![From::from("core")],
                            [
                                "+", "-", "*", "/", "%", "not", "and", "or", "xor", "=", "/=", "<", ">", "<=", ">=",
                                "rev", "map", "filter", "fold", "exists?", "for-all?", "++", "len", "@", "<-",
                                "promote", "ord", "chr",
                            ]
                            .iter()
                            .cloned()
                            .map(From::from)
                            .collect(),
                        )?;
                    }
                    program.block.evaluate(self)?;
                }
                Module::Ehlib(ehlib) => {
                    for ty in &ehlib.types {
                        self.new_type(ty.clone());
                    }
                    for (k, v) in &ehlib.symbols {
                        self.bindings.get_mut(&self.ptr()).unwrap().insert(
                            k.iter().map(String::as_str).map(From::from).collect(),
                            (Boxed::new(v.clone()), Vis::Pub),
                        );
                    }
                }
            }
            mods.modules.insert(name, module);
            Ok(())
        } else {
            Err(errors)
        }
    }

    pub fn import(&mut self, module: &[RcStr], other: &Self) {
        for (_, bindings) in &other.bindings {
            for (binding, (ty, vis)) in bindings {
                if *vis == Vis::Pub {
                    let binding = module.iter().chain(binding).cloned().collect();
                    self.bindings
                        .entry(self.main())
                        .or_default()
                        .insert(binding, (ty.clone(), Vis::Priv));
                }
            }
        }
        for (mac, cons) in &other.macros {
            let mac = module.iter().chain(mac).cloned().collect();
            self.macros.insert(mac, cons.clone());
        }
        self.constructors.extend(other.constructors.iter().map(|(k, v)| (k.clone(), v.clone())));
        self.types
            .extend(other.types.iter().map(|(k, v)| (k.clone(), v.clone())));
        self.variants
            .extend(other.variants.iter().map(|(k, v)| (k.clone(), v.clone())));
        self.type_id = self.type_id.max(other.type_id);
    }

    pub fn types(&self) -> &HashMap<Type, u32> {
        &self.types
    }

    pub fn variants(&self) -> &HashMap<Type, (u32, u32)> {
        &self.variants
    }

    pub fn type_id(&self, ty: &Type) -> Option<u32> {
        self.types.get(ty).cloned()
    }

    pub fn new_type(&mut self, ty: Type) -> u32 {
        let type_id = self.type_id;
        self.type_id += 1;
        self.types.insert(ty, type_id);
        type_id
    }

    pub fn new_variant(&mut self, ty: Type, type_id: u32, variant_id: u32) -> u32 {
        self.variants.insert(ty, (type_id, variant_id));
        type_id
    }

    fn main(&self) -> RcStr {
        self.ptr.first().unwrap().clone()
    }

    fn ptr(&self) -> RcStr {
        self.ptr.last().unwrap().clone()
    }

    pub fn get(&self, path: &[RcStr]) -> Option<Boxed<Type>> {
        if let Some(ty) = self.macros.get(path).cloned() {
            return Some(ty);
        }
        for ptr in self.ptr.iter().rev() {
            if let Some(bindings) = self.bindings.get(ptr) {
                if let Some(ty) = bindings.get(path).map(|(ty, _)| ty.clone()) {
                    return Some(ty);
                }
            }
        }
        None
    }

    pub fn insert(&mut self, path: Vec<RcStr>, vis: Vis, ty: Boxed<Type>) {
        self.bindings.entry(self.ptr()).or_default().insert(path, (ty, vis));
    }

    pub fn insert_macro(&mut self, path: Vec<RcStr>, ty: Boxed<Type>) {
        self.macros.insert(path, ty);
    }

    pub fn get_cons(&self, path: &[RcStr]) -> Option<Boxed<Type>> {
        self.constructors.get(path).cloned()
    }

    pub fn insert_cons(&mut self, path: Vec<RcStr>, ty: Boxed<Type>) {
        self.constructors.insert(path, ty);
    }

    pub fn enter(&mut self, name: RcStr) {
        self.ptr.push(name);
    }

    pub fn leave(&mut self) {
        self.ptr.pop();
    }
}

bitflags! {
    #[derive(Serialize, Deserialize)]
    pub struct TypeFlags: u32 {
        const NONE = 0;
        const VARIADIC = EHTYPE_VARIADIC;
        const UNION = EHTYPE_UNION;
        const UNIONALL = EHTYPE_UNIONALL;
        const CLOSURE = EHTYPE_CLOSURE;
        const DYNAMIC = EHTYPE_DYNAMIC;
        const MUTABLE = EHTYPE_MUTABLE;
        const MACRO = EHTYPE_MACRO;
    }
}

#[derive(Debug, Clone, PartialEq, Eq, Hash, Serialize, Deserialize)]
pub struct TypeAtom {
    pub ty: RcStr,
}

impl Display for TypeAtom {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{}", self.ty)
    }
}

impl From<RcStr> for TypeAtom {
    fn from(string: RcStr) -> Self {
        TypeAtom { ty: string }
    }
}

impl<'a> From<&'a str> for TypeAtom {
    fn from(string: &'a str) -> Self {
        TypeAtom { ty: From::from(string) }
    }
}

impl<'a> From<&'a ast::Type> for TypeAtom {
    fn from(ty: &'a ast::Type) -> Self {
        TypeAtom { ty: ty.atom.clone() }
    }
}

#[derive(Debug, Clone, PartialEq, Eq, Hash, Serialize, Deserialize)]
pub enum TypeExpr {
    Atom(TypeAtom),
    Type(Type),
}

impl Display for TypeExpr {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            TypeExpr::Atom(atom) => Display::fmt(atom, f),
            TypeExpr::Type(ty) => Display::fmt(ty, f),
        }
    }
}

impl<'a> From<&'a ast::TypeWord> for TypeExpr {
    fn from(expr: &'a ast::TypeWord) -> Self {
        match &expr.kind {
            ast::TypeWordKind::Type(atom) => TypeExpr::Atom(TypeAtom::from(atom)),
            ast::TypeWordKind::Parenth(expr) => TypeExpr::Type(Type::from(expr.as_ref())),
        }
    }
}

impl From<TypeAtom> for TypeExpr {
    fn from(atom: TypeAtom) -> Self {
        TypeExpr::Atom(atom)
    }
}

impl From<Type> for TypeExpr {
    fn from(ty: Type) -> Self {
        if ty.flags.is_empty() && ty.rest.is_empty() {
            TypeExpr::from(ty.first)
        } else {
            TypeExpr::Type(ty)
        }
    }
}

#[derive(Debug, Clone, PartialEq, Eq, Hash, Serialize, Deserialize)]
pub struct Type {
    pub flags: TypeFlags,
    pub first: TypeAtom,
    pub rest: Vec<TypeExpr>,
}

impl Type {
    pub fn with_flags(self, flags: TypeFlags) -> Self {
        Type { flags, ..self }
    }

    pub fn is_func(&self) -> bool {
        self.flags.contains(TypeFlags::CLOSURE)
    }

    pub fn is_dynamic(&self) -> bool {
        self.flags.contains(TypeFlags::CLOSURE | TypeFlags::DYNAMIC)
    }

    pub fn is_mutable(&self) -> bool {
        self.flags
            .contains(TypeFlags::CLOSURE | TypeFlags::DYNAMIC | TypeFlags::MUTABLE)
    }

    pub fn is_macro(&self) -> bool {
        self.flags.contains(TypeFlags::CLOSURE | TypeFlags::MACRO)
    }

    pub fn returns(&self) -> Option<&TypeExpr> {
        if self.is_func() {
            self.rest.first()
        } else {
            None
        }
    }
}

impl Display for Type {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{}", self.first)?;
        for ty in &self.rest {
            write!(f, " {}", ty)?;
        }
        Ok(())
    }
}

impl From<TypeExpr> for Type {
    fn from(ty: TypeExpr) -> Self {
        match ty {
            TypeExpr::Atom(atom) => Type::from(atom.ty),
            TypeExpr::Type(ty) => ty,
        }
    }
}

impl From<RcStr> for Type {
    fn from(string: RcStr) -> Self {
        let flags = if string.as_str() == "Any" {
            TypeFlags::UNIONALL
        } else if string.as_str() == "Union" {
            TypeFlags::UNION
        } else if string.as_str() == "Closure" {
            TypeFlags::CLOSURE
        } else {
            TypeFlags::NONE
        };
        Type {
            flags,
            first: TypeAtom::from(string),
            rest: vec![],
        }
    }
}

impl<'a> From<&'a str> for Type {
    fn from(string: &'a str) -> Self {
        Self::from(RcStr::from(string))
    }
}

impl<'a> From<&'a ast::TypeExpr> for Type {
    fn from(expr: &'a ast::TypeExpr) -> Self {
        let flags = if expr.first.atom.as_str() == "Any" {
            TypeFlags::UNIONALL
        } else if expr.first.atom.as_str() == "Union" {
            TypeFlags::UNION
        } else if expr.first.atom.as_str() == "Closure" {
            TypeFlags::CLOSURE
        } else {
            TypeFlags::NONE
        };
        Type {
            flags,
            first: TypeAtom::from(&expr.first),
            rest: expr.rest.iter().map(|word| TypeExpr::from(word)).collect(),
        }
    }
}

pub trait TypeOf<Seal>: AsMeta {
    fn evaluate(&mut self, ctx: &mut TypeContext) -> Result<(), Vec<Error>>;

    fn type_of(&self) -> &Boxed<Type> {
        self.as_meta()
            .ty
            .as_ref()
            .unwrap_or_else(|| {
                panic!("{}", Error::new(ErrorKind::TypeExpected).with_span(self.span()))
            })
    }

    fn eval_type(&self) -> &Boxed<Type> {
        self.as_meta()
            .eval
            .as_ref()
            .unwrap_or_else(|| {
                panic!("{}", Error::new(ErrorKind::TypeExpected).with_span(self.span()))
            })
    }

    fn match_type(&self) -> &Boxed<Type> {
        self.as_meta()
            .matches
            .as_ref()
            .unwrap_or_else(|| {
                panic!("{}", Error::new(ErrorKind::TypeExpected).with_span(self.span()))
            })
    }

    fn ret_type(&self) -> Option<Boxed<Type>> {
        self.eval_type().returns().cloned().map(From::from).map(Boxed::new)
    }
}

pub trait Quote<Seal>: TypeOf<Seal> {
    fn evaluate_quote(&mut self, ctx: &mut TypeContext) -> Result<(), Vec<Error>>;
}

pub trait Quasi<Seal>: TypeOf<Seal> {
    fn evaluate_quasi(&mut self, ctx: &mut TypeContext) -> Result<(), Vec<Error>>;
}

pub trait Param<Seal>: TypeOf<Seal> {
    fn evaluate_param(&mut self, ctx: &mut TypeContext) -> Result<(), Vec<Error>>;
}

impl<T: BlockLike> TypeOf<ast::BlockSeal> for T {
    fn evaluate(&mut self, ctx: &mut TypeContext) -> Result<(), Vec<Error>> {
        let mut errors = vec![];
        for stmt in self.stmts_mut() {
            stmt.evaluate(ctx).unwrap_or_else(|err| errors.extend(err));
        }
        let (ty, eval, matches) = if let Some(expr) = self.expr_mut() {
            expr.evaluate(ctx).unwrap_or_else(|err| errors.extend(err));
            if errors.is_empty() {
                (
                    Some(expr.type_of().clone()),
                    Some(expr.eval_type().clone()),
                    Some(expr.match_type().clone()),
                )
            } else {
                return Err(errors);
            }
        } else {
            if errors.is_empty() {
                (
                    Some(Boxed::new(Type::from("Nil"))),
                    Some(Boxed::new(Type::from("Nil"))),
                    Some(Boxed::new(Type::from("Nil"))),
                )
            } else {
                return Err(errors);
            }
        };
        self.as_meta_mut().ty = ty;
        self.as_meta_mut().eval = eval;
        self.as_meta_mut().matches = matches;
        Ok(())
    }
}

impl<T: BlockLike> Quote<ast::BlockSeal> for T {
    fn evaluate_quote(&mut self, ctx: &mut TypeContext) -> Result<(), Vec<Error>> {
        let mut errors = vec![];
        for stmt in self.stmts_mut() {
            stmt.evaluate_quote(ctx).unwrap_or_else(|err| errors.extend(err));
        }
        let (ty, eval, matches) = if let Some(expr) = self.expr_mut() {
            expr.evaluate_quote(ctx).unwrap_or_else(|err| errors.extend(err));
            if errors.is_empty() {
                (
                    Some(expr.type_of().clone()),
                    Some(expr.eval_type().clone()),
                    Some(expr.match_type().clone()),
                )
            } else {
                return Err(errors);
            }
        } else {
            if errors.is_empty() {
                (
                    Some(Boxed::new(Type::from("Nil"))),
                    Some(Boxed::new(Type::from("Nil"))),
                    Some(Boxed::new(Type::from("Nil"))),
                )
            } else {
                return Err(errors);
            }
        };
        self.as_meta_mut().ty = ty;
        self.as_meta_mut().eval = eval;
        self.as_meta_mut().matches = matches;
        Ok(())
    }
}

impl<T: BlockLike> Quasi<ast::BlockSeal> for T {
    fn evaluate_quasi(&mut self, ctx: &mut TypeContext) -> Result<(), Vec<Error>> {
        let mut errors = vec![];
        for stmt in self.stmts_mut() {
            stmt.evaluate_quasi(ctx).unwrap_or_else(|err| errors.extend(err));
        }
        let (ty, eval, matches) = if let Some(expr) = self.expr_mut() {
            expr.evaluate_quasi(ctx).unwrap_or_else(|err| errors.extend(err));
            if errors.is_empty() {
                (
                    Some(expr.type_of().clone()),
                    Some(expr.eval_type().clone()),
                    Some(expr.match_type().clone()),
                )
            } else {
                return Err(errors);
            }
        } else {
            if errors.is_empty() {
                (
                    Some(Boxed::new(Type::from("Nil"))),
                    Some(Boxed::new(Type::from("Nil"))),
                    Some(Boxed::new(Type::from("Nil"))),
                )
            } else {
                return Err(errors);
            }
        };
        self.as_meta_mut().ty = ty;
        self.as_meta_mut().eval = eval;
        self.as_meta_mut().matches = matches;
        Ok(())
    }
}

impl<T: StmtLike> TypeOf<ast::StmtSeal> for T {
    fn evaluate(&mut self, ctx: &mut TypeContext) -> Result<(), Vec<Error>> {
        self.expr_mut().evaluate(ctx)?;
        self.as_meta_mut().ty = Some(Boxed::new(Type::from("Nil")));
        self.as_meta_mut().eval = Some(Boxed::new(Type::from("Nil")));
        self.as_meta_mut().matches = Some(Boxed::new(Type::from("Nil")));
        Ok(())
    }
}

impl<T: StmtLike> Quote<ast::StmtSeal> for T {
    fn evaluate_quote(&mut self, ctx: &mut TypeContext) -> Result<(), Vec<Error>> {
        self.expr_mut().evaluate_quote(ctx)?;
        self.as_meta_mut().ty = Some(Boxed::new(Type::from("Nil")));
        self.as_meta_mut().eval = Some(Boxed::new(Type::from("Nil")));
        self.as_meta_mut().matches = Some(Boxed::new(Type::from("Nil")));
        Ok(())
    }
}

impl<T: StmtLike> Quasi<ast::StmtSeal> for T {
    fn evaluate_quasi(&mut self, ctx: &mut TypeContext) -> Result<(), Vec<Error>> {
        self.expr_mut().evaluate_quasi(ctx)?;
        self.as_meta_mut().ty = Some(Boxed::new(Type::from("Nil")));
        self.as_meta_mut().eval = Some(Boxed::new(Type::from("Nil")));
        self.as_meta_mut().matches = Some(Boxed::new(Type::from("Nil")));
        Ok(())
    }
}

fn evaluate_from<T: ExprLike>(
    expr: &T,
    ctx: &mut TypeContext,
    module: Vec<RcStr>,
    imports: Vec<RcStr>,
) -> Result<(), Vec<Error>> {
    for var in imports {
        let path = module.iter().chain(iter::once(&var)).cloned().collect::<Vec<_>>();
        let ty = ctx
            .get(&path)
            .ok_or_else(|| vec![Error::new(ErrorKind::TypeExpected).with_span(expr.span())])?;
        ctx.insert(vec![var], Vis::Priv, ty);
    }
    Ok(())
}

impl<T: ExprLike> TypeOf<ast::ExprSeal> for T {
    fn evaluate(&mut self, ctx: &mut TypeContext) -> Result<(), Vec<Error>> {
        match &self.first().kind {
            ast::WordKind::Atom(atom) if atom.atom.as_str() == "let" => {
                self.first_mut().as_meta_mut().ty = Some(ctx.get(&[From::from("let")]).unwrap());
                self.first_mut().as_meta_mut().eval = self.first().as_meta().ty.clone();
                self.first_mut().as_meta_mut().matches = self.first().as_meta().ty.clone();
                self.as_meta_mut().ty = Some(Boxed::new(Type::from("Nil")));
                self.as_meta_mut().eval = self.as_meta().ty.clone();
                self.as_meta_mut().matches = self.as_meta().ty.clone();
                let binding = self.rest().get(0).ok_or_else(|| {
                    vec![Error::new(ErrorKind::LetSyntax)
                        .with_span(self.span())
                        .describe("missing binding")]
                })?;
                match &binding.kind {
                    ast::WordKind::Atom(atom) if atom.atom.as_str() == "fun" => evaluate_let_fun(self, ctx),
                    ast::WordKind::Atom(atom) if atom.atom.as_str() == "rec" => evaluate_let_rec(self, ctx),
                    ast::WordKind::Atom(..) => evaluate_let_bind(self, ctx),
                    _ => {
                        Err(vec![Error::new(ErrorKind::LetSyntax)
                            .with_span(&binding.span())
                            .describe("binding must be an atom")])
                    }
                }
            }
            ast::WordKind::Atom(atom) if atom.atom.as_str() == "def" => {
                self.first_mut().as_meta_mut().ty = Some(ctx.get(&[From::from("def")]).unwrap());
                self.first_mut().as_meta_mut().eval = self.first().as_meta().ty.clone();
                self.first_mut().as_meta_mut().matches = self.first().as_meta().ty.clone();
                self.as_meta_mut().ty = Some(Boxed::new(Type::from("Nil")));
                self.as_meta_mut().eval = self.as_meta().ty.clone();
                self.as_meta_mut().matches = self.as_meta().ty.clone();
                let binding = self.rest().get(0).ok_or_else(|| {
                    vec![Error::new(ErrorKind::LetSyntax)
                        .with_span(self.span())
                        .describe("missing binding")]
                })?;
                match &binding.kind {
                    ast::WordKind::Atom(atom) if atom.atom.as_str() == "fun" => evaluate_def_fun(self, ctx),
                    ast::WordKind::Atom(atom) if atom.atom.as_str() == "rec" => evaluate_def_rec(self, ctx),
                    ast::WordKind::Atom(..) => evaluate_def_bind(self, ctx),
                    _ => {
                        Err(vec![Error::new(ErrorKind::LetSyntax)
                            .with_span(&binding.span())
                            .describe("binding must be an atom")])
                    }
                }
            }
            ast::WordKind::Atom(atom) if atom.atom.as_str() == "use" => {
                self.first_mut().as_meta_mut().ty = Some(ctx.get(&[From::from("use")]).unwrap());
                self.first_mut().as_meta_mut().eval = self.first().as_meta().ty.clone();
                self.first_mut().as_meta_mut().matches = self.first().as_meta().ty.clone();
                self.as_meta_mut().ty = Some(Boxed::new(Type::from("Nil")));
                self.as_meta_mut().eval = self.as_meta().ty.clone();
                self.as_meta_mut().matches = self.as_meta().ty.clone();
                Ok(())
            }
            ast::WordKind::Atom(atom) if atom.atom.as_str() == "from" => {
                self.first_mut().as_meta_mut().ty = Some(ctx.get(&[From::from("from")]).unwrap());
                self.first_mut().as_meta_mut().eval = self.first().as_meta().ty.clone();
                self.first_mut().as_meta_mut().matches = self.first().as_meta().ty.clone();
                self.as_meta_mut().ty = Some(Boxed::new(Type::from("Nil")));
                self.as_meta_mut().eval = self.as_meta().ty.clone();
                self.as_meta_mut().matches = self.as_meta().ty.clone();
                let module = self.rest().get(0).ok_or_else(|| {
                    vec![Error::new(ErrorKind::FromSyntax)
                        .describe("missing module")
                        .with_span(self.span())]
                })?;
                let module = match &module.kind {
                    ast::WordKind::Atom(atom) => vec![atom.atom.clone()],
                    ast::WordKind::Path(path) => path.path.clone(),
                    _ => {
                        return Err(vec![Error::new(ErrorKind::FromSyntax)
                            .describe("imports must be atoms")
                            .with_span(module.span())])
                    }
                };

                let vars = &self.rest()[2..];
                let mut imports = vec![];
                for var in vars {
                    match &var.kind {
                        ast::WordKind::Atom(atom) => imports.push(atom.atom.clone()),
                        _ => {
                            return Err(vec![Error::new(ErrorKind::FromSyntax)
                                .describe("imports must be atoms")
                                .with_span(var.span())])
                        }
                    }
                }
                evaluate_from(self, ctx, module, imports)
            }
            ast::WordKind::Atom(atom) if atom.atom.as_str() == "type" => {
                self.first_mut().as_meta_mut().ty = Some(ctx.get(&[From::from("type")]).unwrap());
                self.first_mut().as_meta_mut().eval = self.first().as_meta().ty.clone();
                self.first_mut().as_meta_mut().matches = self.first().as_meta().ty.clone();
                self.as_meta_mut().ty = Some(Boxed::new(Type::from("Nil")));
                self.as_meta_mut().eval = self.as_meta().ty.clone();
                self.as_meta_mut().matches = self.as_meta().ty.clone();
                let word = &self.rest().get(0).ok_or_else(|| {
                    vec![Error::new(ErrorKind::TypeSyntax)
                        .describe("missing type name")
                        .with_span(self.span())]
                })?;

                let (ty, name) = match &word.kind {
                    ast::WordKind::TypeAtom(ty) => {
                        match &ty.kind {
                            ast::TypeWordKind::Type(name) => (Type::from(TypeExpr::from(ty)), name.atom.clone()),
                            _ => unimplemented!(),
                        }
                    }
                    _ => {
                        return Err(vec![Error::new(ErrorKind::TypeSyntax)
                            .describe("type name must be a type atom")
                            .with_span(word.span())])
                    }
                };

                let type_id = ctx.new_type(ty);

                if self.rest().len() == 1 {
                    return Ok(());
                }

                let is = self.rest().get(1).unwrap();
                match &is.kind {
                    ast::WordKind::Atom(atom) if atom.atom.as_str() == "is:" => {}
                    _ => {
                        return Err(vec![Error::new(ErrorKind::TypeSyntax)
                            .describe("missing `is:`")
                            .with_span(is.span())])
                    }
                }

                let span = self.span().clone();
                let block = self.rest_mut().get_mut(2).ok_or_else(|| {
                    vec![Error::new(ErrorKind::TypeSyntax)
                        .describe("missing type definition")
                        .with_span(&span)]
                })?;
                let block = match &mut block.kind {
                    ast::WordKind::Compound(block) => block,
                    _ => {
                        return Err(vec![Error::new(ErrorKind::TypeSyntax)
                            .describe("missing type definition")
                            .with_span(block.span())])
                    }
                };

                evaluate_type_def(&mut *block.borrow_mut(), &name, type_id, ctx)
            }
            ast::WordKind::Atom(atom) if atom.atom.as_str() == "macro" => {
                self.first_mut().as_meta_mut().ty = Some(ctx.get(&[From::from("macro")]).unwrap());
                self.first_mut().as_meta_mut().eval = self.first().as_meta().ty.clone();
                self.first_mut().as_meta_mut().matches = self.first().as_meta().ty.clone();
                self.as_meta_mut().ty = Some(Boxed::new(Type::from("Nil")));
                self.as_meta_mut().eval = self.as_meta().ty.clone();
                self.as_meta_mut().matches = self.as_meta().ty.clone();
                evaluate_macro(self, ctx)
            }
            _ => {
                let mut errors = vec![];
                self.first_mut().evaluate(ctx).unwrap_or_else(|err| errors.extend(err));
                if self.first().eval_type().is_macro() {
                    for word in self.rest_mut() {
                        word.evaluate_quasi(ctx).unwrap_or_else(|err| errors.extend(err));
                    }
                } else {
                    for word in self.rest_mut() {
                        word.evaluate(ctx).unwrap_or_else(|err| errors.extend(err));
                    }
                }
                if errors.is_empty() {
                    self.as_meta_mut().ty = Some(Boxed::new(Type::from("List")));
                    if self.rest().is_empty() {
                        self.as_meta_mut().eval = Some(self.first().eval_type().clone());
                    } else {
                        self.as_meta_mut().eval = Some(
                            self.first()
                                .ret_type()
                                .or_else(|| self.rest()[0].ret_type())
                                .ok_or_else(
                                    || vec![Error::new(ErrorKind::NoAdequateFunction).with_span(self.span())],
                                )?,
                        );
                    }
                    self.as_meta_mut().matches = self.as_meta().ty.clone();
                    Ok(())
                } else {
                    Err(errors)
                }
            }
        }
    }
}

impl<T: ExprLike> Quote<ast::ExprSeal> for T {
    fn evaluate_quote(&mut self, ctx: &mut TypeContext) -> Result<(), Vec<Error>> {
        let mut errors = vec![];
        self.first_mut()
            .evaluate_quote(ctx)
            .unwrap_or_else(|err| errors.extend(err));
        for word in self.rest_mut() {
            word.evaluate_quote(ctx).unwrap_or_else(|err| errors.extend(err));
        }
        if errors.is_empty() {
            self.as_meta_mut().ty = Some(Boxed::new(Type::from("List")));
            self.as_meta_mut().eval = self.as_meta().ty.clone();
            self.as_meta_mut().matches = self.as_meta().ty.clone();
            Ok(())
        } else {
            Err(errors)
        }
    }
}

impl<T: ExprLike> Quasi<ast::ExprSeal> for T {
    fn evaluate_quasi(&mut self, ctx: &mut TypeContext) -> Result<(), Vec<Error>> {
        let mut errors = vec![];
        self.first_mut()
            .evaluate_quasi(ctx)
            .unwrap_or_else(|err| errors.extend(err));
        for word in self.rest_mut() {
            word.evaluate_quasi(ctx).unwrap_or_else(|err| errors.extend(err));
        }
        if errors.is_empty() {
            self.as_meta_mut().ty = Some(Boxed::new(Type::from("List")));
            self.as_meta_mut().eval = self.as_meta().ty.clone();
            self.as_meta_mut().matches = self.as_meta().ty.clone();
            Ok(())
        } else {
            Err(errors)
        }
    }
}

fn evaluate_let_fun<T: ExprLike>(expr: &mut T, ctx: &mut TypeContext) -> Result<(), Vec<Error>> {
    let binding = expr.rest().get(1).ok_or_else(|| {
        vec![Error::new(ErrorKind::LetSyntax)
            .with_span(expr.span())
            .describe("missing binding")]
    })?;
    let binding = match &binding.kind {
        ast::WordKind::Atom(atom) => atom.atom.clone(),
        _ => {
            return Err(vec![Error::new(ErrorKind::LetSyntax)
                .with_span(binding.span())
                .describe("binding must be an atom")])
        }
    };

    ctx.enter(expr.to_ident());
    let mut params = vec![];
    let mut idx = 2;
    for (i, word) in expr.rest_mut().iter_mut().enumerate().skip(2) {
        idx = i;
        match &word.kind {
            ast::WordKind::Atom(atom) if atom.atom.as_str() == "=" => break,
            _ => params.push(word),
        }
    }

    let mut param_types = vec![];
    let mut errors = vec![];

    for param in &mut params {
        param.evaluate_param(ctx).unwrap_or_else(|err| errors.extend(err));
        param_types.push(From::from(param.match_type().as_ref().clone()));
    }
    expr.as_meta_mut().params = Some(Boxed::new(RefCell::new(
        params.into_iter().map(|word| word.clone()).collect(),
    )));

    if !errors.is_empty() {
        return Err(errors);
    }

    let word = expr.rest().get(idx).ok_or_else(|| {
        vec![Error::new(ErrorKind::LetSyntax)
            .with_span(expr.span())
            .describe("missing `=`")]
    })?;
    match &word.kind {
        ast::WordKind::Atom(atom) if atom.atom.as_str() == "=" => {}
        _ => {
            return Err(vec![Error::new(ErrorKind::LetSyntax)
                .with_span(word.span())
                .describe("missing `=`")])
        }
    };
    let first = expr.rest().get(idx + 1).ok_or_else(|| {
        vec![Error::new(ErrorKind::LetSyntax)
            .with_span(expr.span())
            .describe("missing expression")]
    })?;
    let first = first.clone();
    let rest = expr.rest()[idx + 2..].to_vec();
    let mut span = first.span().clone();
    if let Some(last) = rest.last() {
        span = span.join(last.span().clone());
    }
    let mut body = T::new(span, first, rest);

    body.evaluate(ctx)?;
    let ret = From::from(body.type_of().as_ref().clone());
    let ty = Boxed::new(Type {
        flags: TypeFlags::CLOSURE,
        first: TypeAtom::from("Closure"),
        rest: iter::once(ret).chain(param_types).collect(),
    });

    expr.as_meta_mut().def = Some(Boxed::new(RefCell::new(body.into())));
    expr.as_meta_mut().ty = Some(Boxed::new(Type::from("Nil")));
    expr.as_meta_mut().eval = Some(Boxed::new(Type::from("Nil")));
    expr.as_meta_mut().matches = Some(Boxed::new(Type::from("Nil")));
    ctx.leave();
    ctx.insert(vec![binding], Vis::Priv, ty.clone());

    Ok(())
}

fn evaluate_let_rec<T: ExprLike>(expr: &mut T, ctx: &mut TypeContext) -> Result<(), Vec<Error>> {
    let binding = expr.rest().get(1).ok_or_else(|| {
        vec![Error::new(ErrorKind::LetSyntax)
            .with_span(expr.span())
            .describe("missing binding")]
    })?;
    let binding = match &binding.kind {
        ast::WordKind::Atom(atom) => atom.atom.clone(),
        _ => {
            return Err(vec![Error::new(ErrorKind::LetSyntax)
                .with_span(binding.span())
                .describe("binding must be an atom")])
        }
    };

    ctx.enter(expr.to_ident());
    let mut params = vec![];
    let mut idx = 2;
    for (i, word) in expr.rest_mut().iter_mut().enumerate().skip(2) {
        idx = i;
        match &word.kind {
            ast::WordKind::Atom(atom) if atom.atom.as_str() == "=" => break,
            _ => params.push(word),
        }
    }

    let mut param_types = vec![];
    let mut errors = vec![];

    for param in &mut params {
        param.evaluate_param(ctx).unwrap_or_else(|err| errors.extend(err));
        param_types.push(From::from(param.match_type().as_ref().clone()));
    }
    expr.as_meta_mut().params = Some(Boxed::new(RefCell::new(
        params.into_iter().map(|word| word.clone()).collect(),
    )));

    if !errors.is_empty() {
        return Err(errors);
    }

    let word = expr.rest().get(idx).ok_or_else(|| {
        vec![Error::new(ErrorKind::LetSyntax)
            .with_span(expr.span())
            .describe("missing `=`")]
    })?;
    match &word.kind {
        ast::WordKind::Atom(atom) if atom.atom.as_str() == "=" => {}
        _ => {
            return Err(vec![Error::new(ErrorKind::LetSyntax)
                .with_span(word.span())
                .describe("missing `=`")])
        }
    };
    let first = expr.rest().get(idx + 1).ok_or_else(|| {
        vec![Error::new(ErrorKind::LetSyntax)
            .with_span(expr.span())
            .describe("missing expression")]
    })?;
    let first = first.clone();
    let rest = expr.rest()[idx + 2..].to_vec();
    let mut span = first.span().clone();
    if let Some(last) = rest.last() {
        span = span.join(last.span().clone());
    }
    let mut body = T::new(span, first, rest);

    let ret = TypeExpr::from(TypeAtom::from("Any"));
    let ty = Boxed::new(Type {
        flags: TypeFlags::CLOSURE,
        first: TypeAtom::from("Closure"),
        rest: iter::once(ret).chain(param_types.clone()).collect(),
    });
    ctx.insert(vec![binding.clone()], Vis::Priv, ty.clone());

    body.evaluate(ctx)?;

    let ret = From::from(body.type_of().as_ref().clone());
    let ty = Boxed::new(Type {
        flags: TypeFlags::CLOSURE,
        first: TypeAtom::from("Closure"),
        rest: iter::once(ret).chain(param_types).collect(),
    });
    ctx.insert(vec![binding.clone()], Vis::Priv, ty.clone());
    body.evaluate(ctx)?;
    expr.as_meta_mut().def = Some(Boxed::new(RefCell::new(body.into())));
    expr.as_meta_mut().ty = Some(Boxed::new(Type::from("Nil")));
    expr.as_meta_mut().eval = Some(Boxed::new(Type::from("Nil")));
    expr.as_meta_mut().matches = Some(Boxed::new(Type::from("Nil")));
    ctx.leave();
    ctx.insert(vec![binding], Vis::Priv, ty.clone());

    Ok(())
}

fn evaluate_let_bind<T: ExprLike>(expr: &mut T, ctx: &mut TypeContext) -> Result<(), Vec<Error>> {
    let binding = match &expr.rest().get(0).unwrap_or_else(|| unreachable!()).kind {
        ast::WordKind::Atom(atom) => atom.atom.clone(),
        _ => unreachable!(),
    };

    let word = expr.rest().get(1).ok_or_else(|| {
        vec![Error::new(ErrorKind::LetSyntax)
            .with_span(expr.span())
            .describe("missing `=`")]
    })?;
    match &word.kind {
        ast::WordKind::Atom(atom) if atom.atom.as_str() == "=" => {}
        _ => {
            return Err(vec![Error::new(ErrorKind::LetSyntax)
                .with_span(word.span())
                .describe("missing `=`")])
        }
    }
    let first = expr.rest().get(2).ok_or_else(|| {
        vec![Error::new(ErrorKind::LetSyntax)
            .with_span(expr.span())
            .describe("missing expression")]
    })?;
    let first = first.clone();
    let rest = expr.rest()[3..].to_vec();
    let mut span = first.span().clone();
    if let Some(last) = rest.last() {
        span = span.join(last.span().clone());
    }
    let mut body = T::new(span, first, rest);

    body.evaluate(ctx)?;
    let ty = body.eval_type();
    ctx.insert(vec![binding], Vis::Priv, ty.clone());

    expr.as_meta_mut().def = Some(Boxed::new(RefCell::new(body.into())));
    expr.as_meta_mut().ty = Some(Boxed::new(Type::from("Nil")));
    expr.as_meta_mut().eval = Some(Boxed::new(Type::from("Nil")));
    expr.as_meta_mut().matches = Some(Boxed::new(Type::from("Nil")));

    Ok(())
}

fn evaluate_def_fun<T: ExprLike>(expr: &mut T, ctx: &mut TypeContext) -> Result<(), Vec<Error>> {
    let binding = expr.rest().get(1).ok_or_else(|| {
        vec![Error::new(ErrorKind::DefSyntax)
            .with_span(expr.span())
            .describe("missing binding")]
    })?;
    let binding = match &binding.kind {
        ast::WordKind::Atom(atom) => atom.atom.clone(),
        _ => {
            return Err(vec![Error::new(ErrorKind::DefSyntax)
                .with_span(binding.span())
                .describe("binding must be an atom")])
        }
    };

    ctx.enter(expr.to_ident());
    let mut params = vec![];
    let mut idx = 2;
    for (i, word) in expr.rest_mut().iter_mut().enumerate().skip(2) {
        idx = i;
        match &word.kind {
            ast::WordKind::Atom(atom) if atom.atom.as_str() == "=" => break,
            _ => params.push(word),
        }
    }

    let mut param_types = vec![];
    let mut errors = vec![];

    for param in &mut params {
        param.evaluate_param(ctx).unwrap_or_else(|err| errors.extend(err));
        param_types.push(From::from(param.match_type().as_ref().clone()));
    }
    expr.as_meta_mut().params = Some(Boxed::new(RefCell::new(
        params.into_iter().map(|word| word.clone()).collect(),
    )));

    if !errors.is_empty() {
        return Err(errors);
    }

    let word = expr.rest().get(idx).ok_or_else(|| {
        vec![Error::new(ErrorKind::DefSyntax)
            .with_span(expr.span())
            .describe("missing `=`")]
    })?;
    match &word.kind {
        ast::WordKind::Atom(atom) if atom.atom.as_str() == "=" => {}
        _ => {
            return Err(vec![Error::new(ErrorKind::DefSyntax)
                .with_span(word.span())
                .describe("missing `=`")])
        }
    };
    let first = expr.rest().get(idx + 1).ok_or_else(|| {
        vec![Error::new(ErrorKind::DefSyntax)
            .with_span(expr.span())
            .describe("missing expression")]
    })?;
    let first = first.clone();
    let rest = expr.rest()[idx + 2..].to_vec();
    let mut span = first.span().clone();
    if let Some(last) = rest.last() {
        span = span.join(last.span().clone());
    }
    let mut body = T::new(span, first, rest);

    body.evaluate(ctx)?;
    let ret = From::from(body.type_of().as_ref().clone());
    let ty = Boxed::new(Type {
        flags: TypeFlags::CLOSURE,
        first: TypeAtom::from("Closure"),
        rest: iter::once(ret).chain(param_types).collect(),
    });

    expr.as_meta_mut().def = Some(Boxed::new(RefCell::new(body.into())));
    expr.as_meta_mut().ty = Some(Boxed::new(Type::from("Nil")));
    expr.as_meta_mut().eval = Some(Boxed::new(Type::from("Nil")));
    expr.as_meta_mut().matches = Some(Boxed::new(Type::from("Nil")));
    ctx.leave();
    ctx.insert(vec![binding], Vis::Pub, ty.clone());

    Ok(())
}

fn evaluate_def_rec<T: ExprLike>(expr: &mut T, ctx: &mut TypeContext) -> Result<(), Vec<Error>> {
    let binding = expr.rest().get(1).ok_or_else(|| {
        vec![Error::new(ErrorKind::DefSyntax)
            .with_span(expr.span())
            .describe("missing binding")]
    })?;
    let binding = match &binding.kind {
        ast::WordKind::Atom(atom) => atom.atom.clone(),
        _ => {
            return Err(vec![Error::new(ErrorKind::DefSyntax)
                .with_span(binding.span())
                .describe("binding must be an atom")])
        }
    };

    ctx.enter(expr.to_ident());
    let mut params = vec![];
    let mut idx = 2;
    for (i, word) in expr.rest_mut().iter_mut().enumerate().skip(2) {
        idx = i;
        match &word.kind {
            ast::WordKind::Atom(atom) if atom.atom.as_str() == "=" => break,
            _ => params.push(word),
        }
    }

    let mut param_types = vec![];
    let mut errors = vec![];

    for param in &mut params {
        param.evaluate_param(ctx).unwrap_or_else(|err| errors.extend(err));
        param_types.push(From::from(param.match_type().as_ref().clone()));
    }
    expr.as_meta_mut().params = Some(Boxed::new(RefCell::new(
        params.into_iter().map(|word| word.clone()).collect(),
    )));

    if !errors.is_empty() {
        return Err(errors);
    }

    let word = expr.rest().get(idx).ok_or_else(|| {
        vec![Error::new(ErrorKind::DefSyntax)
            .with_span(expr.span())
            .describe("missing `=`")]
    })?;
    match &word.kind {
        ast::WordKind::Atom(atom) if atom.atom.as_str() == "=" => {}
        _ => {
            return Err(vec![Error::new(ErrorKind::DefSyntax)
                .with_span(word.span())
                .describe("missing `=`")])
        }
    };
    let first = expr.rest().get(idx + 1).ok_or_else(|| {
        vec![Error::new(ErrorKind::DefSyntax)
            .with_span(expr.span())
            .describe("missing expression")]
    })?;
    let first = first.clone();
    let rest = expr.rest()[idx + 2..].to_vec();
    let mut span = first.span().clone();
    if let Some(last) = rest.last() {
        span = span.join(last.span().clone());
    }
    let mut body = T::new(span, first, rest);

    let ret = TypeExpr::from(TypeAtom::from("Any"));
    let ty = Boxed::new(Type {
        flags: TypeFlags::CLOSURE,
        first: TypeAtom::from("Closure"),
        rest: iter::once(ret).chain(param_types.clone()).collect(),
    });
    ctx.insert(vec![binding.clone()], Vis::Pub, ty.clone());

    body.evaluate(ctx)?;

    let ret = From::from(body.type_of().as_ref().clone());
    let ty = Boxed::new(Type {
        flags: TypeFlags::CLOSURE,
        first: TypeAtom::from("Closure"),
        rest: iter::once(ret).chain(param_types).collect(),
    });
    ctx.insert(vec![binding.clone()], Vis::Pub, ty.clone());
    body.evaluate(ctx)?;
    expr.as_meta_mut().def = Some(Boxed::new(RefCell::new(body.into())));
    expr.as_meta_mut().ty = Some(Boxed::new(Type::from("Nil")));
    expr.as_meta_mut().eval = Some(Boxed::new(Type::from("Nil")));
    expr.as_meta_mut().matches = Some(Boxed::new(Type::from("Nil")));
    ctx.leave();
    ctx.insert(vec![binding], Vis::Pub, ty.clone());

    Ok(())
}

fn evaluate_def_bind<T: ExprLike>(expr: &mut T, ctx: &mut TypeContext) -> Result<(), Vec<Error>> {
    let binding = match &expr.rest().get(0).unwrap_or_else(|| unreachable!()).kind {
        ast::WordKind::Atom(atom) => atom.atom.clone(),
        _ => unreachable!(),
    };

    let word = expr.rest().get(1).ok_or_else(|| {
        vec![Error::new(ErrorKind::DefSyntax)
            .with_span(expr.span())
            .describe("missing `=`")]
    })?;
    match &word.kind {
        ast::WordKind::Atom(atom) if atom.atom.as_str() == "=" => {}
        _ => {
            return Err(vec![Error::new(ErrorKind::DefSyntax)
                .with_span(word.span())
                .describe("missing `=`")])
        }
    }
    let first = expr.rest().get(2).ok_or_else(|| {
        vec![Error::new(ErrorKind::DefSyntax)
            .with_span(expr.span())
            .describe("missing expression")]
    })?;
    let first = first.clone();
    let rest = expr.rest()[3..].to_vec();
    let mut span = first.span().clone();
    if let Some(last) = rest.last() {
        span = span.join(last.span().clone());
    }
    let mut body = T::new(span, first, rest);

    body.evaluate(ctx)?;
    let ty = body.eval_type();
    ctx.insert(vec![binding], Vis::Pub, ty.clone());

    expr.as_meta_mut().def = Some(Boxed::new(RefCell::new(body.into())));
    expr.as_meta_mut().ty = Some(Boxed::new(Type::from("Nil")));
    expr.as_meta_mut().eval = Some(Boxed::new(Type::from("Nil")));
    expr.as_meta_mut().matches = Some(Boxed::new(Type::from("Nil")));

    Ok(())
}

fn evaluate_type_def<T: BlockLike>(
    block: &mut T,
    name: &RcStr,
    type_id: u32,
    ctx: &mut TypeContext,
) -> Result<(), Vec<Error>> {
    let mut variant_id = 0;
    for stmt in block.stmts_mut() {
        evaluate_type_variant(stmt.expr_mut(), name, type_id, variant_id, ctx)?;
        variant_id += 1;
    }
    if let Some(expr) = block.expr_mut() {
        evaluate_type_variant(expr, name, type_id, variant_id, ctx)?;
    }
    Ok(())
}

fn evaluate_type_variant<T: ExprLike>(
    expr: &mut T,
    _name: &RcStr,
    type_id: u32,
    variant_id: u32,
    ctx: &mut TypeContext,
) -> Result<(), Vec<Error>> {
    match &mut expr.first_mut().kind {
        ast::WordKind::Destructure(variant, words) => {
            let ty = Type::from(&*variant);
            let mut errors = vec![];

            for word in words {
                word.evaluate_param(ctx).unwrap_or_else(|err| errors.extend(err));
            }

            ctx.new_variant(ty.clone(), type_id, variant_id);
            let ret = ty;
            let ty = Boxed::new(Type {
                flags: TypeFlags::CLOSURE,
                first: TypeAtom::from("Closure"),
                rest: vec![From::from(ret)],
            });
            ctx.insert_cons(vec![variant.first.atom.clone()], ty);

            if errors.is_empty() {
                Ok(())
            } else {
                Err(errors)
            }
        }
        _ => {
            return Err(vec![Error::new(ErrorKind::TypeSyntax)
                .describe("variants must be declared using the destructure syntax")
                .with_span(expr.span())])
        }
    }
}

fn evaluate_macro<T: ExprLike>(expr: &mut T, ctx: &mut TypeContext) -> Result<(), Vec<Error>> {
    let binding = match &expr
        .rest()
        .get(0)
        .ok_or_else(|| {
            vec![Error::new(ErrorKind::MacroSyntax)
                .describe("missing binding")
                .with_span(expr.span())]
        })?
        .kind
    {
        ast::WordKind::Atom(atom) => atom.atom.clone(),
        _ => unreachable!(),
    };

    let word = expr.rest().get(1).ok_or_else(|| {
        vec![Error::new(ErrorKind::MacroSyntax)
            .with_span(expr.span())
            .describe("missing `=`")]
    })?;
    match &word.kind {
        ast::WordKind::Atom(atom) if atom.atom.as_str() == "=" => {}
        _ => {
            return Err(vec![Error::new(ErrorKind::MacroSyntax)
                .with_span(word.span())
                .describe("missing `=`")])
        }
    }
    let first = expr.rest().get(2).ok_or_else(|| {
        vec![Error::new(ErrorKind::MacroSyntax)
            .with_span(expr.span())
            .describe("missing expression")]
    })?;
    let first = first.clone();
    let span = first.span().clone();
    let mut body = ast::Expr::new(span, first, vec![]);

    body.evaluate(ctx)?;
    let ty = body.eval_type();
    ctx.insert_macro(vec![binding], ty.clone());

    expr.as_meta_mut().def = Some(Boxed::new(RefCell::new(body)));
    expr.as_meta_mut().ty = Some(Boxed::new(Type::from("Nil")));
    expr.as_meta_mut().eval = Some(Boxed::new(Type::from("Nil")));
    expr.as_meta_mut().matches = Some(Boxed::new(Type::from("Nil")));

    Ok(())
}

impl TypeOf<()> for ast::Word {
    fn evaluate(&mut self, ctx: &mut TypeContext) -> Result<(), Vec<Error>> {
        let (ty, eval, matches) = match &mut self.kind {
            ast::WordKind::Atom(atom) => {
                atom.evaluate(ctx)?;
                (
                    Some(atom.type_of().clone()),
                    Some(atom.eval_type().clone()),
                    Some(atom.match_type().clone()),
                )
            }
            ast::WordKind::Path(path) => {
                path.evaluate(ctx)?;
                (
                    Some(path.type_of().clone()),
                    Some(path.eval_type().clone()),
                    Some(path.match_type().clone()),
                )
            }
            ast::WordKind::Integer(..) => {
                (
                    Some(Boxed::new(Type::from("Int"))),
                    Some(Boxed::new(Type::from("Int"))),
                    Some(Boxed::new(Type::from("Int"))),
                )
            }
            ast::WordKind::Real(..) => {
                (
                    Some(Boxed::new(Type::from("Real"))),
                    Some(Boxed::new(Type::from("Real"))),
                    Some(Boxed::new(Type::from("Real"))),
                )
            }
            ast::WordKind::Ratio(..) => {
                (
                    Some(Boxed::new(Type::from("Real"))),
                    Some(Boxed::new(Type::from("Real"))),
                    Some(Boxed::new(Type::from("Real"))),
                )
            }
            ast::WordKind::Char(..) => {
                (
                    Some(Boxed::new(Type::from("Char"))),
                    Some(Boxed::new(Type::from("Char"))),
                    Some(Boxed::new(Type::from("Char"))),
                )
            }
            ast::WordKind::Str(string) => {
                string.evaluate(ctx)?;
                (
                    Some(string.type_of().clone()),
                    Some(string.eval_type().clone()),
                    Some(string.match_type().clone()),
                )
            }
            ast::WordKind::TypeAtom(ty) => {
                ty.evaluate(ctx)?;
                (
                    Some(ty.type_of().clone()),
                    Some(ty.eval_type().clone()),
                    Some(ty.match_type().clone()),
                )
            }
            ast::WordKind::Quote(inner) => {
                inner.borrow_mut().evaluate_quote(ctx)?;
                (
                    Some(Boxed::new(Type::from("Quote"))),
                    Some(inner.borrow().type_of().clone()),
                    Some(inner.borrow().type_of().clone()),
                )
            }
            ast::WordKind::Quasiquote(inner) => {
                inner.borrow_mut().evaluate_quasi(ctx)?;
                (
                    Some(Boxed::new(Type::from("Quote"))),
                    Some(inner.borrow().type_of().clone()),
                    Some(inner.borrow().type_of().clone()),
                )
            }
            ast::WordKind::Interpolate(inner) => {
                inner.borrow_mut().evaluate(ctx)?;
                (
                    Some(inner.borrow().eval_type().clone()),
                    Some(inner.borrow().eval_type().clone()),
                    Some(inner.borrow().eval_type().clone()),
                )
            }
            ast::WordKind::Empty => {
                (
                    Some(Boxed::new(Type::from("List"))),
                    Some(Boxed::new(Type::from("Nil"))),
                    Some(Boxed::new(Type::from("Nil"))),
                )
            }
            ast::WordKind::Parenth(inner) => {
                inner.borrow_mut().evaluate(ctx)?;
                (
                    Some(Boxed::new(Type::from("List"))),
                    Some(inner.borrow().eval_type().clone()),
                    Some(Boxed::new(Type::from("List"))),
                )
            }
            ast::WordKind::Bind(_, ty, pred, var) => {
                if let Some(pred) = pred {
                    pred.borrow_mut().evaluate(ctx)?;
                }
                let mut ty = ty
                    .as_ref()
                    .map(|ty| Type::from(ty))
                    .unwrap_or_else(|| Type::from("Any"));
                if *var {
                    ty = ty.with_flags(TypeFlags::VARIADIC);
                }
                let ty = (
                    Some(Boxed::new(Type::from("Bind"))),
                    Some(Boxed::new(Type::from("Bind"))),
                    Some(Boxed::new(ty)),
                );
                ty
            }
            ast::WordKind::Destructure(ty, words) => {
                for word in words {
                    word.evaluate(ctx)?;
                }
                (
                    Some(Boxed::new(Type::from("Bind"))),
                    Some(Boxed::new(Type::from("Bind"))),
                    Some(Boxed::new(Type::from(&*ty))),
                )
            }
            ast::WordKind::Closure(closure) => {
                for closure in closure.iter_mut() {
                    closure.evaluate(ctx)?;
                }
                (
                    Some(closure[0].type_of().clone()),
                    Some(closure[0].eval_type().clone()),
                    Some(closure[0].match_type().clone()),
                )
            }
            ast::WordKind::Compound(inner) => {
                inner.borrow_mut().evaluate(ctx)?;
                (
                    Some(inner.borrow().type_of().clone()),
                    Some(inner.borrow().eval_type().clone()),
                    Some(inner.borrow().match_type().clone()),
                )
            }
        };
        self.as_meta_mut().ty = ty;
        self.as_meta_mut().eval = eval;
        self.as_meta_mut().matches = matches;
        Ok(())
    }
}

impl Quote<()> for ast::Word {
    fn evaluate_quote(&mut self, ctx: &mut TypeContext) -> Result<(), Vec<Error>> {
        let (ty, eval, matches) = match &mut self.kind {
            ast::WordKind::Atom(atom) => {
                atom.evaluate_quote(ctx)?;
                (
                    Some(atom.type_of().clone()),
                    Some(atom.eval_type().clone()),
                    Some(atom.match_type().clone()),
                )
            }
            ast::WordKind::Path(path) => {
                path.evaluate_quote(ctx)?;
                (
                    Some(path.type_of().clone()),
                    Some(path.eval_type().clone()),
                    Some(path.match_type().clone()),
                )
            }
            ast::WordKind::Integer(..) => {
                (
                    Some(Boxed::new(Type::from("Int"))),
                    Some(Boxed::new(Type::from("Int"))),
                    Some(Boxed::new(Type::from("Int"))),
                )
            }
            ast::WordKind::Real(..) => {
                (
                    Some(Boxed::new(Type::from("Real"))),
                    Some(Boxed::new(Type::from("Real"))),
                    Some(Boxed::new(Type::from("Real"))),
                )
            }
            ast::WordKind::Ratio(..) => {
                (
                    Some(Boxed::new(Type::from("Real"))),
                    Some(Boxed::new(Type::from("Real"))),
                    Some(Boxed::new(Type::from("Real"))),
                )
            }
            ast::WordKind::Char(..) => {
                (
                    Some(Boxed::new(Type::from("Char"))),
                    Some(Boxed::new(Type::from("Char"))),
                    Some(Boxed::new(Type::from("Char"))),
                )
            }
            ast::WordKind::Str(string) => {
                string.evaluate_quote(ctx)?;
                (
                    Some(string.type_of().clone()),
                    Some(string.eval_type().clone()),
                    Some(string.match_type().clone()),
                )
            }
            ast::WordKind::TypeAtom(ty) => {
                ty.evaluate_quote(ctx)?;
                (
                    Some(Boxed::new(Type::from("Type"))),
                    Some(ty.eval_type().clone()),
                    Some(ty.match_type().clone()),
                )
            }
            ast::WordKind::Quote(inner) => {
                (
                    Some(Boxed::new(Type::from("Quote"))),
                    Some(inner.borrow().type_of().clone()),
                    Some(inner.borrow().type_of().clone()),
                )
            }
            ast::WordKind::Quasiquote(inner) => {
                inner.borrow_mut().evaluate_quote(ctx)?;
                (
                    Some(Boxed::new(Type::from("Quote"))),
                    Some(inner.borrow().type_of().clone()),
                    Some(inner.borrow().type_of().clone()),
                )
            }
            ast::WordKind::Interpolate(inner) => {
                inner.borrow_mut().evaluate_quote(ctx)?;
                (
                    Some(Boxed::new(Type::from("Interp"))),
                    Some(inner.borrow().type_of().clone()),
                    Some(inner.borrow().type_of().clone()),
                )
            }
            ast::WordKind::Empty => {
                (
                    Some(Boxed::new(Type::from("List"))),
                    Some(Boxed::new(Type::from("List"))),
                    Some(Boxed::new(Type::from("Nil"))),
                )
            }
            ast::WordKind::Parenth(inner) => {
                inner.borrow_mut().evaluate_quote(ctx)?;
                (
                    Some(Boxed::new(Type::from("List"))),
                    Some(Boxed::new(Type::from("List"))),
                    Some(Boxed::new(Type::from("List"))),
                )
            }
            ast::WordKind::Bind(_, ty, pred, var) => {
                if let Some(pred) = pred {
                    pred.borrow_mut().evaluate_quote(ctx)?;
                }
                let mut ty = ty
                    .as_ref()
                    .map(|ty| Type::from(ty))
                    .unwrap_or_else(|| Type::from("Any"));
                if *var {
                    ty = ty.with_flags(TypeFlags::VARIADIC);
                }
                let ty = (
                    Some(Boxed::new(Type::from("Bind"))),
                    Some(Boxed::new(Type::from("Bind"))),
                    Some(Boxed::new(ty)),
                );
                ty
            }
            ast::WordKind::Destructure(ty, words) => {
                for word in words {
                    word.evaluate_quote(ctx)?;
                }
                (
                    Some(Boxed::new(Type::from("Bind"))),
                    Some(Boxed::new(Type::from("Bind"))),
                    Some(Boxed::new(Type::from(&*ty))),
                )
            }
            ast::WordKind::Closure(closure) => {
                for closure in closure.iter_mut() {
                    closure.evaluate_quote(ctx)?;
                }
                (
                    Some(closure[0].type_of().clone()),
                    Some(closure[0].eval_type().clone()),
                    Some(closure[0].match_type().clone()),
                )
            }
            ast::WordKind::Compound(inner) => {
                inner.borrow_mut().evaluate_quote(ctx)?;
                (
                    Some(inner.borrow().type_of().clone()),
                    Some(inner.borrow().eval_type().clone()),
                    Some(inner.borrow().match_type().clone()),
                )
            }
        };
        self.as_meta_mut().ty = ty;
        self.as_meta_mut().eval = eval;
        self.as_meta_mut().matches = matches;
        Ok(())
    }
}

impl Quasi<()> for ast::Word {
    fn evaluate_quasi(&mut self, ctx: &mut TypeContext) -> Result<(), Vec<Error>> {
        let (ty, eval, matches) = match &mut self.kind {
            ast::WordKind::Atom(atom) => {
                atom.evaluate_quasi(ctx)?;
                (
                    Some(atom.type_of().clone()),
                    Some(atom.eval_type().clone()),
                    Some(atom.match_type().clone()),
                )
            }
            ast::WordKind::Path(path) => {
                path.evaluate_quasi(ctx)?;
                (
                    Some(path.type_of().clone()),
                    Some(path.eval_type().clone()),
                    Some(path.match_type().clone()),
                )
            }
            ast::WordKind::Integer(..) => {
                (
                    Some(Boxed::new(Type::from("Int"))),
                    Some(Boxed::new(Type::from("Int"))),
                    Some(Boxed::new(Type::from("Int"))),
                )
            }
            ast::WordKind::Real(..) => {
                (
                    Some(Boxed::new(Type::from("Real"))),
                    Some(Boxed::new(Type::from("Real"))),
                    Some(Boxed::new(Type::from("Real"))),
                )
            }
            ast::WordKind::Ratio(..) => {
                (
                    Some(Boxed::new(Type::from("Real"))),
                    Some(Boxed::new(Type::from("Real"))),
                    Some(Boxed::new(Type::from("Real"))),
                )
            }
            ast::WordKind::Char(..) => {
                (
                    Some(Boxed::new(Type::from("Char"))),
                    Some(Boxed::new(Type::from("Char"))),
                    Some(Boxed::new(Type::from("Char"))),
                )
            }
            ast::WordKind::Str(string) => {
                string.evaluate_quasi(ctx)?;
                (
                    Some(string.type_of().clone()),
                    Some(string.eval_type().clone()),
                    Some(string.match_type().clone()),
                )
            }
            ast::WordKind::TypeAtom(ty) => {
                ty.evaluate_quasi(ctx)?;
                (
                    Some(ty.type_of().clone()),
                    Some(ty.eval_type().clone()),
                    Some(ty.match_type().clone()),
                )
            }
            ast::WordKind::Quote(inner) => {
                inner.borrow_mut().evaluate_quote(ctx)?;
                (
                    Some(Boxed::new(Type::from("Quote"))),
                    Some(inner.borrow().type_of().clone()),
                    Some(inner.borrow().type_of().clone()),
                )
            }
            ast::WordKind::Quasiquote(inner) => {
                inner.borrow_mut().evaluate_quasi(ctx)?;
                (
                    Some(Boxed::new(Type::from("Quote"))),
                    Some(inner.borrow().type_of().clone()),
                    Some(inner.borrow().type_of().clone()),
                )
            }
            ast::WordKind::Interpolate(inner) => {
                inner.borrow_mut().evaluate(ctx)?;
                (
                    Some(inner.borrow().type_of().clone()),
                    Some(inner.borrow().type_of().clone()),
                    Some(inner.borrow().type_of().clone()),
                )
            }
            ast::WordKind::Empty => {
                (
                    Some(Boxed::new(Type::from("List"))),
                    Some(Boxed::new(Type::from("List"))),
                    Some(Boxed::new(Type::from("Nil"))),
                )
            }
            ast::WordKind::Parenth(inner) => {
                inner.borrow_mut().evaluate_quasi(ctx)?;
                (
                    Some(Boxed::new(Type::from("List"))),
                    Some(Boxed::new(Type::from("List"))),
                    Some(Boxed::new(Type::from("List"))),
                )
            }
            ast::WordKind::Bind(_, ty, pred, var) => {
                if let Some(pred) = pred {
                    pred.borrow_mut().evaluate_quasi(ctx)?;
                }
                let mut ty = ty
                    .as_ref()
                    .map(|ty| Type::from(ty))
                    .unwrap_or_else(|| Type::from("Any"));
                if *var {
                    ty = ty.with_flags(TypeFlags::VARIADIC);
                }
                let ty = (
                    Some(Boxed::new(Type::from("Bind"))),
                    Some(Boxed::new(Type::from("Bind"))),
                    Some(Boxed::new(ty)),
                );
                ty
            }
            ast::WordKind::Destructure(ty, words) => {
                for word in words {
                    word.evaluate_quasi(ctx)?;
                }
                (
                    Some(Boxed::new(Type::from("Bind"))),
                    Some(Boxed::new(Type::from("Bind"))),
                    Some(Boxed::new(Type::from(&*ty))),
                )
            }
            ast::WordKind::Closure(closure) => {
                for closure in closure.iter_mut() {
                    closure.evaluate_quasi(ctx)?;
                }
                (
                    Some(closure[0].type_of().clone()),
                    Some(closure[0].eval_type().clone()),
                    Some(closure[0].match_type().clone()),
                )
            }
            ast::WordKind::Compound(inner) => {
                inner.borrow_mut().evaluate_quasi(ctx)?;
                (
                    Some(inner.borrow().type_of().clone()),
                    Some(inner.borrow().eval_type().clone()),
                    Some(inner.borrow().match_type().clone()),
                )
            }
        };
        self.as_meta_mut().ty = ty;
        self.as_meta_mut().eval = eval;
        self.as_meta_mut().matches = matches;
        Ok(())
    }
}

impl Param<()> for ast::Word {
    fn evaluate_param(&mut self, ctx: &mut TypeContext) -> Result<(), Vec<Error>> {
        let (ty, eval, matches) = match &mut self.kind {
            ast::WordKind::Bind(atom, ty, pred, var) => {
                if let Some(pred) = pred {
                    pred.borrow_mut().evaluate(ctx)?;
                }
                let mut ty = ty
                    .as_ref()
                    .map(|ty| Type::from(ty))
                    .unwrap_or_else(|| Type::from("Any"));
                if *var {
                    ty = ty.with_flags(TypeFlags::VARIADIC);
                }
                let ty = (
                    Some(Boxed::new(Type::from("Bind"))),
                    Some(Boxed::new(Type::from("Bind"))),
                    Some(Boxed::new(ty)),
                );
                ctx.insert(vec![atom.atom.clone()], Vis::Priv, ty.2.as_ref().unwrap().clone());
                ty
            }
            ast::WordKind::Destructure(ty, words) => {
                for word in words {
                    word.evaluate_param(ctx)?;
                }
                (
                    Some(Boxed::new(Type::from("Bind"))),
                    Some(Boxed::new(Type::from("Bind"))),
                    Some(Boxed::new(Type::from(&*ty))),
                )
            }
            _ => return self.evaluate_quasi(ctx),
        };
        self.as_meta_mut().ty = ty;
        self.as_meta_mut().eval = eval;
        self.as_meta_mut().matches = matches;
        Ok(())
    }
}

impl TypeOf<()> for ast::Atom {
    fn evaluate(&mut self, ctx: &mut TypeContext) -> Result<(), Vec<Error>> {
        self.as_meta_mut().ty = Some(Boxed::new(Type::from("Atom")));
        self.as_meta_mut().eval = Some(ctx.get(&[self.atom.clone()]).ok_or_else(|| {
            vec![Error::new(ErrorKind::UndefinedSymbol)
                .describe(&self.atom)
                .with_span(self.span())]
        })?);
        self.as_meta_mut().matches = self.as_meta().ty.clone();
        Ok(())
    }
}

impl Quote<()> for ast::Atom {
    fn evaluate_quote(&mut self, _ctx: &mut TypeContext) -> Result<(), Vec<Error>> {
        self.as_meta_mut().ty = Some(Boxed::new(Type::from("Atom")));
        self.as_meta_mut().eval = self.as_meta().ty.clone();
        self.as_meta_mut().matches = self.as_meta().ty.clone();
        Ok(())
    }
}

impl Quasi<()> for ast::Atom {
    fn evaluate_quasi(&mut self, _ctx: &mut TypeContext) -> Result<(), Vec<Error>> {
        self.as_meta_mut().ty = Some(Boxed::new(Type::from("Atom")));
        self.as_meta_mut().eval = self.as_meta().ty.clone();
        self.as_meta_mut().matches = self.as_meta().ty.clone();
        Ok(())
    }
}

impl TypeOf<()> for ast::Path {
    fn evaluate(&mut self, ctx: &mut TypeContext) -> Result<(), Vec<Error>> {
        self.as_meta_mut().ty = Some(Boxed::new(Type::from("Atom")));
        self.as_meta_mut().eval = Some(ctx.get(&self.path).ok_or_else(|| {
            vec![Error::new(ErrorKind::UndefinedSymbol)
                .describe(
                    self.path
                        .iter()
                        .skip(1)
                        .fold(self.path[0].to_string(), |mut acc, path| {
                            acc.push('.');
                            acc.push_str(&path);
                            acc
                        }),
                )
                .with_span(self.span())]
        })?);
        self.as_meta_mut().matches = self.as_meta().ty.clone();
        Ok(())
    }
}

impl Quote<()> for ast::Path {
    fn evaluate_quote(&mut self, _ctx: &mut TypeContext) -> Result<(), Vec<Error>> {
        self.as_meta_mut().ty = Some(Boxed::new(Type::from("Atom")));
        self.as_meta_mut().eval = self.as_meta().ty.clone();
        self.as_meta_mut().matches = self.as_meta().ty.clone();
        Ok(())
    }
}

impl Quasi<()> for ast::Path {
    fn evaluate_quasi(&mut self, _ctx: &mut TypeContext) -> Result<(), Vec<Error>> {
        self.as_meta_mut().ty = Some(Boxed::new(Type::from("Atom")));
        self.as_meta_mut().eval = self.as_meta().ty.clone();
        self.as_meta_mut().matches = self.as_meta().ty.clone();
        Ok(())
    }
}

impl TypeOf<()> for ast::Interpolated {
    fn evaluate(&mut self, ctx: &mut TypeContext) -> Result<(), Vec<Error>> {
        let mut errors = vec![];
        for part in &mut self.parts {
            match part {
                ast::InterpolatedPart::Str(..) => {}
                ast::InterpolatedPart::Interpolate(inner) => {
                    inner
                        .borrow_mut()
                        .evaluate(ctx)
                        .unwrap_or_else(|err| errors.extend(err));
                }
            }
        }
        if errors.is_empty() {
            self.as_meta_mut().ty = Some(Boxed::new(Type::from("List")));
            self.as_meta_mut().eval = Some(Boxed::new(Type::from("List")));
            self.as_meta_mut().matches = Some(Boxed::new(Type::from("List")));
            Ok(())
        } else {
            Err(errors)
        }
    }
}

impl Quote<()> for ast::Interpolated {
    fn evaluate_quote(&mut self, ctx: &mut TypeContext) -> Result<(), Vec<Error>> {
        let mut errors = vec![];
        for part in &mut self.parts {
            match part {
                ast::InterpolatedPart::Str(..) => {}
                ast::InterpolatedPart::Interpolate(inner) => {
                    inner
                        .borrow_mut()
                        .evaluate_quote(ctx)
                        .unwrap_or_else(|err| errors.extend(err));
                }
            }
        }
        if errors.is_empty() {
            self.as_meta_mut().ty = Some(Boxed::new(Type::from("List")));
            self.as_meta_mut().eval = Some(Boxed::new(Type::from("List")));
            self.as_meta_mut().matches = Some(Boxed::new(Type::from("List")));
            Ok(())
        } else {
            Err(errors)
        }
    }
}

impl Quasi<()> for ast::Interpolated {
    fn evaluate_quasi(&mut self, ctx: &mut TypeContext) -> Result<(), Vec<Error>> {
        let mut errors = vec![];
        for part in &mut self.parts {
            match part {
                ast::InterpolatedPart::Str(..) => {}
                ast::InterpolatedPart::Interpolate(inner) => {
                    inner
                        .borrow_mut()
                        .evaluate_quasi(ctx)
                        .unwrap_or_else(|err| errors.extend(err));
                }
            }
        }
        if errors.is_empty() {
            self.as_meta_mut().ty = Some(Boxed::new(Type::from("List")));
            self.as_meta_mut().eval = Some(Boxed::new(Type::from("List")));
            self.as_meta_mut().matches = Some(Boxed::new(Type::from("List")));
            Ok(())
        } else {
            Err(errors)
        }
    }
}

impl TypeOf<()> for ast::TypeWord {
    fn evaluate(&mut self, ctx: &mut TypeContext) -> Result<(), Vec<Error>> {
        match &self.kind {
            ast::TypeWordKind::Type(ty) => {
                self.as_meta_mut().ty = Some(ctx.get_cons(&[ty.atom.clone()]).ok_or_else(|| {
                    vec![Error::new(ErrorKind::UndefinedSymbol)
                        .describe(&ty.atom)
                        .with_span(self.span())]
                })?);
                self.as_meta_mut().eval = self.as_meta().ty.clone();
                self.as_meta_mut().matches = self.as_meta().ty.clone();
                Ok(())
            }
            _ => unimplemented!(),
        }
    }
}

impl Quote<()> for ast::TypeWord {
    fn evaluate_quote(&mut self, ctx: &mut TypeContext) -> Result<(), Vec<Error>> {
        match &self.kind {
            ast::TypeWordKind::Type(ty) => {
                self.as_meta_mut().ty = Some(ctx.get_cons(&[ty.atom.clone()]).ok_or_else(|| {
                    vec![Error::new(ErrorKind::UndefinedSymbol)
                        .describe(&ty.atom)
                        .with_span(self.span())]
                })?);
                self.as_meta_mut().eval = self.as_meta().ty.clone();
                self.as_meta_mut().matches = self.as_meta().ty.clone();
                Ok(())
            }
            _ => unimplemented!(),
        }
    }
}

impl Quasi<()> for ast::TypeWord {
    fn evaluate_quasi(&mut self, ctx: &mut TypeContext) -> Result<(), Vec<Error>> {
        match &self.kind {
            ast::TypeWordKind::Type(ty) => {
                self.as_meta_mut().ty = Some(ctx.get_cons(&[ty.atom.clone()]).ok_or_else(|| {
                    vec![Error::new(ErrorKind::UndefinedSymbol)
                        .describe(&ty.atom)
                        .with_span(self.span())]
                })?);
                self.as_meta_mut().eval = self.as_meta().ty.clone();
                self.as_meta_mut().matches = self.as_meta().ty.clone();
                Ok(())
            }
            _ => unimplemented!(),
        }
    }
}

impl<T: ExprLike> TypeOf<()> for ast::Closure<T> {
    fn evaluate(&mut self, ctx: &mut TypeContext) -> Result<(), Vec<Error>> {
        let mut errors = vec![];
        ctx.enter(self.to_ident());
        for param in &mut self.params {
            param.evaluate_param(ctx).unwrap_or_else(|err| errors.extend(err));
        }
        self.body
            .borrow_mut()
            .evaluate(ctx)
            .unwrap_or_else(|err| errors.extend(err));
        if errors.is_empty() {
            let flags = if self.macroeh {
                TypeFlags::CLOSURE | TypeFlags::MACRO
            } else {
                TypeFlags::CLOSURE
            };
            let ret = self.body.borrow().eval_type().as_ref().clone();
            self.as_meta_mut().ty = Some(Boxed::new(Type {
                flags,
                first: TypeAtom::from("Closure"),
                rest: iter::once(From::from(ret))
                    .chain(
                        self.params
                            .iter()
                            .map(|param| param.match_type().as_ref())
                            .cloned()
                            .map(From::from),
                    )
                    .collect(),
            }));
            self.as_meta_mut().eval = self.as_meta().ty.clone();
            self.as_meta_mut().matches = self.as_meta().ty.clone();
            ctx.leave();
            Ok(())
        } else {
            ctx.leave();
            Err(errors)
        }
    }
}

impl<T: ExprLike> Quote<()> for ast::Closure<T> {
    fn evaluate_quote(&mut self, ctx: &mut TypeContext) -> Result<(), Vec<Error>> {
        let mut errors = vec![];
        ctx.enter(self.to_ident());
        for param in &mut self.params {
            param.evaluate_param(ctx).unwrap_or_else(|err| errors.extend(err));
        }
        self.body
            .borrow_mut()
            .evaluate(ctx)
            .unwrap_or_else(|err| errors.extend(err));
        if errors.is_empty() {
            let ret = self.body.borrow().eval_type().as_ref().clone();
            self.as_meta_mut().ty = Some(Boxed::new(Type {
                flags: TypeFlags::CLOSURE,
                first: TypeAtom::from("Closure"),
                rest: iter::once(From::from(ret))
                    .chain(
                        self.params
                            .iter()
                            .map(|param| param.match_type().as_ref())
                            .cloned()
                            .map(From::from),
                    )
                    .collect(),
            }));
            self.as_meta_mut().eval = self.as_meta().ty.clone();
            self.as_meta_mut().matches = self.as_meta().ty.clone();
            ctx.leave();
            Ok(())
        } else {
            ctx.leave();
            Err(errors)
        }
    }
}

impl<T: ExprLike> Quasi<()> for ast::Closure<T> {
    fn evaluate_quasi(&mut self, ctx: &mut TypeContext) -> Result<(), Vec<Error>> {
        let mut errors = vec![];
        ctx.enter(self.to_ident());
        for param in &mut self.params {
            param.evaluate_param(ctx).unwrap_or_else(|err| errors.extend(err));
        }
        self.body
            .borrow_mut()
            .evaluate(ctx)
            .unwrap_or_else(|err| errors.extend(err));
        if errors.is_empty() {
            let ret = self.body.borrow().eval_type().as_ref().clone();
            self.as_meta_mut().ty = Some(Boxed::new(Type {
                flags: TypeFlags::CLOSURE,
                first: TypeAtom::from("Closure"),
                rest: iter::once(From::from(ret))
                    .chain(
                        self.params
                            .iter()
                            .map(|param| param.match_type().as_ref())
                            .cloned()
                            .map(From::from),
                    )
                    .collect(),
            }));
            self.as_meta_mut().eval = self.as_meta().ty.clone();
            self.as_meta_mut().matches = self.as_meta().ty.clone();
            ctx.leave();
            Ok(())
        } else {
            ctx.leave();
            Err(errors)
        }
    }
}

pub fn is_core_type(type_id: u32) -> bool {
    type_id < EH_LAST
}

fn init_lib_cmath(bindings: &mut HashMap<Vec<RcStr>, (Boxed<Type>, Vis)>) {
    macro_rules! init_func {
        ( $name:ident, $( $ty:ident ),+ ) => {
            bindings.insert(
                vec![From::from("cmath"), From::from(stringify!($name))],
                (
                    Boxed::new(Type {
                        flags: TypeFlags::CLOSURE,
                        first: TypeAtom::from("Closure"),
                        rest: vec![
                            $(
                                TypeExpr::from(TypeAtom::from(stringify!($ty)))
                            ),+
                        ],
                    }),
                    Vis::Priv,
                ),
            );
        }
    }

    init_func!(sin, Real, Real);
    init_func!(tan, Real, Real);
    init_func!(acos, Real, Real);
    init_func!(asin, Real, Real);
    init_func!(atan, Real, Real);
    init_func!(atan2, Real, Real, Real);

    init_func!(cosh, Real, Real);
    init_func!(sinh, Real, Real);
    init_func!(tanh, Real, Real);
    init_func!(acosh, Real, Real);
    init_func!(asinh, Real, Real);
    init_func!(atanh, Real, Real);

    init_func!(exp, Real, Real);
    init_func!(frexp, Real, Real);
    init_func!(ldexp, Real, Real, Int);
    init_func!(log, Real, Real);
    init_func!(log10, Real, Real);
    init_func!(modf, Real, Real);
    init_func!(exp2, Real, Real);
    init_func!(expm1, Real, Real);
    init_func!(ilogb, Real, Real);
    init_func!(log1p, Real, Real);
    init_func!(log2, Real, Real);
    init_func!(logb, Real, Real);
    init_func!(scalbn, Real, Real, Int);
    init_func!(scalbln, Real, Real, Int);

    init_func!(pow, Real, Real, Real);
    init_func!(sqrt, Real, Real);
    init_func!(cbrt, Real, Real);
    init_func!(hypot, Real, Real, Real);

    init_func!(erf, Real, Real);
    init_func!(erfc, Real, Real);
    init_func!(tgamma, Real, Real);
    init_func!(lgamma, Real, Real);

    init_func!(ceil, Real, Real);
    init_func!(floor, Real, Real);
    init_func!(fmod, Real, Real, Real);
    init_func!(trunc, Real, Real);
    init_func!(round, Real, Real);
    init_func!(lround, Real, Real);
    init_func!(rint, Real, Real);
    init_func!(lrint, Real, Real);
    init_func!(nearbyint, Real, Real);
    init_func!(remainder, Real, Real, Real);
    init_func!(remquo, Real, Real, Real);

    init_func!(copysign, Real, Real, Real);
    init_func!(nan, Real);
    init_func!(nextafter, Real, Real, Real);
    init_func!(nexttoward, Real, Real, Real);

    init_func!(fdim, Real, Real, Real);
    init_func!(fmax, Real, Real, Real);
    init_func!(fmin, Real, Real, Real);

    init_func!(fabs, Real, Real);
    init_func!(abs, Real, Int);
    init_func!(fma, Real, Real, Real, Real);

    init_func!(fpclassify, Real, Real);
    init_func!(isfinite, Real, Real);
    init_func!(isinf, Real, Real);
    init_func!(isnan, Real, Real);
    init_func!(isnormal, Real, Real);
    init_func!(signbit, Real, Real);
}

fn init_lib_sys(bindings: &mut HashMap<Vec<RcStr>, (Boxed<Type>, Vis)>) {
    macro_rules! init_func {
        ( $name:expr, $( $ty:ident ),+ ) => {
            bindings.insert(
                vec![From::from("sys"), From::from($name)],
                (
                    Boxed::new(Type {
                        flags: TypeFlags::CLOSURE,
                        first: TypeAtom::from("Closure"),
                        rest: vec![
                            $(
                                TypeExpr::from(TypeAtom::from(stringify!($ty)))
                            ),+
                        ],
                    }),
                    Vis::Priv,
                ),
            );
        }
    }

    // ffi
    init_func!("null", Nil);
    init_func!("ptr-offset", Ptr, Int);
    init_func!("ptr-diff", Ptr, Int);
    init_func!("cstr->string", Ptr);
    init_func!("string->cstr", List);

    // stdlib.h
    init_func!("exit", Int);
    init_func!("abort", Nil);
    init_func!("system", Ptr);

    init_func!("rand", Nil);
    init_func!("srand", Int);
    init_func!("randmax", Nil);

    init_func!("malloc", Int);
    init_func!("calloc", Int, Int);
    init_func!("free", Ptr);

    // stdio.h
    init_func!("stdin", Nil);
    init_func!("stdout", Nil);
    init_func!("stderr", Nil);

    init_func!("fopen", Ptr, Ptr);
    init_func!("fclose", Ptr);
    init_func!("fflush", Ptr);

    init_func!("fread", Ptr, Int, Int, Ptr);
    init_func!("fgetc", Ptr);
    init_func!("fgets", Ptr, Int, Ptr);
    init_func!("fwrite", Ptr, Int, Int, Ptr);
    init_func!("fputc", Int, Ptr);
    init_func!("fputs", Ptr, Ptr);

    init_func!("feof", Ptr);
    init_func!("ferror", Ptr);
    init_func!("fseek", Ptr, Int, Int);
    init_func!("ftell", Ptr);

    init_func!("remove", Ptr);
    init_func!("rename", Ptr, Ptr);

    // string.h
    init_func!("memchr", Ptr, Int, Int);
    init_func!("memcmp", Ptr, Ptr, Int);
    init_func!("memcpy", Ptr, Ptr, Int);
    init_func!("memmove", Ptr, Ptr, Int);
    init_func!("memset", Ptr, Int, Int);

    // time.h
    init_func!("clock", Nil);
    init_func!("clocks-per-sec", Nil);
    init_func!("time", Nil);
    init_func!("localtime", Nil);
    init_func!("gmtime", Nil);

    init_func!("tm-sec", Ptr);
    init_func!("tm-min", Ptr);
    init_func!("tm-hour", Ptr);
    init_func!("tm-mday", Ptr);
    init_func!("tm-mon", Ptr);
    init_func!("tm-year", Ptr);
    init_func!("tm-wday", Ptr);
    init_func!("tm-yday", Ptr);
    init_func!("tm-isdst", Ptr);
}
