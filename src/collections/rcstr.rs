use std::borrow::Borrow;
use std::ops::{Deref, Index, Range, RangeFrom, RangeTo, RangeFull, RangeInclusive, RangeToInclusive};
use std::cmp::Ordering;
use std::hash::{Hash, Hasher};
use std::fmt::{self, Display};

use serde::{Serialize, Serializer};
use serde::de::{self, Visitor, Deserialize, Deserializer};

use crate::collections::Boxed;

#[derive(Debug, Clone)]
pub struct RcStr {
    inner: Boxed<String>,
    slice: Range<usize>,
}

impl RcStr {
    pub fn substring(&self, sub: Range<usize>) -> RcStr {
        let start = self.slice.start + sub.start;
        let end = self.slice.start + sub.start + sub.end;
        RcStr {
            inner: Boxed::clone(&self.inner),
            slice: start..end,
        }
    }

    pub fn as_str(&self) -> &str {
        self.as_ref()
    }

    pub(crate) fn extend(&mut self, by: usize) {
        self.slice.end += by;
    }

    pub(crate) fn extend_to(&mut self, to: usize) {
        self.slice.end = to;
    }

    pub(crate) fn end(&self) -> usize {
        self.slice.end
    }

    pub fn into_inner(self) -> (Boxed<String>, Range<usize>) {
        (self.inner, self.slice)
    }

    pub fn ptr_eq(&self, other: &Self) -> bool {
        Boxed::ptr_eq(&self.inner, &other.inner)
    }
}

impl PartialEq for RcStr {
    fn eq(&self, other: &Self) -> bool {
        self.as_ref() == other.as_ref()
    }
}

impl PartialEq<str> for RcStr {
    fn eq(&self, other: &str) -> bool {
        self.as_ref() == other
    }
}

impl<'a> PartialEq<&'a str> for RcStr {
    fn eq(&self, other: &&'a str) -> bool {
        self.as_ref() == *other
    }
}

impl Eq for RcStr {}

impl PartialOrd for RcStr {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        self.as_ref().partial_cmp(other.as_ref())
    }
}

impl Ord for RcStr {
    fn cmp(&self, other: &Self) -> Ordering {
        self.as_ref().cmp(other.as_ref())
    }
}

impl Hash for RcStr {
    fn hash<H: Hasher>(&self, h: &mut H) {
        self.as_ref().hash(h)
    }
}

impl Default for RcStr {
    fn default() -> Self {
        RcStr {
            inner: Boxed::new(String::default()),
            slice: 0..0,
        }
    }
}

impl AsRef<str> for RcStr {
    fn as_ref(&self) -> &str {
        &self.inner[self.slice.clone()]
    }
}

impl Deref for RcStr {
    type Target = str;

    fn deref(&self) -> &Self::Target {
        self.as_ref()
    }
}

impl Borrow<str> for RcStr {
    fn borrow(&self) -> &str {
        self.as_ref()
    }
}

impl Index<Range<usize>> for RcStr {
    type Output = str;

    fn index(&self, idx: Range<usize>) -> &Self::Output {
        &self.inner[self.slice.clone()][idx]
    }
}

impl Index<RangeFrom<usize>> for RcStr {
    type Output = str;

    fn index(&self, idx: RangeFrom<usize>) -> &Self::Output {
        &self.inner[self.slice.clone()][idx]
    }
}

impl Index<RangeTo<usize>> for RcStr {
    type Output = str;

    fn index(&self, idx: RangeTo<usize>) -> &Self::Output {
        &self.inner[self.slice.clone()][idx]
    }
}

impl Index<RangeFull> for RcStr {
    type Output = str;

    fn index(&self, idx: RangeFull) -> &Self::Output {
        &self.inner[self.slice.clone()][idx]
    }
}

impl Index<RangeInclusive<usize>> for RcStr {
    type Output = str;

    fn index(&self, idx: RangeInclusive<usize>) -> &Self::Output {
        &self.inner[self.slice.clone()][idx]
    }
}

impl Index<RangeToInclusive<usize>> for RcStr {
    type Output = str;

    fn index(&self, idx: RangeToInclusive<usize>) -> &Self::Output {
        &self.inner[self.slice.clone()][idx]
    }
}

impl From<String> for RcStr {
    fn from(string: String) -> Self {
        Self::from(Boxed::new(string))
    }
}

impl From<Boxed<String>> for RcStr {
    fn from(string: Boxed<String>) -> Self {
        RcStr {
            slice: 0..string.len(),
            inner: string,
        }
    }
}

impl<'a> From<&'a str> for RcStr {
    fn from(string: &'a str) -> Self {
        Self::from(Boxed::new(string.to_string()))
    }
}

impl Display for RcStr {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        Display::fmt(self.as_ref(), f)
    }
}

impl Serialize for RcStr {
    fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
    where
        S: Serializer,
    {
        serializer.serialize_str(self.as_str())
    }
}

struct RcStrVisitor;

impl<'de> Visitor<'de> for RcStrVisitor {
    type Value = RcStr;

    fn expecting(&self, formatter: &mut fmt::Formatter) -> fmt::Result {
        formatter.write_str("an UTF-8 string")
    }

    fn visit_str<E>(self, value: &str) -> Result<Self::Value, E>
    where
        E: de::Error,
    {
        Ok(RcStr::from(value))
    }
}

impl<'de> Deserialize<'de> for RcStr {
    fn deserialize<D>(deserializer: D) -> Result<RcStr, D::Error>
    where
        D: Deserializer<'de>,
    {
        deserializer.deserialize_str(RcStrVisitor)
    }
}
