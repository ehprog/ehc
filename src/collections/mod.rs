#[cfg(not(feature = "thread_safe"))]
use std::rc::Rc;
#[cfg(feature = "thread_safe")]
use std::sync::Arc;

pub mod rcstr;

pub use rcstr::RcStr;

#[cfg(not(feature = "thread_safe"))]
pub type Boxed<T> = Rc<T>;

#[cfg(feature = "thread_safe")]
pub type Boxed<T> = Arc<T>;
