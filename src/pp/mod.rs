use std::collections::VecDeque;

use crate::error::Error;
use crate::lex::{Lexer, Token, TokenKind};

pub struct Preprocessor {
    lex: Lexer,
    queue: VecDeque<Token>,
}

impl Preprocessor {
    pub fn new(lex: Lexer) -> Self {
        Preprocessor {
            lex,
            queue: VecDeque::new(),
        }
    }

    pub fn lexer(&self) -> &Lexer {
        &self.lex
    }
}

impl Iterator for Preprocessor {
    type Item = Result<Token, Error>;

    fn next(&mut self) -> Option<Self::Item> {
        self.queue.pop_front().map(Ok).or_else(|| {
            let tok = match self.lex.next()? {
                Err(err) => return Some(Err(err)),
                Ok(ok) => ok,
            };

            // a single Semi followed by multiple ParEnds shall be
            // replaced by as many Semis as there are ParEnds
            match tok.kind() {
                TokenKind::Semi => {
                    let mut semi = tok;
                    let mut semis = vec![semi.clone()];
                    let mut parends = vec![];
                    let last;
                    let mut n = 1;
                    let mut target = 0;
                    loop {
                        let tok = match self.lex.next()? {
                            Err(err) => return Some(Err(err)),
                            Ok(ok) => ok,
                        };
                        match tok.kind() {
                            TokenKind::Semi => {
                                n += 1;
                                semi = tok;
                                semis.push(semi.clone());
                            }
                            TokenKind::ParEnd => {
                                target += 1;
                                parends.push(tok);
                            }
                            _ => {
                                last = Some(tok);
                                break;
                            }
                        }
                    }
                    if target > n {
                        for _ in n..target {
                            semis.push(semi.clone());
                        }
                    }
                    if parends.len() > 0 {
                        assert_eq!(semis.len(), parends.len());
                        for (semi, parend) in semis.into_iter().zip(parends) {
                            self.queue.push_back(semi);
                            self.queue.push_back(parend);
                        }
                    } else {
                        self.queue.extend(semis);
                    }
                    self.queue.extend(last);
                    self.queue.pop_front().map(Ok)
                }
                TokenKind::DoubleSemi => {
                    let semi = tok;
                    let mut parends = vec![];
                    let last;
                    loop {
                        let tok = match self.lex.next()? {
                            Err(err) => return Some(Err(err)),
                            Ok(ok) => ok,
                        };
                        match tok.kind() {
                            TokenKind::ParEnd => {
                                parends.push(tok);
                            }
                            _ => {
                                last = Some(tok);
                                break;
                            }
                        }
                    }
                    self.queue.extend(parends);
                    self.queue.push_back(Token::new(semi.span().clone(), TokenKind::Semi));
                    self.queue.extend(last);
                    self.queue.pop_front().map(Ok)
                }
                _ => Some(Ok(tok)),
            }
        })
    }
}

#[cfg(test)]
mod tests {
    use crate::collections::RcStr;
    use super::*;

    #[test]
    fn sanity() {
        let input = "a;;";
        let input = RcStr::from(input);
        let lexer = Lexer::new(RcStr::from("<test>"), input);
        let pp = Preprocessor::new(lexer);
        println!(
            "{:?}",
            pp.map(|tok| {
                tok.map(|tok| {
                    (
                        tok.kind(),
                        tok.span().src().to_string(),
                        tok.span().line(),
                        tok.span().col(),
                    )
                })
                .unwrap()
            })
            .collect::<Vec<_>>()
        );
    }

    #[test]
    fn par() {
        let input = "a\n b\n  c;;";
        let input = RcStr::from(input);
        let lexer = Lexer::new(RcStr::from("<test>"), input);
        let pp = Preprocessor::new(lexer);
        println!(
            "{:?}",
            pp.map(|tok| {
                tok.map(|tok| {
                    (
                        tok.kind(),
                        tok.span().src().to_string(),
                        tok.span().line(),
                        tok.span().col(),
                    )
                })
                .unwrap()
            })
            .collect::<Vec<_>>()
        );
    }

    #[test]
    fn par_2() {
        let input = "a\n  b;;\n  c";
        let input = RcStr::from(input);
        let lexer = Lexer::new(RcStr::from("<test>"), input);
        let pp = Preprocessor::new(lexer);
        println!(
            "{:?}",
            pp.map(|tok| {
                tok.map(|tok| {
                    (
                        tok.kind(),
                        tok.span().src().to_string(),
                        tok.span().line(),
                        tok.span().col(),
                    )
                })
                .unwrap()
            })
            .collect::<Vec<_>>()
        );
    }
}
