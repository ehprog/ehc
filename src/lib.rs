#![feature(arbitrary_self_types)]

#[macro_use]
extern crate serde_derive;

pub mod error;
pub mod collections;
pub mod lex;
pub mod pp;
pub mod parse;
pub mod module;
pub mod ty;
pub mod macros;
pub mod llvm;

pub static CORE: &str = include_str!("core.ehp");
