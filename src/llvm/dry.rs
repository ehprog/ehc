use std::iter;
use std::ptr;
use std::collections::HashSet;

use crate::error::{Error, ErrorKind};
use crate::collections::RcStr;
use crate::parse::ast::{self, normalize_string, AsMeta, ToIdent, BlockLike, StmtLike, ExprLike};
use crate::ty::{TypeOf, Type};
use super::{CompilerContext, FindFree};

#[derive(Debug, Clone, PartialEq, Eq)]
enum Function {
    Let,
    LetFun,
    LetRec,
    Def,
    DefFun,
    DefRec,
    Import,
    From,
    Type,
    Cons,
    Fun,
    Rec,
    Closure,
}

pub trait DryCompile<Seal>: AsMeta {
    fn drycompile(&self, ctx: &mut CompilerContext) -> Result<(), Vec<Error>>;
}

pub trait DryUneval<Seal>: DryCompile<Seal> {
    fn drycompile_uneval(&self, ctx: &mut CompilerContext) -> Result<(), Vec<Error>>;
}

pub trait DryQuasi<Seal>: DryCompile<Seal> {
    fn drycompile_quasi(&self, ctx: &mut CompilerContext) -> Result<(), Vec<Error>>;
}

pub trait DryParam<Seal>: DryCompile<Seal> {
    fn drycompile_param(&self, ctx: &mut CompilerContext) -> Result<(), Vec<Error>>;
}

pub trait DryArg<Seal>: DryCompile<Seal> {
    fn drycompile_arg(&self, ctx: &mut CompilerContext, idx: &mut u16) -> Result<(), Vec<Error>>;
}

impl<T: BlockLike> DryCompile<ast::BlockSeal> for T {
    fn drycompile(&self, ctx: &mut CompilerContext) -> Result<(), Vec<Error>> {
        let mut errors = vec![];
        for stmt in self.stmts() {
            stmt.drycompile(ctx).unwrap_or_else(|err| errors.extend(err));
        }
        if errors.is_empty() {
            if let Some(expr) = self.expr() {
                expr.drycompile(ctx)
            } else {
                Ok(())
            }
        } else {
            Err(errors)
        }
    }
}

impl<T: StmtLike> DryCompile<ast::StmtSeal> for T {
    fn drycompile(&self, ctx: &mut CompilerContext) -> Result<(), Vec<Error>> {
        self.expr().drycompile(ctx)
    }
}

impl<T: ExprLike> DryCompile<ast::ExprSeal> for T {
    fn drycompile(&self, ctx: &mut CompilerContext) -> Result<(), Vec<Error>> {
        if self.rest().is_empty() {
            self.first().drycompile(ctx)
        } else {
            let (func, rest) = if self.first().eval_type().is_func() {
                let func = match &self.first().kind {
                    ast::WordKind::Atom(atom) => {
                        match atom.atom.as_str() {
                            "let" => {
                                let kind = self.rest().get(0).ok_or_else(|| {
                                    vec![Error::new(ErrorKind::LetSyntax)
                                        .with_span(self.span())
                                        .describe("missing binding")]
                                })?;
                                match &kind.kind {
                                    ast::WordKind::Atom(atom) if atom.atom.as_str() == "fun" => Function::LetFun,
                                    ast::WordKind::Atom(atom) if atom.atom.as_str() == "rec" => Function::LetRec,
                                    ast::WordKind::Atom(_) => Function::Let,
                                    _ => {
                                        return Err(vec![Error::new(ErrorKind::LetSyntax)
                                            .with_span(&kind.span())
                                            .describe("binding must be an atom")])
                                    }
                                }
                            }
                            "def" => {
                                let kind = self.rest().get(0).ok_or_else(|| {
                                    vec![Error::new(ErrorKind::LetSyntax)
                                        .with_span(self.span())
                                        .describe("missing binding")]
                                })?;
                                match &kind.kind {
                                    ast::WordKind::Atom(atom) if atom.atom.as_str() == "fun" => Function::DefFun,
                                    ast::WordKind::Atom(atom) if atom.atom.as_str() == "rec" => Function::DefRec,
                                    ast::WordKind::Atom(_) => Function::Def,
                                    _ => {
                                        return Err(vec![Error::new(ErrorKind::LetSyntax)
                                            .with_span(&kind.span())
                                            .describe("binding must be an atom")])
                                    }
                                }
                            }
                            "use" => Function::Import,
                            "from" => Function::From,
                            "type" => Function::Type,
                            _ => {
                                if ctx.is_function(&[atom.atom.clone()]) {
                                    Function::Fun
                                } else if ctx.is_recursive(&[atom.atom.clone()]) {
                                    Function::Rec
                                } else {
                                    Function::Closure
                                }
                            }
                        }
                    }
                    ast::WordKind::Path(path) => {
                        if ctx.is_function(&path.path) {
                            Function::Fun
                        } else if ctx.is_recursive(&path.path) {
                            Function::Rec
                        } else {
                            Function::Closure
                        }
                    }
                    ast::WordKind::TypeAtom(ty) => {
                        match &ty.kind {
                            ast::TypeWordKind::Type(..) => Function::Cons,
                            ast::TypeWordKind::Parenth(..) => unimplemented!(),
                        }
                    }
                    _ => Function::Closure,
                };
                let rest = self.rest().to_vec();
                (func, rest)
            } else {
                let snd = &self.rest()[0];
                let ty = snd.eval_type();
                if !ty.is_func() {
                    return Err(vec![Error::new(ErrorKind::NoAdequateFunction)
                        .with_span(&snd.span())
                        .describe("cannot call a scalar value")]);
                } else {
                    let func = match &snd.kind {
                        ast::WordKind::Atom(atom) => {
                            if ctx.is_function(&[atom.atom.clone()]) {
                                Function::Fun
                            } else if ctx.is_recursive(&[atom.atom.clone()]) {
                                Function::Rec
                            } else {
                                Function::Closure
                            }
                        }
                        ast::WordKind::Path(path) => {
                            if ctx.is_function(&path.path) {
                                Function::Fun
                            } else if ctx.is_recursive(&path.path) {
                                Function::Rec
                            } else {
                                Function::Closure
                            }
                        }
                        ast::WordKind::TypeAtom(ty) => {
                            match &ty.kind {
                                ast::TypeWordKind::Type(..) => Function::Cons,
                                ast::TypeWordKind::Parenth(..) => unimplemented!(),
                            }
                        }
                        _ => Function::Closure,
                    };
                    let mut rest = self.rest().to_vec();
                    rest[0] = self.first().clone();
                    (func, rest)
                }
            };
            match func {
                Function::LetFun => drycompile_let_fun(self, ctx),
                Function::LetRec => drycompile_let_rec(self, ctx),
                Function::Let => drycompile_let_bind(self, ctx),
                Function::DefFun => drycompile_def_fun(self, ctx),
                Function::DefRec => drycompile_def_rec(self, ctx),
                Function::Def => drycompile_def_bind(self, ctx),
                Function::Import => {
                    let module = self.rest().get(0).ok_or_else(|| {
                        vec![Error::new(ErrorKind::ImportSyntax)
                            .describe("missing module")
                            .with_span(self.span())]
                    })?;
                    let module = match &module.kind {
                        ast::WordKind::Atom(atom) => vec![atom.atom.clone()],
                        ast::WordKind::Path(path) => path.path.clone(),
                        _ => {
                            return Err(vec![Error::new(ErrorKind::ImportSyntax)
                                .describe("imports must be atoms")
                                .with_span(module.span())])
                        }
                    };

                    if !ctx.modules.contains(&module) {
                        ctx.modules.insert(module.clone());
                        drycompile_import(self, module, ctx)
                    } else {
                        Ok(())
                    }
                }
                Function::From => {
                    let module = self.rest().get(0).ok_or_else(|| {
                        vec![Error::new(ErrorKind::FromSyntax)
                            .describe("missing module")
                            .with_span(self.span())]
                    })?;
                    let module = match &module.kind {
                        ast::WordKind::Atom(atom) => vec![atom.atom.clone()],
                        ast::WordKind::Path(path) => path.path.clone(),
                        _ => {
                            return Err(vec![Error::new(ErrorKind::FromSyntax)
                                .describe("imports must be atoms")
                                .with_span(module.span())])
                        }
                    };

                    let word = self.rest().get(1).ok_or_else(|| {
                        vec![Error::new(ErrorKind::FromSyntax)
                            .describe("missing `import`")
                            .with_span(self.span())]
                    })?;
                    match &word.kind {
                        ast::WordKind::Atom(atom) if atom.atom.as_str() == "use" => {}
                        _ => {
                            return Err(vec![Error::new(ErrorKind::FromSyntax)
                                .describe("missing `import`")
                                .with_span(word.span())])
                        }
                    }

                    if !ctx.modules.contains(&module) {
                        drycompile_import(self, module.clone(), ctx)?;
                        ctx.modules.insert(module.clone());
                    }

                    let mut imports = vec![];
                    for word in &self.rest()[2..] {
                        match &word.kind {
                            ast::WordKind::Atom(atom) => imports.push(atom.atom.clone()),
                            _ => {
                                return Err(vec![Error::new(ErrorKind::FromSyntax)
                                    .describe("imports must be atoms")
                                    .with_span(word.span())])
                            }
                        }
                    }

                    drycompile_from(self, module, imports, ctx)
                }
                Function::Type => drycompile_type(self, ctx),
                Function::Cons => {
                    ctx.tailcall_disable();
                    let mut args = vec![];
                    for arg in &rest {
                        let arg = arg.drycompile(ctx)?;
                        args.push(arg);
                    }
                    ctx.tailcall_pop();
                    Ok(())
                }
                Function::Fun => {
                    ctx.tailcall_disable();
                    let mut args = vec![];
                    for arg in &rest {
                        let arg = arg.drycompile(ctx)?;
                        args.push(arg);
                    }
                    ctx.tailcall_pop();
                    Ok(())
                }
                Function::Rec => {
                    ctx.tailcall_disable();
                    let mut args = vec![];
                    for arg in &rest {
                        let arg = arg.drycompile(ctx)?;
                        args.push(arg);
                    }
                    ctx.tailcall_pop();
                    Ok(())
                }
                Function::Closure => {
                    ctx.tailcall_disable();
                    let mut args = vec![];
                    for arg in &rest {
                        let arg = arg.drycompile(ctx)?;
                        args.push(arg);
                    }
                    ctx.tailcall_pop();
                    Ok(())
                }
            }
        }
    }
}

fn drycompile_let_fun<T: ExprLike>(expr: &T, ctx: &mut CompilerContext) -> Result<(), Vec<Error>> {
    let binding = match &expr.rest().get(1).unwrap_or_else(|| unreachable!()).kind {
        ast::WordKind::Atom(atom) => &atom.atom,
        _ => unreachable!(),
    };
    let body = expr.as_meta().def.as_ref().unwrap();
    let paramv = expr.as_meta().params.as_ref().unwrap();

    let mut errors = vec![];

    ctx.enter(body.borrow().to_ident());

    let mut free = HashSet::new();
    let mut bindings = HashSet::new();
    expr.find_free(ctx, &mut bindings, &mut free).unwrap_or_else(|errs| {
        errors.extend(errs);
    });
    let mut free = free.into_iter().collect::<Vec<_>>();
    free.sort_unstable();

    {
        let mut i = 0;
        for param in &*paramv.borrow() {
            param
                .drycompile_arg(ctx, &mut i)
                .map(|_| ())
                .unwrap_or_else(|err| errors.extend(err));
        }
        for name in free {
            ctx.insert_binding(name.to_string(), ptr::null_mut());
        }
        body.borrow().drycompile(ctx).unwrap_or_else(|err| errors.extend(err))
    };
    ctx.leave();

    for param in &*paramv.borrow() {
        param.drycompile_param(ctx).unwrap_or_else(|err| {
            errors.extend(err);
        })
    }
    if errors.is_empty() {
        if ctx.is_multi(&[binding.clone()]) {
            Ok(())
        } else {
            ctx.insert_binding(binding.to_string(), ptr::null_mut());
            ctx.insert_multi(vec![binding.clone()]);
            ctx.insert_function(vec![binding.clone()]);
            Ok(())
        }
    } else {
        Err(errors)
    }
}

fn drycompile_let_rec<T: ExprLike>(expr: &T, ctx: &mut CompilerContext) -> Result<(), Vec<Error>> {
    let binding = match &expr.rest().get(1).unwrap_or_else(|| unreachable!()).kind {
        ast::WordKind::Atom(atom) => &atom.atom,
        _ => unreachable!(),
    };
    let body = expr.as_meta().def.as_ref().unwrap();
    let paramv = expr.as_meta().params.as_ref().unwrap();

    let mut errors = vec![];

    ctx.enter(body.borrow().to_ident());

    let mut free = HashSet::new();
    let mut bindings = HashSet::new();
    expr.find_free(ctx, &mut bindings, &mut free).unwrap_or_else(|errs| {
        errors.extend(errs);
    });
    let mut free = free.into_iter().collect::<Vec<_>>();
    free.sort_unstable();

    {
        let mut i = 0;
        for param in &*paramv.borrow() {
            param
                .drycompile_arg(ctx, &mut i)
                .unwrap_or_else(|err| errors.extend(err));
        }
        for name in free {
            ctx.insert_binding(name.to_string(), ptr::null_mut());
        }
        body.borrow().drycompile(ctx)?
    }
    ctx.leave();

    for param in &*paramv.borrow() {
        param.drycompile_param(ctx).unwrap_or_else(|err| {
            errors.extend(err);
        })
    }
    if errors.is_empty() {
        if ctx.is_multi(&[binding.clone()]) {
            Ok(())
        } else {
            ctx.insert_binding(binding.to_string(), ptr::null_mut());
            ctx.insert_multi(vec![binding.clone()]);
            ctx.insert_recursive(vec![binding.clone()]);

            Ok(())
        }
    } else {
        Err(errors)
    }
}

fn drycompile_let_bind<T: ExprLike>(expr: &T, ctx: &mut CompilerContext) -> Result<(), Vec<Error>> {
    let binding = match &expr.rest().get(0).unwrap_or_else(|| unreachable!()).kind {
        ast::WordKind::Atom(atom) => &atom.atom,
        _ => unreachable!(),
    };

    let def = expr.as_meta().def.as_ref().unwrap();
    let var = def.borrow().drycompile(ctx);
    ctx.insert_binding(binding.to_string(), ptr::null_mut());
    var
}

fn drycompile_def_fun<T: ExprLike>(expr: &T, ctx: &mut CompilerContext) -> Result<(), Vec<Error>> {
    let binding = match &expr.rest().get(1).unwrap_or_else(|| unreachable!()).kind {
        ast::WordKind::Atom(atom) => &atom.atom,
        _ => unreachable!(),
    };
    let body = expr.as_meta().def.as_ref().unwrap();
    let paramv = expr.as_meta().params.as_ref().unwrap();

    let mut errors = vec![];

    ctx.enter(body.borrow().to_ident());

    let mut free = HashSet::new();
    let mut bindings = HashSet::new();
    expr.find_free(ctx, &mut bindings, &mut free).unwrap_or_else(|errs| {
        errors.extend(errs);
    });
    let mut free = free.into_iter().collect::<Vec<_>>();
    free.sort_unstable();

    {
        let mut i = 0;
        for param in &*paramv.borrow() {
            param
                .drycompile_arg(ctx, &mut i)
                .unwrap_or_else(|err| errors.extend(err));
        }
        for name in free {
            ctx.insert_binding(name.to_string(), ptr::null_mut());
        }
        body.borrow().drycompile(ctx)?
    }
    ctx.leave();

    for param in &*paramv.borrow() {
        param.drycompile_param(ctx).unwrap_or_else(|err| {
            errors.extend(err);
        })
    }
    if errors.is_empty() {
        if ctx.is_multi(&[binding.clone()]) {
            Ok(())
        } else {
            ctx.insert_named(binding.clone());
            ctx.insert_multi(vec![binding.clone()]);
            ctx.insert_function(vec![binding.clone()]);

            Ok(())
        }
    } else {
        Err(errors)
    }
}

fn drycompile_def_rec<T: ExprLike>(expr: &T, ctx: &mut CompilerContext) -> Result<(), Vec<Error>> {
    let binding = match &expr.rest().get(1).unwrap_or_else(|| unreachable!()).kind {
        ast::WordKind::Atom(atom) => &atom.atom,
        _ => unreachable!(),
    };
    let body = expr.as_meta().def.as_ref().unwrap();
    let paramv = expr.as_meta().params.as_ref().unwrap();

    let mut errors = vec![];

    ctx.enter(body.borrow().to_ident());

    let mut free = HashSet::new();
    let mut bindings = HashSet::new();
    expr.find_free(ctx, &mut bindings, &mut free).unwrap_or_else(|errs| {
        errors.extend(errs);
    });
    let mut free = free.into_iter().collect::<Vec<_>>();
    free.sort_unstable();

    {
        let mut i = 0;
        for param in &*paramv.borrow() {
            param
                .drycompile_arg(ctx, &mut i)
                .unwrap_or_else(|err| errors.extend(err));
        }
        for name in free {
            ctx.insert_binding(name.to_string(), ptr::null_mut());
        }
    }
    ctx.leave();

    for param in &*paramv.borrow() {
        param.drycompile_param(ctx).unwrap_or_else(|err| {
            errors.extend(err);
        })
    }
    if errors.is_empty() {
        if ctx.is_multi(&[binding.clone()]) {
            let _var = ctx.binding(&binding).unwrap();
            Ok(())
        } else {
            ctx.insert_named(binding.clone());
            ctx.insert_multi(vec![binding.clone()]);
            ctx.insert_recursive(vec![binding.clone()]);

            Ok(())
        }
    } else {
        Err(errors)
    }
}

fn drycompile_def_bind<T: ExprLike>(expr: &T, ctx: &mut CompilerContext) -> Result<(), Vec<Error>> {
    let def = expr.as_meta().def.as_ref().unwrap();
    def.borrow().drycompile(ctx)
}

pub(super) fn drycompile_import<T: ExprLike>(
    _expr: &T,
    module: Vec<RcStr>,
    ctx: &mut CompilerContext,
) -> Result<(), Vec<Error>> {
    let name = RcStr::from(module[1..].iter().fold(module[0].to_string(), |mut acc, path| {
        acc.push('.');
        acc.push_str(path);
        acc
    }));
    let normalized = RcStr::from(normalize_string(&name));

    ctx.insert_named(normalized);
    Ok(())
}

pub(super) fn drycompile_from<T: ExprLike>(
    _expr: &T,
    module: Vec<RcStr>,
    imports: Vec<RcStr>,
    ctx: &mut CompilerContext,
) -> Result<(), Vec<Error>> {
    for binding in &imports {
        ctx.insert_binding(binding.to_string(), ptr::null_mut());

        let path = module.iter().chain(iter::once(binding)).cloned().collect::<Vec<_>>();
        if ctx.is_function(&path) {
            ctx.insert_function(vec![binding.clone()]);
        }
        if ctx.is_recursive(&path) {
            ctx.insert_recursive(vec![binding.clone()]);
        }
        if ctx.is_multi(&path) {
            ctx.insert_multi(vec![binding.clone()]);
        }
    }

    Ok(())
}

fn drycompile_type<T: ExprLike>(expr: &T, ctx: &mut CompilerContext) -> Result<(), Vec<Error>> {
    let word = expr.rest().get(0).ok_or_else(|| {
        vec![Error::new(ErrorKind::TypeSyntax)
            .describe("missing type name")
            .with_span(expr.span())]
    })?;
    let ty = match &word.kind {
        ast::WordKind::TypeAtom(ty) => {
            match &ty.kind {
                ast::TypeWordKind::Type(ty) => &ty.atom,
                _ => unimplemented!(),
            }
        }
        _ => {
            return Err(vec![Error::new(ErrorKind::TypeSyntax)
                .describe("type name must be a type atom")
                .with_span(word.span())])
        }
    };

    if expr.rest().len() == 1 {
        return Ok(());
    }

    let is = expr.rest().get(1).unwrap();
    match &is.kind {
        ast::WordKind::Atom(atom) if atom.atom.as_str() == "is:" => {}
        _ => {
            return Err(vec![Error::new(ErrorKind::TypeSyntax)
                .describe("missing `is:`")
                .with_span(is.span())])
        }
    }

    let block = expr.rest().get(2).ok_or_else(|| {
        vec![Error::new(ErrorKind::TypeSyntax)
            .describe("missing type definition")
            .with_span(expr.span())]
    })?;
    let block = match &block.kind {
        ast::WordKind::Compound(block) => block,
        _ => {
            return Err(vec![Error::new(ErrorKind::TypeSyntax)
                .describe("missing type definition")
                .with_span(block.span())])
        }
    };

    let typ = Type::from(ty.clone());
    let _type_id = *ctx.types.get(&typ).unwrap();
    drycompile_type_def(&*block.borrow(), ctx)?;

    Ok(())
}

fn drycompile_type_def<T: BlockLike>(block: &T, ctx: &mut CompilerContext) -> Result<(), Vec<Error>> {
    for stmt in block.stmts() {
        drycompile_type_variant(stmt.expr(), ctx)?;
    }
    if let Some(expr) = block.expr() {
        drycompile_type_variant(expr, ctx)?;
    }
    Ok(())
}

fn drycompile_type_variant<T: ExprLike>(expr: &T, ctx: &mut CompilerContext) -> Result<(), Vec<Error>> {
    match &expr.first().kind {
        ast::WordKind::Destructure(variant, paramv) => {
            let variant = &variant.first.atom;
            let mut errors = vec![];

            ctx.enter(expr.to_ident());

            {
                let mut i = 0;
                for param in paramv {
                    param
                        .drycompile_arg(ctx, &mut i)
                        .unwrap_or_else(|err| errors.extend(err));
                }
            }
            ctx.leave();

            for param in paramv {
                param.drycompile_param(ctx).unwrap_or_else(|err| {
                    errors.extend(err);
                })
            }
            if errors.is_empty() {
                ctx.insert_cons(vec![variant.clone()]);

                Ok(())
            } else {
                Err(errors)
            }
        }
        _ => {
            return Err(vec![Error::new(ErrorKind::TypeSyntax)
                .describe("variants must be declared using the destructure syntax")
                .with_span(expr.span())])
        }
    }
}

impl DryCompile<()> for ast::Word {
    fn drycompile(&self, ctx: &mut CompilerContext) -> Result<(), Vec<Error>> {
        match &self.kind {
            ast::WordKind::Atom(atom) => atom.drycompile(ctx),
            ast::WordKind::Path(path) => path.drycompile(ctx),
            ast::WordKind::Str(string) => string.drycompile(ctx),
            ast::WordKind::TypeAtom(ty) => ty.drycompile(ctx),
            ast::WordKind::Quote(inner) => inner.borrow().drycompile_uneval(ctx),
            ast::WordKind::Quasiquote(inner) => inner.borrow().drycompile_quasi(ctx),
            ast::WordKind::Interpolate(..) => Err(vec![Error::new(ErrorKind::PatternInExpr).with_span(self.span())]),
            ast::WordKind::Parenth(expr) => expr.borrow().drycompile(ctx),
            ast::WordKind::Bind(..) => Ok(()),
            ast::WordKind::Destructure(..) => Err(vec![Error::new(ErrorKind::PatternInExpr).with_span(self.span())]),
            ast::WordKind::Closure(closure) => {
                if closure.len() == 1 {
                    closure[0].drycompile(ctx)
                } else {
                    drycompile_multi(closure, ctx)
                }
            }
            ast::WordKind::Compound(block) => block.borrow().drycompile(ctx),
            _ => Ok(()),
        }
    }
}

impl DryUneval<()> for ast::Word {
    fn drycompile_uneval(&self, ctx: &mut CompilerContext) -> Result<(), Vec<Error>> {
        match &self.kind {
            ast::WordKind::Atom(atom) => atom.drycompile_uneval(ctx),
            ast::WordKind::Path(path) => path.drycompile_uneval(ctx),
            ast::WordKind::TypeAtom(ty) => ty.drycompile_uneval(ctx),
            ast::WordKind::Quote(..) => {
                Err(vec![Error::new(ErrorKind::UnexpectedWord)
                    .describe("quote in quote")
                    .with_span(self.span())])
            }
            ast::WordKind::Quasiquote(..) => {
                Err(vec![Error::new(ErrorKind::UnexpectedWord)
                    .describe("quasiquote in quote")
                    .with_span(self.span())])
            }
            ast::WordKind::Interpolate(..) => {
                Err(vec![Error::new(ErrorKind::UnexpectedWord)
                    .describe("interpolate in quote")
                    .with_span(self.span())])
            }
            ast::WordKind::Empty => Ok(()),
            ast::WordKind::Parenth(expr) => {
                let mut errors = vec![];
                expr.borrow()
                    .first()
                    .drycompile_uneval(ctx)
                    .unwrap_or_else(|err| errors.extend(err));
                for word in expr.borrow().rest() {
                    word.drycompile_uneval(ctx).unwrap_or_else(|err| errors.extend(err));
                }
                if errors.is_empty() {
                    Ok(())
                } else {
                    Err(errors)
                }
            }
            ast::WordKind::Compound(..) => {
                Err(vec![Error::new(ErrorKind::UnexpectedWord)
                    .describe("compound in quote")
                    .with_span(self.span())])
            }
            _ => self.drycompile(ctx),
        }
    }
}

impl DryQuasi<()> for ast::Word {
    fn drycompile_quasi(&self, ctx: &mut CompilerContext) -> Result<(), Vec<Error>> {
        match &self.kind {
            ast::WordKind::Interpolate(inner) => inner.borrow().drycompile(ctx),
            ast::WordKind::Parenth(expr) => {
                let mut errors = vec![];
                expr.borrow()
                    .first()
                    .drycompile_quasi(ctx)
                    .unwrap_or_else(|err| errors.extend(err));
                for word in expr.borrow().rest() {
                    word.drycompile_quasi(ctx).unwrap_or_else(|err| errors.extend(err));
                }
                if errors.is_empty() {
                    Ok(())
                } else {
                    Err(errors)
                }
            }
            _ => self.drycompile_uneval(ctx),
        }
    }
}

impl DryParam<()> for ast::Word {
    fn drycompile_param(&self, ctx: &mut CompilerContext) -> Result<(), Vec<Error>> {
        match &self.kind {
            ast::WordKind::Quote(..) => self.drycompile(ctx),
            ast::WordKind::Quasiquote(..) => self.drycompile(ctx),
            ast::WordKind::Empty => Ok(()),
            ast::WordKind::Parenth(expr) => {
                let mut errors = vec![];
                expr.borrow()
                    .first()
                    .drycompile_param(ctx)
                    .unwrap_or_else(|err| errors.extend(err));
                for word in expr.borrow().rest() {
                    word.drycompile_param(ctx).unwrap_or_else(|err| errors.extend(err));
                }
                if errors.is_empty() {
                    Ok(())
                } else {
                    Err(errors)
                }
            }
            ast::WordKind::Destructure(optty, words) => {
                let ty = From::from(optty);
                ctx.variants
                    .get(&ty)
                    .cloned()
                    .ok_or_else(|| vec![Error::new(ErrorKind::UndefinedType).with_span(optty.span())])?;
                let mut errors = vec![];
                for word in words {
                    let word = word.drycompile_param(ctx);
                    if let Err(err) = word {
                        errors.extend(err);
                    }
                }
                if errors.is_empty() {
                    Ok(())
                } else {
                    Err(errors)
                }
            }
            _ => self.drycompile_quasi(ctx),
        }
    }
}

impl DryArg<()> for ast::Word {
    fn drycompile_arg(&self, ctx: &mut CompilerContext, idx: &mut u16) -> Result<(), Vec<Error>> {
        match &self.kind {
            ast::WordKind::Bind(binding, ..) => {
                ctx.insert_binding(binding.atom.to_string(), ptr::null_mut());
                *idx += 1;
                Ok(())
            }
            ast::WordKind::Destructure(_, words) => {
                let mut errors = vec![];
                for word in words {
                    word.drycompile_arg(ctx, idx).unwrap_or_else(|err| errors.extend(err));
                }
                if errors.is_empty() {
                    Ok(())
                } else {
                    Err(errors)
                }
            }
            _ => {
                *idx += 1;
                Ok(())
            }
        }
    }
}

impl DryCompile<()> for ast::Atom {
    fn drycompile(&self, ctx: &mut CompilerContext) -> Result<(), Vec<Error>> {
        if ctx.named.contains(&self.atom) {
            Ok(())
        } else {
            ctx.binding(self.atom.as_str()).map(|_| ()).ok_or_else(|| {
                vec![Error::new(ErrorKind::UndefinedSymbol)
                    .describe(&self.atom)
                    .with_span(self.span())]
            })
        }
    }
}

impl DryUneval<()> for ast::Atom {
    fn drycompile_uneval(&self, _ctx: &mut CompilerContext) -> Result<(), Vec<Error>> {
        Ok(())
    }
}

impl DryQuasi<()> for ast::Atom {
    fn drycompile_quasi(&self, ctx: &mut CompilerContext) -> Result<(), Vec<Error>> {
        self.drycompile_uneval(ctx)
    }
}

impl DryParam<()> for ast::Atom {
    fn drycompile_param(&self, ctx: &mut CompilerContext) -> Result<(), Vec<Error>> {
        self.drycompile_uneval(ctx)
    }
}

impl DryCompile<()> for ast::Path {
    fn drycompile(&self, _ctx: &mut CompilerContext) -> Result<(), Vec<Error>> {
        Ok(())
    }
}

impl DryUneval<()> for ast::Path {
    fn drycompile_uneval(&self, _ctx: &mut CompilerContext) -> Result<(), Vec<Error>> {
        Err(vec![Error::new(ErrorKind::UnexpectedWord)
            .describe("path in quote")
            .with_span(self.span())])
    }
}

impl DryQuasi<()> for ast::Path {
    fn drycompile_quasi(&self, _ctx: &mut CompilerContext) -> Result<(), Vec<Error>> {
        Err(vec![Error::new(ErrorKind::UnexpectedWord)
            .describe("path in quasiquote")
            .with_span(self.span())])
    }
}

impl DryParam<()> for ast::Path {
    fn drycompile_param(&self, _ctx: &mut CompilerContext) -> Result<(), Vec<Error>> {
        Err(vec![Error::new(ErrorKind::UnexpectedWord)
            .describe("path in parameter list")
            .with_span(self.span())])
    }
}

impl DryCompile<()> for ast::Interpolated {
    fn drycompile(&self, ctx: &mut CompilerContext) -> Result<(), Vec<Error>> {
        let mut errors = vec![];
        for part in &self.parts {
            match part {
                ast::InterpolatedPart::Str(..) => {}
                ast::InterpolatedPart::Interpolate(expr) => {
                    expr.borrow().drycompile(ctx).unwrap_or_else(|err| errors.extend(err));
                }
            }
        }
        if errors.is_empty() {
            Ok(())
        } else {
            Err(errors)
        }
    }
}

impl DryUneval<()> for ast::Interpolated {
    fn drycompile_uneval(&self, ctx: &mut CompilerContext) -> Result<(), Vec<Error>> {
        self.drycompile(ctx)
    }
}

impl DryQuasi<()> for ast::Interpolated {
    fn drycompile_quasi(&self, ctx: &mut CompilerContext) -> Result<(), Vec<Error>> {
        self.drycompile(ctx)
    }
}

impl DryParam<()> for ast::Interpolated {
    fn drycompile_param(&self, ctx: &mut CompilerContext) -> Result<(), Vec<Error>> {
        self.drycompile(ctx)
    }
}

impl DryCompile<()> for ast::TypeWord {
    fn drycompile(&self, _ctx: &mut CompilerContext) -> Result<(), Vec<Error>> {
        match &self.kind {
            ast::TypeWordKind::Type(..) => Ok(()),
            ast::TypeWordKind::Parenth(..) => unimplemented!(),
        }
    }
}

impl DryUneval<()> for ast::TypeWord {
    fn drycompile_uneval(&self, _ctx: &mut CompilerContext) -> Result<(), Vec<Error>> {
        match &self.kind {
            ast::TypeWordKind::Type(..) => Ok(()),
            ast::TypeWordKind::Parenth(..) => unimplemented!(),
        }
    }
}

impl DryQuasi<()> for ast::TypeWord {
    fn drycompile_quasi(&self, ctx: &mut CompilerContext) -> Result<(), Vec<Error>> {
        self.drycompile_uneval(ctx)
    }
}

impl DryParam<()> for ast::TypeWord {
    fn drycompile_param(&self, ctx: &mut CompilerContext) -> Result<(), Vec<Error>> {
        self.drycompile_uneval(ctx)
    }
}

impl<T: ExprLike> DryCompile<()> for ast::Closure<T> {
    fn drycompile(&self, ctx: &mut CompilerContext) -> Result<(), Vec<Error>> {
        let mut errors = vec![];

        ctx.enter(self.to_ident());

        let mut free = HashSet::new();
        let mut bindings = HashSet::new();
        self.find_free(ctx, &mut bindings, &mut free).unwrap_or_else(|errs| {
            errors.extend(errs);
        });
        let mut free = free.into_iter().collect::<Vec<_>>();
        free.sort_unstable();

        {
            let mut i = 0;
            for param in &self.params {
                param
                    .drycompile_arg(ctx, &mut i)
                    .unwrap_or_else(|err| errors.extend(err));
            }
            for name in free {
                ctx.insert_binding(name.to_string(), ptr::null_mut());
            }
            self.body.borrow().drycompile(ctx)?;
        }

        ctx.leave();

        for param in &self.params {
            param.drycompile_param(ctx).unwrap_or_else(|err| {
                errors.extend(err);
            })
        }
        if errors.is_empty() {
            if self.dynamic {
                Ok(())
            } else {
                Ok(())
            }
        } else {
            Err(errors)
        }
    }
}

impl<T: ExprLike> DryUneval<()> for ast::Closure<T> {
    fn drycompile_uneval(&self, _ctx: &mut CompilerContext) -> Result<(), Vec<Error>> {
        Err(vec![Error::new(ErrorKind::UnexpectedWord)
            .describe("closure in quote")
            .with_span(self.span())])
    }
}

impl<T: ExprLike> DryQuasi<()> for ast::Closure<T> {
    fn drycompile_quasi(&self, _ctx: &mut CompilerContext) -> Result<(), Vec<Error>> {
        Err(vec![Error::new(ErrorKind::UnexpectedWord)
            .describe("closure in quasi")
            .with_span(self.span())])
    }
}

impl<T: ExprLike> DryParam<()> for ast::Closure<T> {
    fn drycompile_param(&self, _ctx: &mut CompilerContext) -> Result<(), Vec<Error>> {
        Err(vec![Error::new(ErrorKind::UnexpectedWord)
            .describe("closure in parameter list")
            .with_span(self.span())])
    }
}

fn drycompile_multi<T: ExprLike>(closure: &[ast::Closure<T>], ctx: &mut CompilerContext) -> Result<(), Vec<Error>> {
    let mut errors = vec![];

    for closure in closure {
        ctx.enter(closure.to_ident());
        let mut free = HashSet::new();
        let mut bindings = HashSet::new();
        closure.find_free(ctx, &mut bindings, &mut free).unwrap_or_else(|errs| {
            errors.extend(errs);
        });
        let mut free = free.into_iter().collect::<Vec<_>>();
        free.sort_unstable();

        {
            let mut i = 0;
            for param in &closure.params {
                param
                    .drycompile_arg(ctx, &mut i)
                    .unwrap_or_else(|err| errors.extend(err));
            }
            for name in free {
                ctx.insert_binding(name.to_string(), ptr::null_mut());
            }
            closure.body.borrow().drycompile(ctx)?;
        }
        ctx.leave();

        for param in &closure.params {
            param.drycompile_param(ctx).unwrap_or_else(|err| {
                errors.extend(err);
            })
        }
    }

    if errors.is_empty() {
        Ok(())
    } else {
        Err(errors)
    }
}
