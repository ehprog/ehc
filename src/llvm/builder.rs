use std::iter;
use std::mem;

use llvm_sys::core::*;
use llvm_sys::prelude::*;

use crate::ty::{Type, TypeAtom};

use super::CompilerContext;

pub fn value_ptr_type(ctx: &mut CompilerContext) -> LLVMTypeRef {
    unsafe { LLVMPointerType(byte_type(ctx), 0) }
}

pub fn type_ptr_type(ctx: &mut CompilerContext) -> LLVMTypeRef {
    unsafe { LLVMPointerType(byte_type(ctx), 0) }
}

pub fn int_type(ctx: &mut CompilerContext) -> LLVMTypeRef {
    unsafe { LLVMIntTypeInContext(ctx.context, mem::size_of::<i64>() as u32 * 8) }
}

pub fn real_type(ctx: &mut CompilerContext) -> LLVMTypeRef {
    unsafe { LLVMDoubleTypeInContext(ctx.context) }
}

pub fn char_type(ctx: &mut CompilerContext) -> LLVMTypeRef {
    unsafe { LLVMIntTypeInContext(ctx.context, mem::size_of::<u32>() as u32 * 8) }
}

pub fn byte_type(ctx: &mut CompilerContext) -> LLVMTypeRef {
    unsafe { LLVMIntTypeInContext(ctx.context, mem::size_of::<u8>() as u32 * 8) }
}

pub fn enum_type(ctx: &mut CompilerContext) -> LLVMTypeRef {
    unsafe { LLVMIntTypeInContext(ctx.context, mem::size_of::<u32>() as u32 * 8) }
}

pub fn u32_type(ctx: &mut CompilerContext) -> LLVMTypeRef {
    unsafe { LLVMIntTypeInContext(ctx.context, mem::size_of::<u32>() as u32 * 8) }
}

pub fn usize_type(ctx: &mut CompilerContext) -> LLVMTypeRef {
    unsafe { LLVMIntTypeInContext(ctx.context, mem::size_of::<usize>() as u32 * 8) }
}

pub fn array_type(ty: LLVMTypeRef, elems: usize) -> LLVMTypeRef {
    unsafe { LLVMArrayType(ty, elems as _) }
}

pub fn rt(function: LLVMValueRef) -> LLVMValueRef {
    unsafe { LLVMGetParam(function, 0) }
}

pub fn cstr(ctx: &mut CompilerContext, string: &str) -> LLVMValueRef {
    let char_type = byte_type(ctx);
    unsafe {
        let mut chars = string
            .bytes()
            .chain(iter::once(0))
            .map(|ch| LLVMConstInt(char_type, ch as _, 0))
            .collect::<Vec<_>>();
        let strv = LLVMConstArray(char_type, chars.as_mut_ptr(), chars.len() as _);
        let name = format!("{}_gstrv\0", ctx.prefix);
        let gstrv = LLVMAddGlobal(
            ctx.module,
            array_type(char_type, chars.len() as _),
            name.as_bytes().as_ptr() as _,
        );
        LLVMSetInitializer(gstrv, strv);
        LLVMBuildBitCast(
            ctx.builder,
            gstrv,
            LLVMPointerType(char_type, 0),
            b"strptr\0".as_ptr() as _,
        )
    }
}

pub fn pstr(ctx: &mut CompilerContext, string: &str) -> (LLVMValueRef, LLVMValueRef) {
    let char_type = byte_type(ctx);
    unsafe {
        let mut chars = string
            .bytes()
            .map(|ch| LLVMConstInt(char_type, ch as _, 0))
            .collect::<Vec<_>>();
        let strv = LLVMConstArray(char_type, chars.as_mut_ptr(), chars.len() as _);
        let name = format!("{}_gstrv\0", ctx.prefix);
        let gstrv = LLVMAddGlobal(
            ctx.module,
            array_type(char_type, chars.len() as _),
            name.as_bytes().as_ptr() as _,
        );
        LLVMSetInitializer(gstrv, strv);
        let n = LLVMConstInt(usize_type(ctx), chars.len() as _, 0);
        let ptr = LLVMBuildBitCast(
            ctx.builder,
            gstrv,
            LLVMPointerType(char_type, 0),
            b"strptr\0".as_ptr() as _,
        );
        (n, ptr)
    }
}

pub fn push(
    ctx: &mut CompilerContext,
    function: LLVMValueRef,
    _bb: LLVMBasicBlockRef,
    value: LLVMValueRef,
) -> LLVMValueRef {
    unsafe {
        let rt = rt(function);
        let args = &mut [rt, value];
        let function = LLVMGetNamedFunction(ctx.module, b"ehrt_push\0".as_ptr() as _);
        LLVMBuildCall(
            ctx.builder,
            function,
            args.as_mut_ptr(),
            args.len() as _,
            b"\0".as_ptr() as _,
        )
    }
}

pub fn pop(ctx: &mut CompilerContext, function: LLVMValueRef, _bb: LLVMBasicBlockRef) -> LLVMValueRef {
    unsafe {
        let rt = rt(function);
        let args = &mut [rt];
        let function = LLVMGetNamedFunction(ctx.module, b"ehrt_pop\0".as_ptr() as _);
        LLVMBuildCall(
            ctx.builder,
            function,
            args.as_mut_ptr(),
            args.len() as _,
            b"\0".as_ptr() as _,
        )
    }
}

pub fn get(
    ctx: &mut CompilerContext,
    function: LLVMValueRef,
    _bb: LLVMBasicBlockRef,
    atom: LLVMValueRef,
) -> LLVMValueRef {
    unsafe {
        let rt = rt(function);
        let args = &mut [rt, atom];
        let function = LLVMGetNamedFunction(ctx.module, b"ehrt_get\0".as_ptr() as _);
        LLVMBuildCall(
            ctx.builder,
            function,
            args.as_mut_ptr(),
            args.len() as _,
            b"value\0".as_ptr() as _,
        )
    }
}

pub fn set(
    ctx: &mut CompilerContext,
    function: LLVMValueRef,
    _bb: LLVMBasicBlockRef,
    atom: LLVMValueRef,
    value: LLVMValueRef,
) {
    unsafe {
        let rt = rt(function);
        let args = &mut [rt, atom, value];
        let function = LLVMGetNamedFunction(ctx.module, b"ehrt_set\0".as_ptr() as _);
        LLVMBuildCall(
            ctx.builder,
            function,
            args.as_mut_ptr(),
            args.len() as _,
            b"\0".as_ptr() as _,
        );
    }
}

pub fn getfrom(
    ctx: &mut CompilerContext,
    function: LLVMValueRef,
    _bb: LLVMBasicBlockRef,
    dict: LLVMValueRef,
    atom: LLVMValueRef,
) -> LLVMValueRef {
    unsafe {
        let rt = rt(function);
        let args = &mut [rt, dict, atom];
        let function = LLVMGetNamedFunction(ctx.module, b"ehhtrie_find\0".as_ptr() as _);
        LLVMBuildCall(
            ctx.builder,
            function,
            args.as_mut_ptr(),
            args.len() as _,
            b"value\0".as_ptr() as _,
        )
    }
}

pub fn gc_enable(ctx: &mut CompilerContext, function: LLVMValueRef, _bb: LLVMBasicBlockRef) {
    unsafe {
        let rt = rt(function);
        let function = LLVMGetNamedFunction(ctx.module, b"ehrt_enable_gc\0".as_ptr() as _);
        let args = &mut [rt];
        LLVMBuildCall(
            ctx.builder,
            function,
            args.as_mut_ptr(),
            args.len() as _,
            b"\0".as_ptr() as _,
        );
    }
}

pub fn gc_disable(ctx: &mut CompilerContext, function: LLVMValueRef, _bb: LLVMBasicBlockRef) {
    unsafe {
        let rt = rt(function);
        let function = LLVMGetNamedFunction(ctx.module, b"ehrt_disable_gc\0".as_ptr() as _);
        let args = &mut [rt];
        LLVMBuildCall(
            ctx.builder,
            function,
            args.as_mut_ptr(),
            args.len() as _,
            b"\0".as_ptr() as _,
        );
    }
}

pub fn array(ctx: &mut CompilerContext, ty: LLVMTypeRef, array: &[LLVMValueRef]) -> (LLVMValueRef, LLVMValueRef) {
    unsafe {
        let array_type = array_type(ty, array.len());
        let arrayc = LLVMConstInt(usize_type(ctx), array.len() as _, 0);
        let arrayv = LLVMBuildAlloca(ctx.builder, array_type, b"array\0".as_ptr() as _);
        let arrayptr = LLVMBuildBitCast(
            ctx.builder,
            arrayv,
            LLVMPointerType(value_ptr_type(ctx), 0),
            b"arrayptr\0".as_ptr() as _,
        );
        for (i, &elem) in array.iter().enumerate() {
            let mut idx = LLVMConstInt(usize_type(ctx), i as _, 0);
            let addr = LLVMBuildGEP(ctx.builder, arrayptr, &mut idx, 1, b"arraygep\0".as_ptr() as _);
            LLVMBuildStore(ctx.builder, elem, addr);
        }
        (arrayc, arrayptr)
    }
}

pub fn nil(ctx: &mut CompilerContext, function: LLVMValueRef, _bb: LLVMBasicBlockRef) -> LLVMValueRef {
    unsafe {
        let rt = rt(function);
        let null = LLVMConstNull(value_ptr_type(ctx));
        let args = &mut [rt, null];
        let function = LLVMGetNamedFunction(ctx.module, b"new_nil\0".as_ptr() as _);
        LLVMBuildCall(
            ctx.builder,
            function,
            args.as_mut_ptr(),
            args.len() as _,
            b"nil\0".as_ptr() as _,
        )
    }
}

pub fn int(ctx: &mut CompilerContext, function: LLVMValueRef, _bb: LLVMBasicBlockRef, int: i64) -> LLVMValueRef {
    unsafe {
        let rt = rt(function);
        let null = LLVMConstNull(value_ptr_type(ctx));
        let int = LLVMConstInt(int_type(ctx), int as _, 1);
        let args = &mut [rt, null, int];
        let function = LLVMGetNamedFunction(ctx.module, b"new_int\0".as_ptr() as _);
        LLVMBuildCall(
            ctx.builder,
            function,
            args.as_mut_ptr(),
            args.len() as _,
            b"int\0".as_ptr() as _,
        )
    }
}

pub fn ratio(
    ctx: &mut CompilerContext,
    function: LLVMValueRef,
    _bb: LLVMBasicBlockRef,
    sign: i32,
    p: u64,
    q: u64,
) -> LLVMValueRef {
    let sign = (sign < 0) as u32;
    unsafe {
        let rt = rt(function);
        let null = LLVMConstNull(value_ptr_type(ctx));
        let sign = LLVMConstInt(enum_type(ctx), sign as _, 0);
        let p = LLVMConstInt(int_type(ctx), p as _, 0);
        let q = LLVMConstInt(int_type(ctx), q as _, 0);
        let args = &mut [rt, null, sign, p, q];
        let function = LLVMGetNamedFunction(ctx.module, b"new_ratio\0".as_ptr() as _);
        LLVMBuildCall(
            ctx.builder,
            function,
            args.as_mut_ptr(),
            args.len() as _,
            b"int\0".as_ptr() as _,
        )
    }
}

pub fn real(ctx: &mut CompilerContext, function: LLVMValueRef, _bb: LLVMBasicBlockRef, real: f64) -> LLVMValueRef {
    unsafe {
        let rt = rt(function);
        let null = LLVMConstNull(value_ptr_type(ctx));
        let real = LLVMConstReal(real_type(ctx), real);
        let args = &mut [rt, null, real];
        let function = LLVMGetNamedFunction(ctx.module, b"new_real\0".as_ptr() as _);
        LLVMBuildCall(
            ctx.builder,
            function,
            args.as_mut_ptr(),
            args.len() as _,
            b"real\0".as_ptr() as _,
        )
    }
}

pub fn character(
    ctx: &mut CompilerContext,
    function: LLVMValueRef,
    _bb: LLVMBasicBlockRef,
    character: u32,
) -> LLVMValueRef {
    unsafe {
        let rt = rt(function);
        let null = LLVMConstNull(value_ptr_type(ctx));
        let character = LLVMConstInt(char_type(ctx), character as _, 0);
        let args = &mut [rt, null, character];
        let function = LLVMGetNamedFunction(ctx.module, b"new_char\0".as_ptr() as _);
        LLVMBuildCall(
            ctx.builder,
            function,
            args.as_mut_ptr(),
            args.len() as _,
            b"char\0".as_ptr() as _,
        )
    }
}

pub fn list(ctx: &mut CompilerContext, function: LLVMValueRef, _bb: LLVMBasicBlockRef) -> LLVMValueRef {
    unsafe {
        let rt = rt(function);
        let null = LLVMConstNull(value_ptr_type(ctx));
        let args = &mut [rt, null];
        let function = LLVMGetNamedFunction(ctx.module, b"new_list_empty\0".as_ptr() as _);
        LLVMBuildCall(
            ctx.builder,
            function,
            args.as_mut_ptr(),
            args.len() as _,
            b"list\0".as_ptr() as _,
        )
    }
}

pub fn cons(
    ctx: &mut CompilerContext,
    function: LLVMValueRef,
    _bb: LLVMBasicBlockRef,
    value: LLVMValueRef,
    list: LLVMValueRef,
) -> LLVMValueRef {
    unsafe {
        let rt = rt(function);
        let null = LLVMConstNull(value_ptr_type(ctx));
        let args = &mut [rt, null, value, list];
        let function = LLVMGetNamedFunction(ctx.module, b"new_list_cons\0".as_ptr() as _);
        LLVMBuildCall(
            ctx.builder,
            function,
            args.as_mut_ptr(),
            args.len() as _,
            b"cons\0".as_ptr() as _,
        )
    }
}

pub fn bind(
    ctx: &mut CompilerContext,
    function: LLVMValueRef,
    _bb: LLVMBasicBlockRef,
    ty: u32,
    pred: Option<LLVMValueRef>,
) -> LLVMValueRef {
    unsafe {
        let rt = rt(function);
        let null = LLVMConstNull(value_ptr_type(ctx));
        let ty = LLVMConstInt(enum_type(ctx), ty as _, 0);
        let pred = pred.unwrap_or(null);
        let args = &mut [rt, null, ty, pred];
        let function = LLVMGetNamedFunction(ctx.module, b"new_bind\0".as_ptr() as _);
        LLVMBuildCall(
            ctx.builder,
            function,
            args.as_mut_ptr(),
            args.len() as _,
            b"bind\0".as_ptr() as _,
        )
    }
}

pub fn destr(
    ctx: &mut CompilerContext,
    function: LLVMValueRef,
    _bb: LLVMBasicBlockRef,
    ty: u32,
    variant: u32,
    words: &[LLVMValueRef],
) -> LLVMValueRef {
    unsafe {
        let rt = rt(function);
        let null = LLVMConstNull(value_ptr_type(ctx));
        let ty = LLVMConstInt(enum_type(ctx), ty as _, 0);
        let variant = LLVMConstInt(enum_type(ctx), variant as _, 0);
        let value_type = value_ptr_type(ctx);
        let (patternc, patternv) = array(ctx, value_type, words);
        let args = &mut [rt, null, ty, variant, patternc, patternv];
        let function = LLVMGetNamedFunction(ctx.module, b"new_destr\0".as_ptr() as _);
        LLVMBuildCall(
            ctx.builder,
            function,
            args.as_mut_ptr(),
            args.len() as _,
            b"destr\0".as_ptr() as _,
        )
    }
}

pub fn atom(ctx: &mut CompilerContext, function: LLVMValueRef, _bb: LLVMBasicBlockRef, atom: &str) -> LLVMValueRef {
    unsafe {
        let (atomc, atomptr) = pstr(ctx, atom);

        let rt = rt(function);
        let null = LLVMConstNull(value_ptr_type(ctx));
        let args = &mut [rt, null, atomc, atomptr];
        let function = LLVMGetNamedFunction(ctx.module, b"new_atom\0".as_ptr() as _);
        LLVMBuildCall(
            ctx.builder,
            function,
            args.as_mut_ptr(),
            args.len() as _,
            b"atom\0".as_ptr() as _,
        )
    }
}

pub fn string(ctx: &mut CompilerContext, function: LLVMValueRef, _bb: LLVMBasicBlockRef, string: &str) -> LLVMValueRef {
    unsafe {
        let (strc, strptr) = pstr(ctx, string);

        let rt = rt(function);
        let null = LLVMConstNull(value_ptr_type(ctx));
        let args = &mut [rt, null, strc, strptr];
        let function = LLVMGetNamedFunction(ctx.module, b"ehstr_from_pstr\0".as_ptr() as _);
        LLVMBuildCall(
            ctx.builder,
            function,
            args.as_mut_ptr(),
            args.len() as _,
            b"str\0".as_ptr() as _,
        )
    }
}

pub fn function(
    ctx: &mut CompilerContext,
    function: LLVMValueRef,
    _bb: LLVMBasicBlockRef,
    closure: LLVMValueRef,
    free: &[LLVMValueRef],
    params: &[LLVMValueRef],
) -> LLVMValueRef {
    unsafe {
        let rt = rt(function);
        let null = LLVMConstNull(value_ptr_type(ctx));
        let func = LLVMBuildBitCast(
            ctx.builder,
            closure,
            LLVMPointerType(byte_type(ctx), 0),
            b"impl\0".as_ptr() as _,
        );
        let zero = LLVMConstInt(int_type(ctx), 0, 0);
        let args = &mut [rt, zero];
        let function = LLVMGetNamedFunction(ctx.module, b"ehrt_peek\0".as_ptr() as _);
        let env = LLVMBuildCall(
            ctx.builder,
            function,
            args.as_mut_ptr(),
            args.len() as _,
            b"env\0".as_ptr() as _,
        );
        let value_type = value_ptr_type(ctx);
        let (datac, datav) = array(ctx, value_type, free);
        let (paramc, paramv) = array(ctx, value_type, params);
        let args = &mut [rt, null, func, env, paramc, paramv, datac, datav];
        let function = LLVMGetNamedFunction(ctx.module, b"new_function\0".as_ptr() as _);
        LLVMBuildCall(
            ctx.builder,
            function,
            args.as_mut_ptr(),
            args.len() as _,
            b"impl\0".as_ptr() as _,
        )
    }
}

pub fn closure_lexical(
    ctx: &mut CompilerContext,
    function: LLVMValueRef,
    _bb: LLVMBasicBlockRef,
    impls: &[LLVMValueRef],
) -> LLVMValueRef {
    unsafe {
        let rt = rt(function);
        let null = LLVMConstNull(value_ptr_type(ctx));
        let zero = LLVMConstInt(u32_type(ctx), 0, 0);
        let value_type = value_ptr_type(ctx);
        let (implc, implv) = array(ctx, value_type, impls);
        let implc = LLVMConstIntCast(implc, u32_type(ctx), 0);
        let args = &mut [rt, null, zero, implc, implv];
        let function = LLVMGetNamedFunction(ctx.module, b"new_closure\0".as_ptr() as _);
        LLVMBuildCall(
            ctx.builder,
            function,
            args.as_mut_ptr(),
            args.len() as _,
            b"closure\0".as_ptr() as _,
        )
    }
}

pub fn closure_dynamic(
    ctx: &mut CompilerContext,
    function: LLVMValueRef,
    _bb: LLVMBasicBlockRef,
    impls: &[LLVMValueRef],
) -> LLVMValueRef {
    unsafe {
        let rt = rt(function);
        let null = LLVMConstNull(value_ptr_type(ctx));
        let one = LLVMConstInt(u32_type(ctx), 1, 0);
        let value_type = value_ptr_type(ctx);
        let (implc, implv) = array(ctx, value_type, impls);
        let implc = LLVMConstIntCast(implc, u32_type(ctx), 0);
        let args = &mut [rt, null, one, implc, implv];
        let function = LLVMGetNamedFunction(ctx.module, b"new_closure\0".as_ptr() as _);
        LLVMBuildCall(
            ctx.builder,
            function,
            args.as_mut_ptr(),
            args.len() as _,
            b"closure\0".as_ptr() as _,
        )
    }
}

pub fn var(
    ctx: &mut CompilerContext,
    function: LLVMValueRef,
    _bb: LLVMBasicBlockRef,
    value: LLVMValueRef,
) -> LLVMValueRef {
    unsafe {
        let rt = rt(function);
        let null = LLVMConstNull(value_ptr_type(ctx));
        let args = &mut [rt, null, value];
        let function = LLVMGetNamedFunction(ctx.module, b"new_var\0".as_ptr() as _);
        LLVMBuildCall(
            ctx.builder,
            function,
            args.as_mut_ptr(),
            args.len() as _,
            b"var\0".as_ptr() as _,
        )
    }
}

pub fn getvar(
    ctx: &mut CompilerContext,
    function: LLVMValueRef,
    _bb: LLVMBasicBlockRef,
    var: LLVMValueRef,
) -> LLVMValueRef {
    unsafe {
        let rt = rt(function);
        let args = &mut [rt, var];
        let function = LLVMGetNamedFunction(ctx.module, b"ehvar_get\0".as_ptr() as _);
        LLVMBuildCall(
            ctx.builder,
            function,
            args.as_mut_ptr(),
            args.len() as _,
            b"value\0".as_ptr() as _,
        )
    }
}

pub fn setvar(
    ctx: &mut CompilerContext,
    function: LLVMValueRef,
    _bb: LLVMBasicBlockRef,
    var: LLVMValueRef,
    value: LLVMValueRef,
) {
    unsafe {
        let rt = rt(function);
        let args = &mut [rt, var, value];
        let function = LLVMGetNamedFunction(ctx.module, b"ehvar_set\0".as_ptr() as _);
        LLVMBuildCall(
            ctx.builder,
            function,
            args.as_mut_ptr(),
            args.len() as _,
            b"\0".as_ptr() as _,
        );
    }
}

pub fn closure_impl(
    ctx: &mut CompilerContext,
    function: LLVMValueRef,
    _bb: LLVMBasicBlockRef,
    closure: LLVMValueRef,
    cimpl: LLVMValueRef,
) -> LLVMValueRef {
    unsafe {
        let rt = rt(function);
        let args = &mut [rt, closure, cimpl];
        let function = LLVMGetNamedFunction(ctx.module, b"ehclosure_impl\0".as_ptr() as _);
        LLVMBuildCall(
            ctx.builder,
            function,
            args.as_mut_ptr(),
            args.len() as _,
            b"closure\0".as_ptr() as _,
        )
    }
}

pub fn data(
    ctx: &mut CompilerContext,
    function: LLVMValueRef,
    _bb: LLVMBasicBlockRef,
    type_id: u32,
    variant: u32,
    fields: &[LLVMValueRef],
) -> LLVMValueRef {
    unsafe {
        let rt = rt(function);
        let null = LLVMConstNull(value_ptr_type(ctx));
        let type_id = LLVMConstInt(enum_type(ctx), type_id as _, 0);
        let variant = LLVMConstInt(enum_type(ctx), variant as _, 0);
        let value_type = value_ptr_type(ctx);
        let (fieldc, fieldv) = array(ctx, value_type, fields);
        let args = &mut [rt, null, type_id, variant, fieldc, fieldv];
        let function = LLVMGetNamedFunction(ctx.module, b"new_data\0".as_ptr() as _);
        LLVMBuildCall(
            ctx.builder,
            function,
            args.as_mut_ptr(),
            args.len() as _,
            b"data\0".as_ptr() as _,
        )
    }
}

pub fn type_from(ctx: &mut CompilerContext, function: LLVMValueRef, bb: LLVMBasicBlockRef, ty: &Type) -> LLVMValueRef {
    let atom = type_from_atom(ctx, function, bb, &ty.first);
    unsafe {
        let rt = rt(function);
        let null = LLVMConstNull(type_ptr_type(ctx));
        let null2 = LLVMConstNull(LLVMPointerType(type_ptr_type(ctx), 0));
        let zero = LLVMConstInt(usize_type(ctx), 0, 0);
        let flags = LLVMConstInt(enum_type(ctx), ty.flags.bits() as _, 0);
        let args = &mut [rt, null, flags, atom, zero, null2];
        let function = LLVMGetNamedFunction(ctx.module, b"new_type\0".as_ptr() as _);
        LLVMBuildCall(
            ctx.builder,
            function,
            args.as_mut_ptr(),
            args.len() as _,
            b"type\0".as_ptr() as _,
        )
    }
}

pub fn type_from_atom(
    ctx: &mut CompilerContext,
    function: LLVMValueRef,
    _bb: LLVMBasicBlockRef,
    ty: &TypeAtom,
) -> LLVMValueRef {
    let char_type = byte_type(ctx);
    unsafe {
        let mut chars = ty
            .ty
            .chars()
            .map(|ch| {
                let ch = ch as u32;
                LLVMConstInt(char_type, ch as _, 0)
            })
            .collect::<Vec<_>>();
        let atomc = LLVMConstInt(usize_type(ctx), chars.len() as _, 0);
        let atomv = LLVMConstArray(char_type, chars.as_mut_ptr(), chars.len() as _);
        let aatomv = LLVMBuildAlloca(
            ctx.builder,
            array_type(char_type, chars.len() as _),
            b"aatomv\0".as_ptr() as _,
        );
        LLVMBuildStore(ctx.builder, atomv, aatomv);
        let atomptr = LLVMBuildBitCast(
            ctx.builder,
            aatomv,
            LLVMPointerType(char_type, 0),
            b"atomptr\0".as_ptr() as _,
        );

        let rt = rt(function);
        let null = LLVMConstNull(type_ptr_type(ctx));
        let args = &mut [rt, null, atomc, atomptr];
        let function = LLVMGetNamedFunction(ctx.module, b"new_type_atom\0".as_ptr() as _);
        LLVMBuildCall(
            ctx.builder,
            function,
            args.as_mut_ptr(),
            args.len() as _,
            b"type_atom\0".as_ptr() as _,
        )
    }
}

pub fn newtype(
    ctx: &mut CompilerContext,
    function: LLVMValueRef,
    _bb: LLVMBasicBlockRef,
    ty: LLVMValueRef,
    type_id: u32,
) {
    unsafe {
        let rt = rt(function);
        let type_id = LLVMConstInt(enum_type(ctx), type_id as _, 0);
        let args = &mut [rt, ty, type_id];
        let function = LLVMGetNamedFunction(ctx.module, b"ehrt_newtype\0".as_ptr() as _);
        LLVMBuildCall(
            ctx.builder,
            function,
            args.as_mut_ptr(),
            args.len() as _,
            b"\0".as_ptr() as _,
        );
    }
}
