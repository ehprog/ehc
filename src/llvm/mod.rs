use std::mem;
use std::ptr;
use std::iter;
use std::ffi::CString;
use std::collections::{HashMap, HashSet};

use llvm_sys::LLVMCallConv;
use llvm_sys::core::*;
use llvm_sys::prelude::*;
use llvm_sys::analysis::{LLVMVerifyFunction, LLVMVerifyModule, LLVMVerifierFailureAction};
use llvm_sys::transforms::scalar::{
    LLVMAddInstructionCombiningPass,
    LLVMAddReassociatePass,
    LLVMAddGVNPass,
    LLVMAddCFGSimplificationPass,
    LLVMAddEarlyCSEPass,
};
use llvm_sys::target::{
    LLVMTargetDataRef,
    LLVMSetModuleDataLayout,
    LLVMDisposeTargetData,
    LLVM_InitializeNativeTarget,
    LLVM_InitializeNativeAsmPrinter,
    LLVM_InitializeNativeAsmParser,
};
use llvm_sys::target_machine::{
    LLVMTargetMachineRef,
    LLVMGetDefaultTargetTriple,
    LLVMGetTargetFromTriple,
    LLVMCreateTargetMachine,
    LLVMDisposeTargetMachine,
    LLVMCreateTargetDataLayout,
    LLVMCodeGenOptLevel,
    LLVMRelocMode,
    LLVMCodeModel,
};
use libc::c_char;

use ehrt_sys::TypeId_EH_ANY as EH_ANY;

use crate::error::{Error, ErrorKind};
use crate::collections::RcStr;
use crate::lex::Span;
use crate::parse::ast::{self, normalize_string, Meta, AsMeta, ToIdent, BlockLike, StmtLike, ExprLike};
use crate::module::{Modules, Module};
use crate::ty::{TypeContext, TypeOf, Type};
use crate::macros::{MacroExpander, ExpanderContext};

use self::dry::{DryCompile, drycompile_import, drycompile_from};

mod builder;
mod dry;

pub unsafe fn init() {
    assert!(LLVM_InitializeNativeTarget() != 1);
    assert!(LLVM_InitializeNativeAsmPrinter() != 1);
    assert!(LLVM_InitializeNativeAsmParser() != 1);
}

#[derive(Debug, Clone, Copy, PartialEq)]
pub struct Config {
    pub optimizations: u32,
    pub debug_symbols: bool,
    pub target_machine: bool,
}

impl Default for Config {
    fn default() -> Self {
        Config {
            optimizations: 0,
            debug_symbols: true,
            target_machine: false,
        }
    }
}

pub struct Compiler {
    macros: MacroExpander,
    ctx: Option<CompilerContext>,
    config: Option<Config>,
    modname: Vec<RcStr>,
}

impl Compiler {
    pub fn new(macros: MacroExpander) -> Self {
        let modname = macros.modname().to_vec();
        Compiler {
            macros,
            ctx: None,
            config: Some(Default::default()),
            modname,
        }
    }

    pub fn with_ctx(macros: MacroExpander, ctx: CompilerContext) -> Self {
        let modname = macros.modname().to_vec();
        Compiler {
            macros,
            ctx: Some(ctx),
            config: None,
            modname,
        }
    }

    pub fn with_config(macros: MacroExpander, config: Config) -> Self {
        let modname = macros.modname().to_vec();
        Compiler {
            macros,
            ctx: None,
            config: Some(config),
            modname,
        }
    }

    pub fn modname(&self) -> &[RcStr] {
        &self.modname
    }

    pub fn is_core(&self) -> bool {
        self.modname == ["core"]
    }

    pub fn compile(
        self,
    ) -> Result<
        (
            (String, HashMap<RcStr, LLVMModuleRef>),
            (TypeContext, ExpanderContext, CompilerContext),
        ),
        Vec<Error>,
    > {
        let is_core = self.is_core();
        let (modules, (types, macros)) = self.macros.expand()?;

        let name = &modules.main;
        let main = modules.modules.get(name).unwrap();

        let mut module_refs = HashMap::new();
        let config = self.config.unwrap_or_default();
        let mut ctx = self
            .ctx
            .unwrap_or_else(|| CompilerContext::with_config(name.clone(), config, false));
        if config.target_machine && ctx.target_machine.is_none() {
            unsafe {
                ctx.create_target_default().map_err(|err| vec![err])?;
            }
        }
        ctx.types
            .extend(types.types().iter().map(|(k, v)| (k.clone(), v.clone())));
        ctx.variants
            .extend(types.variants().iter().map(|(k, v)| (k.clone(), v.clone())));
        let (main, _) = ctx.compile(
            &mut module_refs,
            &modules,
            &mut HashSet::new(),
            name,
            main,
            !is_core,
            is_core,
        )?;

        Ok(((main, module_refs), (types, macros, ctx)))
    }
}

#[derive(Debug, Clone)]
pub struct Debug {
    ptr: LLVMValueRef,
    line: usize,
    col: usize,
    file: String,
    ident: String,
}

#[derive(Debug, Clone)]
pub struct CompilerContext {
    dry: bool,
    context: LLVMContextRef,
    builder: LLVMBuilderRef,
    module: LLVMModuleRef,
    target_machine: Option<LLVMTargetMachineRef>,
    mp: Option<LLVMModuleProviderRef>,
    fpm: Option<LLVMPassManagerRef>,
    pm: Option<LLVMPassManagerRef>,
    target_data: Option<LLVMTargetDataRef>,
    triple: *mut c_char,
    optimizations: u32,

    symbols: Vec<CString>,
    bindings: HashMap<RcStr, HashMap<String, LLVMValueRef>>,

    named: HashSet<RcStr>,
    ptr: Vec<RcStr>,
    types: HashMap<Type, u32>,
    variants: HashMap<Type, (u32, u32)>,
    constructors: HashSet<Vec<RcStr>>,
    functions: HashMap<RcStr, HashSet<Vec<RcStr>>>,
    recursive: HashMap<RcStr, HashSet<Vec<RcStr>>>,
    multifuns: HashMap<RcStr, HashSet<Vec<RcStr>>>,
    captured: HashMap<RcStr, HashMap<RcStr, u16>>,
    tailstack: Vec<Vec<RcStr>>,
    tailcall_stack: Vec<bool>,
    tailcall: bool,
    prefix: RcStr,
    modules: HashSet<Vec<RcStr>>,

    debug_frame: LLVMTypeRef,
    debug_data: LLVMTypeRef,
    debug_symbols: Vec<Debug>,
    generate_debug: bool,
}

impl CompilerContext {
    pub fn new(module: Vec<RcStr>, dry: bool) -> Self {
        let mut named = HashSet::new();
        named.insert(From::from("let"));
        named.insert(From::from("def"));
        named.insert(From::from("use"));
        named.insert(From::from("from"));
        named.insert(From::from("type"));
        named.insert(From::from("intrinsic"));
        named.insert(From::from("cmath"));
        named.insert(From::from("sys"));

        let name = RcStr::from(module[1..].iter().fold(module[0].to_string(), |mut acc, path| {
            acc.push('.');
            acc.push_str(path);
            acc
        }));
        let name = RcStr::from(normalize_string(&name));
        unsafe {
            let context = LLVMContextCreate();
            let triple = LLVMGetDefaultTargetTriple();
            let ptr_type = LLVMPointerType(LLVMIntTypeInContext(context, mem::size_of::<i8>() as u32 * 8), 0);
            let int_type = LLVMIntTypeInContext(context, mem::size_of::<i32>() as u32 * 8);
            let usize_type = LLVMIntTypeInContext(context, mem::size_of::<usize>() as u32 * 8);
            let debug_frame = &mut [ptr_type, int_type, int_type, ptr_type, ptr_type];
            let debug_frame = LLVMStructTypeInContext(context, debug_frame.as_mut_ptr(), debug_frame.len() as _, 1);
            let debug_data = &mut [usize_type, ptr_type];
            let debug_data = LLVMStructTypeInContext(context, debug_data.as_mut_ptr(), debug_data.len() as _, 1);
            CompilerContext {
                dry,
                context,
                builder: ptr::null_mut(),
                module: ptr::null_mut(),
                target_machine: None,
                mp: None,
                fpm: None,
                pm: None,
                target_data: None,
                triple,
                optimizations: 0,

                symbols: vec![],
                bindings: iter::once((name.clone(), HashMap::new())).collect(),

                ptr: vec![name.clone()],
                named,
                types: HashMap::new(),
                variants: HashMap::new(),
                constructors: HashSet::new(),
                functions: iter::once((name.clone(), HashSet::new())).collect(),
                recursive: iter::once((name.clone(), HashSet::new())).collect(),
                multifuns: iter::once((name.clone(), HashSet::new())).collect(),
                captured: iter::once((name.clone(), HashMap::new())).collect(),
                tailstack: vec![],
                tailcall_stack: vec![],
                tailcall: false,
                prefix: name,
                modules: HashSet::new(),

                debug_frame,
                debug_data,
                debug_symbols: vec![],
                generate_debug: true,
            }
        }
    }

    pub fn with_config(module: Vec<RcStr>, config: Config, dry: bool) -> Self {
        let mut named = HashSet::new();
        named.insert(From::from("let"));
        named.insert(From::from("def"));
        named.insert(From::from("use"));
        named.insert(From::from("from"));
        named.insert(From::from("type"));
        named.insert(From::from("intrinsic"));
        named.insert(From::from("cmath"));
        named.insert(From::from("sys"));
        let name = RcStr::from(module[1..].iter().fold(module[0].to_string(), |mut acc, path| {
            acc.push('.');
            acc.push_str(path);
            acc
        }));
        let name = RcStr::from(normalize_string(&name));
        unsafe {
            let context = LLVMContextCreate();
            let triple = LLVMGetDefaultTargetTriple();
            let ptr_type = LLVMPointerType(LLVMIntTypeInContext(context, mem::size_of::<i8>() as u32 * 8), 0);
            let int_type = LLVMIntTypeInContext(context, mem::size_of::<i32>() as u32 * 8);
            let usize_type = LLVMIntTypeInContext(context, mem::size_of::<usize>() as u32 * 8);
            let debug_frame = &mut [ptr_type, int_type, int_type, ptr_type, ptr_type];
            let debug_frame = LLVMStructTypeInContext(context, debug_frame.as_mut_ptr(), debug_frame.len() as _, 1);
            let debug_data = &mut [usize_type, ptr_type];
            let debug_data = LLVMStructTypeInContext(context, debug_data.as_mut_ptr(), debug_data.len() as _, 1);
            CompilerContext {
                dry,
                context,
                builder: ptr::null_mut(),
                module: ptr::null_mut(),
                target_machine: None,
                mp: None,
                fpm: None,
                pm: None,
                target_data: None,
                triple,
                optimizations: config.optimizations,

                symbols: vec![],
                bindings: iter::once((name.clone(), HashMap::new())).collect(),

                ptr: vec![name.clone()],
                named,
                types: HashMap::new(),
                variants: HashMap::new(),
                constructors: HashSet::new(),
                functions: iter::once((name.clone(), HashSet::new())).collect(),
                recursive: iter::once((name.clone(), HashSet::new())).collect(),
                multifuns: iter::once((name.clone(), HashSet::new())).collect(),
                captured: iter::once((name.clone(), HashMap::new())).collect(),
                tailstack: vec![],
                tailcall_stack: vec![],
                tailcall: false,
                prefix: name,
                modules: HashSet::new(),

                debug_frame,
                debug_data,
                debug_symbols: vec![],
                generate_debug: config.debug_symbols,
            }
        }
    }

    pub fn with_ctx(module: Vec<RcStr>, ctx: &Self, dry: bool) -> Self {
        let mut named = HashSet::new();
        named.insert(From::from("let"));
        named.insert(From::from("def"));
        named.insert(From::from("use"));
        named.insert(From::from("from"));
        named.insert(From::from("type"));
        named.insert(From::from("intrinsic"));
        named.insert(From::from("cmath"));
        named.insert(From::from("sys"));
        let name = RcStr::from(module[1..].iter().fold(module[0].to_string(), |mut acc, path| {
            acc.push('.');
            acc.push_str(path);
            acc
        }));
        let name = RcStr::from(normalize_string(&name));
        CompilerContext {
            dry,
            context: ctx.context,
            builder: ptr::null_mut(),
            module: ptr::null_mut(),
            target_machine: ctx.target_machine,
            mp: ctx.mp,
            fpm: ctx.fpm,
            pm: ctx.pm,
            target_data: ctx.target_data,
            triple: ctx.triple,
            optimizations: ctx.optimizations,

            symbols: vec![],
            bindings: iter::once((name.clone(), HashMap::new())).collect(),

            ptr: vec![name.clone()],
            named,
            types: HashMap::new(),
            variants: HashMap::new(),
            constructors: HashSet::new(),
            functions: iter::once((name.clone(), HashSet::new())).collect(),
            recursive: iter::once((name.clone(), HashSet::new())).collect(),
            multifuns: iter::once((name.clone(), HashSet::new())).collect(),
            captured: iter::once((name.clone(), HashMap::new())).collect(),
            tailstack: vec![],
            tailcall_stack: vec![],
            tailcall: false,
            prefix: name,
            modules: HashSet::new(),

            debug_frame: ctx.debug_frame,
            debug_data: ctx.debug_data,
            debug_symbols: vec![],
            generate_debug: ctx.generate_debug,
        }
    }

    pub fn compile(
        &mut self,
        modules: &mut HashMap<RcStr, LLVMModuleRef>,
        mods: &Modules,
        evaluated: &mut HashSet<Vec<RcStr>>,
        name: &[RcStr],
        module: &Module,
        with_core: bool,
        is_core: bool,
    ) -> Result<(String, LLVMModuleRef), Vec<Error>> {
        evaluated.insert(name.to_vec());
        match &module {
            Module::Source { dependencies, .. } => {
                for name in dependencies {
                    let with_core = with_core && name != &["core"];
                    if !evaluated.contains(name) {
                        let mut ctx = CompilerContext::with_ctx(name.clone(), self, true);
                        ctx.types.extend(self.types.iter().map(|(k, v)| (k.clone(), v.clone())));
                        ctx.variants
                            .extend(self.variants.iter().map(|(k, v)| (k.clone(), v.clone())));
                        let dep = mods.modules.get(name).unwrap();
                        ctx.compile_module(modules, mods, evaluated, name, dep, with_core)?;
                        self.import(name, &ctx);
                    }
                }
            }
            Module::Ehlib(..) => unimplemented!(),
        }

        let ast = match &module {
            Module::Source { program, .. } => program,
            Module::Ehlib(_ehlib) => {
                unimplemented!();
            }
        };

        let name = RcStr::from(name[1..].iter().fold(name[0].to_string(), |mut acc, path| {
            acc.push('.');
            acc.push_str(path);
            acc
        }));
        let name = RcStr::from(normalize_string(&name));
        let name_c =
            CString::new(name.as_str()).map_err(|_| vec![Error::new(ErrorKind::InvalidSymbol).describe(&name)])?;
        unsafe {
            let module = LLVMModuleCreateWithNameInContext(name_c.as_ptr(), self.context);

            let i8_type = builder::byte_type(self);
            let debug_frame_type = LLVMPointerType(i8_type, 0);
            let debug_data_type = LLVMPointerType(i8_type, 0);
            let ptr_type = LLVMPointerType(i8_type, 0);
            let rt_type = LLVMPointerType(i8_type, 0);
            let type_type = LLVMPointerType(i8_type, 0);
            let type_ptr_type = LLVMPointerType(type_type, 0);
            let value_type = LLVMPointerType(i8_type, 0);
            let value_ptr_type = LLVMPointerType(value_type, 0);
            let fn_ptr_type = LLVMPointerType(i8_type, 0);
            let isize_type = LLVMIntTypeInContext(self.context, mem::size_of::<isize>() as u32 * 8);
            let usize_type = LLVMIntTypeInContext(self.context, mem::size_of::<usize>() as u32 * 8);
            let int_type = LLVMIntTypeInContext(self.context, mem::size_of::<i64>() as u32 * 8);
            let real_type = LLVMDoubleTypeInContext(self.context);
            let char_type = LLVMIntTypeInContext(self.context, mem::size_of::<u32>() as u32 * 8);
            let enum_type = LLVMIntTypeInContext(self.context, mem::size_of::<u32>() as u32 * 8);
            let u32_type = LLVMIntTypeInContext(self.context, mem::size_of::<u32>() as u32 * 8);
            let byte_type = LLVMIntTypeInContext(self.context, mem::size_of::<u8>() as u32 * 8);
            let string_type = LLVMPointerType(byte_type, 0);

            let new_debug_frame_params =
                &mut [debug_frame_type, ptr_type, u32_type, u32_type, string_type, string_type];
            let new_debug_frame_type = LLVMFunctionType(
                i8_type,
                new_debug_frame_params.as_mut_ptr(),
                new_debug_frame_params.len() as _,
                0,
            );

            let new_debug_data_params = &mut [debug_data_type, usize_type, debug_frame_type];
            let new_debug_data_type = LLVMFunctionType(
                i8_type,
                new_debug_data_params.as_mut_ptr(),
                new_debug_data_params.len() as _,
                0,
            );

            let ehrt_set_debug_params = &mut [rt_type, debug_data_type];
            let ehrt_set_debug_type = LLVMFunctionType(
                i8_type,
                ehrt_set_debug_params.as_mut_ptr(),
                ehrt_set_debug_params.len() as _,
                0,
            );

            let ehrt_push_debug_params = &mut [rt_type, debug_data_type];
            let ehrt_push_debug_type = LLVMFunctionType(
                i8_type,
                ehrt_push_debug_params.as_mut_ptr(),
                ehrt_push_debug_params.len() as _,
                0,
            );

            let ehrt_find_debug_params = &mut [rt_type, ptr_type, debug_frame_type];
            let ehrt_find_debug_type = LLVMFunctionType(
                i8_type,
                ehrt_find_debug_params.as_mut_ptr(),
                ehrt_find_debug_params.len() as _,
                0,
            );

            let ehrt_push_params = &mut [rt_type, value_type];
            let ehrt_push_type =
                LLVMFunctionType(i8_type, ehrt_push_params.as_mut_ptr(), ehrt_push_params.len() as _, 0);

            let ehrt_pop_params = &mut [rt_type];
            let ehrt_pop_type =
                LLVMFunctionType(value_type, ehrt_pop_params.as_mut_ptr(), ehrt_pop_params.len() as _, 0);

            let ehrt_peek_params = &mut [rt_type, isize_type];
            let ehrt_peek_type = LLVMFunctionType(
                value_type,
                ehrt_peek_params.as_mut_ptr(),
                ehrt_peek_params.len() as _,
                0,
            );

            let ehrt_get_params = &mut [rt_type, value_type];
            let ehrt_get_type =
                LLVMFunctionType(value_type, ehrt_get_params.as_mut_ptr(), ehrt_get_params.len() as _, 0);

            let ehrt_set_params = &mut [rt_type, value_type, value_type];
            let ehrt_set_type =
                LLVMFunctionType(value_type, ehrt_set_params.as_mut_ptr(), ehrt_set_params.len() as _, 0);

            let ehrt_newtype_params = &mut [rt_type, type_type, u32_type];
            let ehrt_newtype_type = LLVMFunctionType(
                value_type,
                ehrt_newtype_params.as_mut_ptr(),
                ehrt_newtype_params.len() as _,
                0,
            );

            let ehrt_enable_gc_params = &mut [rt_type];
            let ehrt_enable_gc_type = LLVMFunctionType(
                i8_type,
                ehrt_enable_gc_params.as_mut_ptr(),
                ehrt_enable_gc_params.len() as _,
                0,
            );

            let ehrt_disable_gc_params = &mut [rt_type];
            let ehrt_disable_gc_type = LLVMFunctionType(
                i8_type,
                ehrt_disable_gc_params.as_mut_ptr(),
                ehrt_disable_gc_params.len() as _,
                0,
            );

            let ehvalue_display_params = &mut [rt_type, value_type];
            let ehvalue_display_type = LLVMFunctionType(
                value_type,
                ehvalue_display_params.as_mut_ptr(),
                ehvalue_display_params.len() as _,
                0,
            );

            let ehmod_import_params = &mut [rt_type, value_type];
            let ehmod_import_type = LLVMFunctionType(
                value_type,
                ehmod_import_params.as_mut_ptr(),
                ehmod_import_params.len() as _,
                0,
            );

            let ehclosure_call_params = &mut [rt_type, value_type, u32_type, value_ptr_type];
            let ehclosure_call_type = LLVMFunctionType(
                value_type,
                ehclosure_call_params.as_mut_ptr(),
                ehclosure_call_params.len() as _,
                0,
            );

            let ehclosure_tailcall_params = &mut [rt_type, value_type, u32_type, value_ptr_type];
            let ehclosure_tailcall_type = LLVMFunctionType(
                value_type,
                ehclosure_tailcall_params.as_mut_ptr(),
                ehclosure_tailcall_params.len() as _,
                0,
            );

            let ehclosure_impl_params = &mut [rt_type, value_type, value_type];
            let ehclosure_impl_type = LLVMFunctionType(
                value_type,
                ehclosure_impl_params.as_mut_ptr(),
                ehclosure_impl_params.len() as _,
                0,
            );

            let ehvar_set_params = &mut [rt_type, value_type, value_type];
            let ehvar_set_type = LLVMFunctionType(
                value_type,
                ehvar_set_params.as_mut_ptr(),
                ehvar_set_params.len() as _,
                0,
            );

            let ehvar_get_params = &mut [rt_type, value_type];
            let ehvar_get_type = LLVMFunctionType(
                value_type,
                ehvar_get_params.as_mut_ptr(),
                ehvar_get_params.len() as _,
                0,
            );

            let ehhtrie_find_params = &mut [rt_type, value_type, value_type];
            let ehhtrie_find_type = LLVMFunctionType(
                value_type,
                ehhtrie_find_params.as_mut_ptr(),
                ehhtrie_find_params.len() as _,
                0,
            );

            let ehlist_concatv_params = &mut [rt_type, usize_type, value_ptr_type];
            let ehlist_concatv_type = LLVMFunctionType(
                value_type,
                ehlist_concatv_params.as_mut_ptr(),
                ehlist_concatv_params.len() as _,
                0,
            );

            let new_nil_params = &mut [rt_type, value_type];
            let new_nil_type = LLVMFunctionType(value_type, new_nil_params.as_mut_ptr(), new_nil_params.len() as _, 0);

            let new_atom_params = &mut [rt_type, value_type, int_type, string_type];
            let new_atom_type =
                LLVMFunctionType(value_type, new_atom_params.as_mut_ptr(), new_atom_params.len() as _, 0);

            let new_int_params = &mut [rt_type, value_type, int_type];
            let new_int_type = LLVMFunctionType(value_type, new_int_params.as_mut_ptr(), new_int_params.len() as _, 0);

            let new_ratio_params = &mut [rt_type, value_type, enum_type, int_type, int_type];
            let new_ratio_type = LLVMFunctionType(
                value_type,
                new_ratio_params.as_mut_ptr(),
                new_ratio_params.len() as _,
                0,
            );

            let new_real_params = &mut [rt_type, value_type, real_type];
            let new_real_type =
                LLVMFunctionType(value_type, new_real_params.as_mut_ptr(), new_real_params.len() as _, 0);

            let new_char_params = &mut [rt_type, value_type, char_type];
            let new_char_type =
                LLVMFunctionType(value_type, new_char_params.as_mut_ptr(), new_char_params.len() as _, 0);

            let new_list_empty_params = &mut [rt_type, value_type];
            let new_list_empty_type = LLVMFunctionType(
                value_type,
                new_list_empty_params.as_mut_ptr(),
                new_list_empty_params.len() as _,
                0,
            );

            let new_list_cons_params = &mut [rt_type, value_type, value_type, value_type];
            let new_list_cons_type = LLVMFunctionType(
                value_type,
                new_list_cons_params.as_mut_ptr(),
                new_list_cons_params.len() as _,
                0,
            );

            let new_bind_params = &mut [rt_type, value_type, enum_type, value_type];
            let new_bind_type =
                LLVMFunctionType(value_type, new_bind_params.as_mut_ptr(), new_bind_params.len() as _, 0);

            let new_destr_params = &mut [rt_type, value_type, enum_type, enum_type, usize_type, value_ptr_type];
            let new_destr_type = LLVMFunctionType(
                value_type,
                new_destr_params.as_mut_ptr(),
                new_destr_params.len() as _,
                0,
            );

            let new_function_params = &mut [
                rt_type,
                value_type,
                fn_ptr_type,
                value_type,
                usize_type,
                value_ptr_type,
                usize_type,
                value_ptr_type,
            ];
            let new_function_type = LLVMFunctionType(
                value_type,
                new_function_params.as_mut_ptr(),
                new_function_params.len() as _,
                0,
            );

            let new_closure_params = &mut [rt_type, value_type, enum_type, u32_type, value_ptr_type];
            let new_closure_type = LLVMFunctionType(
                value_type,
                new_closure_params.as_mut_ptr(),
                new_closure_params.len() as _,
                0,
            );

            let new_var_params = &mut [rt_type, value_type, value_type];
            let new_var_type = LLVMFunctionType(value_type, new_var_params.as_mut_ptr(), new_var_params.len() as _, 0);

            let new_data_params = &mut [rt_type, value_type, enum_type, enum_type, usize_type, value_ptr_type];
            let new_data_type =
                LLVMFunctionType(value_type, new_data_params.as_mut_ptr(), new_data_params.len() as _, 0);

            let new_type_params = &mut [rt_type, type_type, enum_type, type_type, usize_type, type_ptr_type];
            let new_type_type =
                LLVMFunctionType(value_type, new_type_params.as_mut_ptr(), new_type_params.len() as _, 0);

            let new_type_atom_params = &mut [rt_type, type_type, usize_type, string_type];
            let new_type_atom_type = LLVMFunctionType(
                value_type,
                new_type_atom_params.as_mut_ptr(),
                new_type_atom_params.len() as _,
                0,
            );

            let ehstr_from_pstr_params = &mut [rt_type, value_type, int_type, string_type];
            let ehstr_from_pstr_type = LLVMFunctionType(
                value_type,
                ehstr_from_pstr_params.as_mut_ptr(),
                ehstr_from_pstr_params.len() as _,
                0,
            );

            LLVMAddFunction(module, b"new_debug_frame\0".as_ptr() as _, new_debug_frame_type);
            LLVMAddFunction(module, b"new_debug_data\0".as_ptr() as _, new_debug_data_type);
            LLVMAddFunction(module, b"ehrt_set_debug\0".as_ptr() as _, ehrt_set_debug_type);
            LLVMAddFunction(module, b"ehrt_push_debug\0".as_ptr() as _, ehrt_push_debug_type);
            LLVMAddFunction(module, b"ehrt_find_debug\0".as_ptr() as _, ehrt_find_debug_type);
            LLVMAddFunction(module, b"ehrt_push\0".as_ptr() as _, ehrt_push_type);
            LLVMAddFunction(module, b"ehrt_pop\0".as_ptr() as _, ehrt_pop_type);
            LLVMAddFunction(module, b"ehrt_peek\0".as_ptr() as _, ehrt_peek_type);
            LLVMAddFunction(module, b"ehrt_get\0".as_ptr() as _, ehrt_get_type);
            LLVMAddFunction(module, b"ehrt_set\0".as_ptr() as _, ehrt_set_type);
            LLVMAddFunction(module, b"ehrt_newtype\0".as_ptr() as _, ehrt_newtype_type);
            LLVMAddFunction(module, b"ehrt_enable_gc\0".as_ptr() as _, ehrt_enable_gc_type);
            LLVMAddFunction(module, b"ehrt_disable_gc\0".as_ptr() as _, ehrt_disable_gc_type);
            LLVMAddFunction(module, b"ehvalue_display\0".as_ptr() as _, ehvalue_display_type);
            LLVMAddFunction(module, b"ehmod_import\0".as_ptr() as _, ehmod_import_type);
            LLVMAddFunction(module, b"ehclosure_call\0".as_ptr() as _, ehclosure_call_type);
            LLVMAddFunction(module, b"ehclosure_tailcall\0".as_ptr() as _, ehclosure_tailcall_type);
            LLVMAddFunction(module, b"ehclosure_impl\0".as_ptr() as _, ehclosure_impl_type);
            LLVMAddFunction(module, b"ehvar_set\0".as_ptr() as _, ehvar_set_type);
            LLVMAddFunction(module, b"ehvar_get\0".as_ptr() as _, ehvar_get_type);
            LLVMAddFunction(module, b"ehhtrie_find\0".as_ptr() as _, ehhtrie_find_type);
            LLVMAddFunction(module, b"ehlist_concatv\0".as_ptr() as _, ehlist_concatv_type);
            LLVMAddFunction(module, b"new_nil\0".as_ptr() as _, new_nil_type);
            LLVMAddFunction(module, b"new_atom\0".as_ptr() as _, new_atom_type);
            LLVMAddFunction(module, b"new_int\0".as_ptr() as _, new_int_type);
            LLVMAddFunction(module, b"new_ratio\0".as_ptr() as _, new_ratio_type);
            LLVMAddFunction(module, b"new_real\0".as_ptr() as _, new_real_type);
            LLVMAddFunction(module, b"new_char\0".as_ptr() as _, new_char_type);
            LLVMAddFunction(module, b"new_list_empty\0".as_ptr() as _, new_list_empty_type);
            LLVMAddFunction(module, b"new_list_cons\0".as_ptr() as _, new_list_cons_type);
            LLVMAddFunction(module, b"new_bind\0".as_ptr() as _, new_bind_type);
            LLVMAddFunction(module, b"new_destr\0".as_ptr() as _, new_destr_type);
            LLVMAddFunction(module, b"new_function\0".as_ptr() as _, new_function_type);
            LLVMAddFunction(module, b"new_closure\0".as_ptr() as _, new_closure_type);
            LLVMAddFunction(module, b"new_var\0".as_ptr() as _, new_var_type);
            LLVMAddFunction(module, b"new_data\0".as_ptr() as _, new_data_type);
            LLVMAddFunction(module, b"new_type\0".as_ptr() as _, new_type_type);
            LLVMAddFunction(module, b"new_type_atom\0".as_ptr() as _, new_type_atom_type);
            LLVMAddFunction(module, b"ehstr_from_pstr\0".as_ptr() as _, ehstr_from_pstr_type);

            LLVMSetTarget(module, self.triple);

            if let Some(target_data) = self.target_data {
                LLVMSetModuleDataLayout(module, target_data);
            }

            if self.optimizations > 0 {
                let mp = LLVMCreateModuleProviderForExistingModule(module);
                let fpm = LLVMCreateFunctionPassManager(mp);
                LLVMAddInstructionCombiningPass(fpm);
                LLVMAddReassociatePass(fpm);
                LLVMAddGVNPass(fpm);
                LLVMAddCFGSimplificationPass(fpm);
                LLVMInitializeFunctionPassManager(fpm);
                self.mp = Some(mp);
                self.fpm = Some(fpm);

                let pm = LLVMCreatePassManager();
                LLVMAddEarlyCSEPass(pm);
                self.pm = Some(pm);
            }

            self.symbols.push(name_c);
            self.module = module;
            self.builder = LLVMCreateBuilderInContext(self.context);

            let datav_type = LLVMPointerType(value_type, 0);
            let argv_type = LLVMPointerType(value_type, 0);
            let params = &mut [rt_type, datav_type, argv_type];
            let function_type = LLVMFunctionType(value_type, params.as_mut_ptr(), params.len() as _, 0);
            let function_name = format!("__{}_init__", name);
            let name_c = CString::new(function_name.as_str())
                .map_err(|_| vec![Error::new(ErrorKind::InvalidSymbol).describe(&function_name)])?;
            let function = LLVMAddFunction(self.module, name_c.as_ptr(), function_type);

            self.add_debug(Debug {
                ptr: function,
                line: ast.span().line(),
                col: ast.span().col(),
                file: ast.span().file().to_string(),
                ident: function_name,
            });

            self.symbols.push(name_c);
            LLVMSetFunctionCallConv(function, LLVMCallConv::LLVMCCallConv as _);
            let debug = LLVMAppendBasicBlockInContext(self.context, function, b"debug_init\0".as_ptr() as _);
            let block = LLVMAppendBasicBlockInContext(self.context, function, b"init\0".as_ptr() as _);
            LLVMPositionBuilderAtEnd(self.builder, block);

            if is_core {
                for (ty, type_id) in self.types.clone() {
                    let ty = builder::type_from(self, function, block, &ty);
                    builder::newtype(self, function, block, ty, type_id);
                }
            }

            if with_core {
                compile_import(
                    &ast::Expr::new(
                        Span::new(From::from(file!()), From::from(""), line!() as _, 0),
                        ast::Word {
                            meta: Meta::new(Span::new(From::from(file!()), From::from(""), line!() as _, 0)),
                            kind: ast::WordKind::Empty,
                        },
                        vec![],
                    ),
                    vec![From::from("core")],
                    self,
                    function,
                    block,
                )?;
                compile_from(
                    &ast::Expr::new(
                        Span::new(From::from(file!()), From::from(""), line!() as _, 0),
                        ast::Word {
                            meta: Meta::new(Span::new(From::from(file!()), From::from(""), line!() as _, 0)),
                            kind: ast::WordKind::Empty,
                        },
                        vec![],
                    ),
                    vec![From::from("core")],
                    [
                        "+", "-", "*", "/", "%", "not", "and", "or", "xor", "=", "/=", "<", ">", "<=", ">=", "rev",
                        "map", "filter", "fold", "exists?", "for-all?", "++", "len", "@", "<-", "promote", "ord",
                        "chr",
                    ]
                    .iter()
                    .cloned()
                    .map(From::from)
                    .collect(),
                    self,
                    function,
                    block,
                )?;
            }

            if let Some(value) = ast.block.compile(self, function, block)? {
                LLVMBuildRet(self.builder, value);
            } else {
                let nil = builder::nil(self, function, block);
                LLVMBuildRet(self.builder, nil);
            }

            if self.generate_debug {
                LLVMPositionBuilderAtEnd(self.builder, debug);
                let new_debug_frame = LLVMGetNamedFunction(self.module, b"new_debug_frame\0".as_ptr() as _);
                let ehrt_push_debug = LLVMGetNamedFunction(self.module, b"ehrt_push_debug\0".as_ptr() as _);
                let int_type = LLVMIntTypeInContext(self.context, mem::size_of::<i32>() as u32 * 8);
                let debug_symbols = self.debug_symbols.clone();
                let rt = builder::rt(function);
                debug_symbols.iter().for_each(|debug| {
                    let ptr = debug.ptr;
                    let ptr = LLVMBuildBitCast(self.builder, ptr, ptr_type, b"function\0".as_ptr() as _);
                    let line = LLVMConstInt(int_type, debug.line as _, 0);
                    let col = LLVMConstInt(int_type, debug.col as _, 0);
                    let file = builder::cstr(self, &debug.file);
                    let ident = builder::cstr(self, &debug.ident);
                    let debug = LLVMBuildAlloca(self.builder, self.debug_frame, b"debug_frame\0".as_ptr() as _);
                    let debug = LLVMBuildBitCast(self.builder, debug, ptr_type, b"debug_ptr\0".as_ptr() as _);
                    let args = &mut [debug, ptr, line, col, file, ident];
                    LLVMBuildCall(
                        self.builder,
                        new_debug_frame,
                        args.as_mut_ptr(),
                        args.len() as _,
                        b"\0".as_ptr() as _,
                    );
                    let args = &mut [rt, debug];
                    LLVMBuildCall(
                        self.builder,
                        ehrt_push_debug,
                        args.as_mut_ptr(),
                        args.len() as _,
                        b"\0".as_ptr() as _,
                    );
                });
            }
            LLVMBuildBr(self.builder, block);

            if LLVMVerifyFunction(function, LLVMVerifierFailureAction::LLVMPrintMessageAction) == 1 {
                return Err(vec![Error::new(ErrorKind::LLVM).describe("this function is invalid")]);
            }
            if let Some(fpm) = self.fpm {
                LLVMRunFunctionPassManager(fpm, function);
            }
            if LLVMVerifyModule(
                self.module,
                LLVMVerifierFailureAction::LLVMPrintMessageAction,
                ptr::null_mut(),
            ) == 1
            {
                return Err(vec![Error::new(ErrorKind::LLVM).describe("this module is invalid")]);
            }
            if let Some(pm) = self.pm {
                LLVMRunPassManager(pm, module);
            }

            modules.insert(name.clone(), module);
            LLVMDisposeBuilder(self.builder);
        }
        Ok((name.to_string(), self.module))
    }

    pub fn compile_module(
        &mut self,
        modules: &mut HashMap<RcStr, LLVMModuleRef>,
        mods: &Modules,
        evaluated: &mut HashSet<Vec<RcStr>>,
        name: &[RcStr],
        module: &Module,
        with_core: bool,
    ) -> Result<String, Vec<Error>> {
        evaluated.insert(name.to_vec());
        match &module {
            Module::Source { dependencies, .. } => {
                for name in dependencies {
                    if !evaluated.contains(name) {
                        let mut ctx = CompilerContext::with_ctx(name.clone(), self, true);
                        ctx.types.extend(self.types.iter().map(|(k, v)| (k.clone(), v.clone())));
                        ctx.variants
                            .extend(self.variants.iter().map(|(k, v)| (k.clone(), v.clone())));
                        let dep = mods.modules.get(name).unwrap();
                        ctx.compile_module(modules, mods, evaluated, name, dep, with_core)?;
                        self.import(name, &ctx);
                    }
                }
            }
            Module::Ehlib(..) => unimplemented!(),
        }

        let ast = match &module {
            Module::Source { program, .. } => program,
            Module::Ehlib(_ehlib) => {
                unimplemented!();
            }
        };

        if with_core {
            drycompile_import(
                &ast::Expr::new(
                    Span::new(From::from(file!()), From::from(""), line!() as _, 0),
                    ast::Word {
                        meta: Meta::new(Span::new(From::from(file!()), From::from(""), line!() as _, 0)),
                        kind: ast::WordKind::Empty,
                    },
                    vec![],
                ),
                vec![From::from("core")],
                self,
            )?;
            drycompile_from(
                &ast::Expr::new(
                    Span::new(From::from(file!()), From::from(""), line!() as _, 0),
                    ast::Word {
                        meta: Meta::new(Span::new(From::from(file!()), From::from(""), line!() as _, 0)),
                        kind: ast::WordKind::Empty,
                    },
                    vec![],
                ),
                vec![From::from("core")],
                [
                    "+", "-", "*", "/", "%", "not", "and", "or", "xor", "=", "/=", "<", ">", "<=", ">=", "rev", "map",
                    "filter", "fold", "exists?", "for-all?", "++", "len", "@", "<-", "promote", "ord", "chr",
                ]
                .iter()
                .cloned()
                .map(From::from)
                .collect(),
                self,
            )?;
        }

        let name = RcStr::from(name[1..].iter().fold(name[0].to_string(), |mut acc, path| {
            acc.push('.');
            acc.push_str(path);
            acc
        }));
        ast.block.drycompile(self).map(|_| name.to_string())
    }

    pub unsafe fn create_target_default(&mut self) -> Result<(), Error> {
        let mut target = ptr::null_mut();
        LLVMGetTargetFromTriple(self.triple, &mut target, ptr::null_mut());
        if target.is_null() {
            return Err(Error::new(ErrorKind::NoTargetFound));
        }

        let cpu = b"generic\0".as_ptr();
        let features = b"\0".as_ptr();

        let target_machine = LLVMCreateTargetMachine(
            target,
            self.triple,
            cpu as _,
            features as _,
            LLVMCodeGenOptLevel::LLVMCodeGenLevelAggressive,
            LLVMRelocMode::LLVMRelocDefault,
            LLVMCodeModel::LLVMCodeModelDefault,
        );
        self.target_machine = Some(target_machine);
        self.target_data = Some(LLVMCreateTargetDataLayout(target_machine));

        Ok(())
    }

    pub fn add_debug(&mut self, debug: Debug) {
        self.debug_symbols.push(debug);
    }

    pub fn target_machine(&self) -> Option<LLVMTargetMachineRef> {
        self.target_machine
    }

    pub fn opt_level(&mut self, opt: u32) {
        self.optimizations = opt;
    }

    pub fn import(&mut self, module: &[RcStr], other: &Self) {
        self.modules.extend(other.modules.clone());
        for (_, multifuns) in &other.multifuns {
            for multi in multifuns {
                let multi = module.iter().chain(multi).cloned().collect();
                self.multifuns.entry(self.main()).or_default().insert(multi);
            }
        }
        for (_, functions) in &other.functions {
            for fun in functions {
                let fun = module.iter().chain(fun).cloned().collect();
                self.functions.entry(self.main()).or_default().insert(fun);
            }
        }
        for (_, recursive) in &other.recursive {
            for rec in recursive {
                let rec = module.iter().chain(rec).cloned().collect();
                self.recursive.entry(self.main()).or_default().insert(rec);
            }
        }
    }

    fn main(&self) -> RcStr {
        self.ptr.first().unwrap().clone()
    }

    fn ptr(&self) -> RcStr {
        self.ptr.last().unwrap().clone()
    }

    pub fn insert_named(&mut self, function: RcStr) {
        self.named.insert(function);
    }

    pub fn insert_cons(&mut self, cons: Vec<RcStr>) {
        self.constructors.insert(cons);
    }

    pub fn insert_binding(&mut self, binding: String, value: LLVMValueRef) {
        let ptr = self.ptr();
        match self.bindings.get_mut(&ptr) {
            Some(scope) => {
                scope.insert(binding, value);
            }
            None => {
                let mut scope = HashMap::new();
                scope.insert(binding, value);
                self.bindings.insert(ptr, scope);
            }
        }
    }

    pub fn insert_function(&mut self, function: Vec<RcStr>) {
        let ptr = self.ptr();
        match self.functions.get_mut(&ptr) {
            Some(scope) => {
                scope.insert(function);
            }
            None => {
                let mut scope = HashSet::new();
                scope.insert(function);
                self.functions.insert(ptr, scope);
            }
        }
    }

    pub fn insert_recursive(&mut self, recursive: Vec<RcStr>) {
        let ptr = self.ptr();
        match self.recursive.get_mut(&ptr) {
            Some(scope) => {
                scope.insert(recursive);
            }
            None => {
                let mut scope = HashSet::new();
                scope.insert(recursive);
                self.recursive.insert(ptr, scope);
            }
        }
    }

    pub fn insert_multi(&mut self, multi: Vec<RcStr>) {
        let ptr = self.ptr();
        match self.multifuns.get_mut(&ptr) {
            Some(scope) => {
                scope.insert(multi);
            }
            None => {
                let mut scope = HashSet::new();
                scope.insert(multi);
                self.multifuns.insert(ptr, scope);
            }
        }
    }

    pub fn binding(&self, binding: &str) -> Option<LLVMValueRef> {
        for ptr in self.ptr.iter().rev() {
            if let Some(scope) = self.bindings.get(ptr) {
                if let Some(&binding) = scope.get(binding) {
                    return Some(binding);
                }
            }
        }
        None
    }

    pub fn is_function(&self, fun: &[RcStr]) -> bool {
        for ptr in self.ptr.iter().rev() {
            if let Some(scope) = self.functions.get(ptr) {
                if scope.contains(fun) {
                    return true;
                }
            }
        }
        false
    }

    pub fn is_recursive(&self, rec: &[RcStr]) -> bool {
        for ptr in self.ptr.iter().rev() {
            if let Some(scope) = self.recursive.get(ptr) {
                if scope.contains(rec) {
                    return true;
                }
            }
        }
        false
    }

    pub fn is_captured(&self, key: &RcStr) -> Option<u16> {
        for ptr in self.ptr.iter().rev() {
            if let Some(scope) = self.captured.get(ptr) {
                if let Some(&capture) = scope.get(key) {
                    return Some(capture);
                }
            }
        }
        None
    }

    pub fn is_multi(&self, fun: &[RcStr]) -> bool {
        for ptr in self.ptr.iter().rev() {
            if let Some(scope) = self.multifuns.get(ptr) {
                if scope.contains(fun) {
                    return true;
                }
            }
        }
        false
    }

    pub fn capture(&mut self, key: RcStr, var: u16) {
        let ptr = self.ptr();
        self.captured.get_mut(&ptr).unwrap().insert(key, var);
    }

    pub fn enter(&mut self, name: RcStr) {
        self.ptr.push(name.clone());
    }

    pub fn leave(&mut self) {
        self.ptr.pop();
    }

    pub fn tailcall_enable(&mut self) {
        self.tailcall_stack.push(self.tailcall);
        self.tailcall = true;
    }

    pub fn tailcall_disable(&mut self) {
        self.tailcall_stack.push(self.tailcall);
        self.tailcall = false;
    }

    pub fn tailcall_pop(&mut self) {
        self.tailcall = self.tailcall_stack.pop().expect("invalid tailcall pop");
    }
}

impl Drop for CompilerContext {
    fn drop(&mut self) {
        if !self.dry {
            unsafe {
                if let Some(fpm) = self.fpm {
                    LLVMFinalizeFunctionPassManager(fpm);
                    LLVMDisposePassManager(fpm);
                }
                if let Some(pm) = self.pm {
                    LLVMDisposePassManager(pm);
                }
                if let Some(target_machine) = self.target_machine {
                    LLVMDisposeTargetMachine(target_machine);
                }
                if let Some(target_data) = self.target_data {
                    LLVMDisposeTargetData(target_data);
                }
                LLVMDisposeMessage(self.triple);
                LLVMContextDispose(self.context);
            }
        }
    }
}

#[derive(Debug, Clone, PartialEq, Eq)]
enum Function {
    Let,
    LetFun,
    LetRec,
    Def,
    DefFun,
    DefRec,
    Import,
    From,
    Type,
    Cons(Vec<RcStr>),
    Fun(Vec<RcStr>),
    Rec(Vec<RcStr>),
    Closure(LLVMValueRef),
}

pub trait Compile<Seal>: AsMeta {
    fn compile(
        &self,
        ctx: &mut CompilerContext,
        function: LLVMValueRef,
        bb: LLVMBasicBlockRef,
    ) -> Result<Option<LLVMValueRef>, Vec<Error>>;
}

pub trait Uneval<Seal>: Compile<Seal> {
    fn compile_uneval(
        &self,
        ctx: &mut CompilerContext,
        function: LLVMValueRef,
        bb: LLVMBasicBlockRef,
    ) -> Result<Option<LLVMValueRef>, Vec<Error>>;
}

pub trait Quasi<Seal>: Compile<Seal> {
    fn compile_quasi(
        &self,
        ctx: &mut CompilerContext,
        function: LLVMValueRef,
        bb: LLVMBasicBlockRef,
    ) -> Result<Option<LLVMValueRef>, Vec<Error>>;
}

pub trait Param<Seal>: Compile<Seal> {
    fn compile_param(
        &self,
        ctx: &mut CompilerContext,
        function: LLVMValueRef,
        bb: LLVMBasicBlockRef,
    ) -> Result<Option<LLVMValueRef>, Vec<Error>>;
}

pub trait Arg<Seal>: Compile<Seal> {
    fn compile_arg(
        &self,
        ctx: &mut CompilerContext,
        function: LLVMValueRef,
        bb: LLVMBasicBlockRef,
        idx: &mut u16,
    ) -> Result<Vec<LLVMValueRef>, Vec<Error>>;
}

impl<T: BlockLike> Compile<ast::BlockSeal> for T {
    fn compile(
        &self,
        ctx: &mut CompilerContext,
        function: LLVMValueRef,
        bb: LLVMBasicBlockRef,
    ) -> Result<Option<LLVMValueRef>, Vec<Error>> {
        let mut errors = vec![];
        for stmt in self.stmts() {
            stmt.compile(ctx, function, bb)
                .map(|value| {
                    if let Some(_) = value {
                        builder::pop(ctx, function, bb);
                    }
                })
                .unwrap_or_else(|err| errors.extend(err));
        }
        if errors.is_empty() {
            if let Some(expr) = self.expr() {
                expr.compile(ctx, function, bb)
            } else {
                Ok(Some(builder::nil(ctx, function, bb)))
            }
        } else {
            Err(errors)
        }
    }
}

impl<T: StmtLike> Compile<ast::StmtSeal> for T {
    fn compile(
        &self,
        ctx: &mut CompilerContext,
        function: LLVMValueRef,
        bb: LLVMBasicBlockRef,
    ) -> Result<Option<LLVMValueRef>, Vec<Error>> {
        self.expr()
            .compile(ctx, function, bb)
            .map(|expr| expr.and_then(|_| None))
    }
}

impl<T: ExprLike> Compile<ast::ExprSeal> for T {
    fn compile(
        &self,
        ctx: &mut CompilerContext,
        function: LLVMValueRef,
        bb: LLVMBasicBlockRef,
    ) -> Result<Option<LLVMValueRef>, Vec<Error>> {
        let result = if self.rest().is_empty() {
            self.first().compile(ctx, function, bb)
        } else {
            let (func, rest) = if self.first().eval_type().is_func() {
                let func = match &self.first().kind {
                    ast::WordKind::Atom(atom) => {
                        match atom.atom.as_str() {
                            "let" => {
                                let kind = self.rest().get(0).ok_or_else(|| {
                                    vec![Error::new(ErrorKind::LetSyntax)
                                        .with_span(self.span())
                                        .describe("missing binding")]
                                })?;
                                match &kind.kind {
                                    ast::WordKind::Atom(atom) if atom.atom.as_str() == "fun" => Function::LetFun,
                                    ast::WordKind::Atom(atom) if atom.atom.as_str() == "rec" => Function::LetRec,
                                    ast::WordKind::Atom(_) => Function::Let,
                                    _ => {
                                        return Err(vec![Error::new(ErrorKind::LetSyntax)
                                            .with_span(&kind.span())
                                            .describe("binding must be an atom")])
                                    }
                                }
                            }
                            "def" => {
                                let kind = self.rest().get(0).ok_or_else(|| {
                                    vec![Error::new(ErrorKind::LetSyntax)
                                        .with_span(self.span())
                                        .describe("missing binding")]
                                })?;
                                match &kind.kind {
                                    ast::WordKind::Atom(atom) if atom.atom.as_str() == "fun" => Function::DefFun,
                                    ast::WordKind::Atom(atom) if atom.atom.as_str() == "rec" => Function::DefRec,
                                    ast::WordKind::Atom(_) => Function::Def,
                                    _ => {
                                        return Err(vec![Error::new(ErrorKind::LetSyntax)
                                            .with_span(&kind.span())
                                            .describe("binding must be an atom")])
                                    }
                                }
                            }
                            "use" => Function::Import,
                            "from" => Function::From,
                            "type" => Function::Type,
                            _ => {
                                if ctx.is_function(&[atom.atom.clone()]) {
                                    Function::Fun(vec![atom.atom.clone()])
                                } else if ctx.is_recursive(&[atom.atom.clone()]) {
                                    Function::Rec(vec![atom.atom.clone()])
                                } else {
                                    Function::Closure(self.first().compile(ctx, function, bb)?.unwrap())
                                }
                            }
                        }
                    }
                    ast::WordKind::Path(path) => {
                        if ctx.is_function(&path.path) {
                            Function::Fun(path.path.clone())
                        } else if ctx.is_recursive(&path.path) {
                            Function::Rec(path.path.clone())
                        } else {
                            Function::Closure(self.first().compile(ctx, function, bb)?.unwrap())
                        }
                    }
                    ast::WordKind::TypeAtom(ty) => {
                        match &ty.kind {
                            ast::TypeWordKind::Type(ty) => Function::Cons(vec![ty.atom.clone()]),
                            ast::TypeWordKind::Parenth(..) => unimplemented!(),
                        }
                    }
                    _ => Function::Closure(self.first().compile(ctx, function, bb)?.unwrap()),
                };
                let rest = self.rest().to_vec();
                (func, rest)
            } else {
                let snd = &self.rest()[0];
                let ty = snd.eval_type();
                if !ty.is_func() {
                    return Err(vec![Error::new(ErrorKind::NoAdequateFunction)
                        .with_span(&snd.span())
                        .describe("cannot call a scalar value")]);
                } else {
                    let func = match &snd.kind {
                        ast::WordKind::Atom(atom) => {
                            if ctx.is_function(&[atom.atom.clone()]) {
                                Function::Fun(vec![atom.atom.clone()])
                            } else if ctx.is_recursive(&[atom.atom.clone()]) {
                                Function::Rec(vec![atom.atom.clone()])
                            } else {
                                Function::Closure(snd.compile(ctx, function, bb)?.unwrap())
                            }
                        }
                        ast::WordKind::Path(path) => {
                            if ctx.is_function(&path.path) {
                                Function::Fun(path.path.clone())
                            } else if ctx.is_recursive(&path.path) {
                                Function::Rec(path.path.clone())
                            } else {
                                Function::Closure(snd.compile(ctx, function, bb)?.unwrap())
                            }
                        }
                        ast::WordKind::TypeAtom(ty) => {
                            match &ty.kind {
                                ast::TypeWordKind::Type(ty) => Function::Cons(vec![ty.atom.clone()]),
                                ast::TypeWordKind::Parenth(..) => unimplemented!(),
                            }
                        }
                        _ => Function::Closure(snd.compile(ctx, function, bb)?.unwrap()),
                    };
                    let mut rest = self.rest().to_vec();
                    rest[0] = self.first().clone();
                    (func, rest)
                }
            };
            match func {
                Function::LetFun => compile_let_fun(self, ctx, function, bb),
                Function::LetRec => compile_let_rec(self, ctx, function, bb),
                Function::Let => compile_let_bind(self, ctx, function, bb),
                Function::DefFun => compile_def_fun(self, ctx, function, bb),
                Function::DefRec => compile_def_rec(self, ctx, function, bb),
                Function::Def => compile_def_bind(self, ctx, function, bb),
                Function::Import => {
                    let module = self.rest().get(0).ok_or_else(|| {
                        vec![Error::new(ErrorKind::ImportSyntax)
                            .describe("missing module")
                            .with_span(self.span())]
                    })?;
                    let module = match &module.kind {
                        ast::WordKind::Atom(atom) => vec![atom.atom.clone()],
                        ast::WordKind::Path(path) => path.path.clone(),
                        _ => {
                            return Err(vec![Error::new(ErrorKind::ImportSyntax)
                                .describe("imports must be atoms")
                                .with_span(module.span())])
                        }
                    };

                    if !ctx.modules.contains(&module) {
                        ctx.modules.insert(module.clone());
                        compile_import(self, module, ctx, function, bb)
                    } else {
                        Ok(None)
                    }
                }
                Function::From => {
                    let module = self.rest().get(0).ok_or_else(|| {
                        vec![Error::new(ErrorKind::FromSyntax)
                            .describe("missing module")
                            .with_span(self.span())]
                    })?;
                    let module = match &module.kind {
                        ast::WordKind::Atom(atom) => vec![atom.atom.clone()],
                        ast::WordKind::Path(path) => path.path.clone(),
                        _ => {
                            return Err(vec![Error::new(ErrorKind::FromSyntax)
                                .describe("imports must be atoms")
                                .with_span(module.span())])
                        }
                    };

                    let word = self.rest().get(1).ok_or_else(|| {
                        vec![Error::new(ErrorKind::FromSyntax)
                            .describe("missing `import`")
                            .with_span(self.span())]
                    })?;
                    match &word.kind {
                        ast::WordKind::Atom(atom) if atom.atom.as_str() == "use" => {}
                        _ => {
                            return Err(vec![Error::new(ErrorKind::FromSyntax)
                                .describe("missing `import`")
                                .with_span(word.span())])
                        }
                    }

                    if !ctx.modules.contains(&module) {
                        compile_import(self, module.clone(), ctx, function, bb)?;
                        ctx.modules.insert(module.clone());
                    }

                    let mut imports = vec![];
                    for word in &self.rest()[2..] {
                        match &word.kind {
                            ast::WordKind::Atom(atom) => imports.push(atom.atom.clone()),
                            _ => {
                                return Err(vec![Error::new(ErrorKind::FromSyntax)
                                    .describe("imports must be atoms")
                                    .with_span(word.span())])
                            }
                        }
                    }

                    compile_from(self, module, imports, ctx, function, bb)
                }
                Function::Type => compile_type(self, ctx, function, bb),
                Function::Cons(cons) => {
                    if cons.len() > 1 {
                        unimplemented!()
                    }
                    let atom = builder::atom(ctx, function, bb, cons[0].as_str());
                    let cons = builder::get(ctx, function, bb, atom);
                    ctx.tailcall_disable();
                    let mut args = vec![];
                    for arg in &rest {
                        let arg = arg.compile(ctx, function, bb)?.unwrap();
                        builder::push(ctx, function, bb, arg);
                        args.push(arg);
                    }
                    let value = unsafe {
                        let value_type = builder::value_ptr_type(ctx);
                        let (argc, argv) = builder::array(ctx, value_type, &args);
                        let argc = LLVMConstIntCast(argc, builder::u32_type(ctx), 0);
                        let rt = builder::rt(function);
                        let args = &mut [rt, cons, argc, argv];
                        let function = LLVMGetNamedFunction(ctx.module, b"ehclosure_call\0".as_ptr() as _);
                        LLVMBuildCall(
                            ctx.builder,
                            function,
                            args.as_mut_ptr(),
                            args.len() as _,
                            b"new\0".as_ptr() as _,
                        )
                    };
                    ctx.tailcall_pop();
                    Ok(Some(value))
                }
                Function::Fun(func) => {
                    let func = if func.len() > 1 || ctx.named.contains(&func[0]) {
                        builder::gc_disable(ctx, function, bb);
                        let atom = builder::atom(ctx, function, bb, func[0].as_str());
                        let mut var = builder::get(ctx, function, bb, atom);
                        for path in &func[1..] {
                            let atom = builder::atom(ctx, function, bb, path.as_str());
                            var = builder::getfrom(ctx, function, bb, var, atom);
                        }
                        builder::gc_enable(ctx, function, bb);
                        builder::getvar(ctx, function, bb, var)
                    } else {
                        let var = ctx.binding(&func[0]).ok_or_else(|| {
                            vec![Error::new(ErrorKind::UndefinedSymbol)
                                .describe(&func[0])
                                .with_span(self.span())]
                        })?;
                        builder::getvar(ctx, function, bb, var)
                    };
                    ctx.tailcall_disable();
                    let mut args = vec![];
                    for arg in &rest {
                        let arg = arg.compile(ctx, function, bb)?.unwrap();
                        builder::push(ctx, function, bb, arg);
                        args.push(arg);
                    }
                    let value = unsafe {
                        let value_type = builder::value_ptr_type(ctx);
                        let (argc, argv) = builder::array(ctx, value_type, &args);
                        let argc = LLVMConstIntCast(argc, builder::u32_type(ctx), 0);
                        let rt = builder::rt(function);
                        let args = &mut [rt, func, argc, argv];
                        let function = LLVMGetNamedFunction(ctx.module, b"ehclosure_call\0".as_ptr() as _);
                        LLVMBuildCall(
                            ctx.builder,
                            function,
                            args.as_mut_ptr(),
                            args.len() as _,
                            b"app\0".as_ptr() as _,
                        )
                    };
                    ctx.tailcall_pop();
                    Ok(Some(value))
                }
                Function::Rec(func) => {
                    let path = func;
                    let func = if path.len() > 1 || ctx.named.contains(&path[0]) {
                        builder::gc_disable(ctx, function, bb);
                        let atom = builder::atom(ctx, function, bb, path[0].as_str());
                        let mut var = builder::get(ctx, function, bb, atom);
                        for path in &path[1..] {
                            let atom = builder::atom(ctx, function, bb, path.as_str());
                            var = builder::getfrom(ctx, function, bb, var, atom);
                        }
                        builder::gc_enable(ctx, function, bb);
                        builder::getvar(ctx, function, bb, var)
                    } else {
                        let var = ctx.binding(&path[0]).ok_or_else(|| {
                            vec![Error::new(ErrorKind::UndefinedSymbol)
                                .describe(&path[0])
                                .with_span(self.span())]
                        })?;
                        builder::getvar(ctx, function, bb, var)
                    };
                    ctx.tailcall_disable();
                    let mut args = vec![];
                    for arg in &rest {
                        let arg = arg.compile(ctx, function, bb)?.unwrap();
                        builder::push(ctx, function, bb, arg);
                        args.push(arg);
                    }
                    ctx.tailcall_pop();
                    let value = unsafe {
                        let value_type = builder::value_ptr_type(ctx);
                        let (argc, argv) = builder::array(ctx, value_type, &args);
                        let argc = LLVMConstIntCast(argc, builder::u32_type(ctx), 0);
                        let rt = builder::rt(function);
                        let args = &mut [rt, func, argc, argv];
                        let function = if ctx.tailcall && ctx.tailstack.contains(&path) {
                            LLVMGetNamedFunction(ctx.module, b"ehclosure_tailcall\0".as_ptr() as _)
                        } else {
                            LLVMGetNamedFunction(ctx.module, b"ehclosure_call\0".as_ptr() as _)
                        };
                        LLVMBuildCall(
                            ctx.builder,
                            function,
                            args.as_mut_ptr(),
                            args.len() as _,
                            b"app\0".as_ptr() as _,
                        )
                    };
                    Ok(Some(value))
                }
                Function::Closure(func) => {
                    ctx.tailcall_disable();
                    let mut args = vec![];
                    for arg in &rest {
                        let arg = arg.compile(ctx, function, bb)?.unwrap();
                        builder::push(ctx, function, bb, arg);
                        args.push(arg);
                    }
                    let value = unsafe {
                        let value_type = builder::value_ptr_type(ctx);
                        let (argc, argv) = builder::array(ctx, value_type, &args);
                        let argc = LLVMConstIntCast(argc, builder::u32_type(ctx), 0);
                        let rt = builder::rt(function);
                        let args = &mut [rt, func, argc, argv];
                        let function = LLVMGetNamedFunction(ctx.module, b"ehclosure_call\0".as_ptr() as _);
                        LLVMBuildCall(
                            ctx.builder,
                            function,
                            args.as_mut_ptr(),
                            args.len() as _,
                            b"app\0".as_ptr() as _,
                        )
                    };
                    ctx.tailcall_pop();
                    Ok(Some(value))
                }
            }
        };
        if let Ok(Some(value)) = result {
            builder::push(ctx, function, bb, value);
            Ok(Some(value))
        } else {
            result
        }
    }
}

fn compile_let_fun<T: ExprLike>(
    expr: &T,
    ctx: &mut CompilerContext,
    function: LLVMValueRef,
    bb: LLVMBasicBlockRef,
) -> Result<Option<LLVMValueRef>, Vec<Error>> {
    let binding = match &expr.rest().get(1).unwrap_or_else(|| unreachable!()).kind {
        ast::WordKind::Atom(atom) => &atom.atom,
        _ => unreachable!(),
    };
    let body = expr.as_meta().def.as_ref().unwrap();
    let paramv = expr.as_meta().params.as_ref().unwrap();

    let mut errors = vec![];

    ctx.enter(body.borrow().to_ident());

    let mut free = HashSet::new();
    let mut bindings = HashSet::new();
    expr.find_free(ctx, &mut bindings, &mut free).unwrap_or_else(|errs| {
        errors.extend(errs);
    });
    let mut freev = free.into_iter().collect::<Vec<_>>();
    freev.sort_unstable();
    let free = freev
        .iter()
        .map(|free| {
            ctx.binding(free.as_str()).ok_or_else(|| {
                vec![Error::new(ErrorKind::UndefinedSymbol)
                    .describe(free)
                    .with_span(expr.span())]
            })
        })
        .fold(Ok(vec![]), |acc: Result<Vec<_>, Vec<_>>, e| {
            match acc {
                Ok(mut acc) => {
                    acc.push(e?);
                    Ok(acc)
                }
                Err(mut err) => {
                    match e {
                        Ok(_) => Err(err),
                        Err(e) => {
                            err.extend(e);
                            Err(err)
                        }
                    }
                }
            }
        })?;

    let closure = unsafe {
        let i8_type = builder::byte_type(ctx);
        let rt_type = LLVMPointerType(i8_type, 0);
        let value_type = builder::value_ptr_type(ctx);
        let datav_type = LLVMPointerType(value_type, 0);
        let argv_type = LLVMPointerType(value_type, 0);
        let params = &mut [rt_type, datav_type, argv_type];
        let function_type = LLVMFunctionType(value_type, params.as_mut_ptr(), params.len() as _, 0);
        let function_name = body.borrow().to_ident();
        let name_c = CString::new(function_name.as_str())
            .map_err(|_| vec![Error::new(ErrorKind::InvalidSymbol).describe(function_name)])?;
        let function = LLVMAddFunction(ctx.module, name_c.as_ptr(), function_type);

        ctx.add_debug(Debug {
            ptr: function,
            line: expr.span().line(),
            col: expr.span().col(),
            file: expr.span().file().to_string(),
            ident: binding.to_string(),
        });

        ctx.symbols.push(name_c);
        LLVMSetFunctionCallConv(function, LLVMCallConv::LLVMCCallConv as _);
        let block = LLVMAppendBasicBlockInContext(ctx.context, function, b"init\0".as_ptr() as _);
        LLVMPositionBuilderAtEnd(ctx.builder, block);

        let mut i = 0;
        for param in &*paramv.borrow() {
            param
                .compile_arg(ctx, function, block, &mut i)
                .map(|_| ())
                .unwrap_or_else(|err| errors.extend(err));
        }
        let datav = LLVMGetParam(function, 1);
        let usize_type = builder::usize_type(ctx);
        for (i, name) in freev.iter().enumerate() {
            let name_c =
                CString::new(name.as_str()).map_err(|_| vec![Error::new(ErrorKind::InvalidSymbol).describe(name)])?;
            let index = &mut [LLVMConstInt(usize_type, i as _, 0)];
            let ptr = LLVMBuildGEP(
                ctx.builder,
                datav,
                index.as_mut_ptr(),
                index.len() as _,
                b"ptr\0".as_ptr() as _,
            );
            let value = LLVMBuildLoad(ctx.builder, ptr, name_c.as_ptr());
            ctx.insert_binding(name.to_string(), value);
        }
        let result = body.borrow().compile(ctx, function, block)?;
        if let Some(result) = result {
            LLVMBuildRet(ctx.builder, result);
        } else {
            let nil = builder::nil(ctx, function, block);
            LLVMBuildRet(ctx.builder, nil);
        }

        if LLVMVerifyFunction(function, LLVMVerifierFailureAction::LLVMPrintMessageAction) == 1 {
            return Err(vec![Error::new(ErrorKind::LLVM).describe("this function is invalid")]);
        }
        if let Some(fpm) = ctx.fpm {
            LLVMRunFunctionPassManager(fpm, function);
        }

        function
    };
    ctx.leave();
    unsafe {
        LLVMPositionBuilderAtEnd(ctx.builder, bb);
    }

    let mut params = vec![];
    for param in &*paramv.borrow() {
        param
            .compile_param(ctx, function, bb)
            .map(|param| {
                params.push(param.unwrap());
            })
            .unwrap_or_else(|err| {
                errors.extend(err);
            })
    }
    if errors.is_empty() {
        if ctx.is_multi(&[binding.clone()]) {
            builder::gc_disable(ctx, function, bb);
            let nil = builder::nil(ctx, function, bb);
            let cimpl = builder::function(ctx, function, bb, closure, &free, &params);
            let var = ctx.binding(&binding).unwrap();
            let value = builder::getvar(ctx, function, bb, var);
            let closure = builder::closure_impl(ctx, function, bb, value, cimpl);
            builder::setvar(ctx, function, bb, var, closure);
            builder::gc_enable(ctx, function, bb);

            Ok(Some(nil))
        } else {
            builder::gc_disable(ctx, function, bb);
            let nil = builder::nil(ctx, function, bb);
            let implv = &[builder::function(ctx, function, bb, closure, &free, &params)];
            let closure = builder::closure_lexical(ctx, function, bb, implv);
            let var = builder::var(ctx, function, bb, closure);
            builder::gc_enable(ctx, function, bb);

            builder::push(ctx, function, bb, var);
            ctx.insert_binding(binding.to_string(), var);
            ctx.insert_multi(vec![binding.clone()]);
            ctx.insert_function(vec![binding.clone()]);

            Ok(Some(nil))
        }
    } else {
        Err(errors)
    }
}

fn compile_let_rec<T: ExprLike>(
    expr: &T,
    ctx: &mut CompilerContext,
    function: LLVMValueRef,
    bb: LLVMBasicBlockRef,
) -> Result<Option<LLVMValueRef>, Vec<Error>> {
    let binding = match &expr.rest().get(1).unwrap_or_else(|| unreachable!()).kind {
        ast::WordKind::Atom(atom) => &atom.atom,
        _ => unreachable!(),
    };
    let body = expr.as_meta().def.as_ref().unwrap();
    let paramv = expr.as_meta().params.as_ref().unwrap();

    let mut errors = vec![];

    ctx.enter(body.borrow().to_ident());

    let var = if !ctx.is_multi(&[binding.clone()]) {
        builder::gc_disable(ctx, function, bb);
        let nil = builder::nil(ctx, function, bb);
        let var = builder::var(ctx, function, bb, nil);
        builder::gc_enable(ctx, function, bb);
        builder::push(ctx, function, bb, var);
        ctx.insert_binding(binding.to_string(), var);
        ctx.insert_recursive(vec![binding.clone()]);
        Some(var)
    } else {
        None
    };

    let mut free = HashSet::new();
    let mut bindings = HashSet::new();
    expr.find_free(ctx, &mut bindings, &mut free).unwrap_or_else(|errs| {
        errors.extend(errs);
    });
    let mut freev = free.into_iter().collect::<Vec<_>>();
    freev.sort_unstable();
    let free = freev
        .iter()
        .map(|free| {
            ctx.binding(free.as_str()).ok_or_else(|| {
                vec![Error::new(ErrorKind::UndefinedSymbol)
                    .describe(free)
                    .with_span(expr.span())]
            })
        })
        .fold(Ok(vec![]), |acc: Result<Vec<_>, Vec<_>>, e| {
            match acc {
                Ok(mut acc) => {
                    acc.push(e?);
                    Ok(acc)
                }
                Err(mut err) => {
                    match e {
                        Ok(_) => Err(err),
                        Err(e) => {
                            err.extend(e);
                            Err(err)
                        }
                    }
                }
            }
        })?;

    let closure = unsafe {
        let i8_type = builder::byte_type(ctx);
        let rt_type = LLVMPointerType(i8_type, 0);
        let value_type = builder::value_ptr_type(ctx);
        let datav_type = LLVMPointerType(value_type, 0);
        let argv_type = LLVMPointerType(value_type, 0);
        let params = &mut [rt_type, datav_type, argv_type];
        let function_type = LLVMFunctionType(value_type, params.as_mut_ptr(), params.len() as _, 0);
        let function_name = body.borrow().to_ident();
        let name_c = CString::new(function_name.as_str())
            .map_err(|_| vec![Error::new(ErrorKind::InvalidSymbol).describe(function_name)])?;
        let function = LLVMAddFunction(ctx.module, name_c.as_ptr(), function_type);

        ctx.add_debug(Debug {
            ptr: function,
            line: expr.span().line(),
            col: expr.span().col(),
            file: expr.span().file().to_string(),
            ident: binding.to_string(),
        });

        ctx.symbols.push(name_c);
        LLVMSetFunctionCallConv(function, LLVMCallConv::LLVMCCallConv as _);
        let block = LLVMAppendBasicBlockInContext(ctx.context, function, b"init\0".as_ptr() as _);
        LLVMPositionBuilderAtEnd(ctx.builder, block);

        let mut i = 0;
        for param in &*paramv.borrow() {
            param
                .compile_arg(ctx, function, block, &mut i)
                .map(|_| ())
                .unwrap_or_else(|err| errors.extend(err));
        }
        let datav = LLVMGetParam(function, 1);
        let usize_type = builder::usize_type(ctx);
        for (i, name) in freev.iter().enumerate() {
            let name_c =
                CString::new(name.as_str()).map_err(|_| vec![Error::new(ErrorKind::InvalidSymbol).describe(name)])?;
            let index = &mut [LLVMConstInt(usize_type, i as _, 0)];
            let ptr = LLVMBuildGEP(
                ctx.builder,
                datav,
                index.as_mut_ptr(),
                index.len() as _,
                b"ptr\0".as_ptr() as _,
            );
            let value = LLVMBuildLoad(ctx.builder, ptr, name_c.as_ptr());
            ctx.insert_binding(name.to_string(), value);
        }
        ctx.tailstack.push(vec![binding.clone()]);
        ctx.tailcall_enable();
        let result = body.borrow().compile(ctx, function, block)?;
        ctx.tailcall_pop();
        ctx.tailstack.pop();
        if let Some(result) = result {
            LLVMBuildRet(ctx.builder, result);
        } else {
            let nil = builder::nil(ctx, function, block);
            LLVMBuildRet(ctx.builder, nil);
        }

        if LLVMVerifyFunction(function, LLVMVerifierFailureAction::LLVMPrintMessageAction) == 1 {
            return Err(vec![Error::new(ErrorKind::LLVM).describe("this function is invalid")]);
        }
        if let Some(fpm) = ctx.fpm {
            LLVMRunFunctionPassManager(fpm, function);
        }

        function
    };
    ctx.leave();
    unsafe {
        LLVMPositionBuilderAtEnd(ctx.builder, bb);
    }

    let mut params = vec![];
    builder::gc_disable(ctx, function, bb);
    for param in &*paramv.borrow() {
        param
            .compile_param(ctx, function, bb)
            .map(|param| {
                params.push(param.unwrap());
            })
            .unwrap_or_else(|err| {
                errors.extend(err);
            })
    }
    if errors.is_empty() {
        if ctx.is_multi(&[binding.clone()]) {
            let nil = builder::nil(ctx, function, bb);
            let cimpl = builder::function(ctx, function, bb, closure, &free, &params);
            let var = ctx.binding(&binding).unwrap();
            let value = builder::getvar(ctx, function, bb, var);
            let closure = builder::closure_impl(ctx, function, bb, value, cimpl);
            builder::setvar(ctx, function, bb, var, closure);
            builder::gc_enable(ctx, function, bb);

            Ok(Some(nil))
        } else {
            let var = var.unwrap();
            builder::gc_disable(ctx, function, bb);
            let nil = builder::nil(ctx, function, bb);
            let implv = &[builder::function(ctx, function, bb, closure, &free, &params)];
            let closure = builder::closure_lexical(ctx, function, bb, implv);
            builder::setvar(ctx, function, bb, var, closure);
            builder::gc_enable(ctx, function, bb);

            ctx.insert_binding(binding.to_string(), var);
            ctx.insert_multi(vec![binding.clone()]);
            ctx.insert_recursive(vec![binding.clone()]);

            Ok(Some(nil))
        }
    } else {
        Err(errors)
    }
}

fn compile_let_bind<T: ExprLike>(
    expr: &T,
    ctx: &mut CompilerContext,
    function: LLVMValueRef,
    bb: LLVMBasicBlockRef,
) -> Result<Option<LLVMValueRef>, Vec<Error>> {
    let binding = match &expr.rest().get(0).unwrap_or_else(|| unreachable!()).kind {
        ast::WordKind::Atom(atom) => &atom.atom,
        _ => unreachable!(),
    };

    let def = expr.as_meta().def.as_ref().unwrap();
    let var = def.borrow().compile(ctx, function, bb)?.unwrap();
    ctx.insert_binding(binding.to_string(), var);
    builder::push(ctx, function, bb, var);
    Ok(Some(builder::nil(ctx, function, bb)))
}

fn compile_def_fun<T: ExprLike>(
    expr: &T,
    ctx: &mut CompilerContext,
    function: LLVMValueRef,
    bb: LLVMBasicBlockRef,
) -> Result<Option<LLVMValueRef>, Vec<Error>> {
    let binding = match &expr.rest().get(1).unwrap_or_else(|| unreachable!()).kind {
        ast::WordKind::Atom(atom) => &atom.atom,
        _ => unreachable!(),
    };
    let body = expr.as_meta().def.as_ref().unwrap();
    let paramv = expr.as_meta().params.as_ref().unwrap();

    let mut errors = vec![];

    ctx.enter(body.borrow().to_ident());

    let mut free = HashSet::new();
    let mut bindings = HashSet::new();
    expr.find_free(ctx, &mut bindings, &mut free).unwrap_or_else(|errs| {
        errors.extend(errs);
    });
    let mut freev = free.into_iter().collect::<Vec<_>>();
    freev.sort_unstable();
    let free = freev
        .iter()
        .map(|free| {
            ctx.binding(free.as_str()).ok_or_else(|| {
                vec![Error::new(ErrorKind::UndefinedSymbol)
                    .describe(free)
                    .with_span(expr.span())]
            })
        })
        .fold(Ok(vec![]), |acc: Result<Vec<_>, Vec<_>>, e| {
            match acc {
                Ok(mut acc) => {
                    acc.push(e?);
                    Ok(acc)
                }
                Err(mut err) => {
                    match e {
                        Ok(_) => Err(err),
                        Err(e) => {
                            err.extend(e);
                            Err(err)
                        }
                    }
                }
            }
        })?;

    let closure = unsafe {
        let i8_type = builder::byte_type(ctx);
        let rt_type = LLVMPointerType(i8_type, 0);
        let value_type = builder::value_ptr_type(ctx);
        let datav_type = LLVMPointerType(value_type, 0);
        let argv_type = LLVMPointerType(value_type, 0);
        let params = &mut [rt_type, datav_type, argv_type];
        let function_type = LLVMFunctionType(value_type, params.as_mut_ptr(), params.len() as _, 0);
        let function_name = body.borrow().to_ident();
        let name_c = CString::new(function_name.as_str())
            .map_err(|_| vec![Error::new(ErrorKind::InvalidSymbol).describe(function_name)])?;
        let function = LLVMAddFunction(ctx.module, name_c.as_ptr(), function_type);

        ctx.add_debug(Debug {
            ptr: function,
            line: expr.span().line(),
            col: expr.span().col(),
            file: expr.span().file().to_string(),
            ident: binding.to_string(),
        });

        ctx.symbols.push(name_c);
        LLVMSetFunctionCallConv(function, LLVMCallConv::LLVMCCallConv as _);
        let block = LLVMAppendBasicBlockInContext(ctx.context, function, b"init\0".as_ptr() as _);
        LLVMPositionBuilderAtEnd(ctx.builder, block);

        let mut i = 0;
        for param in &*paramv.borrow() {
            param
                .compile_arg(ctx, function, block, &mut i)
                .map(|_| ())
                .unwrap_or_else(|err| errors.extend(err));
        }
        let datav = LLVMGetParam(function, 1);
        let usize_type = builder::usize_type(ctx);
        for (i, name) in freev.iter().enumerate() {
            let name_c =
                CString::new(name.as_str()).map_err(|_| vec![Error::new(ErrorKind::InvalidSymbol).describe(name)])?;
            let index = &mut [LLVMConstInt(usize_type, i as _, 0)];
            let ptr = LLVMBuildGEP(
                ctx.builder,
                datav,
                index.as_mut_ptr(),
                index.len() as _,
                b"ptr\0".as_ptr() as _,
            );
            let value = LLVMBuildLoad(ctx.builder, ptr, name_c.as_ptr());
            ctx.insert_binding(name.to_string(), value);
        }
        let result = body.borrow().compile(ctx, function, block)?;
        if let Some(result) = result {
            LLVMBuildRet(ctx.builder, result);
        } else {
            let nil = builder::nil(ctx, function, block);
            LLVMBuildRet(ctx.builder, nil);
        }

        if LLVMVerifyFunction(function, LLVMVerifierFailureAction::LLVMPrintMessageAction) == 1 {
            return Err(vec![Error::new(ErrorKind::LLVM).describe("this function is invalid")]);
        }
        if let Some(fpm) = ctx.fpm {
            LLVMRunFunctionPassManager(fpm, function);
        }

        function
    };
    ctx.leave();
    unsafe {
        LLVMPositionBuilderAtEnd(ctx.builder, bb);
    }

    let mut params = vec![];
    for param in &*paramv.borrow() {
        param
            .compile_param(ctx, function, bb)
            .map(|param| {
                params.push(param.unwrap());
            })
            .unwrap_or_else(|err| {
                errors.extend(err);
            })
    }
    if errors.is_empty() {
        if ctx.is_multi(&[binding.clone()]) {
            builder::gc_disable(ctx, function, bb);
            let nil = builder::nil(ctx, function, bb);
            let cimpl = builder::function(ctx, function, bb, closure, &free, &params);
            let atom = builder::atom(ctx, function, bb, binding.as_str());
            let var = builder::get(ctx, function, bb, atom);
            let value = builder::getvar(ctx, function, bb, var);
            let closure = builder::closure_impl(ctx, function, bb, value, cimpl);
            builder::setvar(ctx, function, bb, var, closure);
            builder::gc_enable(ctx, function, bb);

            Ok(Some(nil))
        } else {
            builder::gc_disable(ctx, function, bb);
            let nil = builder::nil(ctx, function, bb);
            let implv = &[builder::function(ctx, function, bb, closure, &free, &params)];
            let closure = builder::closure_lexical(ctx, function, bb, implv);
            let var = builder::var(ctx, function, bb, closure);
            let atom = builder::atom(ctx, function, bb, binding.as_str());
            builder::set(ctx, function, bb, atom, var);
            builder::gc_enable(ctx, function, bb);

            ctx.insert_named(binding.clone());
            ctx.insert_multi(vec![binding.clone()]);
            ctx.insert_function(vec![binding.clone()]);

            Ok(Some(nil))
        }
    } else {
        Err(errors)
    }
}

fn compile_def_rec<T: ExprLike>(
    expr: &T,
    ctx: &mut CompilerContext,
    function: LLVMValueRef,
    bb: LLVMBasicBlockRef,
) -> Result<Option<LLVMValueRef>, Vec<Error>> {
    let binding = match &expr.rest().get(1).unwrap_or_else(|| unreachable!()).kind {
        ast::WordKind::Atom(atom) => &atom.atom,
        _ => unreachable!(),
    };
    let body = expr.as_meta().def.as_ref().unwrap();
    let paramv = expr.as_meta().params.as_ref().unwrap();

    let mut errors = vec![];

    ctx.enter(body.borrow().to_ident());

    let var = if !ctx.is_multi(&[binding.clone()]) {
        builder::gc_disable(ctx, function, bb);
        let nil = builder::nil(ctx, function, bb);
        let var = builder::var(ctx, function, bb, nil);
        let atom = builder::atom(ctx, function, bb, binding.as_str());
        builder::set(ctx, function, bb, atom, var);
        builder::gc_enable(ctx, function, bb);
        ctx.insert_named(binding.clone());
        ctx.insert_recursive(vec![binding.clone()]);
        Some(var)
    } else {
        None
    };

    let mut free = HashSet::new();
    let mut bindings = HashSet::new();
    expr.find_free(ctx, &mut bindings, &mut free).unwrap_or_else(|errs| {
        errors.extend(errs);
    });
    let mut freev = free.into_iter().collect::<Vec<_>>();
    freev.sort_unstable();
    let free = freev
        .iter()
        .map(|free| {
            ctx.binding(free.as_str()).ok_or_else(|| {
                vec![Error::new(ErrorKind::UndefinedSymbol)
                    .describe(free)
                    .with_span(expr.span())]
            })
        })
        .fold(Ok(vec![]), |acc: Result<Vec<_>, Vec<_>>, e| {
            match acc {
                Ok(mut acc) => {
                    acc.push(e?);
                    Ok(acc)
                }
                Err(mut err) => {
                    match e {
                        Ok(_) => Err(err),
                        Err(e) => {
                            err.extend(e);
                            Err(err)
                        }
                    }
                }
            }
        })?;

    let closure = unsafe {
        let i8_type = builder::byte_type(ctx);
        let rt_type = LLVMPointerType(i8_type, 0);
        let value_type = builder::value_ptr_type(ctx);
        let datav_type = LLVMPointerType(value_type, 0);
        let argv_type = LLVMPointerType(value_type, 0);
        let params = &mut [rt_type, datav_type, argv_type];
        let function_type = LLVMFunctionType(value_type, params.as_mut_ptr(), params.len() as _, 0);
        let function_name = body.borrow().to_ident();
        let name_c = CString::new(function_name.as_str())
            .map_err(|_| vec![Error::new(ErrorKind::InvalidSymbol).describe(function_name)])?;
        let function = LLVMAddFunction(ctx.module, name_c.as_ptr(), function_type);

        ctx.add_debug(Debug {
            ptr: function,
            line: expr.span().line(),
            col: expr.span().col(),
            file: expr.span().file().to_string(),
            ident: binding.to_string(),
        });

        ctx.symbols.push(name_c);
        LLVMSetFunctionCallConv(function, LLVMCallConv::LLVMCCallConv as _);
        let block = LLVMAppendBasicBlockInContext(ctx.context, function, b"init\0".as_ptr() as _);
        LLVMPositionBuilderAtEnd(ctx.builder, block);

        let mut i = 0;
        for param in &*paramv.borrow() {
            param
                .compile_arg(ctx, function, block, &mut i)
                .map(|_| ())
                .unwrap_or_else(|err| errors.extend(err));
        }
        let datav = LLVMGetParam(function, 1);
        let usize_type = builder::usize_type(ctx);
        for (i, name) in freev.iter().enumerate() {
            let name_c =
                CString::new(name.as_str()).map_err(|_| vec![Error::new(ErrorKind::InvalidSymbol).describe(name)])?;
            let index = &mut [LLVMConstInt(usize_type, i as _, 0)];
            let ptr = LLVMBuildGEP(
                ctx.builder,
                datav,
                index.as_mut_ptr(),
                index.len() as _,
                b"ptr\0".as_ptr() as _,
            );
            let value = LLVMBuildLoad(ctx.builder, ptr, name_c.as_ptr());
            ctx.insert_binding(name.to_string(), value);
        }
        ctx.tailstack.push(vec![binding.clone()]);
        ctx.tailcall_enable();
        let result = body.borrow().compile(ctx, function, block)?;
        ctx.tailcall_pop();
        ctx.tailstack.pop();
        if let Some(result) = result {
            LLVMBuildRet(ctx.builder, result);
        } else {
            let nil = builder::nil(ctx, function, block);
            LLVMBuildRet(ctx.builder, nil);
        }

        if LLVMVerifyFunction(function, LLVMVerifierFailureAction::LLVMPrintMessageAction) == 1 {
            return Err(vec![Error::new(ErrorKind::LLVM).describe("this function is invalid")]);
        }
        if let Some(fpm) = ctx.fpm {
            LLVMRunFunctionPassManager(fpm, function);
        }

        function
    };
    ctx.leave();
    unsafe {
        LLVMPositionBuilderAtEnd(ctx.builder, bb);
    }

    let mut params = vec![];
    for param in &*paramv.borrow() {
        param
            .compile_param(ctx, function, bb)
            .map(|param| {
                params.push(param.unwrap());
            })
            .unwrap_or_else(|err| {
                errors.extend(err);
            })
    }
    if errors.is_empty() {
        if ctx.is_multi(&[binding.clone()]) {
            builder::gc_disable(ctx, function, bb);
            let nil = builder::nil(ctx, function, bb);
            let cimpl = builder::function(ctx, function, bb, closure, &free, &params);
            let var = ctx.binding(&binding).unwrap();
            let value = builder::getvar(ctx, function, bb, var);
            let closure = builder::closure_impl(ctx, function, bb, value, cimpl);
            builder::setvar(ctx, function, bb, var, closure);
            builder::gc_enable(ctx, function, bb);

            Ok(Some(nil))
        } else {
            let var = var.unwrap();
            builder::gc_disable(ctx, function, bb);
            let nil = builder::nil(ctx, function, bb);
            let implv = &[builder::function(ctx, function, bb, closure, &free, &params)];
            let closure = builder::closure_lexical(ctx, function, bb, implv);
            builder::setvar(ctx, function, bb, var, closure);
            builder::gc_enable(ctx, function, bb);

            ctx.insert_named(binding.clone());
            ctx.insert_multi(vec![binding.clone()]);
            ctx.insert_recursive(vec![binding.clone()]);

            Ok(Some(nil))
        }
    } else {
        Err(errors)
    }
}

fn compile_def_bind<T: ExprLike>(
    expr: &T,
    ctx: &mut CompilerContext,
    function: LLVMValueRef,
    bb: LLVMBasicBlockRef,
) -> Result<Option<LLVMValueRef>, Vec<Error>> {
    let binding = match &expr.rest().get(0).unwrap_or_else(|| unreachable!()).kind {
        ast::WordKind::Atom(atom) => &atom.atom,
        _ => unreachable!(),
    };

    let def = expr.as_meta().def.as_ref().unwrap();
    let var = def.borrow().compile(ctx, function, bb)?.unwrap();

    unsafe {
        let rt = builder::rt(function);
        let atom = builder::atom(ctx, function, bb, binding.as_str());
        let args = &mut [rt, atom, var];
        let function = LLVMGetNamedFunction(ctx.module, b"ehrt_set\0".as_ptr() as _);
        LLVMBuildCall(
            ctx.builder,
            function,
            args.as_mut_ptr(),
            args.len() as _,
            b"\0".as_ptr() as _,
        );
    }
    Ok(Some(builder::nil(ctx, function, bb)))
}

fn compile_import<T: ExprLike>(
    _expr: &T,
    module: Vec<RcStr>,
    ctx: &mut CompilerContext,
    function: LLVMValueRef,
    bb: LLVMBasicBlockRef,
) -> Result<Option<LLVMValueRef>, Vec<Error>> {
    let name = RcStr::from(module[1..].iter().fold(module[0].to_string(), |mut acc, path| {
        acc.push('.');
        acc.push_str(path);
        acc
    }));
    let normalized = RcStr::from(normalize_string(&name));

    let name = format!("__{}_init__", normalized);
    let name_c = CString::new(name.as_str()).map_err(|_| vec![Error::new(ErrorKind::InvalidSymbol).describe(name)])?;

    let value = unsafe {
        let i8_type = builder::byte_type(ctx);
        let rt_type = LLVMPointerType(i8_type, 0);
        let value_type = builder::value_ptr_type(ctx);
        let datav_type = LLVMPointerType(value_type, 0);
        let argv_type = LLVMPointerType(value_type, 0);
        let params = &mut [rt_type, datav_type, argv_type];
        let function_type = LLVMFunctionType(value_type, params.as_mut_ptr(), params.len() as _, 0);
        let closure = LLVMAddFunction(ctx.module, name_c.as_ptr(), function_type);
        builder::gc_disable(ctx, function, bb);
        let implv = &[builder::function(ctx, function, bb, closure, &[], &[])];
        let func = builder::closure_lexical(ctx, function, bb, implv);
        builder::gc_enable(ctx, function, bb);
        let rt = builder::rt(function);
        let args = &mut [rt, func];
        let function = LLVMGetNamedFunction(ctx.module, b"ehmod_import\0".as_ptr() as _);
        LLVMBuildCall(
            ctx.builder,
            function,
            args.as_mut_ptr(),
            args.len() as _,
            b"new\0".as_ptr() as _,
        )
    };
    let atom = builder::atom(ctx, function, bb, &normalized);
    builder::set(ctx, function, bb, atom, value);
    ctx.insert_named(normalized);

    let nil = builder::nil(ctx, function, bb);
    Ok(Some(nil))
}

fn compile_from<T: ExprLike>(
    _expr: &T,
    module: Vec<RcStr>,
    imports: Vec<RcStr>,
    ctx: &mut CompilerContext,
    function: LLVMValueRef,
    bb: LLVMBasicBlockRef,
) -> Result<Option<LLVMValueRef>, Vec<Error>> {
    let name = RcStr::from(module[1..].iter().fold(module[0].to_string(), |mut acc, path| {
        acc.push('.');
        acc.push_str(path);
        acc
    }));
    let normalized = RcStr::from(normalize_string(&name));

    let atom = builder::atom(ctx, function, bb, &normalized);
    let value = builder::get(ctx, function, bb, atom);
    for binding in &imports {
        let atom = builder::atom(ctx, function, bb, binding.as_str());
        let import = builder::getfrom(ctx, function, bb, value, atom);
        ctx.insert_binding(binding.to_string(), import);

        let path = module.iter().chain(iter::once(binding)).cloned().collect::<Vec<_>>();
        if ctx.is_function(&path) {
            ctx.insert_function(vec![binding.clone()]);
        }
        if ctx.is_recursive(&path) {
            ctx.insert_recursive(vec![binding.clone()]);
        }
        if ctx.is_multi(&path) {
            ctx.insert_multi(vec![binding.clone()]);
        }
    }

    let nil = builder::nil(ctx, function, bb);
    Ok(Some(nil))
}

fn compile_type<T: ExprLike>(
    expr: &T,
    ctx: &mut CompilerContext,
    function: LLVMValueRef,
    bb: LLVMBasicBlockRef,
) -> Result<Option<LLVMValueRef>, Vec<Error>> {
    let word = expr.rest().get(0).ok_or_else(|| {
        vec![Error::new(ErrorKind::TypeSyntax)
            .describe("missing type name")
            .with_span(expr.span())]
    })?;
    let ty = match &word.kind {
        ast::WordKind::TypeAtom(ty) => {
            match &ty.kind {
                ast::TypeWordKind::Type(ty) => &ty.atom,
                _ => unimplemented!(),
            }
        }
        _ => {
            return Err(vec![Error::new(ErrorKind::TypeSyntax)
                .describe("type name must be a type atom")
                .with_span(word.span())])
        }
    };

    if expr.rest().len() == 1 {
        let nil = builder::nil(ctx, function, bb);
        return Ok(Some(nil));
    }

    let is = expr.rest().get(1).unwrap();
    match &is.kind {
        ast::WordKind::Atom(atom) if atom.atom.as_str() == "is:" => {}
        _ => {
            return Err(vec![Error::new(ErrorKind::TypeSyntax)
                .describe("missing `is:`")
                .with_span(is.span())])
        }
    }

    let block = expr.rest().get(2).ok_or_else(|| {
        vec![Error::new(ErrorKind::TypeSyntax)
            .describe("missing type definition")
            .with_span(expr.span())]
    })?;
    let block = match &block.kind {
        ast::WordKind::Compound(block) => block,
        _ => {
            return Err(vec![Error::new(ErrorKind::TypeSyntax)
                .describe("missing type definition")
                .with_span(block.span())])
        }
    };

    let typ = Type::from(ty.clone());
    let type_id = *ctx.types.get(&typ).unwrap();
    compile_type_def(&*block.borrow(), ty, type_id, ctx, function, bb)?;

    let nil = builder::nil(ctx, function, bb);
    Ok(Some(nil))
}

fn compile_type_def<T: BlockLike>(
    block: &T,
    name: &RcStr,
    type_id: u32,
    ctx: &mut CompilerContext,
    function: LLVMValueRef,
    bb: LLVMBasicBlockRef,
) -> Result<(), Vec<Error>> {
    let mut variant_id = 0;
    for stmt in block.stmts() {
        compile_type_variant(stmt.expr(), &name, type_id, variant_id, ctx, function, bb)?;
        variant_id += 1;
    }
    if let Some(expr) = block.expr() {
        compile_type_variant(expr, &name, type_id, variant_id, ctx, function, bb)?;
    }
    Ok(())
}

fn compile_type_variant<T: ExprLike>(
    expr: &T,
    name: &RcStr,
    type_id: u32,
    variant_id: u32,
    ctx: &mut CompilerContext,
    function: LLVMValueRef,
    bb: LLVMBasicBlockRef,
) -> Result<(), Vec<Error>> {
    match &expr.first().kind {
        ast::WordKind::Destructure(variant, paramv) => {
            let variant = &variant.first.atom;
            let mut errors = vec![];

            ctx.enter(expr.to_ident());

            let closure = unsafe {
                let i8_type = builder::byte_type(ctx);
                let rt_type = LLVMPointerType(i8_type, 0);
                let value_type = builder::value_ptr_type(ctx);
                let datav_type = LLVMPointerType(value_type, 0);
                let argv_type = LLVMPointerType(value_type, 0);
                let params = &mut [rt_type, datav_type, argv_type];
                let function_type = LLVMFunctionType(value_type, params.as_mut_ptr(), params.len() as _, 0);
                let function_name = expr.first().to_ident();
                let name_c = CString::new(function_name.as_str())
                    .map_err(|_| vec![Error::new(ErrorKind::InvalidSymbol).describe(function_name)])?;
                let function = LLVMAddFunction(ctx.module, name_c.as_ptr(), function_type);
                ctx.symbols.push(name_c);
                LLVMSetFunctionCallConv(function, LLVMCallConv::LLVMCCallConv as _);
                let block = LLVMAppendBasicBlockInContext(ctx.context, function, b"init\0".as_ptr() as _);
                LLVMPositionBuilderAtEnd(ctx.builder, block);

                let mut i = 0;
                let mut args = vec![];
                for param in paramv {
                    param
                        .compile_arg(ctx, function, block, &mut i)
                        .map(|arg| args.extend(arg))
                        .unwrap_or_else(|err| errors.extend(err));
                }
                let data = builder::data(ctx, function, block, type_id, variant_id, &args);
                LLVMBuildRet(ctx.builder, data);
                function
            };
            ctx.leave();
            unsafe {
                LLVMPositionBuilderAtEnd(ctx.builder, bb);
            }

            let mut params = vec![];
            for param in paramv {
                param
                    .compile_param(ctx, function, bb)
                    .map(|param| {
                        params.push(param.unwrap());
                    })
                    .unwrap_or_else(|err| {
                        errors.extend(err);
                    })
            }
            if errors.is_empty() {
                let path = vec![name.clone(), variant.clone()];
                let binding = path[1..].iter().fold(path[0].to_string(), |mut acc, path| {
                    acc.push('.');
                    acc.push_str(path);
                    acc
                });
                let binding = normalize_string(&binding);

                builder::gc_disable(ctx, function, bb);
                let implv = &[builder::function(ctx, function, bb, closure, &[], &params)];
                let atom = builder::atom(ctx, function, bb, &binding);
                let closure = builder::closure_lexical(ctx, function, bb, implv);
                builder::set(ctx, function, bb, atom, closure);
                builder::gc_enable(ctx, function, bb);

                ctx.insert_cons(vec![variant.clone()]);

                Ok(())
            } else {
                Err(errors)
            }
        }
        _ => {
            return Err(vec![Error::new(ErrorKind::TypeSyntax)
                .describe("variants must be declared using the destructure syntax")
                .with_span(expr.span())])
        }
    }
}

impl Compile<()> for ast::Word {
    fn compile(
        &self,
        ctx: &mut CompilerContext,
        function: LLVMValueRef,
        bb: LLVMBasicBlockRef,
    ) -> Result<Option<LLVMValueRef>, Vec<Error>> {
        match &self.kind {
            ast::WordKind::Atom(atom) => atom.compile(ctx, function, bb),
            ast::WordKind::Path(path) => path.compile(ctx, function, bb),
            ast::WordKind::Integer(int) => Ok(Some(builder::int(ctx, function, bb, *int))),
            ast::WordKind::Real(real) => Ok(Some(builder::real(ctx, function, bb, *real))),
            ast::WordKind::Ratio(sign, p, q) => Ok(Some(builder::ratio(ctx, function, bb, *sign, *p, *q))),
            ast::WordKind::Char(ch) => Ok(Some(builder::character(ctx, function, bb, *ch as _))),
            ast::WordKind::Str(string) => string.compile(ctx, function, bb),
            ast::WordKind::TypeAtom(ty) => ty.compile(ctx, function, bb),
            ast::WordKind::Quote(inner) => inner.borrow().compile_uneval(ctx, function, bb),
            ast::WordKind::Quasiquote(inner) => inner.borrow().compile_quasi(ctx, function, bb),
            ast::WordKind::Interpolate(..) => Err(vec![Error::new(ErrorKind::PatternInExpr).with_span(self.span())]),
            ast::WordKind::Empty => Ok(Some(builder::nil(ctx, function, bb))),
            ast::WordKind::Parenth(expr) => expr.borrow().compile(ctx, function, bb),
            ast::WordKind::Bind(_, optty, pred, _) => {
                let ty = optty.as_ref().map(|ty| From::from(ty));
                let ty = ty
                    .map(|ty| {
                        ctx.types.get(&ty).cloned().ok_or_else(|| {
                            vec![Error::new(ErrorKind::UndefinedType).with_span(optty.as_ref().unwrap().span())]
                        })
                    })
                    .transpose()?;
                let ty = ty.unwrap_or(EH_ANY);
                let pred = pred
                    .as_ref()
                    .map(|pred| pred.borrow().compile(ctx, function, bb))
                    .transpose()?
                    .and_then(|opt| opt);
                let value = builder::bind(ctx, function, bb, ty, pred);
                Ok(Some(value))
            }
            ast::WordKind::Destructure(..) => Err(vec![Error::new(ErrorKind::PatternInExpr).with_span(self.span())]),
            ast::WordKind::Closure(closure) => {
                if closure.len() == 1 {
                    closure[0].compile(ctx, function, bb)
                } else {
                    compile_multi(closure, ctx, function, bb)
                }
            }
            ast::WordKind::Compound(block) => block.borrow().compile(ctx, function, bb),
        }
    }
}

impl Uneval<()> for ast::Word {
    fn compile_uneval(
        &self,
        ctx: &mut CompilerContext,
        function: LLVMValueRef,
        bb: LLVMBasicBlockRef,
    ) -> Result<Option<LLVMValueRef>, Vec<Error>> {
        match &self.kind {
            ast::WordKind::Atom(atom) => atom.compile_uneval(ctx, function, bb),
            ast::WordKind::Path(path) => path.compile_uneval(ctx, function, bb),
            ast::WordKind::TypeAtom(ty) => ty.compile_uneval(ctx, function, bb),
            ast::WordKind::Quote(..) => {
                Err(vec![Error::new(ErrorKind::UnexpectedWord)
                    .describe("quote in quote")
                    .with_span(self.span())])
            }
            ast::WordKind::Quasiquote(..) => {
                Err(vec![Error::new(ErrorKind::UnexpectedWord)
                    .describe("quasiquote in quote")
                    .with_span(self.span())])
            }
            ast::WordKind::Interpolate(..) => {
                Err(vec![Error::new(ErrorKind::UnexpectedWord)
                    .describe("interpolate in quote")
                    .with_span(self.span())])
            }
            ast::WordKind::Empty => Ok(Some(builder::list(ctx, function, bb))),
            ast::WordKind::Parenth(expr) => {
                let mut errors = vec![];
                let mut list = vec![];
                expr.borrow()
                    .first()
                    .compile_uneval(ctx, function, bb)
                    .map(|word| list.push(word.unwrap()))
                    .unwrap_or_else(|err| errors.extend(err));
                for word in expr.borrow().rest() {
                    word.compile_uneval(ctx, function, bb)
                        .map(|word| list.push(word.unwrap()))
                        .unwrap_or_else(|err| errors.extend(err));
                }
                if errors.is_empty() {
                    builder::gc_disable(ctx, function, bb);
                    let mut ret = builder::list(ctx, function, bb);
                    for &value in list.iter().rev() {
                        ret = builder::cons(ctx, function, bb, value, ret);
                    }
                    builder::gc_enable(ctx, function, bb);
                    Ok(Some(ret))
                } else {
                    Err(errors)
                }
            }
            ast::WordKind::Compound(..) => {
                Err(vec![Error::new(ErrorKind::UnexpectedWord)
                    .describe("compound in quote")
                    .with_span(self.span())])
            }
            _ => self.compile(ctx, function, bb),
        }
    }
}

impl Quasi<()> for ast::Word {
    fn compile_quasi(
        &self,
        ctx: &mut CompilerContext,
        function: LLVMValueRef,
        bb: LLVMBasicBlockRef,
    ) -> Result<Option<LLVMValueRef>, Vec<Error>> {
        match &self.kind {
            ast::WordKind::Interpolate(inner) => inner.borrow().compile(ctx, function, bb),
            ast::WordKind::Parenth(expr) => {
                let mut errors = vec![];
                let mut list = vec![];
                expr.borrow()
                    .first()
                    .compile_quasi(ctx, function, bb)
                    .map(|word| list.push(word.unwrap()))
                    .unwrap_or_else(|err| errors.extend(err));
                for word in expr.borrow().rest() {
                    word.compile_quasi(ctx, function, bb)
                        .map(|word| list.push(word.unwrap()))
                        .unwrap_or_else(|err| errors.extend(err));
                }
                if errors.is_empty() {
                    builder::gc_disable(ctx, function, bb);
                    let mut ret = builder::list(ctx, function, bb);
                    for &value in list.iter().rev() {
                        ret = builder::cons(ctx, function, bb, value, ret);
                    }
                    builder::gc_enable(ctx, function, bb);
                    Ok(Some(ret))
                } else {
                    Err(errors)
                }
            }
            _ => self.compile_uneval(ctx, function, bb),
        }
    }
}

impl Param<()> for ast::Word {
    fn compile_param(
        &self,
        ctx: &mut CompilerContext,
        function: LLVMValueRef,
        bb: LLVMBasicBlockRef,
    ) -> Result<Option<LLVMValueRef>, Vec<Error>> {
        match &self.kind {
            ast::WordKind::Quote(..) => self.compile(ctx, function, bb),
            ast::WordKind::Quasiquote(..) => self.compile(ctx, function, bb),
            ast::WordKind::Empty => Ok(Some(builder::nil(ctx, function, bb))),
            ast::WordKind::Parenth(expr) => {
                let mut errors = vec![];
                let mut list = vec![];
                expr.borrow()
                    .first()
                    .compile_param(ctx, function, bb)
                    .map(|word| list.push(word.unwrap()))
                    .unwrap_or_else(|err| errors.extend(err));
                for word in expr.borrow().rest() {
                    word.compile_param(ctx, function, bb)
                        .map(|word| list.push(word.unwrap()))
                        .unwrap_or_else(|err| errors.extend(err));
                }
                if errors.is_empty() {
                    builder::gc_disable(ctx, function, bb);
                    let mut ret = builder::list(ctx, function, bb);
                    for &value in list.iter().rev() {
                        ret = builder::cons(ctx, function, bb, value, ret);
                    }
                    builder::gc_enable(ctx, function, bb);
                    Ok(Some(ret))
                } else {
                    Err(errors)
                }
            }
            ast::WordKind::Destructure(optty, words) => {
                let ty = From::from(optty);
                let (ty, variant) = ctx
                    .variants
                    .get(&ty)
                    .cloned()
                    .ok_or_else(|| vec![Error::new(ErrorKind::UndefinedType).with_span(optty.span())])?;
                let mut patterns = vec![];
                let mut errors = vec![];
                for word in words {
                    let word = word.compile_param(ctx, function, bb);
                    match word {
                        Ok(Some(ok)) => patterns.push(ok),
                        Ok(None) => unreachable!(),
                        Err(err) => errors.extend(err),
                    }
                }
                if errors.is_empty() {
                    let value = builder::destr(ctx, function, bb, ty, variant, &patterns);
                    Ok(Some(value))
                } else {
                    Err(errors)
                }
            }
            _ => self.compile_quasi(ctx, function, bb),
        }
    }
}

impl Arg<()> for ast::Word {
    fn compile_arg(
        &self,
        ctx: &mut CompilerContext,
        function: LLVMValueRef,
        bb: LLVMBasicBlockRef,
        idx: &mut u16,
    ) -> Result<Vec<LLVMValueRef>, Vec<Error>> {
        match &self.kind {
            ast::WordKind::Bind(binding, ..) => {
                let value = unsafe {
                    let name_c = CString::new(binding.atom.as_str()).map_err(|_| {
                        vec![Error::new(ErrorKind::InvalidSymbol)
                            .describe(&binding.atom)
                            .with_span(self.span())]
                    })?;
                    let argv = LLVMGetParam(function, 2);
                    let idx = &mut [LLVMConstInt(builder::usize_type(ctx), *idx as _, 0)];
                    let gep = LLVMBuildGEP(
                        ctx.builder,
                        argv,
                        idx.as_mut_ptr(),
                        idx.len() as _,
                        b"ptr\0".as_ptr() as _,
                    );
                    LLVMBuildLoad(ctx.builder, gep, name_c.as_ptr())
                };
                ctx.insert_binding(binding.atom.to_string(), value);
                *idx += 1;
                Ok(vec![value])
            }
            ast::WordKind::Destructure(_, words) => {
                let mut result = vec![];
                let mut errors = vec![];
                for word in words {
                    word.compile_arg(ctx, function, bb, idx)
                        .map(|word| result.extend(word))
                        .unwrap_or_else(|err| errors.extend(err));
                }
                if errors.is_empty() {
                    Ok(result)
                } else {
                    Err(errors)
                }
            }
            _ => {
                *idx += 1;
                Ok(vec![])
            }
        }
    }
}

impl Compile<()> for ast::Atom {
    fn compile(
        &self,
        ctx: &mut CompilerContext,
        function: LLVMValueRef,
        bb: LLVMBasicBlockRef,
    ) -> Result<Option<LLVMValueRef>, Vec<Error>> {
        if ctx.named.contains(&self.atom) {
            let atom = builder::atom(ctx, function, bb, self.atom.as_str());
            let value = builder::get(ctx, function, bb, atom);
            Ok(Some(value))
        } else {
            ctx.binding(self.atom.as_str())
                .ok_or_else(|| {
                    vec![Error::new(ErrorKind::UndefinedSymbol)
                        .describe(&self.atom)
                        .with_span(self.span())]
                })
                .map(|word| Some(word))
        }
    }
}

impl Uneval<()> for ast::Atom {
    fn compile_uneval(
        &self,
        ctx: &mut CompilerContext,
        function: LLVMValueRef,
        bb: LLVMBasicBlockRef,
    ) -> Result<Option<LLVMValueRef>, Vec<Error>> {
        Ok(Some(builder::atom(ctx, function, bb, self.atom.as_str())))
    }
}

impl Quasi<()> for ast::Atom {
    fn compile_quasi(
        &self,
        ctx: &mut CompilerContext,
        function: LLVMValueRef,
        bb: LLVMBasicBlockRef,
    ) -> Result<Option<LLVMValueRef>, Vec<Error>> {
        self.compile_uneval(ctx, function, bb)
    }
}

impl Param<()> for ast::Atom {
    fn compile_param(
        &self,
        ctx: &mut CompilerContext,
        function: LLVMValueRef,
        bb: LLVMBasicBlockRef,
    ) -> Result<Option<LLVMValueRef>, Vec<Error>> {
        self.compile_uneval(ctx, function, bb)
    }
}

impl Compile<()> for ast::Path {
    fn compile(
        &self,
        ctx: &mut CompilerContext,
        function: LLVMValueRef,
        bb: LLVMBasicBlockRef,
    ) -> Result<Option<LLVMValueRef>, Vec<Error>> {
        let atom = builder::atom(ctx, function, bb, self.path[0].as_str());
        let mut value = builder::get(ctx, function, bb, atom);
        for path in &self.path[1..] {
            let atom = builder::atom(ctx, function, bb, path.as_str());
            value = builder::getfrom(ctx, function, bb, value, atom);;
        }
        Ok(Some(value))
    }
}

impl Uneval<()> for ast::Path {
    fn compile_uneval(
        &self,
        _ctx: &mut CompilerContext,
        _function: LLVMValueRef,
        _bb: LLVMBasicBlockRef,
    ) -> Result<Option<LLVMValueRef>, Vec<Error>> {
        Err(vec![Error::new(ErrorKind::UnexpectedWord)
            .describe("path in quote")
            .with_span(self.span())])
    }
}

impl Quasi<()> for ast::Path {
    fn compile_quasi(
        &self,
        _ctx: &mut CompilerContext,
        _function: LLVMValueRef,
        _bb: LLVMBasicBlockRef,
    ) -> Result<Option<LLVMValueRef>, Vec<Error>> {
        Err(vec![Error::new(ErrorKind::UnexpectedWord)
            .describe("path in quasiquote")
            .with_span(self.span())])
    }
}

impl Param<()> for ast::Path {
    fn compile_param(
        &self,
        _ctx: &mut CompilerContext,
        _function: LLVMValueRef,
        _bb: LLVMBasicBlockRef,
    ) -> Result<Option<LLVMValueRef>, Vec<Error>> {
        Err(vec![Error::new(ErrorKind::UnexpectedWord)
            .describe("path in parameter list")
            .with_span(self.span())])
    }
}

impl Compile<()> for ast::Interpolated {
    fn compile(
        &self,
        ctx: &mut CompilerContext,
        function: LLVMValueRef,
        bb: LLVMBasicBlockRef,
    ) -> Result<Option<LLVMValueRef>, Vec<Error>> {
        let mut errors = vec![];
        let mut parts = vec![];
        for part in &self.parts {
            match part {
                ast::InterpolatedPart::Str(string) => parts.push(builder::string(ctx, function, bb, string)),
                ast::InterpolatedPart::Interpolate(expr) => {
                    let part = match expr.borrow().compile(ctx, function, bb) {
                        Ok(ok) => ok.unwrap(),
                        Err(err) => {
                            errors.extend(err);
                            continue;
                        }
                    };
                    unsafe {
                        let rt = builder::rt(function);
                        let args = &mut [rt, part];
                        let function = LLVMGetNamedFunction(ctx.module, b"ehvalue_display\0".as_ptr() as _);
                        let value = LLVMBuildCall(
                            ctx.builder,
                            function,
                            args.as_mut_ptr(),
                            args.len() as _,
                            b"display\0".as_ptr() as _,
                        );
                        parts.push(value);
                    }
                }
            }
        }
        if parts.len() > 1 {
            unsafe {
                let rt = builder::rt(function);
                let value_type = builder::value_ptr_type(ctx);
                let (listc, listv) = builder::array(ctx, value_type, &parts);
                let args = &mut [rt, listc, listv];
                let function = LLVMGetNamedFunction(ctx.module, b"ehlist_concatv\0".as_ptr() as _);
                let value = LLVMBuildCall(
                    ctx.builder,
                    function,
                    args.as_mut_ptr(),
                    args.len() as _,
                    b"str\0".as_ptr() as _,
                );
                Ok(Some(value))
            }
        } else {
            Ok(Some(parts[0]))
        }
    }
}

impl Uneval<()> for ast::Interpolated {
    fn compile_uneval(
        &self,
        ctx: &mut CompilerContext,
        function: LLVMValueRef,
        bb: LLVMBasicBlockRef,
    ) -> Result<Option<LLVMValueRef>, Vec<Error>> {
        self.compile(ctx, function, bb)
    }
}

impl Quasi<()> for ast::Interpolated {
    fn compile_quasi(
        &self,
        ctx: &mut CompilerContext,
        function: LLVMValueRef,
        bb: LLVMBasicBlockRef,
    ) -> Result<Option<LLVMValueRef>, Vec<Error>> {
        self.compile(ctx, function, bb)
    }
}

impl Param<()> for ast::Interpolated {
    fn compile_param(
        &self,
        ctx: &mut CompilerContext,
        function: LLVMValueRef,
        bb: LLVMBasicBlockRef,
    ) -> Result<Option<LLVMValueRef>, Vec<Error>> {
        self.compile(ctx, function, bb)
    }
}

impl Compile<()> for ast::TypeWord {
    fn compile(
        &self,
        ctx: &mut CompilerContext,
        function: LLVMValueRef,
        bb: LLVMBasicBlockRef,
    ) -> Result<Option<LLVMValueRef>, Vec<Error>> {
        match &self.kind {
            ast::TypeWordKind::Type(ty) => {
                let atom = builder::atom(ctx, function, bb, &ty.atom);
                let cons = builder::get(ctx, function, bb, atom);
                let value = unsafe {
                    let value_type = builder::value_ptr_type(ctx);
                    let (argc, argv) = builder::array(ctx, value_type, &[]);
                    let argc = LLVMConstIntCast(argc, builder::u32_type(ctx), 0);
                    let rt = builder::rt(function);
                    let args = &mut [rt, cons, argc, argv];
                    let function = LLVMGetNamedFunction(ctx.module, b"ehclosure_call\0".as_ptr() as _);
                    LLVMBuildCall(
                        ctx.builder,
                        function,
                        args.as_mut_ptr(),
                        args.len() as _,
                        b"new\0".as_ptr() as _,
                    )
                };
                Ok(Some(value))
            }
            ast::TypeWordKind::Parenth(..) => unimplemented!(),
        }
    }
}

impl Uneval<()> for ast::TypeWord {
    fn compile_uneval(
        &self,
        ctx: &mut CompilerContext,
        function: LLVMValueRef,
        bb: LLVMBasicBlockRef,
    ) -> Result<Option<LLVMValueRef>, Vec<Error>> {
        match &self.kind {
            ast::TypeWordKind::Type(ty) => {
                let atom = builder::atom(ctx, function, bb, &ty.atom);
                let cons = builder::get(ctx, function, bb, atom);
                Ok(Some(cons))
            }
            ast::TypeWordKind::Parenth(..) => unimplemented!(),
        }
    }
}

impl Quasi<()> for ast::TypeWord {
    fn compile_quasi(
        &self,
        ctx: &mut CompilerContext,
        function: LLVMValueRef,
        bb: LLVMBasicBlockRef,
    ) -> Result<Option<LLVMValueRef>, Vec<Error>> {
        self.compile_uneval(ctx, function, bb)
    }
}

impl Param<()> for ast::TypeWord {
    fn compile_param(
        &self,
        ctx: &mut CompilerContext,
        function: LLVMValueRef,
        bb: LLVMBasicBlockRef,
    ) -> Result<Option<LLVMValueRef>, Vec<Error>> {
        self.compile_uneval(ctx, function, bb)
    }
}

impl<T: ExprLike> Compile<()> for ast::Closure<T> {
    fn compile(
        &self,
        ctx: &mut CompilerContext,
        function: LLVMValueRef,
        bb: LLVMBasicBlockRef,
    ) -> Result<Option<LLVMValueRef>, Vec<Error>> {
        let mut errors = vec![];

        ctx.enter(self.to_ident());

        let mut free = HashSet::new();
        let mut bindings = HashSet::new();
        self.find_free(ctx, &mut bindings, &mut free).unwrap_or_else(|errs| {
            errors.extend(errs);
        });
        let mut freev = free.into_iter().collect::<Vec<_>>();
        freev.sort_unstable();
        let free = freev
            .iter()
            .map(|free| {
                ctx.binding(free.as_str()).ok_or_else(|| {
                    vec![Error::new(ErrorKind::UndefinedSymbol)
                        .describe(free)
                        .with_span(self.span())]
                })
            })
            .fold(Ok(vec![]), |acc: Result<Vec<_>, Vec<_>>, e| {
                match acc {
                    Ok(mut acc) => {
                        acc.push(e?);
                        Ok(acc)
                    }
                    Err(mut err) => {
                        match e {
                            Ok(_) => Err(err),
                            Err(e) => {
                                err.extend(e);
                                Err(err)
                            }
                        }
                    }
                }
            })?;

        let closure = unsafe {
            let i8_type = builder::byte_type(ctx);
            let rt_type = LLVMPointerType(i8_type, 0);
            let value_type = builder::value_ptr_type(ctx);
            let datav_type = LLVMPointerType(value_type, 0);
            let argv_type = LLVMPointerType(value_type, 0);
            let params = &mut [rt_type, datav_type, argv_type];
            let function_type = LLVMFunctionType(value_type, params.as_mut_ptr(), params.len() as _, 0);
            let function_name = self.to_ident();
            let name_c = CString::new(function_name.as_str())
                .map_err(|_| vec![Error::new(ErrorKind::InvalidSymbol).describe(function_name)])?;
            let function = LLVMAddFunction(ctx.module, name_c.as_ptr(), function_type);

            ctx.add_debug(Debug {
                ptr: function,
                line: self.span().line(),
                col: self.span().col(),
                file: self.span().file().to_string(),
                ident: self.to_ident().to_string(),
            });

            ctx.symbols.push(name_c);
            LLVMSetFunctionCallConv(function, LLVMCallConv::LLVMCCallConv as _);
            let block = LLVMAppendBasicBlockInContext(ctx.context, function, b"init\0".as_ptr() as _);
            LLVMPositionBuilderAtEnd(ctx.builder, block);

            let mut i = 0;
            for param in &self.params {
                param
                    .compile_arg(ctx, function, block, &mut i)
                    .map(|_| ())
                    .unwrap_or_else(|err| errors.extend(err));
            }
            let datav = LLVMGetParam(function, 1);
            let usize_type = builder::usize_type(ctx);
            for (i, name) in freev.iter().enumerate() {
                let name_c = CString::new(name.as_str())
                    .map_err(|_| vec![Error::new(ErrorKind::InvalidSymbol).describe(name)])?;
                let index = &mut [LLVMConstInt(usize_type, i as _, 0)];
                let ptr = LLVMBuildGEP(
                    ctx.builder,
                    datav,
                    index.as_mut_ptr(),
                    index.len() as _,
                    b"ptr\0".as_ptr() as _,
                );
                let value = LLVMBuildLoad(ctx.builder, ptr, name_c.as_ptr());
                ctx.insert_binding(name.to_string(), value);
            }
            let result = self.body.borrow().compile(ctx, function, block)?;
            if let Some(result) = result {
                LLVMBuildRet(ctx.builder, result);
            } else {
                let nil = builder::nil(ctx, function, block);
                LLVMBuildRet(ctx.builder, nil);
            }

            if LLVMVerifyFunction(function, LLVMVerifierFailureAction::LLVMPrintMessageAction) == 1 {
                return Err(vec![Error::new(ErrorKind::LLVM).describe("this function is invalid")]);
            }
            if let Some(fpm) = ctx.fpm {
                LLVMRunFunctionPassManager(fpm, function);
            }

            function
        };

        ctx.leave();

        unsafe {
            LLVMPositionBuilderAtEnd(ctx.builder, bb);
        }

        let mut params = vec![];
        for param in &self.params {
            param
                .compile_param(ctx, function, bb)
                .map(|param| {
                    params.push(param.unwrap());
                })
                .unwrap_or_else(|err| {
                    errors.extend(err);
                })
        }
        if errors.is_empty() {
            if self.dynamic {
                builder::gc_disable(ctx, function, bb);
                let function = builder::function(ctx, function, bb, closure, &[], &params);
                let value = builder::closure_dynamic(ctx, function, bb, &[function]);
                builder::gc_enable(ctx, function, bb);
                Ok(Some(value))
            } else {
                builder::gc_disable(ctx, function, bb);
                let function = builder::function(ctx, function, bb, closure, &free, &params);
                let value = builder::closure_lexical(ctx, function, bb, &[function]);
                builder::gc_enable(ctx, function, bb);
                Ok(Some(value))
            }
        } else {
            Err(errors)
        }
    }
}

impl<T: ExprLike> Uneval<()> for ast::Closure<T> {
    fn compile_uneval(
        &self,
        _ctx: &mut CompilerContext,
        _function: LLVMValueRef,
        _bb: LLVMBasicBlockRef,
    ) -> Result<Option<LLVMValueRef>, Vec<Error>> {
        Err(vec![Error::new(ErrorKind::UnexpectedWord)
            .describe("closure in quote")
            .with_span(self.span())])
    }
}

impl<T: ExprLike> Quasi<()> for ast::Closure<T> {
    fn compile_quasi(
        &self,
        _ctx: &mut CompilerContext,
        _function: LLVMValueRef,
        _bb: LLVMBasicBlockRef,
    ) -> Result<Option<LLVMValueRef>, Vec<Error>> {
        Err(vec![Error::new(ErrorKind::UnexpectedWord)
            .describe("closure in quasi")
            .with_span(self.span())])
    }
}

impl<T: ExprLike> Param<()> for ast::Closure<T> {
    fn compile_param(
        &self,
        _ctx: &mut CompilerContext,
        _function: LLVMValueRef,
        _bb: LLVMBasicBlockRef,
    ) -> Result<Option<LLVMValueRef>, Vec<Error>> {
        Err(vec![Error::new(ErrorKind::UnexpectedWord)
            .describe("closure in parameter list")
            .with_span(self.span())])
    }
}

fn compile_multi<T: ExprLike>(
    closure: &[ast::Closure<T>],
    ctx: &mut CompilerContext,
    function: LLVMValueRef,
    bb: LLVMBasicBlockRef,
) -> Result<Option<LLVMValueRef>, Vec<Error>> {
    let mut errors = vec![];
    let mut impls = vec![];

    for closure in closure {
        ctx.enter(closure.to_ident());
        let mut free = HashSet::new();
        let mut bindings = HashSet::new();
        closure.find_free(ctx, &mut bindings, &mut free).unwrap_or_else(|errs| {
            errors.extend(errs);
        });
        let mut freev = free.into_iter().collect::<Vec<_>>();
        freev.sort_unstable();
        let free = freev
            .iter()
            .map(|free| {
                ctx.binding(free.as_str()).ok_or_else(|| {
                    vec![Error::new(ErrorKind::UndefinedSymbol)
                        .describe(free)
                        .with_span(closure.span())]
                })
            })
            .fold(Ok(vec![]), |acc: Result<Vec<_>, Vec<_>>, e| {
                match acc {
                    Ok(mut acc) => {
                        acc.push(e?);
                        Ok(acc)
                    }
                    Err(mut err) => {
                        match e {
                            Ok(_) => Err(err),
                            Err(e) => {
                                err.extend(e);
                                Err(err)
                            }
                        }
                    }
                }
            })?;

        let cimpl = unsafe {
            let i8_type = builder::byte_type(ctx);
            let rt_type = LLVMPointerType(i8_type, 0);
            let value_type = builder::value_ptr_type(ctx);
            let datav_type = LLVMPointerType(value_type, 0);
            let argv_type = LLVMPointerType(value_type, 0);
            let params = &mut [rt_type, datav_type, argv_type];
            let function_type = LLVMFunctionType(value_type, params.as_mut_ptr(), params.len() as _, 0);
            let function_name = closure.to_ident();
            let name_c = CString::new(function_name.as_str())
                .map_err(|_| vec![Error::new(ErrorKind::InvalidSymbol).describe(function_name)])?;
            let function = LLVMAddFunction(ctx.module, name_c.as_ptr(), function_type);

            ctx.add_debug(Debug {
                ptr: function,
                line: closure.span().line(),
                col: closure.span().col(),
                file: closure.span().file().to_string(),
                ident: closure.to_ident().to_string(),
            });

            ctx.symbols.push(name_c);
            LLVMSetFunctionCallConv(function, LLVMCallConv::LLVMCCallConv as _);
            let block = LLVMAppendBasicBlockInContext(ctx.context, function, b"init\0".as_ptr() as _);
            LLVMPositionBuilderAtEnd(ctx.builder, block);

            let mut i = 0;
            for param in &closure.params {
                param
                    .compile_arg(ctx, function, block, &mut i)
                    .map(|_| ())
                    .unwrap_or_else(|err| errors.extend(err));
            }
            let datav = LLVMGetParam(function, 1);
            let usize_type = builder::usize_type(ctx);
            for (i, name) in freev.iter().enumerate() {
                let name_c = CString::new(name.as_str())
                    .map_err(|_| vec![Error::new(ErrorKind::InvalidSymbol).describe(name)])?;
                let index = &mut [LLVMConstInt(usize_type, i as _, 0)];
                let ptr = LLVMBuildGEP(
                    ctx.builder,
                    datav,
                    index.as_mut_ptr(),
                    index.len() as _,
                    b"ptr\0".as_ptr() as _,
                );
                let value = LLVMBuildLoad(ctx.builder, ptr, name_c.as_ptr());
                ctx.insert_binding(name.to_string(), value);
            }
            let result = closure.body.borrow().compile(ctx, function, block)?;
            if let Some(result) = result {
                LLVMBuildRet(ctx.builder, result);
            } else {
                let nil = builder::nil(ctx, function, block);
                LLVMBuildRet(ctx.builder, nil);
            }

            function
        };
        ctx.leave();

        unsafe {
            LLVMPositionBuilderAtEnd(ctx.builder, bb);
        }

        let mut params = vec![];
        for param in &closure.params {
            param
                .compile_param(ctx, function, bb)
                .map(|param| {
                    params.push(param.unwrap());
                })
                .unwrap_or_else(|err| {
                    errors.extend(err);
                })
        }
        builder::gc_disable(ctx, function, bb);
        impls.push(builder::function(ctx, function, bb, cimpl, &free, &params));
        builder::gc_enable(ctx, function, bb);
    }

    if errors.is_empty() {
        if closure[0].dynamic {
            builder::gc_disable(ctx, function, bb);
            let value = builder::closure_dynamic(ctx, function, bb, &impls);
            builder::gc_enable(ctx, function, bb);
            Ok(Some(value))
        } else {
            builder::gc_disable(ctx, function, bb);
            let value = builder::closure_lexical(ctx, function, bb, &impls);
            builder::gc_enable(ctx, function, bb);
            Ok(Some(value))
        }
    } else {
        Err(errors)
    }
}

pub trait FindFree<Seal>: AsMeta {
    fn find_free(
        &self,
        ctx: &mut CompilerContext,
        bindings: &mut HashSet<RcStr>,
        free: &mut HashSet<RcStr>,
    ) -> Result<(), Vec<Error>>;

    fn find_free_quasi(
        &self,
        ctx: &mut CompilerContext,
        bindings: &mut HashSet<RcStr>,
        free: &mut HashSet<RcStr>,
    ) -> Result<(), Vec<Error>>;

    fn find_free_param(
        &self,
        ctx: &mut CompilerContext,
        bindings: &mut HashSet<RcStr>,
        free: &mut HashSet<RcStr>,
    ) -> Result<(), Vec<Error>>;
}

impl<T: BlockLike> FindFree<ast::BlockSeal> for T {
    fn find_free(
        &self,
        ctx: &mut CompilerContext,
        bindings: &mut HashSet<RcStr>,
        free: &mut HashSet<RcStr>,
    ) -> Result<(), Vec<Error>> {
        for stmt in self.stmts() {
            stmt.find_free(ctx, bindings, free)?;
        }
        if let Some(expr) = self.expr() {
            expr.find_free(ctx, bindings, free)?;
        }
        Ok(())
    }

    fn find_free_quasi(
        &self,
        ctx: &mut CompilerContext,
        bindings: &mut HashSet<RcStr>,
        free: &mut HashSet<RcStr>,
    ) -> Result<(), Vec<Error>> {
        for stmt in self.stmts() {
            stmt.find_free_quasi(ctx, bindings, free)?;
        }
        if let Some(expr) = self.expr() {
            expr.find_free_quasi(ctx, bindings, free)?;
        }
        Ok(())
    }

    fn find_free_param(
        &self,
        ctx: &mut CompilerContext,
        bindings: &mut HashSet<RcStr>,
        free: &mut HashSet<RcStr>,
    ) -> Result<(), Vec<Error>> {
        for stmt in self.stmts() {
            stmt.find_free_param(ctx, bindings, free)?;
        }
        if let Some(expr) = self.expr() {
            expr.find_free_param(ctx, bindings, free)?;
        }
        Ok(())
    }
}

impl<T: StmtLike> FindFree<ast::StmtSeal> for T {
    fn find_free(
        &self,
        ctx: &mut CompilerContext,
        bindings: &mut HashSet<RcStr>,
        free: &mut HashSet<RcStr>,
    ) -> Result<(), Vec<Error>> {
        self.expr().find_free(ctx, bindings, free)
    }

    fn find_free_quasi(
        &self,
        ctx: &mut CompilerContext,
        bindings: &mut HashSet<RcStr>,
        free: &mut HashSet<RcStr>,
    ) -> Result<(), Vec<Error>> {
        self.expr().find_free_quasi(ctx, bindings, free)
    }

    fn find_free_param(
        &self,
        ctx: &mut CompilerContext,
        bindings: &mut HashSet<RcStr>,
        free: &mut HashSet<RcStr>,
    ) -> Result<(), Vec<Error>> {
        self.expr().find_free_param(ctx, bindings, free)
    }
}

impl<T: ExprLike> FindFree<ast::ExprSeal> for T {
    fn find_free(
        &self,
        ctx: &mut CompilerContext,
        bindings: &mut HashSet<RcStr>,
        free: &mut HashSet<RcStr>,
    ) -> Result<(), Vec<Error>> {
        match &self.first().kind {
            ast::WordKind::Atom(atom) => {
                match atom.atom.as_str() {
                    "let" => {
                        let kind = self.rest().get(0).ok_or_else(|| {
                            vec![Error::new(ErrorKind::LetSyntax)
                                .with_span(self.span())
                                .describe("missing binding")]
                        })?;
                        match &kind.kind {
                            ast::WordKind::Atom(atom) if atom.atom.as_str() == "fun" => {
                                let binding = match &self.rest().get(1).unwrap_or_else(|| unreachable!()).kind {
                                    ast::WordKind::Atom(atom) => &atom.atom,
                                    _ => {
                                        return Err(vec![Error::new(ErrorKind::LetSyntax)
                                            .describe("imports must be atoms")
                                            .with_span(atom.span())])
                                    }
                                };

                                for param in &*self.as_meta().params.as_ref().unwrap().borrow() {
                                    param.find_free_param(ctx, bindings, free)?;
                                }
                                let result = self
                                    .as_meta()
                                    .def
                                    .as_ref()
                                    .unwrap()
                                    .borrow()
                                    .find_free(ctx, bindings, free);
                                bindings.insert(binding.clone());
                                result
                            }
                            ast::WordKind::Atom(atom) if atom.atom.as_str() == "rec" => {
                                let binding = match &self.rest().get(1).unwrap_or_else(|| unreachable!()).kind {
                                    ast::WordKind::Atom(atom) => &atom.atom,
                                    _ => {
                                        return Err(vec![Error::new(ErrorKind::LetSyntax)
                                            .describe("imports must be atoms")
                                            .with_span(atom.span())])
                                    }
                                };

                                for param in &*self.as_meta().params.as_ref().unwrap().borrow() {
                                    param.find_free_param(ctx, bindings, free)?;
                                }
                                let result = self
                                    .as_meta()
                                    .def
                                    .as_ref()
                                    .unwrap()
                                    .borrow()
                                    .find_free(ctx, bindings, free);
                                bindings.insert(binding.clone());
                                result
                            }
                            ast::WordKind::Atom(atom) => {
                                bindings.insert(atom.atom.clone());
                                self.as_meta()
                                    .def
                                    .as_ref()
                                    .unwrap()
                                    .borrow()
                                    .find_free(ctx, bindings, free)
                            }
                            _ => {
                                return Err(vec![Error::new(ErrorKind::LetSyntax)
                                    .with_span(&kind.span())
                                    .describe("binding must be an atom")])
                            }
                        }
                    }
                    "def" => {
                        let kind = self.rest().get(0).ok_or_else(|| {
                            vec![Error::new(ErrorKind::LetSyntax)
                                .with_span(self.span())
                                .describe("missing binding")]
                        })?;
                        match &kind.kind {
                            ast::WordKind::Atom(atom) if atom.atom.as_str() == "fun" => {
                                let binding = match &self.rest().get(1).unwrap_or_else(|| unreachable!()).kind {
                                    ast::WordKind::Atom(atom) => &atom.atom,
                                    _ => {
                                        return Err(vec![Error::new(ErrorKind::LetSyntax)
                                            .describe("imports must be atoms")
                                            .with_span(atom.span())])
                                    }
                                };

                                for param in &*self.as_meta().params.as_ref().unwrap().borrow() {
                                    param.find_free_param(ctx, bindings, free)?;
                                }
                                let result = self
                                    .as_meta()
                                    .def
                                    .as_ref()
                                    .unwrap()
                                    .borrow()
                                    .find_free(ctx, bindings, free);
                                bindings.insert(binding.clone());
                                result
                            }
                            ast::WordKind::Atom(atom) if atom.atom.as_str() == "rec" => {
                                let binding = match &self.rest().get(1).unwrap_or_else(|| unreachable!()).kind {
                                    ast::WordKind::Atom(atom) => &atom.atom,
                                    _ => {
                                        return Err(vec![Error::new(ErrorKind::LetSyntax)
                                            .describe("imports must be atoms")
                                            .with_span(atom.span())])
                                    }
                                };

                                for param in &*self.as_meta().params.as_ref().unwrap().borrow() {
                                    param.find_free_param(ctx, bindings, free)?;
                                }
                                let result = self
                                    .as_meta()
                                    .def
                                    .as_ref()
                                    .unwrap()
                                    .borrow()
                                    .find_free(ctx, bindings, free);
                                bindings.insert(binding.clone());
                                result
                            }
                            ast::WordKind::Atom(_) => {
                                bindings.insert(atom.atom.clone());
                                self.as_meta()
                                    .def
                                    .as_ref()
                                    .unwrap()
                                    .borrow()
                                    .find_free(ctx, bindings, free)
                            }
                            _ => {
                                return Err(vec![Error::new(ErrorKind::LetSyntax)
                                    .with_span(&kind.span())
                                    .describe("binding must be an atom")])
                            }
                        }
                    }
                    "use" => Ok(()),
                    "from" => {
                        let vars = &self.rest()[2..];
                        for var in vars {
                            let var = match &var.kind {
                                ast::WordKind::Atom(atom) => atom.atom.clone(),
                                _ => {
                                    return Err(vec![Error::new(ErrorKind::FromSyntax)
                                        .describe("imports must be atoms")
                                        .with_span(var.span())])
                                }
                            };
                            bindings.insert(var);
                        }
                        Ok(())
                    }
                    "type" => Ok(()),
                    _ => {
                        self.first().find_free(ctx, bindings, free)?;
                        for word in self.rest() {
                            word.find_free(ctx, bindings, free)?;
                        }
                        Ok(())
                    }
                }
            }
            _ => {
                self.first().find_free(ctx, bindings, free)?;
                for word in self.rest() {
                    word.find_free(ctx, bindings, free)?;
                }
                Ok(())
            }
        }
    }

    fn find_free_quasi(
        &self,
        ctx: &mut CompilerContext,
        bindings: &mut HashSet<RcStr>,
        free: &mut HashSet<RcStr>,
    ) -> Result<(), Vec<Error>> {
        self.first().find_free_quasi(ctx, bindings, free)?;
        for word in self.rest() {
            word.find_free_quasi(ctx, bindings, free)?;
        }
        Ok(())
    }

    fn find_free_param(
        &self,
        ctx: &mut CompilerContext,
        bindings: &mut HashSet<RcStr>,
        free: &mut HashSet<RcStr>,
    ) -> Result<(), Vec<Error>> {
        self.first().find_free_param(ctx, bindings, free)?;
        for word in self.rest() {
            word.find_free_param(ctx, bindings, free)?;
        }
        Ok(())
    }
}

impl FindFree<()> for ast::Word {
    fn find_free(
        &self,
        ctx: &mut CompilerContext,
        bindings: &mut HashSet<RcStr>,
        free: &mut HashSet<RcStr>,
    ) -> Result<(), Vec<Error>> {
        match &self.kind {
            ast::WordKind::Atom(atom) => atom.find_free(ctx, bindings, free),
            ast::WordKind::Path(path) => path.find_free(ctx, bindings, free),
            ast::WordKind::Integer(..) => Ok(()),
            ast::WordKind::Real(..) => Ok(()),
            ast::WordKind::Ratio(..) => Ok(()),
            ast::WordKind::Char(..) => Ok(()),
            ast::WordKind::Str(string) => string.find_free(ctx, bindings, free),
            ast::WordKind::TypeAtom(ty) => ty.find_free(ctx, bindings, free),
            ast::WordKind::Quote(..) => Ok(()),
            ast::WordKind::Quasiquote(inner) => inner.borrow().find_free_quasi(ctx, bindings, free),
            ast::WordKind::Interpolate(..) => Err(vec![Error::new(ErrorKind::PatternInExpr).with_span(self.span())]),
            ast::WordKind::Empty => Ok(()),
            ast::WordKind::Parenth(expr) => expr.borrow().find_free(ctx, bindings, free),
            ast::WordKind::Bind(name, _, pred, _) => {
                if let Some(pred) = pred {
                    pred.borrow().find_free(ctx, bindings, free)?;
                }
                bindings.insert(name.atom.clone());
                Ok(())
            }
            ast::WordKind::Destructure(_, words) => {
                for word in words {
                    word.find_free(ctx, bindings, free)?;
                }
                Ok(())
            }
            ast::WordKind::Closure(closure) => {
                for closure in closure {
                    closure.find_free(ctx, bindings, free)?;
                }
                Ok(())
            }
            ast::WordKind::Compound(block) => block.borrow().find_free(ctx, bindings, free),
        }
    }

    fn find_free_quasi(
        &self,
        ctx: &mut CompilerContext,
        bindings: &mut HashSet<RcStr>,
        free: &mut HashSet<RcStr>,
    ) -> Result<(), Vec<Error>> {
        match &self.kind {
            ast::WordKind::Atom(atom) => atom.find_free_quasi(ctx, bindings, free),
            ast::WordKind::Path(path) => path.find_free_quasi(ctx, bindings, free),
            ast::WordKind::Integer(..) => Ok(()),
            ast::WordKind::Real(..) => Ok(()),
            ast::WordKind::Ratio(..) => Ok(()),
            ast::WordKind::Char(..) => Ok(()),
            ast::WordKind::Str(string) => string.find_free_quasi(ctx, bindings, free),
            ast::WordKind::TypeAtom(ty) => ty.find_free_quasi(ctx, bindings, free),
            ast::WordKind::Quote(..) => Ok(()),
            ast::WordKind::Quasiquote(inner) => inner.borrow().find_free_quasi(ctx, bindings, free),
            ast::WordKind::Interpolate(inner) => inner.borrow().find_free(ctx, bindings, free),
            ast::WordKind::Empty => Ok(()),
            ast::WordKind::Parenth(expr) => expr.borrow().find_free_quasi(ctx, bindings, free),
            ast::WordKind::Bind(_, _, None, _) => Ok(()),
            ast::WordKind::Bind(_, _, Some(pred), _) => pred.borrow().find_free_quasi(ctx, bindings, free),
            ast::WordKind::Destructure(_, words) => {
                for word in words {
                    word.find_free_quasi(ctx, bindings, free)?;
                }
                Ok(())
            }
            ast::WordKind::Closure(closure) => {
                for closure in closure {
                    closure.find_free(ctx, bindings, free)?;
                }
                Ok(())
            }
            ast::WordKind::Compound(block) => block.borrow().find_free_quasi(ctx, bindings, free),
        }
    }

    fn find_free_param(
        &self,
        ctx: &mut CompilerContext,
        bindings: &mut HashSet<RcStr>,
        free: &mut HashSet<RcStr>,
    ) -> Result<(), Vec<Error>> {
        match &self.kind {
            ast::WordKind::Atom(atom) => atom.find_free_param(ctx, bindings, free),
            ast::WordKind::Path(path) => path.find_free_param(ctx, bindings, free),
            ast::WordKind::Integer(..) => Ok(()),
            ast::WordKind::Real(..) => Ok(()),
            ast::WordKind::Ratio(..) => Ok(()),
            ast::WordKind::Char(..) => Ok(()),
            ast::WordKind::Str(string) => string.find_free_param(ctx, bindings, free),
            ast::WordKind::TypeAtom(ty) => ty.find_free_param(ctx, bindings, free),
            ast::WordKind::Quote(..) => Ok(()),
            ast::WordKind::Quasiquote(inner) => inner.borrow().find_free_param(ctx, bindings, free),
            ast::WordKind::Interpolate(inner) => inner.borrow().find_free(ctx, bindings, free),
            ast::WordKind::Empty => Ok(()),
            ast::WordKind::Parenth(expr) => expr.borrow().find_free_param(ctx, bindings, free),
            ast::WordKind::Bind(atom, _, None, _) => {
                bindings.insert(atom.atom.clone());
                Ok(())
            }
            ast::WordKind::Bind(atom, _, Some(pred), _) => {
                bindings.insert(atom.atom.clone());
                pred.borrow().find_free(ctx, bindings, free)
            }
            ast::WordKind::Destructure(_, words) => {
                for word in words {
                    word.find_free_param(ctx, bindings, free)?;
                }
                Ok(())
            }
            ast::WordKind::Closure(closure) => {
                for closure in closure {
                    closure.find_free(ctx, bindings, free)?;
                }
                Ok(())
            }
            ast::WordKind::Compound(block) => block.borrow().find_free_param(ctx, bindings, free),
        }
    }
}

impl FindFree<()> for ast::Atom {
    fn find_free(
        &self,
        ctx: &mut CompilerContext,
        bindings: &mut HashSet<RcStr>,
        free: &mut HashSet<RcStr>,
    ) -> Result<(), Vec<Error>> {
        if !ctx.named.contains(&self.atom) && !bindings.contains(&self.atom) {
            free.insert(self.atom.clone());
        }
        Ok(())
    }

    fn find_free_quasi(
        &self,
        _ctx: &mut CompilerContext,
        _bindings: &mut HashSet<RcStr>,
        _free: &mut HashSet<RcStr>,
    ) -> Result<(), Vec<Error>> {
        Ok(())
    }

    fn find_free_param(
        &self,
        _ctx: &mut CompilerContext,
        _bindings: &mut HashSet<RcStr>,
        _free: &mut HashSet<RcStr>,
    ) -> Result<(), Vec<Error>> {
        Ok(())
    }
}

impl FindFree<()> for ast::Path {
    fn find_free(
        &self,
        ctx: &mut CompilerContext,
        bindings: &mut HashSet<RcStr>,
        free: &mut HashSet<RcStr>,
    ) -> Result<(), Vec<Error>> {
        let atom = &self.path[0].clone();
        if !ctx.named.contains(atom) && !bindings.contains(atom) {
            free.insert(atom.clone());
        }
        Ok(())
    }

    fn find_free_quasi(
        &self,
        _ctx: &mut CompilerContext,
        _bindings: &mut HashSet<RcStr>,
        _free: &mut HashSet<RcStr>,
    ) -> Result<(), Vec<Error>> {
        Ok(())
    }

    fn find_free_param(
        &self,
        _ctx: &mut CompilerContext,
        _bindings: &mut HashSet<RcStr>,
        _free: &mut HashSet<RcStr>,
    ) -> Result<(), Vec<Error>> {
        Ok(())
    }
}

impl FindFree<()> for ast::Interpolated {
    fn find_free(
        &self,
        ctx: &mut CompilerContext,
        bindings: &mut HashSet<RcStr>,
        free: &mut HashSet<RcStr>,
    ) -> Result<(), Vec<Error>> {
        let mut errors = vec![];
        for part in &self.parts {
            match &part {
                ast::InterpolatedPart::Str(..) => {}
                ast::InterpolatedPart::Interpolate(expr) => {
                    expr.borrow()
                        .find_free(ctx, bindings, free)
                        .unwrap_or_else(|err| errors.extend(err))
                }
            }
        }
        Ok(())
    }

    fn find_free_quasi(
        &self,
        ctx: &mut CompilerContext,
        bindings: &mut HashSet<RcStr>,
        free: &mut HashSet<RcStr>,
    ) -> Result<(), Vec<Error>> {
        let mut errors = vec![];
        for part in &self.parts {
            match &part {
                ast::InterpolatedPart::Str(..) => {}
                ast::InterpolatedPart::Interpolate(expr) => {
                    expr.borrow()
                        .find_free_quasi(ctx, bindings, free)
                        .unwrap_or_else(|err| errors.extend(err))
                }
            }
        }
        Ok(())
    }

    fn find_free_param(
        &self,
        ctx: &mut CompilerContext,
        bindings: &mut HashSet<RcStr>,
        free: &mut HashSet<RcStr>,
    ) -> Result<(), Vec<Error>> {
        let mut errors = vec![];
        for part in &self.parts {
            match &part {
                ast::InterpolatedPart::Str(..) => {}
                ast::InterpolatedPart::Interpolate(expr) => {
                    expr.borrow()
                        .find_free_param(ctx, bindings, free)
                        .unwrap_or_else(|err| errors.extend(err))
                }
            }
        }
        Ok(())
    }
}

impl FindFree<()> for ast::TypeWord {
    fn find_free(
        &self,
        _ctx: &mut CompilerContext,
        _bindings: &mut HashSet<RcStr>,
        _free: &mut HashSet<RcStr>,
    ) -> Result<(), Vec<Error>> {
        Ok(())
    }

    fn find_free_quasi(
        &self,
        _ctx: &mut CompilerContext,
        _bindings: &mut HashSet<RcStr>,
        _free: &mut HashSet<RcStr>,
    ) -> Result<(), Vec<Error>> {
        Ok(())
    }

    fn find_free_param(
        &self,
        _ctx: &mut CompilerContext,
        _bindings: &mut HashSet<RcStr>,
        _free: &mut HashSet<RcStr>,
    ) -> Result<(), Vec<Error>> {
        Ok(())
    }
}

impl<T: ExprLike> FindFree<()> for ast::Closure<T> {
    fn find_free(
        &self,
        ctx: &mut CompilerContext,
        bindings: &mut HashSet<RcStr>,
        free: &mut HashSet<RcStr>,
    ) -> Result<(), Vec<Error>> {
        let mut errors = vec![];
        for param in &self.params {
            param
                .find_free_param(ctx, bindings, free)
                .unwrap_or_else(|err| errors.extend(err));
        }
        self.body
            .borrow()
            .find_free(ctx, bindings, free)
            .unwrap_or_else(|err| errors.extend(err));
        if errors.is_empty() {
            Ok(())
        } else {
            Err(errors)
        }
    }

    fn find_free_quasi(
        &self,
        ctx: &mut CompilerContext,
        bindings: &mut HashSet<RcStr>,
        free: &mut HashSet<RcStr>,
    ) -> Result<(), Vec<Error>> {
        let mut errors = vec![];
        for param in &self.params {
            param
                .find_free_param(ctx, bindings, free)
                .unwrap_or_else(|err| errors.extend(err));
        }
        self.body
            .borrow()
            .find_free(ctx, bindings, free)
            .unwrap_or_else(|err| errors.extend(err));
        if errors.is_empty() {
            Ok(())
        } else {
            Err(errors)
        }
    }

    fn find_free_param(
        &self,
        ctx: &mut CompilerContext,
        bindings: &mut HashSet<RcStr>,
        free: &mut HashSet<RcStr>,
    ) -> Result<(), Vec<Error>> {
        let mut errors = vec![];
        for param in &self.params {
            param
                .find_free_param(ctx, bindings, free)
                .unwrap_or_else(|err| errors.extend(err));
        }
        self.body
            .borrow()
            .find_free(ctx, bindings, free)
            .unwrap_or_else(|err| errors.extend(err));
        if errors.is_empty() {
            Ok(())
        } else {
            Err(errors)
        }
    }
}
