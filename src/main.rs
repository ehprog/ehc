use std::ptr;
use std::process;
use std::ffi::{CStr, CString};
use std::fs;
use std::path::Path;
use std::io::{self, Write};

use clap::{Arg, App};

use llvm_sys::prelude::*;
use llvm_sys::core::*;
use llvm_sys::bit_writer::LLVMWriteBitcodeToFile;
use llvm_sys::target_machine::{LLVMTargetMachineEmitToFile, LLVMCodeGenFileType};

use ehc::error::Error;
use ehc::lex::Lexer;
use ehc::pp::Preprocessor;
use ehc::parse::Parser;
use ehc::module::ModuleHandler;
use ehc::ty::{TypeInferrer, TypeContext};
use ehc::macros::{MacroExpander, ExpanderContext};
use ehc::llvm::{self, Config, Compiler, CompilerContext};

const AUTHOR: &str = env!("CARGO_PKG_AUTHORS");
const NAME: &str = env!("CARGO_PKG_NAME");
const VERSION: &str = env!("CARGO_PKG_VERSION");
const ABOUT: &str = "The ehprog compiler";

fn compile_core(
    config: Config,
) -> Result<(String, LLVMModuleRef, (TypeContext, ExpanderContext, CompilerContext)), Vec<Error>> {
    let path = "core.ehp";
    let dir = None;
    let src = ehc::CORE;

    let lexer = Lexer::new(From::from(path), From::from(src));
    let pp = Preprocessor::new(lexer);
    let parser = Parser::new(pp);
    let mods = ModuleHandler::new(parser, path, dir);
    let types = TypeInferrer::new(mods);
    let macros = MacroExpander::new(types);
    let comp = Compiler::with_config(macros, config);
    let ((main, modules), ctx) = comp.compile()?;
    let module = *modules.get(main.as_str()).unwrap();
    Ok((main, module, ctx))
}

fn compile(
    path: &str,
    config: Config,
) -> Result<(String, LLVMModuleRef, (TypeContext, ExpanderContext, CompilerContext)), Vec<Error>> {
    let dir = AsRef::<Path>::as_ref(&path)
        .parent()
        .map(|path| From::from(path.to_string_lossy().as_ref()));
    let src = fs::read_to_string(path).unwrap();

    let lexer = Lexer::new(From::from(path), From::from(src));
    let pp = Preprocessor::new(lexer);
    let parser = Parser::new(pp);
    let mods = ModuleHandler::new(parser, path, dir.clone());
    let types = TypeInferrer::new(mods);
    let macros = MacroExpander::new(types);
    let comp = Compiler::with_config(macros, config);
    let ((main, modules), ctx) = comp.compile()?;
    let module = *modules.get(main.as_str()).unwrap();
    Ok((main, module, ctx))
}

unsafe fn emit_llvm(file: &str, module: LLVMModuleRef) -> Result<(), io::Error> {
    let mut error = ptr::null_mut();
    let file = CString::new(file).expect("invalid file name");
    let _p = LLVMPrintModuleToFile(module, file.as_ptr(), &mut error);

    if !error.is_null() {
        let err = CStr::from_ptr(error);
        io::stderr().write(err.to_bytes())?;
        LLVMDisposeMessage(error);
    }
    Ok(())
}

unsafe fn emit_bc(file: &str, module: LLVMModuleRef) -> Result<(), io::Error> {
    let file = CString::new(file).expect("invalid file name");
    LLVMWriteBitcodeToFile(module, file.as_ptr());
    Ok(())
}

unsafe fn emit_asm(file: &str, ctx: &CompilerContext, module: LLVMModuleRef) -> Result<(), io::Error> {
    let file = CString::new(file).expect("invalid file name");
    let mut error = ptr::null_mut();
    let target_machine = ctx.target_machine().unwrap();
    let _p = LLVMTargetMachineEmitToFile(
        target_machine,
        module,
        file.as_ptr() as _,
        LLVMCodeGenFileType::LLVMAssemblyFile,
        &mut error,
    );

    if !error.is_null() {
        let err = CStr::from_ptr(error);
        io::stderr().write(err.to_bytes())?;
        LLVMDisposeMessage(error);
    }

    Ok(())
}

unsafe fn emit_obj(file: &str, ctx: &CompilerContext, module: LLVMModuleRef) -> Result<(), io::Error> {
    let file = CString::new(file).expect("invalid file name");
    let mut error = ptr::null_mut();
    let target_machine = ctx.target_machine().unwrap();
    let _p = LLVMTargetMachineEmitToFile(
        target_machine,
        module,
        file.as_ptr() as _,
        LLVMCodeGenFileType::LLVMObjectFile,
        &mut error,
    );

    if !error.is_null() {
        let err = CStr::from_ptr(error);
        io::stderr().write(err.to_bytes())?;
        LLVMDisposeMessage(error);
    }

    Ok(())
}

fn main() -> Result<(), io::Error> {
    let matches = App::new(NAME)
        .version(VERSION)
        .author(AUTHOR)
        .about(ABOUT)
        .arg(
            Arg::with_name("output")
                .help("output file")
                .takes_value(true)
                .short("o")
                .long("out")
                .required(true),
        )
        .arg(
            Arg::with_name("opt-level")
                .help("optimization level")
                .takes_value(true)
                .short("O")
                .long("opt")
                .possible_values(&["0", "1", "2", "3"]),
        )
        .arg(
            Arg::with_name("generate-symbols")
                .help("generate debug symbols")
                .takes_value(false)
                .short("g")
                .long("gdebug")
                .possible_values(&["0", "1", "2", "3"]),
        )
        .arg(
            Arg::with_name("emit-llvm")
                .help("emit human-readable LLVM IR")
                .takes_value(false)
                .long("emit-llvm"),
        )
        .arg(
            Arg::with_name("emit-bc")
                .help("emit LLVM bitcode")
                .takes_value(false)
                .long("emit-bc"),
        )
        .arg(
            Arg::with_name("emit-asm")
                .help("emit local target assembly")
                .takes_value(false)
                .long("emit-asm"),
        )
        .arg(
            Arg::with_name("emit-obj")
                .help("emit local target object")
                .takes_value(false)
                .long("emit-obj"),
        )
        .arg(
            Arg::with_name("core")
                .help("compile compiler internal core library instead of file(-s)")
                .takes_value(false)
                .long("core"),
        )
        .arg(
            Arg::with_name("file")
                .help("file(-s) to compile")
                .takes_value(true)
                .last(true)
                .multiple(true)
                .required_unless("core")
                .conflicts_with("core"),
        )
        .get_matches();

    let mut config = Config::default();
    if let Some(opt) = matches.value_of("opt-level") {
        let opt = opt.parse().unwrap();
        config.optimizations = opt;
    }
    if matches.is_present("generate-symbols") {
        config.debug_symbols = true;
    }
    if matches.is_present("emit-asm") || matches.is_present("emit-obj") {
        unsafe {
            llvm::init();
        }
        config.target_machine = true;
    }

    let (_main, module, ctx) = if matches.is_present("core") {
        match compile_core(config) {
            Ok(ok) => ok,
            Err(errs) => {
                let n = errs.len();
                for err in errs {
                    eprintln!("{}", err);
                }
                eprintln!("could not compile because of {} error(-s)", n);
                process::exit(1);
            }
        }
    } else {
        let path = matches.values_of("file").unwrap().next().unwrap();
        match compile(path, config) {
            Ok(ok) => ok,
            Err(errs) => {
                let n = errs.len();
                for err in errs {
                    eprintln!("{}", err);
                }
                eprintln!("could not compile because of {} error(-s)", n);
                process::exit(1);
            }
        }
    };

    let file = matches.value_of("output").unwrap();

    unsafe {
        if matches.is_present("emit-llvm") {
            emit_llvm(file, module)?;
        } else if matches.is_present("emit-bc") {
            emit_bc(file, module)?;
        } else if matches.is_present("emit-asm") {
            emit_asm(file, &ctx.2, module)?;
        } else if matches.is_present("emit-obj") {
            emit_obj(file, &ctx.2, module)?;
        }
    }

    unsafe {
        LLVMDisposeModule(module);
    }

    Ok(())
}
