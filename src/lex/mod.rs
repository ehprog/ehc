use std::fmt::{self, Display};
use std::collections::VecDeque;
use std::iter::FromIterator;

use crate::error::{Error, ErrorKind};
use crate::collections::RcStr;

#[derive(Debug, Clone, PartialEq, Eq, Hash)]
pub struct Span {
    file: RcStr,
    src: RcStr,
    line: usize,
    col: usize,
}

impl Span {
    pub fn new(file: RcStr, src: RcStr, line: usize, col: usize) -> Self {
        Span { file, src, line, col }
    }

    pub fn join(mut self, other: Span) -> Self {
        self.src.extend_to(other.src.end());
        self
    }

    pub fn file(&self) -> &RcStr {
        &self.file
    }

    pub fn src(&self) -> &RcStr {
        &self.src
    }

    pub fn line(&self) -> usize {
        self.line
    }

    pub fn col(&self) -> usize {
        self.col
    }
}

impl FromIterator<Span> for Span {
    fn from_iter<I: IntoIterator<Item = Span>>(iter: I) -> Self {
        let mut iter = iter.into_iter();
        let mut first = iter.next().expect("span sequence must have at least one element");

        for other in iter {
            first = first.join(other);
        }

        first
    }
}

impl Display for Span {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        Display::fmt(&self.src, f)
    }
}

#[derive(Debug, Clone, PartialEq, Eq, Hash)]
pub struct Token {
    span: Span,
    kind: TokenKind,
}

impl Token {
    pub fn new(span: Span, kind: TokenKind) -> Self {
        Token { span, kind }
    }

    pub fn span(&self) -> &Span {
        &self.span
    }

    pub fn kind(&self) -> TokenKind {
        self.kind
    }
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, Hash)]
pub enum TokenKind {
    Semi,
    DoubleSemi,
    Caret,
    Backtick,
    Backslash,
    Tilde,
    ParenL,
    ParenR,
    SquareL,
    SquareR,
    CurlyL,
    CurlyR,
    Pipe,
    Dollar,
    DollarParen,
    Dot,
    DoubleDot,
    Colon,
    DoubleColon,
    ParBegin,
    ParEnd,

    Atom,
    Type,
    Integer,
    Real,
    Ratio,
    Str,
    Char,

    EOF,
}

trait CharExt {
    fn is_id_begin(self) -> bool;
    fn is_id_cont(self) -> bool;
    fn is_ty_begin(self) -> bool;
    fn is_ty_cont(self) -> bool;
    fn is_break(self) -> bool;
}

impl CharExt for char {
    fn is_id_begin(self) -> bool {
        static ID_BEGIN: &str = "abcdefghijklmnopqrstuvwxyz_+-*/=!#%&?<>@";
        for c in ID_BEGIN.chars() {
            if self == c {
                return true;
            }
        }
        false
    }

    fn is_id_cont(self) -> bool {
        static ID_CONT: &str = "abcdefghijklmnopqrstuvwxyz0123456789_+-*/=!#%&?<>@:";
        for c in ID_CONT.chars() {
            if self == c {
                return true;
            }
        }
        false
    }

    fn is_ty_begin(self) -> bool {
        self.is_ascii_uppercase()
    }

    fn is_ty_cont(self) -> bool {
        self.is_ascii_alphanumeric() || self == '_' || self == '-' || self == '.'
    }

    fn is_break(self) -> bool {
        static BREAK: &str = " \t\n^`~$()[]{};|\"'.";
        for c in BREAK.chars() {
            if self == c {
                return true;
            }
        }
        false
    }
}

pub struct Lexer {
    file: RcStr,
    src: RcStr,
    lexeme: Option<RcStr>,
    queue: VecDeque<Token>,
    par: Vec<u32>,
    line_next: usize,
    col_next: usize,
    col_prev: usize,
    line: usize,
    col: usize,
    eof: bool,
    err: bool,
    push_back: Option<char>,
}

impl Lexer {
    pub fn new(file: RcStr, src: RcStr) -> Self {
        Lexer {
            file,
            src,
            lexeme: None,
            queue: VecDeque::new(),
            par: vec![0],
            line_next: 1,
            col_next: 1,
            col_prev: 1,
            line: 1,
            col: 1,
            eof: false,
            err: false,
            push_back: None,
        }
    }

    pub fn file(&self) -> &RcStr {
        &self.file
    }

    fn peek(&mut self) -> Option<char> {
        self.push_back.take().or_else(|| self.src.chars().next())
    }

    fn advance(&mut self) -> Option<char> {
        let next = self.peek();
        if let Some(c) = next {
            self.col_prev = self.col_next;
            match c {
                '\n' => {
                    self.line_next += 1;
                    self.col_next = 1;
                }
                _ => self.col_next += 1,
            }
            match self.lexeme {
                Some(ref mut lexeme) => lexeme.extend(c.len_utf8()),
                None => self.lexeme = Some(self.src.substring(0..c.len_utf8())),
            }
            self.src = self.src.substring(c.len_utf8()..self.src.len() - c.len_utf8());
        }
        next
    }

    fn ignore(&mut self) {
        self.lexeme = None;
    }

    fn lexeme(&mut self) -> RcStr {
        self.lexeme.take().unwrap_or_else(|| self.src.substring(0..0))
    }

    fn line(&mut self) -> usize {
        let line = self.line;
        self.line = self.line_next;
        line
    }

    fn col(&mut self) -> usize {
        let col = self.col;
        self.col = self.col_next;
        col
    }
}

impl Iterator for Lexer {
    type Item = Result<Token, Error>;

    fn next(&mut self) -> Option<Self::Item> {
        if self.err {
            return None;
        }

        self.queue.pop_front().map(Ok).or_else(|| {
            let tok = match_token(self);
            if let Some(ref tok) = tok {
                if tok.is_err() {
                    self.err = true;
                }
            }
            tok
        })
    }
}

fn match_token(lex: &mut Lexer) -> Option<Result<Token, Error>> {
    loop {
        match lex.advance() {
            Some(' ') | Some('\t') => lex.ignore(),
            Some('\n') => {
                let mut par = 0;
                loop {
                    match lex.peek() {
                        Some(' ') | Some('\t') => {
                            par += 1;
                            lex.advance();
                        }
                        _ => break,
                    }
                }
                let token = if par > *lex.par.last().unwrap() {
                    let token = Token::new(
                        Span::new(lex.file.clone(), lex.lexeme(), lex.line(), lex.col()),
                        TokenKind::ParBegin,
                    );
                    lex.par.push(par);
                    token
                } else if par < *lex.par.last().unwrap() {
                    let token = Token::new(
                        Span::new(lex.file.clone(), lex.lexeme(), lex.line(), lex.col()),
                        TokenKind::ParEnd,
                    );
                    let mut i = 0;
                    while *lex.par.last().unwrap() > par {
                        lex.par.pop();
                        i += 1;
                    }
                    if *lex.par.last().unwrap() != par {
                        return Some(Err(Error::new(ErrorKind::InvalidParagraph).with_span(&Span::new(
                            lex.file.clone(),
                            lex.lexeme(),
                            lex.line(),
                            lex.col(),
                        ))));
                    }
                    for _ in 0..i - 1 {
                        let token = Token::new(
                            Span::new(lex.file.clone(), lex.lexeme(), lex.line(), lex.col()),
                            TokenKind::ParEnd,
                        );
                        lex.queue.push_back(token);
                    }
                    token
                } else {
                    lex.ignore();
                    continue;
                };
                return Some(Ok(token));
            }
            Some('-') => {
                match lex.peek() {
                    Some('-') => {
                        loop {
                            match lex.peek() {
                                None => break,
                                Some('\n') => break,
                                Some(_) => {
                                    lex.advance();
                                }
                            }
                        }
                        lex.ignore();
                    }
                    Some(c) if c.is_ascii_digit() => return Some(match_number(lex)),
                    _ => return Some(match_id(lex)),
                }
            }
            Some(';') => {
                match lex.peek() {
                    Some(';') => {
                        lex.advance();
                        return Some(Ok(Token::new(
                            Span::new(lex.file.clone(), lex.lexeme(), lex.line(), lex.col()),
                            TokenKind::DoubleSemi,
                        )));
                    }
                    _ => {
                        return Some(Ok(Token::new(
                            Span::new(lex.file.clone(), lex.lexeme(), lex.line(), lex.col()),
                            TokenKind::Semi,
                        )));
                    }
                }
            }
            Some('^') => {
                return Some(Ok(Token::new(
                    Span::new(lex.file.clone(), lex.lexeme(), lex.line(), lex.col()),
                    TokenKind::Caret,
                )));
            }
            Some('\\') => {
                return Some(Ok(Token::new(
                    Span::new(lex.file.clone(), lex.lexeme(), lex.line(), lex.col()),
                    TokenKind::Backslash,
                )));
            }
            Some('`') => {
                return Some(Ok(Token::new(
                    Span::new(lex.file.clone(), lex.lexeme(), lex.line(), lex.col()),
                    TokenKind::Backtick,
                )));
            }
            Some('~') => {
                return Some(Ok(Token::new(
                    Span::new(lex.file.clone(), lex.lexeme(), lex.line(), lex.col()),
                    TokenKind::Tilde,
                )));
            }
            Some('(') => {
                return Some(Ok(Token::new(
                    Span::new(lex.file.clone(), lex.lexeme(), lex.line(), lex.col()),
                    TokenKind::ParenL,
                )));
            }
            Some(')') => {
                return Some(Ok(Token::new(
                    Span::new(lex.file.clone(), lex.lexeme(), lex.line(), lex.col()),
                    TokenKind::ParenR,
                )));
            }
            Some('[') => {
                return Some(Ok(Token::new(
                    Span::new(lex.file.clone(), lex.lexeme(), lex.line(), lex.col()),
                    TokenKind::SquareL,
                )));
            }
            Some(']') => {
                return Some(Ok(Token::new(
                    Span::new(lex.file.clone(), lex.lexeme(), lex.line(), lex.col()),
                    TokenKind::SquareR,
                )));
            }
            Some('{') => {
                return Some(Ok(Token::new(
                    Span::new(lex.file.clone(), lex.lexeme(), lex.line(), lex.col()),
                    TokenKind::CurlyL,
                )));
            }
            Some('}') => {
                return Some(Ok(Token::new(
                    Span::new(lex.file.clone(), lex.lexeme(), lex.line(), lex.col()),
                    TokenKind::CurlyR,
                )));
            }
            Some('|') => {
                return Some(Ok(Token::new(
                    Span::new(lex.file.clone(), lex.lexeme(), lex.line(), lex.col()),
                    TokenKind::Pipe,
                )));
            }
            Some('$') => {
                match lex.peek() {
                    Some('(') => {
                        lex.advance();
                        return Some(Ok(Token::new(
                            Span::new(lex.file.clone(), lex.lexeme(), lex.line(), lex.col()),
                            TokenKind::DollarParen,
                        )));
                    }
                    _ => {
                        return Some(Ok(Token::new(
                            Span::new(lex.file.clone(), lex.lexeme(), lex.line(), lex.col()),
                            TokenKind::Dollar,
                        )));
                    }
                }
            }
            Some(':') => {
                match lex.peek() {
                    Some(':') => {
                        lex.advance();
                        return Some(Ok(Token::new(
                            Span::new(lex.file.clone(), lex.lexeme(), lex.line(), lex.col()),
                            TokenKind::DoubleColon,
                        )));
                    }
                    _ => {
                        return Some(Ok(Token::new(
                            Span::new(lex.file.clone(), lex.lexeme(), lex.line(), lex.col()),
                            TokenKind::Colon,
                        )));
                    }
                }
            }
            Some('.') => {
                match lex.peek() {
                    Some('.') => {
                        lex.advance();
                        return Some(Ok(Token::new(
                            Span::new(lex.file.clone(), lex.lexeme(), lex.line(), lex.col()),
                            TokenKind::DoubleDot,
                        )));
                    }
                    _ => {
                        return Some(Ok(Token::new(
                            Span::new(lex.file.clone(), lex.lexeme(), lex.line(), lex.col()),
                            TokenKind::Dot,
                        )));
                    }
                }
            }
            Some(c) if c.is_id_begin() => return Some(match_id(lex)),
            Some(c) if c.is_ty_begin() => return Some(match_ty(lex)),
            Some(c) if c.is_ascii_digit() => return Some(match_number(lex)),
            Some('"') => return Some(match_str(lex)),
            Some('\'') => return Some(match_char(lex)),
            Some(c) => {
                return Some(Err(Error::new(ErrorKind::UnexpectedCharacter)
                    .describe(format!("{:?}", c))
                    .with_span(&Span::new(lex.file.clone(), lex.lexeme(), lex.line(), lex.col()))));
            }
            None if !lex.eof => {
                lex.eof = true;
                let mut i = 0;
                while *lex.par.last().unwrap() > 0 {
                    lex.par.pop();
                    i += 1;
                }
                for _ in 0..i {
                    let token = Token::new(
                        Span::new(lex.file.clone(), lex.lexeme(), lex.line(), lex.col()),
                        TokenKind::ParEnd,
                    );
                    lex.queue.push_back(token);
                }
                let eof = Token::new(
                    Span::new(lex.file.clone(), lex.lexeme(), lex.line(), lex.col()),
                    TokenKind::EOF,
                );
                lex.queue.push_back(eof);
                return lex.queue.pop_front().map(Ok);
            }
            None => return None,
        }
    }
}

fn match_id(lex: &mut Lexer) -> Result<Token, Error> {
    loop {
        match lex.peek() {
            Some(c) if c.is_id_cont() => {
                lex.advance();
            }
            Some(c) if c.is_break() => break,
            Some(c) => {
                return Err(Error::new(ErrorKind::UnexpectedCharacter)
                    .describe(format!("{:?}", c))
                    .with_span(&Span::new(lex.file.clone(), lex.lexeme(), lex.line(), lex.col())));
            }
            None => break,
        }
    }
    Ok(Token::new(
        Span::new(lex.file.clone(), lex.lexeme(), lex.line(), lex.col()),
        TokenKind::Atom,
    ))
}

fn match_ty(lex: &mut Lexer) -> Result<Token, Error> {
    loop {
        match lex.peek() {
            Some(c) if c.is_ty_cont() => {
                lex.advance();
            }
            Some(c) if c.is_break() => break,
            Some(c) => {
                return Err(Error::new(ErrorKind::UnexpectedCharacter)
                    .describe(format!("{:?}", c))
                    .with_span(&Span::new(lex.file.clone(), lex.lexeme(), lex.line(), lex.col())));
            }
            None => break,
        }
    }
    Ok(Token::new(
        Span::new(lex.file.clone(), lex.lexeme(), lex.line(), lex.col()),
        TokenKind::Type,
    ))
}

fn match_number(lex: &mut Lexer) -> Result<Token, Error> {
    #[derive(Clone, Copy, PartialEq, Eq)]
    enum Kind {
        Int,
        WithBase,
        Real,
        Ratio,
    }

    let mut kind = Kind::Int;

    loop {
        match lex.peek() {
            Some(c) if c.is_ascii_digit() => {
                lex.advance();
            }
            Some('_') => {
                lex.advance();
            }
            Some(':') => {
                if kind == Kind::Int {
                    kind = Kind::WithBase;
                    lex.advance();
                } else {
                    return Err(Error::new(ErrorKind::UnexpectedCharacter)
                        .describe(format!("{:?}", ';'))
                        .with_span(&Span::new(lex.file.clone(), lex.lexeme(), lex.line(), lex.col())));
                }
            }
            Some('.') => {
                if kind == Kind::Int {
                    kind = Kind::Real;
                    lex.advance();
                } else {
                    return Err(Error::new(ErrorKind::UnexpectedCharacter)
                        .describe(format!("{:?}", '.'))
                        .with_span(&Span::new(lex.file.clone(), lex.lexeme(), lex.line(), lex.col())));
                }
            }
            Some('/') => {
                if kind == Kind::Int {
                    kind = Kind::Ratio;
                    lex.advance();
                } else {
                    return Err(Error::new(ErrorKind::UnexpectedCharacter)
                        .describe(format!("{:?}", '/'))
                        .with_span(&Span::new(lex.file.clone(), lex.lexeme(), lex.line(), lex.col())));
                }
            }
            Some(c) if c.is_break() => break,
            Some(c) => {
                return Err(Error::new(ErrorKind::UnexpectedCharacter)
                    .describe(format!("{:?}", c))
                    .with_span(&Span::new(lex.file.clone(), lex.lexeme(), lex.line(), lex.col())));
            }
            None => break,
        }
    }
    let token = match kind {
        Kind::Int => TokenKind::Integer,
        Kind::WithBase => TokenKind::Integer,
        Kind::Real => TokenKind::Real,
        Kind::Ratio => TokenKind::Ratio,
    };
    Ok(Token::new(
        Span::new(lex.file.clone(), lex.lexeme(), lex.line(), lex.col()),
        token,
    ))
}

fn match_str(lex: &mut Lexer) -> Result<Token, Error> {
    loop {
        match lex.peek() {
            Some('\\') => {
                lex.advance();
                match lex.peek() {
                    Some('t') => {
                        lex.advance();
                    }
                    Some('n') => {
                        lex.advance();
                    }
                    Some('e') => {
                        lex.advance();
                    }
                    Some('\\') => {
                        lex.advance();
                    }
                    Some('\"') => {
                        lex.advance();
                    }
                    Some('~') => {
                        lex.advance();
                    }
                    _ => {
                        return Err(Error::new(ErrorKind::InvalidEscape).with_span(&Span::new(
                            lex.file.clone(),
                            lex.lexeme(),
                            lex.line(),
                            lex.col(),
                        )));
                    }
                }
            }
            Some('"') => {
                lex.advance();
                break;
            }
            Some('~') => {
                #[derive(Debug, Clone, Copy, PartialEq, Eq)]
                enum State {
                    Normal,
                    Char,
                    Str,
                    CharBackslash,
                    StrBackslash,
                }

                lex.advance();
                let mut state = State::Normal;
                let mut paren = false;
                let mut nesting = 0;
                loop {
                    match state {
                        State::Normal => {
                            match lex.peek() {
                                Some('(') => {
                                    paren = true;
                                    lex.advance();
                                    nesting += 1;
                                }
                                Some(')') => {
                                    lex.advance();
                                    if nesting == 0 {
                                        return Err(Error::new(ErrorKind::InterpParenMismatch).with_span(&Span::new(
                                            lex.file.clone(),
                                            lex.lexeme(),
                                            lex.line(),
                                            lex.col(),
                                        )));
                                    }
                                    nesting -= 1;
                                    if nesting == 0 {
                                        break;
                                    }
                                }
                                Some('\'') => {
                                    lex.advance();
                                    state = State::Char;
                                }
                                Some('"') => {
                                    lex.advance();
                                    state = State::Str;
                                }
                                Some(_) => {
                                    lex.advance();
                                }
                                None => {
                                    return Err(Error::new(ErrorKind::UnexpectedEof).with_span(&Span::new(
                                        lex.file.clone(),
                                        lex.lexeme(),
                                        lex.line(),
                                        lex.col(),
                                    )));
                                }
                            }
                        }
                        State::Char => {
                            match lex.peek() {
                                Some('\'') => {
                                    lex.advance();
                                    state = State::Normal;
                                }
                                Some('\\') => {
                                    lex.advance();
                                    state = State::CharBackslash;
                                }
                                Some(_) => {
                                    lex.advance();
                                }
                                None => {
                                    return Err(Error::new(ErrorKind::UnexpectedEof).with_span(&Span::new(
                                        lex.file.clone(),
                                        lex.lexeme(),
                                        lex.line(),
                                        lex.col(),
                                    )));
                                }
                            }
                        }
                        State::Str => {
                            match lex.peek() {
                                Some('"') => {
                                    lex.advance();
                                    state = State::Normal;
                                }
                                Some('\\') => {
                                    lex.advance();
                                    state = State::StrBackslash;
                                }
                                Some(_) => {
                                    lex.advance();
                                }
                                None => {
                                    return Err(Error::new(ErrorKind::UnexpectedEof).with_span(&Span::new(
                                        lex.file.clone(),
                                        lex.lexeme(),
                                        lex.line(),
                                        lex.col(),
                                    )));
                                }
                            }
                        }
                        State::CharBackslash => {
                            match lex.advance() {
                                Some(_) => {
                                    state = State::Char;
                                }
                                None => {
                                    return Err(Error::new(ErrorKind::UnexpectedEof).with_span(&Span::new(
                                        lex.file.clone(),
                                        lex.lexeme(),
                                        lex.line(),
                                        lex.col(),
                                    )));
                                }
                            }
                        }
                        State::StrBackslash => {
                            match lex.advance() {
                                Some(_) => {
                                    state = State::Str;
                                }
                                None => {
                                    return Err(Error::new(ErrorKind::UnexpectedEof).with_span(&Span::new(
                                        lex.file.clone(),
                                        lex.lexeme(),
                                        lex.line(),
                                        lex.col(),
                                    )));
                                }
                            }
                        }
                    }
                }
                if !paren {
                    return Err(Error::new(ErrorKind::InterpMissingParen).with_span(&Span::new(
                        lex.file.clone(),
                        lex.lexeme(),
                        lex.line(),
                        lex.col(),
                    )));
                }
            }
            Some(_) => {
                lex.advance();
            }
            None => {
                return Err(Error::new(ErrorKind::MissingClosingQuote).with_span(&Span::new(
                    lex.file.clone(),
                    lex.lexeme(),
                    lex.line(),
                    lex.col(),
                )));
            }
        }
    }

    Ok(Token::new(
        Span::new(lex.file.clone(), lex.lexeme(), lex.line(), lex.col()),
        TokenKind::Str,
    ))
}

fn match_char(lex: &mut Lexer) -> Result<Token, Error> {
    match lex.peek() {
        Some('\\') => {
            lex.advance();
            match lex.peek() {
                Some('t') => {
                    lex.advance();
                }
                Some('n') => {
                    lex.advance();
                }
                Some('e') => {
                    lex.advance();
                }
                Some('\\') => {
                    lex.advance();
                }
                Some('\'') => {
                    lex.advance();
                }
                _ => {
                    return Err(Error::new(ErrorKind::InvalidEscape).with_span(&Span::new(
                        lex.file.clone(),
                        lex.lexeme(),
                        lex.line(),
                        lex.col(),
                    )));
                }
            }
        }
        Some('\'') => {
            return Err(Error::new(ErrorKind::InvalidCharLiteral).with_span(&Span::new(
                lex.file.clone(),
                lex.lexeme(),
                lex.line(),
                lex.col(),
            )));
        }
        Some(_) => {
            lex.advance();
        }
        None => {
            return Err(Error::new(ErrorKind::MissingClosingQuote).with_span(&Span::new(
                lex.file.clone(),
                lex.lexeme(),
                lex.line(),
                lex.col(),
            )));
        }
    }

    match lex.peek() {
        Some('\'') => {
            lex.advance();
        }
        _ => {
            return Err(Error::new(ErrorKind::MissingClosingQuote).with_span(&Span::new(
                lex.file.clone(),
                lex.lexeme(),
                lex.line(),
                lex.col(),
            )));
        }
    }

    Ok(Token::new(
        Span::new(lex.file.clone(), lex.lexeme(), lex.line(), lex.col()),
        TokenKind::Char,
    ))
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn sanity() {
        let input = "(Type T a b [ | $x |
    let x = 5; -- some comment
    let y = + x 1.2 1/2 2:1;
    \"some ~(interpolated) string\" ]);;";
        let input = RcStr::from(input);
        let lexer = Lexer::new(RcStr::from("<test>"), input);
        println!(
            "{:?}",
            lexer
                .map(|tok| {
                    tok.map(|tok| (tok.kind, tok.span.src.to_string(), tok.span.line, tok.span.col))
                        .unwrap()
                })
                .collect::<Vec<_>>()
        );
    }

    #[test]
    fn par() {
        let input = "a\n b\n  c;;";
        let input = RcStr::from(input);
        let lexer = Lexer::new(RcStr::from("<test>"), input);
        println!(
            "{:?}",
            lexer
                .map(|tok| {
                    tok.map(|tok| (tok.kind, tok.span.src.to_string(), tok.span.line, tok.span.col))
                        .unwrap()
                })
                .collect::<Vec<_>>()
        );
    }

    #[test]
    fn negative() {
        let input = RcStr::from("\"~a\"");
        let lexer = Lexer::new(RcStr::from("<test>"), input);
        println!("{:?}", lexer.map(|err| err.unwrap_err()).collect::<Vec<_>>());

        let input = RcStr::from("\"~(\"");
        let lexer = Lexer::new(RcStr::from("<test>"), input);
        println!("{:?}", lexer.map(|err| err.unwrap_err()).collect::<Vec<_>>());

        let input = RcStr::from("\"~)\"");
        let lexer = Lexer::new(RcStr::from("<test>"), input);
        println!("{:?}", lexer.map(|err| err.unwrap_err()).collect::<Vec<_>>());
    }
}
