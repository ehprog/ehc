use std::collections::{HashSet, HashMap};

use crate::ty::Type;

#[derive(Debug, Clone, PartialEq, Eq, Serialize, Deserialize)]
pub struct Ehlib {
    pub dl: String,
    pub types: HashSet<Type>,
    pub symbols: HashMap<Vec<String>, Type>,
}
