use std::env;
use std::fs;
use std::path::Path;
use std::collections::HashMap;

use bincode::deserialize;

use crate::error::{Error, ErrorKind};
use crate::collections::RcStr;
use crate::lex::Lexer;
use crate::pp::Preprocessor;
use crate::parse::Parser;
use crate::parse::ast::{self, normalize_string, BlockLike, StmtLike, ExprLike};
use self::ehlib::Ehlib;

pub mod ehlib;

#[derive(Debug, Clone, PartialEq, Eq)]
enum ModuleKind {
    Source(RcStr, RcStr),
    Ehlib(RcStr, RcStr),
}

#[derive(Debug, Clone, PartialEq)]
pub enum Module {
    Source {
        dependencies: Vec<Vec<RcStr>>,
        program: ast::Program,
    },
    Ehlib(Ehlib),
}

#[derive(Debug, Clone, PartialEq)]
pub struct Modules {
    pub main: Vec<RcStr>,
    pub modules: HashMap<Vec<RcStr>, Module>,
}

pub struct ModuleHandler {
    parser: Parser,
    modname: Vec<RcStr>,
    path: Option<RcStr>,
}

impl ModuleHandler {
    pub fn new(parser: Parser, mut rel: &str, path: Option<RcStr>) -> Self {
        if rel.ends_with(".ehp") {
            rel = &rel[..rel.len() - 4];
        }
        let modname = rel.split('/').map(From::from).collect();
        ModuleHandler { parser, modname, path }
    }

    pub fn modname(&self) -> &[RcStr] {
        &self.modname
    }

    pub fn is_core(&self) -> bool {
        self.modname == ["core"]
    }

    pub fn handle(self) -> Result<Modules, Vec<Error>> {
        let is_core = self.is_core();
        let modname = self.modname.clone();
        let main = self.parser.parse()?;
        let mut dependencies = vec![];
        let mut modules = HashMap::new();
        if !is_core {
            import_core(&mut dependencies, &mut modules)?;
        }
        main.block.handle(self.path.as_ref(), &mut dependencies, &mut modules)?;
        modules.insert(
            modname.clone(),
            Module::Source {
                dependencies,
                program: main,
            },
        );
        Ok(Modules { main: modname, modules })
    }
}

pub trait Handler<Seal> {
    fn handle(
        &self,
        globalpath: Option<&RcStr>,
        dependencies: &mut Vec<Vec<RcStr>>,
        modules: &mut HashMap<Vec<RcStr>, Module>,
    ) -> Result<(), Vec<Error>>;
}

impl<T: BlockLike> Handler<ast::BlockSeal> for T {
    fn handle(
        &self,
        globalpath: Option<&RcStr>,
        dependencies: &mut Vec<Vec<RcStr>>,
        modules: &mut HashMap<Vec<RcStr>, Module>,
    ) -> Result<(), Vec<Error>> {
        let mut errors = vec![];
        for stmt in self.stmts() {
            stmt.handle(globalpath, dependencies, modules)
                .unwrap_or_else(|errs| errors.extend(errs));
        }
        for expr in self.expr() {
            expr.handle(globalpath, dependencies, modules)
                .unwrap_or_else(|errs| errors.extend(errs));
        }
        if errors.is_empty() {
            Ok(())
        } else {
            Err(errors)
        }
    }
}

impl<T: StmtLike> Handler<ast::StmtSeal> for T {
    fn handle(
        &self,
        globalpath: Option<&RcStr>,
        dependencies: &mut Vec<Vec<RcStr>>,
        modules: &mut HashMap<Vec<RcStr>, Module>,
    ) -> Result<(), Vec<Error>> {
        self.expr().handle(globalpath, dependencies, modules)
    }
}

impl<T: ExprLike> Handler<ast::ExprSeal> for T {
    fn handle(
        &self,
        globalpath: Option<&RcStr>,
        dependencies: &mut Vec<Vec<RcStr>>,
        modules: &mut HashMap<Vec<RcStr>, Module>,
    ) -> Result<(), Vec<Error>> {
        match &self.first().kind {
            ast::WordKind::Atom(atom) => {
                match atom.atom.as_str() {
                    "use" | "from" => {
                        let module = self.rest().get(0).ok_or_else(|| {
                            vec![Error::new(ErrorKind::ImportSyntax)
                                .with_span(self.span())
                                .describe("missing module")]
                        })?;
                        match &module.kind {
                            ast::WordKind::Atom(module) => {
                                handle_import(globalpath, dependencies, modules, &[module.atom.clone()], &module.atom)
                            }
                            ast::WordKind::Path(module) => {
                                let path = module.path[1..]
                                    .iter()
                                    .fold(module.path[0].to_string(), |mut acc, path| {
                                        acc.push('/');
                                        acc.push_str(path);
                                        acc
                                    });
                                handle_import(globalpath, dependencies, modules, &module.path, &From::from(path))
                            }
                            _ => {
                                Err(vec![Error::new(ErrorKind::ImportSyntax)
                                    .with_span(self.span())
                                    .describe("module must be an atom")])
                            }
                        }
                    }
                    _ => Ok(()),
                }
            }
            _ => Ok(()),
        }
    }
}

fn handle_import(
    globalpath: Option<&RcStr>,
    dependencies: &mut Vec<Vec<RcStr>>,
    modules: &mut HashMap<Vec<RcStr>, Module>,
    module: &[RcStr],
    path: &RcStr,
) -> Result<(), Vec<Error>> {
    let name = RcStr::from(module[1..].iter().fold(module[0].to_string(), |mut acc, path| {
        acc.push('.');
        acc.push_str(path);
        acc
    }));
    let normalized = RcStr::from(normalize_string(&name));
    if modules.contains_key(module) {
        return Ok(());
    }

    let kind = find_module(globalpath, path).map_err(|searched| {
        let mut descr = format!("{:?}, searched in:\n", name.as_str());
        for dir in &searched {
            descr.push_str(&format!(" * {:?},\n", dir));
        }
        vec![Error::new(ErrorKind::ImportFailed).describe(descr)]
    })?;
    match kind {
        ModuleKind::Source(path, dir) => {
            let src = fs::read_to_string(path.as_str()).expect("couldn't read source");
            import_module(
                globalpath,
                dependencies,
                modules,
                name,
                normalized,
                module,
                path,
                dir,
                From::from(src),
            )
        }
        ModuleKind::Ehlib(path, dir) => {
            let src = fs::read(path.as_str()).expect("couldn't read source");
            import_ehlib(
                globalpath,
                dependencies,
                modules,
                name,
                normalized,
                module,
                path,
                dir,
                From::from(src),
            )
        }
    }
}

fn import_module(
    globalpath: Option<&RcStr>,
    dependencies: &mut Vec<Vec<RcStr>>,
    modules: &mut HashMap<Vec<RcStr>, Module>,
    _name: RcStr,
    _normalized: RcStr,
    module: &[RcStr],
    path: RcStr,
    _dir: RcStr,
    src: RcStr,
) -> Result<(), Vec<Error>> {
    let is_core = module == ["core"];
    let lexer = Lexer::new(From::from(path), src);
    let pp = Preprocessor::new(lexer);
    let parser = Parser::new(pp);
    let ast = parser.parse()?;
    dependencies.push(module.to_vec());
    {
        let mut dependencies = vec![];
        if !is_core {
            dependencies.push(vec![From::from("core")]);
        }
        ast.block.handle(globalpath, &mut dependencies, modules)?;
        modules.insert(
            module.to_vec(),
            Module::Source {
                dependencies,
                program: ast,
            },
        );
    }
    Ok(())
}

fn import_ehlib(
    _globalpath: Option<&RcStr>,
    dependencies: &mut Vec<Vec<RcStr>>,
    modules: &mut HashMap<Vec<RcStr>, Module>,
    _name: RcStr,
    _normalized: RcStr,
    module: &[RcStr],
    _path: RcStr,
    _dir: RcStr,
    src: Vec<u8>,
) -> Result<(), Vec<Error>> {
    let ehlib = deserialize(&src).expect("couldn't deserialize file");
    dependencies.push(module.to_vec());
    modules.insert(module.to_vec(), Module::Ehlib(ehlib));
    Ok(())
}

fn import_core(
    dependencies: &mut Vec<Vec<RcStr>>,
    modules: &mut HashMap<Vec<RcStr>, Module>,
) -> Result<(), Vec<Error>> {
    let name = RcStr::from("core");
    let lexer = Lexer::new(name.clone(), From::from(crate::CORE));
    let pp = Preprocessor::new(lexer);
    let parser = Parser::new(pp);
    let ast = parser.parse()?;
    let module = vec![name.clone()];
    dependencies.push(module.clone());
    {
        let mut dependencies = vec![];
        ast.block.handle(None, &mut dependencies, modules)?;
        modules.insert(
            module.clone(),
            Module::Source {
                dependencies,
                program: ast,
            },
        );
    }
    Ok(())
}

fn find_module(path: Option<&RcStr>, module: &RcStr) -> Result<ModuleKind, Vec<String>> {
    static STATIC_DIRS: &[&str] = &[
        ".",
        "./src",
        "/usr/lib",
        "/usr/lib32",
        "/usr/lib64",
        "/usr/share/ehprog",
    ];
    let home_dirs = [".local/lib", ".local/lib32", ".local/lib64"]
        .iter()
        .map(|path| format!("{}/{}", env::var("HOME").unwrap(), path))
        .collect::<Vec<_>>();
    for dir in path
        .map(AsRef::as_ref)
        .iter()
        .chain(STATIC_DIRS)
        .map(|string| *string)
        .chain(home_dirs.iter().map(String::as_str))
    {
        let path = AsRef::<Path>::as_ref(dir).join(format!("{}.ehp", module));
        if path.exists() {
            return Ok(ModuleKind::Source(
                From::from(path.to_string_lossy().as_ref()),
                From::from(dir),
            ));
        }
        let path = AsRef::<Path>::as_ref(dir).join(format!("{}/_mod.ehp", module));
        if path.exists() {
            return Ok(ModuleKind::Source(
                From::from(path.to_string_lossy().as_ref()),
                From::from(dir),
            ));
        }
        let path = AsRef::<Path>::as_ref(dir).join(format!("{}.ehlib", module));
        if path.exists() {
            return Ok(ModuleKind::Ehlib(
                From::from(path.to_string_lossy().as_ref()),
                From::from(dir),
            ));
        }
        let path = AsRef::<Path>::as_ref(dir).join(format!("{}/_mod.ehlib", module));
        if path.exists() {
            return Ok(ModuleKind::Ehlib(
                From::from(path.to_string_lossy().as_ref()),
                From::from(dir),
            ));
        }
    }
    Err(path
        .map(AsRef::as_ref)
        .iter()
        .chain(STATIC_DIRS)
        .map(|string| *string)
        .chain(home_dirs.iter().map(String::as_str))
        .map(|path| {
            vec![
                format!("{}/{}.ehp", path, module),
                format!("{}/{}/_mod.ehp", path, module),
                format!("{}/{}.ehlib", path, module),
                format!("{}/{}/_mod.ehlib", path, module),
            ]
        })
        .flatten()
        .collect())
}
