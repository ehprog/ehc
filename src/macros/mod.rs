use std::iter;
use std::collections::HashMap;
use std::cell::RefCell;

use num_rational::Rational64;

use crate::error::{Error, ErrorKind};
use crate::collections::{Boxed, RcStr};
use crate::lex::Span;
use crate::parse::ast::{self, AsMeta, BlockLike, StmtLike, ExprLike, Meta};
use crate::module::{Modules, Module};
use crate::ty::{
    TypeOf,
    TypeContext,
    TypeInferrer,
};

pub struct MacroExpander {
    types: TypeInferrer,
    modname: Vec<RcStr>,
    ctx: Option<ExpanderContext>,
}

impl MacroExpander {
    pub fn new(types: TypeInferrer) -> Self {
        let modname = types.modname().to_vec();
        MacroExpander {
            types,
            modname,
            ctx: None,
        }
    }

    pub fn with_ctx(types: TypeInferrer, ctx: ExpanderContext) -> Self {
        let modname = types.modname().to_vec();
        MacroExpander {
            types,
            modname,
            ctx: Some(ctx),
        }
    }

    pub fn modname(&self) -> &[RcStr] {
        &self.modname
    }

    pub fn is_core(&self) -> bool {
        self.modname == ["core"]
    }

    pub fn expand(self) -> Result<(Modules, (TypeContext, ExpanderContext)), Vec<Error>> {
        let (mut modules, mut types) = self.types.evaluate()?;

        let name = modules.main.clone();
        let main = modules.modules.remove(&name).unwrap();

        let mut ctx = self.ctx.unwrap_or_default();
        ctx.evaluate(&mut modules, main, name)?;

        let name = modules.main.clone();
        let main = modules.modules.remove(&name).unwrap();
        types.evaluate(&mut modules, name, main, false)?;

        Ok((modules, (types, ctx)))
    }
}

#[derive(Debug, Clone)]
pub struct ExpanderContext {
    macros: Vec<HashMap<RcStr, Macro>>,
    bindings: Vec<HashMap<RcStr, ast::Word>>,
    gensym: HashMap<String, usize>,
}

impl Default for ExpanderContext {
    fn default() -> Self {
        ExpanderContext {
            macros: vec![Default::default()],
            bindings: vec![Default::default()],
            gensym: HashMap::new(),
        }
    }
}

impl ExpanderContext {
    pub fn evaluate(&mut self, mods: &mut Modules, mut module: Module, name: Vec<RcStr>) -> Result<(), Vec<Error>> {
        let mut errors = vec![];
        match &module {
            Module::Source { dependencies, .. } => {
                for module in dependencies {
                    if let Some(dep) = mods.modules.remove(module) {
                        let mut ctx = ExpanderContext::default();
                        ctx.evaluate(mods, dep, module.clone())
                            .unwrap_or_else(|err| errors.extend(err));
                        self.import(&ctx);
                    }
                }
            }
            Module::Ehlib(..) => {}
        }

        if errors.is_empty() {
            match &mut module {
                Module::Source { program, .. } => program.block = program.block.expand(self)?,
                Module::Ehlib(..) => {}
            }
            mods.modules.insert(name, module);
            Ok(())
        } else {
            Err(errors)
        }
    }

    pub fn import(&mut self, other: &Self) {
        for macros in &other.macros {
            self.macros
                .first_mut()
                .unwrap()
                .extend(macros.iter().map(|(k, v)| (k.clone(), v.clone())));
        }
    }

    pub fn enter(&mut self) {
        self.macros.push(HashMap::new());
        self.bindings.push(HashMap::new());
    }

    pub fn leave(&mut self) {
        self.macros.pop();
        self.bindings.pop();
    }

    pub fn get_macro(&self, atom: &RcStr) -> Option<&Macro> {
        for macros in self.macros.iter().rev() {
            if let Some(m) = macros.get(atom) {
                return Some(m);
            }
        }

        None
    }

    pub fn get_binding(&self, atom: &RcStr) -> Option<&ast::Word> {
        for bindings in self.bindings.iter().rev() {
            if let Some(m) = bindings.get(atom) {
                return Some(m);
            }
        }

        None
    }

    pub fn insert_macro(&mut self, atom: RcStr, value: Macro) {
        self.macros.last_mut().unwrap().insert(atom, value);
    }

    pub fn insert_binding(&mut self, atom: RcStr, value: ast::Word) {
        self.bindings.last_mut().unwrap().insert(atom, value);
    }

    pub fn gensym(&mut self, proto: Option<&str>) -> String {
        let proto = proto.unwrap_or("");
        let num = self.gensym.entry(proto.to_string()).or_default();
        let sym = format!("__gen_{}_{}", proto, num);
        *num += 1;
        sym
    }
}

#[derive(Debug, Clone)]
pub struct Macro {
    implv: Vec<Impl<ast::Expr>>,
}

impl Macro {
    pub fn macro_call(&self, argv: Vec<ast::Word>, expander: &mut ExpanderContext) -> Result<ast::Word, Vec<Error>> {
        for im in &self.implv {
            expander.enter();
            if im.matches(&argv, expander)? {
                return im.macro_call(argv, expander);
            }
            expander.leave();
        }

        Err(vec![Error::new(ErrorKind::MacroMismatch)])
    }

    pub fn call(&self, argv: Vec<ast::Word>, expander: &mut ExpanderContext) -> Result<ast::Word, Vec<Error>> {
        let argv = argv.into_iter().map(|arg| arg.interpret(expander)).fold(
            Ok(vec![]),
            |acc: Result<Vec<_>, Vec<Error>>, stmt| {
                let mut acc = match acc {
                    Ok(ok) => ok,
                    Err(mut errors) => {
                        if let Err(errs) = stmt {
                            errors.extend(errs);
                        }
                        return Err(errors);
                    }
                };
                match stmt {
                    Ok(ok) => acc.push(ok),
                    Err(err) => return Err(err),
                }
                Ok(acc)
            },
        )?;
        for im in &self.implv {
            expander.enter();
            if im.matches(&argv, expander)? {
                return im.call(argv, expander);
            }
            expander.leave();
        }

        Err(vec![Error::new(ErrorKind::MacroMismatch)])
    }
}

impl From<Vec<ast::Closure<ast::Expr>>> for Macro {
    fn from(closure: Vec<ast::Closure<ast::Expr>>) -> Self {
        assert_eq!(closure[0].macroeh, true, "this closure is not a macro");
        Macro {
            implv: closure.into_iter().map(From::from).collect(),
        }
    }
}

#[derive(Debug, Clone)]
pub struct Impl<T: ExprLike> {
    dynamic: bool,
    mutable: bool,
    paramv: Vec<ast::Word>,
    body: Boxed<RefCell<T>>,
}

impl<T: ExprLike> Impl<T> {
    pub fn matches(&self, argv: &[ast::Word], expander: &mut ExpanderContext) -> Result<bool, Vec<Error>> {
        if self.paramv.len() != argv.len() {
            Ok(false)
        } else {
            let mut errors = vec![];
            for (param, arg) in self.paramv.iter().zip(argv) {
                if !param.matches(arg, expander).unwrap_or_else(|err| {
                    errors.extend(err);
                    true
                }) {
                    return Ok(false);
                }
            }

            Ok(true)
        }
    }

    pub fn macro_call(&self, _argv: Vec<ast::Word>, expander: &mut ExpanderContext) -> Result<ast::Word, Vec<Error>> {
        self.body.borrow().interpret(expander)
    }

    pub fn call(&self, _argv: Vec<ast::Word>, expander: &mut ExpanderContext) -> Result<ast::Word, Vec<Error>> {
        self.body.borrow().interpret(expander)
    }
}

impl<T: ExprLike> From<ast::Closure<T>> for Impl<T> {
    fn from(closure: ast::Closure<T>) -> Self {
        assert_eq!(closure.macroeh, true, "this closure is not a macro");
        let dynamic = closure.dynamic;
        let mutable = closure.mutable;
        let paramv = closure.params;
        let body = closure.body;
        Impl {
            dynamic,
            mutable,
            paramv,
            body,
        }
    }
}

pub trait Expand<Seal> {
    type Output;
    fn expand(&self, expander: &mut ExpanderContext) -> Result<Self::Output, Vec<Error>>;
}

pub trait Interpret<Seal> {
    fn interpret(&self, interpreter: &mut ExpanderContext) -> Result<ast::Word, Vec<Error>>;
}

impl<T: BlockLike> Expand<ast::BlockSeal> for T {
    type Output = ast::Block;

    fn expand(&self, expander: &mut ExpanderContext) -> Result<Self::Output, Vec<Error>> {
        let stmts = self.stmts().iter().map(|stmt| stmt.expand(expander)).fold(
            Ok(vec![]),
            |acc: Result<Vec<ast::Stmt>, Vec<Error>>, stmt| {
                let mut acc = match acc {
                    Ok(ok) => ok,
                    Err(mut errors) => {
                        if let Err(errs) = stmt {
                            errors.extend(errs);
                        }
                        return Err(errors);
                    }
                };
                match stmt {
                    Ok(ok) => acc.push(ok),
                    Err(err) => return Err(err),
                }
                Ok(acc)
            },
        )?;
        let expr = self.expr().map(|expr| expr.expand(expander)).transpose()?;
        Ok(ast::Block::new(self.span().clone(), stmts, expr))
    }
}

impl<T: BlockLike> Interpret<ast::BlockSeal> for T {
    fn interpret(&self, interpreter: &mut ExpanderContext) -> Result<ast::Word, Vec<Error>> {
        self.stmts().iter().map(|stmt| stmt.interpret(interpreter)).fold(
            Ok(()),
            |acc: Result<(), Vec<Error>>, stmt| {
                match acc {
                    Ok(_) => {}
                    Err(mut errors) => {
                        if let Err(errs) = stmt {
                            errors.extend(errs);
                        }
                        return Err(errors);
                    }
                }
                match stmt {
                    Ok(_) => Ok(()),
                    Err(err) => Err(err),
                }
            },
        )?;
        let ret = self
            .expr()
            .map(|expr| expr.interpret(interpreter))
            .transpose()?
            .unwrap_or_else(|| {
                ast::Word {
                    meta: Meta::new(self.span().clone()),
                    kind: ast::WordKind::Empty,
                }
            });
        Ok(ret)
    }
}

impl<T: StmtLike> Expand<ast::StmtSeal> for T {
    type Output = ast::Stmt;

    fn expand(&self, expander: &mut ExpanderContext) -> Result<Self::Output, Vec<Error>> {
        Ok(ast::Stmt::new(self.span().clone(), self.expr().expand(expander)?))
    }
}

impl<T: StmtLike> Interpret<ast::StmtSeal> for T {
    fn interpret(&self, interpreter: &mut ExpanderContext) -> Result<ast::Word, Vec<Error>> {
        self.expr().interpret(interpreter)?;
        Ok(ast::Word {
            meta: Meta::new(self.span().clone()),
            kind: ast::WordKind::Empty,
        })
    }
}

impl<T: ExprLike> Expand<ast::ExprSeal> for T {
    type Output = ast::Expr;

    fn expand(&self, expander: &mut ExpanderContext) -> Result<Self::Output, Vec<Error>> {
        fn expand_regular<T: ExprLike>(expr: &T, expander: &mut ExpanderContext) -> Result<ast::Expr, Vec<Error>> {
            let first = expr.first().expand(expander)?;
            let rest = expr.rest().iter().map(|word| word.expand(expander)).fold(
                Ok(vec![]),
                |acc: Result<Vec<ast::Word>, Vec<Error>>, word| {
                    let mut acc = match acc {
                        Ok(ok) => ok,
                        Err(mut errors) => {
                            if let Err(errs) = word {
                                errors.extend(errs);
                            }
                            return Err(errors);
                        }
                    };
                    match word {
                        Ok(ok) => acc.push(ok),
                        Err(err) => return Err(err),
                    }
                    Ok(acc)
                },
            )?;
            Ok(ast::Expr::new(expr.span().clone(), first, rest))
        }

        if self.rest().is_empty() {
            let first = self.first().expand(expander)?;
            Ok(ast::Expr::new(self.span().clone(), first, vec![]))
        } else {
            let ty = self.first().eval_type();
            let (func, rest) = if ty.is_macro() {
                match &self.first().kind {
                    ast::WordKind::Atom(atom) if atom.atom.as_str() == "macro" => {
                        return expand_macro(self, expander);
                    }
                    ast::WordKind::Atom(atom) => {
                        (
                            expander.get_macro(&atom.atom).unwrap().clone(),
                            self.rest().iter().cloned().map(Into::into).collect(),
                        )
                    }
                    ast::WordKind::Closure(closure) if closure[0].macroeh => {
                        let closure = closure.iter().map(|closure| closure.expand(expander)).fold(
                            Ok(vec![]),
                            |acc: Result<Vec<_>, Vec<Error>>, param| {
                                let mut acc = match acc {
                                    Ok(ok) => ok,
                                    Err(mut errors) => {
                                        if let Err(errs) = param {
                                            errors.extend(errs);
                                        }
                                        return Err(errors);
                                    }
                                };
                                match param {
                                    Ok(ok) => acc.push(ok),
                                    Err(err) => return Err(err),
                                }
                                Ok(acc)
                            },
                        )?;
                        (
                            Macro::from(closure),
                            self.rest().iter().cloned().map(Into::into).collect(),
                        )
                    }
                    _ => unreachable!(),
                }
            } else if ty.is_func() {
                return expand_regular(self, expander);
            } else {
                let snd = &self.rest()[0];
                let ty = snd.eval_type();
                if ty.is_macro() {
                    match &snd.kind {
                        ast::WordKind::Atom(atom) => {
                            let func = expander.get_macro(&atom.atom).unwrap().clone();
                            let rest = iter::once(self.first().clone().into())
                                .chain(self.rest().iter().skip(1).cloned().map(Into::into))
                                .collect();
                            (func, rest)
                        }
                        ast::WordKind::Closure(closure) if closure[0].macroeh => {
                            let closure = closure.iter().map(|closure| closure.expand(expander)).fold(
                                Ok(vec![]),
                                |acc: Result<Vec<_>, Vec<Error>>, param| {
                                    let mut acc = match acc {
                                        Ok(ok) => ok,
                                        Err(mut errors) => {
                                            if let Err(errs) = param {
                                                errors.extend(errs);
                                            }
                                            return Err(errors);
                                        }
                                    };
                                    match param {
                                        Ok(ok) => acc.push(ok),
                                        Err(err) => return Err(err),
                                    }
                                    Ok(acc)
                                },
                            )?;
                            let rest = iter::once(self.first().clone().into())
                                .chain(self.rest().iter().skip(1).cloned().map(Into::into))
                                .collect();
                            (Macro::from(closure), rest)
                        }
                        _ => unreachable!(),
                    }
                } else {
                    return expand_regular(self, expander);
                }
            };
            let result = func.macro_call(rest, expander)?;
            match &result.kind {
                ast::WordKind::Parenth(list) => {
                    let list_ref = list.borrow();
                    match list_ref.first.kind {
                        ast::WordKind::Parenth(..) => {
                            let mut stmts = vec![];
                            let mut expr: Option<ast::Expr> = None;
                            let iter = iter::once(list_ref.first.clone()).chain(list_ref.rest.iter().cloned());
                            let mut errors = vec![];
                            for word in iter {
                                match word.kind {
                                    ast::WordKind::Parenth(stmt) => {
                                        if let Some(expr) = expr.take() {
                                            stmts.push(ast::Stmt {
                                                meta: Meta::new(expr.span().clone()),
                                                expr,
                                            });
                                        }
                                        expr = Some(ast::Expr::from(stmt.borrow().clone()));
                                    }
                                    _ => {
                                        errors.push(Error::new(ErrorKind::ExpectedCompoundList).with_span(&word.span()))
                                    }
                                }
                            }
                            if errors.is_empty() {
                                let block = Boxed::new(RefCell::new(ast::Block {
                                    meta: Meta::new(result.span().clone()),
                                    stmts,
                                    expr,
                                }));
                                Ok(ast::Expr::new(
                                    result.span().clone(),
                                    ast::Word {
                                        meta: Meta::new(result.span().clone()),
                                        kind: ast::WordKind::Compound(block),
                                    },
                                    vec![],
                                ))
                            } else {
                                Err(errors)
                            }
                        }
                        _ => Ok(ast::Expr::from(list.borrow().clone())),
                    }
                }
                _ => Ok(ast::Expr::new(self.span().clone(), result, vec![])),
            }
        }
    }
}

pub enum Func<T: ExprLike> {
    Let,
    LetFun,
    LetRec,
    Gensym,
    Head,
    Tail,
    Add,
    Sub,
    Mul,
    Div,
    Mod,
    Closure(Vec<ast::Closure<T>>),
}

macro_rules! binary_arithmetic {
    ($p:expr, $i:expr, $s:expr, $op:tt) => {
        {
            let lhs = $p[0].interpret($i)?;
            let rhs = $p[1].interpret($i)?;
            let result = match (lhs.kind, rhs.kind) {
                (ast::WordKind::Integer(a), ast::WordKind::Integer(b)) => ast::WordKind::Integer(a $op b),
                (ast::WordKind::Integer(int), ast::WordKind::Ratio(sign, p, q)) => {
                    let p = match sign {
                        1 => p as i64,
                        -1 => -(p as i64),
                        _ => unreachable!(),
                    };
                    let q = q as i64;
                    let result = Rational64::from(int) $op Rational64::new(p, q);
                    let p = *result.numer();
                    let q = *result.denom() as u64;
                    let sign = match p.signum() {
                        x if x < 0 => -1,
                        _ => 1,
                    };
                    let p = p as u64;
                    ast::WordKind::Ratio(sign, p, q)
                }
                (ast::WordKind::Integer(a), ast::WordKind::Real(b)) => ast::WordKind::Real(a as f64 $op b),
                (ast::WordKind::Ratio(sign, p, q), ast::WordKind::Integer(int)) => {
                    let p = match sign {
                        1 => p as i64,
                        -1 => -(p as i64),
                        _ => unreachable!(),
                    };
                    let q = q as i64;
                    let result = Rational64::new(p, q) $op Rational64::from(int);
                    let p = *result.numer();
                    let q = *result.denom() as u64;
                    let sign = match p.signum() {
                        x if x < 0 => -1,
                        _ => 1,
                    };
                    let p = p as u64;
                    ast::WordKind::Ratio(sign, p, q)
                }
                (ast::WordKind::Ratio(sign1, p1, q1), ast::WordKind::Ratio(sign2, p2, q2)) => {
                    let p1 = match sign1 {
                        1 => p1 as i64,
                        -1 => -(p1 as i64),
                        _ => unreachable!(),
                    };
                    let q1 = q1 as i64;
                    let p2 = match sign2 {
                        1 => p2 as i64,
                        -1 => -(p2 as i64),
                        _ => unreachable!(),
                    };
                    let q2 = q2 as i64;
                    let result = Rational64::new(p1, q1) $op Rational64::new(p2, q2);
                    let p = *result.numer();
                    let q = *result.denom() as u64;
                    let sign = match p.signum() {
                        x if x < 0 => -1,
                        _ => 1,
                    };
                    let p = p as u64;
                    ast::WordKind::Ratio(sign, p, q)
                }
                (ast::WordKind::Ratio(sign, p, q), ast::WordKind::Real(float)) => {
                    let p = p as f64;
                    let q = q as f64;
                    let ratio = sign as f64 * p/q;
                    let result = ratio $op float;
                    ast::WordKind::Real(result)
                }
                (ast::WordKind::Real(a), ast::WordKind::Integer(b)) => ast::WordKind::Real(a $op b as f64),
                (ast::WordKind::Real(float), ast::WordKind::Ratio(sign, p, q)) => {
                    let p = p as f64;
                    let q = q as f64;
                    let ratio = sign as f64 * p/q;
                    let result = float $op ratio;
                    ast::WordKind::Real(result)
                }
                (ast::WordKind::Real(a), ast::WordKind::Real(b)) => ast::WordKind::Real(a $op b),
                _ => {
                    return Err(vec![Error::new(ErrorKind::TypeMismatch)
                        .describe("expected number")
                        .with_span(&$s)]);
                }

            };
            Ok(From::from(ast::Word { meta: Meta::new($s), kind: result }))
        }
    }
}

impl<T: ExprLike> Func<T> {
    pub fn call(
        &self,
        expr: &T,
        params: Vec<ast::Word>,
        interpreter: &mut ExpanderContext,
        span: Span,
    ) -> Result<ast::Word, Vec<Error>> {
        match self {
            Func::Let => {
                interpret_let_bind(expr, interpreter, span.clone())?;
                Ok(ast::Word {
                    meta: Meta::new(span.clone()),
                    kind: ast::WordKind::Empty,
                })
            }
            Func::LetFun => unimplemented!(),
            Func::LetRec => unimplemented!(),
            Func::Gensym => {
                let sym = RcStr::from(interpreter.gensym(None));
                Ok(ast::Word {
                    meta: Meta::new(span.clone()),
                    kind: ast::WordKind::Atom(ast::Atom {
                        meta: Meta::new(span),
                        atom: sym,
                    }),
                })
            }
            Func::Head => {
                match &*params {
                    [ast::Word {
                        kind: ast::WordKind::Empty,
                        ..
                    }] => {
                        return Err(vec![Error::new(ErrorKind::EmptyList)
                            .describe("cannot call head on an empty list")
                            .with_span(expr.span())])
                    }
                    [ast::Word {
                        kind: ast::WordKind::Parenth(list),
                        ..
                    }] => Ok(list.borrow().first().clone()),
                    [_] => {
                        return Err(vec![Error::new(ErrorKind::TypeMismatch)
                            .describe("expected list")
                            .with_span(expr.span())])
                    }
                    _ => {
                        return Err(vec![Error::new(ErrorKind::ArityMismatch)
                            .describe("expected exactly 1")
                            .with_span(expr.span())])
                    }
                }
            }
            Func::Tail => {
                match &*params {
                    [ast::Word {
                        kind: ast::WordKind::Empty,
                        ..
                    }] => {
                        return Err(vec![Error::new(ErrorKind::EmptyList)
                            .describe("cannot call head on an empty list")
                            .with_span(expr.span())])
                    }
                    [ast::Word {
                        kind: ast::WordKind::Parenth(list),
                        ..
                    }] => {
                        if list.borrow().rest().is_empty() {
                            Ok(ast::Word {
                                meta: Meta::new(expr.span().clone()),
                                kind: ast::WordKind::Empty,
                            })
                        } else {
                            let first = list.borrow().rest()[0].clone();
                            let rest = list.borrow().rest()[1..].to_vec();
                            Ok(ast::Word {
                                meta: Meta::new(expr.span().clone()),
                                kind: ast::WordKind::Parenth(Boxed::new(RefCell::new(ast::Expr {
                                    meta: Meta::new(expr.span().clone()),
                                    first,
                                    rest,
                                }))),
                            })
                        }
                    }
                    [_] => {
                        return Err(vec![Error::new(ErrorKind::TypeMismatch)
                            .describe("expected list")
                            .with_span(expr.span())])
                    }
                    _ => {
                        return Err(vec![Error::new(ErrorKind::ArityMismatch)
                            .describe("expected exactly 1")
                            .with_span(expr.span())])
                    }
                }
            }
            Func::Add => {
                if params.len() == 0 {
                    Ok(From::from(ast::Word {
                        meta: Meta::new(span),
                        kind: ast::WordKind::Integer(0),
                    }))
                } else if params.len() == 1 {
                    let lhs = params[0].interpret(interpreter)?;
                    let result = match lhs.kind {
                        ast::WordKind::Integer(..) => lhs,
                        ast::WordKind::Ratio(..) => lhs,
                        ast::WordKind::Real(..) => lhs,
                        _ => {
                            return Err(vec![Error::new(ErrorKind::TypeMismatch)
                                .describe("expected number")
                                .with_span(lhs.span())])
                        }
                    };
                    Ok(From::from(result))
                } else if params.len() == 2 {
                    binary_arithmetic!(params, interpreter, span, +)
                } else {
                    return Err(vec![Error::new(ErrorKind::ArityMismatch)
                        .describe("expected 0-2 arguments")
                        .with_span(&span)]);
                }
            }
            Func::Sub => {
                if params.len() == 1 {
                    let lhs = params[0].interpret(interpreter)?;
                    let result = match lhs.kind {
                        ast::WordKind::Integer(a) => {
                            ast::Word {
                                meta: Meta::new(lhs.span().clone()),
                                kind: ast::WordKind::Integer(-a),
                            }
                        }
                        ast::WordKind::Ratio(sign, p, q) => {
                            ast::Word {
                                meta: Meta::new(lhs.span().clone()),
                                kind: ast::WordKind::Ratio(-sign, p, q),
                            }
                        }
                        ast::WordKind::Real(a) => {
                            ast::Word {
                                meta: Meta::new(lhs.span().clone()),
                                kind: ast::WordKind::Real(-a),
                            }
                        }
                        _ => {
                            return Err(vec![Error::new(ErrorKind::TypeMismatch)
                                .describe("expected number")
                                .with_span(&lhs.span())])
                        }
                    };
                    Ok(From::from(result))
                } else if params.len() == 2 {
                    binary_arithmetic!(params, interpreter, span, -)
                } else {
                    return Err(vec![Error::new(ErrorKind::ArityMismatch)
                        .describe("expected 1-2 arguments")
                        .with_span(&span)]);
                }
            }
            Func::Mul => {
                if params.len() == 0 {
                    Ok(From::from(ast::Word {
                        meta: Meta::new(span),
                        kind: ast::WordKind::Integer(1),
                    }))
                } else if params.len() == 2 {
                    binary_arithmetic!(params, interpreter, span, *)
                } else {
                    return Err(vec![Error::new(ErrorKind::ArityMismatch)
                        .describe("expected 0 or 2 arguments")
                        .with_span(&span)]);
                }
            }
            Func::Div => {
                if params.len() == 2 {
                    binary_arithmetic!(params, interpreter, span, /)
                } else {
                    return Err(vec![Error::new(ErrorKind::ArityMismatch)
                        .describe("expected 2 arguments")
                        .with_span(&span)]);
                }
            }
            Func::Mod => {
                if params.len() == 2 {
                    binary_arithmetic!(params, interpreter, span, %)
                } else {
                    return Err(vec![Error::new(ErrorKind::ArityMismatch)
                        .describe("expected 2 arguments")
                        .with_span(&span)]);
                }
            }
            Func::Closure(_func) => unimplemented!(),
        }
    }
}

fn interpret_let_bind<T: ExprLike>(expr: &T, interpreter: &mut ExpanderContext, span: Span) -> Result<(), Vec<Error>> {
    let binding = expr.rest().get(0).ok_or_else(|| {
        vec![Error::new(ErrorKind::LetSyntax)
            .with_span(&span)
            .describe("missing binding")]
    })?;
    let binding = match &binding.kind {
        ast::WordKind::Atom(atom) => atom.atom.clone(),
        _ => {
            return Err(vec![Error::new(ErrorKind::LetSyntax)
                .with_span(binding.span())
                .describe("binding must be an atom")])
        }
    };
    let expr = expr.as_meta().def.as_ref().unwrap();;
    let result = expr.borrow().interpret(interpreter)?;
    interpreter.insert_binding(binding, result);
    Ok(())
}

impl<T: ExprLike> Interpret<ast::ExprSeal> for T {
    fn interpret(&self, interpreter: &mut ExpanderContext) -> Result<ast::Word, Vec<Error>> {
        if self.rest().is_empty() {
            self.first().interpret(interpreter)
        } else {
            let ty = self.first().eval_type();
            let (func, rest) = if ty.is_func() {
                let func = match &self.first().kind {
                    ast::WordKind::Atom(atom) if atom.atom.as_str() == "let" => {
                        let kind = self.rest().get(0).ok_or_else(|| {
                            vec![Error::new(ErrorKind::LetSyntax)
                                .with_span(self.span())
                                .describe("missing binding")]
                        })?;
                        match &kind.kind {
                            ast::WordKind::Atom(atom) if atom.atom.as_str() == "fun" => Func::LetFun,
                            ast::WordKind::Atom(atom) if atom.atom.as_str() == "rec" => Func::LetRec,
                            ast::WordKind::Atom(_) => Func::Let,
                            _ => {
                                return Err(vec![Error::new(ErrorKind::LetSyntax)
                                    .with_span(kind.span())
                                    .describe("binding must be an atom")])
                            }
                        }
                    }
                    ast::WordKind::Path(path) if path.path == ["macro", "gensym"] => Func::Gensym,
                    ast::WordKind::Atom(atom) if atom.atom.as_str() == "head" => Func::Head,
                    ast::WordKind::Atom(atom) if atom.atom.as_str() == "tail" => Func::Tail,
                    ast::WordKind::Atom(atom) if atom.atom.as_str() == "+" => Func::Add,
                    ast::WordKind::Atom(atom) if atom.atom.as_str() == "-" => Func::Sub,
                    ast::WordKind::Atom(atom) if atom.atom.as_str() == "*" => Func::Mul,
                    ast::WordKind::Atom(atom) if atom.atom.as_str() == "/" => Func::Div,
                    ast::WordKind::Atom(atom) if atom.atom.as_str() == "%" => Func::Mod,
                    _ => {
                        let value = self.first().interpret(interpreter)?;
                        match value.kind {
                            ast::WordKind::Closure(closure) => Func::Closure(closure),
                            _ => unreachable!(),
                        }
                    }
                };
                let rest = self.rest().iter().map(|word| word.clone()).collect::<Vec<_>>();
                (func, rest)
            } else {
                let snd = self
                    .rest()
                    .get(0)
                    .ok_or_else(|| vec![Error::new(ErrorKind::NoAdequateFunction).with_span(self.span())])?;
                let ty = snd.eval_type();
                if ty.is_func() {
                    let func = match &snd.kind {
                        ast::WordKind::Atom(atom) if atom.atom.as_str() == "+" => Func::Add,
                        ast::WordKind::Atom(atom) if atom.atom.as_str() == "-" => Func::Sub,
                        ast::WordKind::Atom(atom) if atom.atom.as_str() == "*" => Func::Mul,
                        ast::WordKind::Atom(atom) if atom.atom.as_str() == "/" => Func::Div,
                        ast::WordKind::Atom(atom) if atom.atom.as_str() == "%" => Func::Mod,
                        _ => {
                            let value = snd.interpret(interpreter)?;
                            match value.kind {
                                ast::WordKind::Closure(closure) => Func::Closure(closure),
                                _ => unreachable!(),
                            }
                        }
                    };
                    let rest = iter::once(self.first().clone())
                        .chain(self.rest().iter().skip(1).cloned())
                        .collect();
                    (func, rest)
                } else {
                    return Err(vec![Error::new(ErrorKind::NoAdequateFunction).with_span(self.span())]);
                }
            };
            func.call(&self.clone().into(), rest, interpreter, self.span().clone())
        }
    }
}

fn expand_macro<T: ExprLike>(expr: &T, expander: &mut ExpanderContext) -> Result<ast::Expr, Vec<Error>> {
    let binding = expr.rest().get(0).ok_or_else(|| {
        vec![Error::new(ErrorKind::MacroSyntax)
            .with_span(expr.span())
            .describe("missing binding")]
    })?;
    let binding = match &binding.kind {
        ast::WordKind::Atom(atom) => atom,
        _ => {
            return Err(vec![Error::new(ErrorKind::MacroSyntax)
                .with_span(binding.span())
                .describe("binding must be an atom")])
        }
    };
    let equals = expr.rest().get(1).ok_or_else(|| {
        vec![Error::new(ErrorKind::MacroSyntax)
            .with_span(expr.span())
            .describe("missing `=`")]
    })?;
    match &equals.kind {
        ast::WordKind::Atom(atom) if atom.atom.as_str() == "=" => {}
        _ => {
            return Err(vec![Error::new(ErrorKind::MacroSyntax)
                .with_span(equals.span())
                .describe("expected `=`")])
        }
    }
    let value = expr.rest().get(2).ok_or_else(|| {
        vec![Error::new(ErrorKind::MacroSyntax)
            .with_span(expr.span())
            .describe("missing value")]
    })?;
    let closure = match &value.kind {
        ast::WordKind::Closure(closure) => {
            let mut errors = vec![];
            let mut multi = vec![];
            for closure in closure {
                match closure.expand(expander) {
                    Ok(ok) => multi.push(ok),
                    Err(err) => {
                        errors.extend(err);
                        continue;
                    }
                }
            }
            if errors.is_empty() {
                multi
            } else {
                return Err(errors);
            }
        }
        _ => return Err(vec![Error::new(ErrorKind::MacroNotClosure).with_span(value.span())]),
    };
    if !closure[0].macroeh {
        return Err(vec![Error::new(ErrorKind::MacroNotMacro)
            .with_span(&closure.iter().map(|closure| closure.span().clone()).collect())]);
    }
    expander.insert_macro(binding.atom.clone(), Macro::from(closure));
    Ok(ast::Expr::new(
        expr.span().clone(),
        ast::Word {
            meta: Meta::new(expr.span().clone()),
            kind: ast::WordKind::Empty,
        },
        vec![],
    ))
}

impl Expand<()> for ast::Word {
    type Output = ast::Word;

    fn expand(&self, expander: &mut ExpanderContext) -> Result<Self::Output, Vec<Error>> {
        match &self.kind {
            ast::WordKind::Atom(atom) => {
                if let Some(word) = expander.get_binding(&atom.atom) {
                    Ok(From::from(word.clone()))
                } else {
                    Ok(From::from(self.clone()))
                }
            }
            ast::WordKind::Parenth(expr) => {
                Ok(From::from(ast::Word {
                    meta: Meta::new(self.span().clone()),
                    kind: ast::WordKind::Compound(Boxed::new(RefCell::new(ast::Block {
                        meta: Meta::new(self.span().clone()),
                        stmts: vec![],
                        expr: Some(expr.borrow().expand(expander)?),
                    }))),
                }))
            }
            ast::WordKind::Interpolate(inner) => {
                let inner = inner.borrow().expand(expander)?;
                Ok(From::from(ast::Word {
                    meta: Meta::new(self.span().clone()),
                    kind: ast::WordKind::Interpolate(Boxed::new(RefCell::new(inner))),
                }))
            }
            ast::WordKind::Bind(atom, ty, pred, var) => {
                let atom = atom.clone();
                let ty = ty.clone();
                let pred = pred.as_ref().map(|_pred| unimplemented!());
                let var = *var;
                Ok(From::from(ast::Word {
                    meta: Meta::new(self.span().clone()),
                    kind: ast::WordKind::Bind(atom, ty, pred, var),
                }))
            }
            ast::WordKind::Closure(closure) => {
                let mut errors = vec![];
                let mut multi = vec![];
                for closure in closure {
                    match closure.expand(expander) {
                        Ok(ok) => multi.push(ok),
                        Err(err) => {
                            errors.extend(err);
                            continue;
                        }
                    }
                }
                if errors.is_empty() {
                    Ok(From::from(ast::Word {
                        meta: Meta::new(self.span().clone()),
                        kind: ast::WordKind::Closure(multi),
                    }))
                } else {
                    Err(errors)
                }
            }
            ast::WordKind::Compound(block) => {
                Ok(From::from(ast::Word {
                    meta: Meta::new(self.span().clone()),
                    kind: ast::WordKind::Compound(Boxed::new(RefCell::new(block.borrow().expand(expander)?))),
                }))
            }
            _ => Ok(From::from(self.clone())),
        }
    }
}

impl Interpret<()> for ast::Word {
    fn interpret(&self, expander: &mut ExpanderContext) -> Result<ast::Word, Vec<Error>> {
        match &self.kind {
            ast::WordKind::Atom(atom) => {
                if let Some(word) = expander.get_binding(&atom.atom) {
                    Ok(From::from(word.clone()))
                } else {
                    Err(vec![Error::new(ErrorKind::UndefinedSymbol)
                        .describe(&atom.atom)
                        .with_span(self.span())])
                }
            }
            ast::WordKind::Parenth(inner) => inner.borrow().interpret(expander),
            ast::WordKind::Interpolate(inner) => inner.borrow().interpret(expander)?.interpret(expander),
            ast::WordKind::Quote(inner) => Ok(From::from(inner.borrow().clone())),
            ast::WordKind::Quasiquote(inner) => Ok(From::from(inner.borrow().quasiquote(expander)?)),
            ast::WordKind::Compound(inner) => inner.borrow().interpret(expander),
            _ => Ok(From::from(self.clone())),
        }
    }
}

impl<T: ExprLike> Expand<()> for ast::Closure<T> {
    type Output = ast::Closure<T>;

    fn expand(&self, expander: &mut ExpanderContext) -> Result<Self::Output, Vec<Error>> {
        expander.enter();
        let dynamic = self.dynamic;
        let mutable = self.mutable;
        let macroeh = self.macroeh;
        let params = self.params.iter().map(|word| expand_param(word, expander)).fold(
            Ok(vec![]),
            |acc: Result<Vec<ast::Word>, Vec<Error>>, word| {
                let mut acc = match acc {
                    Ok(ok) => ok,
                    Err(mut errors) => {
                        if let Err(errs) = word {
                            errors.extend(errs);
                        }
                        return Err(errors);
                    }
                };
                acc.push(word?);
                Ok(acc)
            },
        )?;
        expander.leave();
        Ok(ast::Closure {
            meta: Meta::new(self.span().clone()),
            dynamic,
            mutable,
            macroeh,
            params,
            body: self.body.clone(),
        })
    }
}

fn expand_param(word: &ast::Word, expander: &mut ExpanderContext) -> Result<ast::Word, Vec<Error>> {
    match &word.kind {
        ast::WordKind::Bind(binding, ty, pred, var) => {
            let atom = binding.clone();
            let ty = ty.clone();
            let pred = pred.as_ref().map(|_pred| unimplemented!());
            let var = *var;
            Ok(From::from(ast::Word {
                meta: Meta::new(word.span().clone()),
                kind: ast::WordKind::Bind(atom, ty, pred, var),
            }))
        }
        _ => word.expand(expander),
    }
}

pub trait Match {
    fn matches(&self, word: &ast::Word, interpreter: &mut ExpanderContext) -> Result<bool, Vec<Error>>;
}

impl Match for ast::Word {
    fn matches(&self, word: &ast::Word, interpreter: &mut ExpanderContext) -> Result<bool, Vec<Error>> {
        match &self.kind {
            ast::WordKind::Interpolate(inner) => {
                let inner = inner.borrow().interpret(interpreter)?;
                inner.matches(word, interpreter)
            }
            ast::WordKind::Bind(atom, _ty, _pred, _var) => {
                interpreter.insert_binding(atom.atom.clone(), word.clone());
                Ok(true)
            }
            ast::WordKind::Destructure(_ty, words) => {
                for param in words {
                    if !param.matches(word, interpreter)? {
                        return Ok(false);
                    }
                }
                Ok(true)
            }
            _ => Ok(self == word),
        }
    }
}

pub trait Quasiquote<Seal>: Sized {
    fn quasiquote(&self, interpreter: &mut ExpanderContext) -> Result<Self, Vec<Error>>;
}

impl Quasiquote<()> for ast::Word {
    fn quasiquote(&self, interpreter: &mut ExpanderContext) -> Result<Self, Vec<Error>> {
        match &self.kind {
            ast::WordKind::Parenth(list) => {
                Ok(ast::Word {
                    meta: Meta::new(self.span().clone()),
                    kind: ast::WordKind::Parenth(Boxed::new(RefCell::new(list.borrow().quasiquote(interpreter)?))),
                })
            }
            ast::WordKind::Interpolate(inner) => inner.borrow().interpret(interpreter),
            ast::WordKind::Closure(closure) => {
                let mut errors = vec![];
                let mut multi = vec![];
                for closure in closure {
                    match closure.quasiquote(interpreter) {
                        Ok(ok) => multi.push(ok),
                        Err(err) => {
                            errors.extend(err);
                            continue;
                        }
                    }
                }
                if errors.is_empty() {
                    Ok(From::from(ast::Word {
                        meta: Meta::new(self.span().clone()),
                        kind: ast::WordKind::Closure(multi),
                    }))
                } else {
                    Err(errors)
                }
            }
            ast::WordKind::Compound(inner) => {
                Ok(ast::Word {
                    meta: Meta::new(self.span().clone()),
                    kind: ast::WordKind::Compound(Boxed::new(RefCell::new(inner.borrow().quasiquote(interpreter)?))),
                })
            }
            _ => Ok(self.clone()),
        }
    }
}

impl<T: ExprLike> Quasiquote<()> for ast::Closure<T> {
    fn quasiquote(&self, interpreter: &mut ExpanderContext) -> Result<Self, Vec<Error>> {
        let params = self.params.iter().map(|param| param.quasiquote(interpreter)).fold(
            Ok(vec![]),
            |acc: Result<Vec<_>, Vec<Error>>, param| {
                let mut acc = match acc {
                    Ok(ok) => ok,
                    Err(mut errors) => {
                        if let Err(errs) = param {
                            errors.extend(errs);
                        }
                        return Err(errors);
                    }
                };
                match param {
                    Ok(ok) => acc.push(ok),
                    Err(err) => return Err(err),
                }
                Ok(acc)
            },
        )?;
        let body = Boxed::new(RefCell::new(self.body.borrow().quasiquote(interpreter)?));
        Ok(ast::Closure {
            meta: Meta::new(self.span().clone()),
            dynamic: self.dynamic,
            mutable: self.mutable,
            macroeh: self.macroeh,
            params,
            body,
        })
    }
}

impl<T: StmtLike> Quasiquote<ast::StmtSeal> for T {
    fn quasiquote(&self, interpreter: &mut ExpanderContext) -> Result<Self, Vec<Error>> {
        Ok(T::new(self.span().clone(), self.expr().quasiquote(interpreter)?))
    }
}

impl<T: ExprLike> Quasiquote<ast::ExprSeal> for T {
    fn quasiquote(&self, interpreter: &mut ExpanderContext) -> Result<Self, Vec<Error>> {
        let first = self.first().quasiquote(interpreter)?;
        let rest = self.rest().iter().map(|word| word.quasiquote(interpreter)).fold(
            Ok(vec![]),
            |acc: Result<Vec<_>, Vec<Error>>, word| {
                let mut acc = match acc {
                    Ok(ok) => ok,
                    Err(mut errors) => {
                        if let Err(errs) = word {
                            errors.extend(errs);
                        }
                        return Err(errors);
                    }
                };
                match word {
                    Ok(ok) => acc.push(ok),
                    Err(err) => return Err(err),
                }
                Ok(acc)
            },
        )?;
        Ok(T::new(self.span().clone(), first, rest))
    }
}

impl<T: BlockLike> Quasiquote<ast::BlockSeal> for T {
    fn quasiquote(&self, interpreter: &mut ExpanderContext) -> Result<Self, Vec<Error>> {
        let stmts = self.stmts().iter().map(|stmt| stmt.quasiquote(interpreter)).fold(
            Ok(vec![]),
            |acc: Result<Vec<_>, Vec<Error>>, stmt| {
                let mut acc = match acc {
                    Ok(ok) => ok,
                    Err(mut errors) => {
                        if let Err(errs) = stmt {
                            errors.extend(errs);
                        }
                        return Err(errors);
                    }
                };
                match stmt {
                    Ok(ok) => acc.push(ok),
                    Err(err) => return Err(err),
                }
                Ok(acc)
            },
        )?;
        let expr = self.expr().map(|expr| expr.quasiquote(interpreter)).transpose()?;
        Ok(T::new(self.span().clone(), stmts, expr))
    }
}
