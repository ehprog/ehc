use std::env;
use std::str::Utf8Error;
use std::fmt::{self, Display};

use failure::{Backtrace, Fail};

use crate::lex::Span;

#[derive(Debug, Clone, Copy, PartialEq, Eq, Fail)]
pub enum ErrorKind {
    #[fail(display = "invalid byte sequence in utf-8 encoded string")]
    Utf8Error,
    #[fail(display = "unexpected character")]
    UnexpectedCharacter,
    #[fail(display = "missing closing quotation")]
    MissingClosingQuote,
    #[fail(display = "invalid character literal")]
    InvalidCharLiteral,
    #[fail(display = "invalid escape sequence")]
    InvalidEscape,
    #[fail(display = "missing opening parenthesis in an interpolated string")]
    InterpMissingParen,
    #[fail(display = "parenthesis mismatch in an interpolated string")]
    InterpParenMismatch,
    #[fail(display = "unexpected end-of-file")]
    UnexpectedEof,
    #[fail(display = "expected end-of-file at this point")]
    ExpectedEof,
    #[fail(display = "unexpected token")]
    UnexpectedToken,
    #[fail(display = "unexpected empty block found")]
    EmptyBlock,
    #[fail(display = "unexpected empty program found")]
    EmptyProgram,
    #[fail(display = "invalid paragraph")]
    InvalidParagraph,
    #[fail(display = "base must be >= 2 and <= 36")]
    InvalidBase,
    #[fail(display = "an invalid attribute was encountered")]
    InvalidAttr,
    #[fail(display = "this `macro` syntax is invalid")]
    MacroSyntax,
    #[fail(display = "no matching macro implementation was found")]
    MacroMismatch,
    #[fail(display = "macro must be a closure")]
    MacroNotClosure,
    #[fail(display = "macro must be a macro")]
    MacroNotMacro,
    #[fail(display = "no adequate macro-like expression could be found")]
    NoAdequateMacro,
    #[fail(display = "expected a compound list in macro")]
    ExpectedCompoundList,
    #[fail(display = "list is empty")]
    EmptyList,
    #[fail(display = "types did not match")]
    TypeMismatch,
    #[fail(display = "arity did not match")]
    ArityMismatch,
    #[fail(display = "this word is unexpected in this context")]
    UnexpectedWord,
    #[fail(display = "couldn't evaluate the type of an expression")]
    TypeExpected,
    #[fail(display = "missing parameter in expression")]
    MissingParameter,
    #[fail(display = "no adequate function-like expression could be found")]
    NoAdequateFunction,
    #[fail(display = "this `let` syntax is invalid")]
    LetSyntax,
    #[fail(display = "this `def` syntax is invalid")]
    DefSyntax,
    #[fail(display = "this `import` syntax is invalid")]
    ImportSyntax,
    #[fail(display = "this `from` syntax is invalid")]
    FromSyntax,
    #[fail(display = "couldn't find module")]
    ImportFailed,
    #[fail(display = "this `type` syntax is invalid")]
    TypeSyntax,
    #[fail(display = "this intrinsic syntax is invalid")]
    IntrinsicSyntax,
    #[fail(display = "cannot use a pattern-like expression in value-like expressions")]
    PatternInExpr,
    #[fail(display = "attempt to access a non-aligned static")]
    StaticNotAligned,
    #[fail(display = "invalid interpretation of a static")]
    StaticInvalidType,
    #[fail(display = "undefined register in bytecode")]
    UndefinedRegister,
    #[fail(display = "undefined constant in bytecode")]
    UndefinedStatic,
    #[fail(display = "undefined label in bytecode")]
    UndefinedLabel,
    #[fail(display = "unknown opcode")]
    UnknownOpcode,
    #[fail(display = "invalid operands for instruction")]
    InvalidOperands,
    #[fail(display = "an illegal NULL reference occured")]
    NullReference,
    #[fail(display = "undefined symbol")]
    UndefinedSymbol,
    #[fail(display = "undefined type")]
    UndefinedType,
    #[fail(display = "a duplicate frame was detected in linked files")]
    LinkDuplicateFrame,
    #[fail(display = "a duplicate basic block was detected in linked files")]
    LinkDuplicateBlock,
    #[fail(display = "a duplicate static was detected in linked files")]
    LinkDuplicateStatic,
    #[fail(display = "this frame doesn't return or tailcall")]
    FrameNoRet,
    #[fail(display = "the runtime panicked")]
    Panic,
    #[fail(display = "no program was loaded into the virtual machine")]
    NoProgram,
    #[fail(display = "this symbol is not a valid c string")]
    InvalidSymbol,
    #[fail(display = "no target was found for this machine")]
    NoTargetFound,
    #[fail(display = "an internal LLVM error occured")]
    LLVM,
}

#[derive(Debug)]
pub struct Error {
    kind: ErrorKind,
    backtrace: Backtrace,
    cause: Option<Box<Fail + Send + Sync>>,
    desc: Option<String>,
    span: Option<(String, String, usize, usize)>,
}

impl Error {
    pub fn new(kind: ErrorKind) -> Self {
        Error {
            kind,
            backtrace: Backtrace::new(),
            cause: None,
            desc: None,
            span: None,
        }
    }

    pub fn with_cause<E: Fail + Send + Sync>(mut self, error: E) -> Self {
        self.cause = Some(Box::new(error));
        self
    }

    pub fn with_span(mut self, span: &Span) -> Self {
        self.span = Some((span.file().to_string(), span.to_string(), span.line(), span.col()));
        self
    }

    pub fn describe<S: ToString>(mut self, desc: S) -> Self {
        self.desc = Some(desc.to_string());
        self
    }

    pub fn kind(&self) -> ErrorKind {
        self.kind
    }
}

impl Display for Error {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let colours = match env::var("EHPROG_NO_COLOURS") {
            Ok(colours) => {
                match colours.to_lowercase().as_str() {
                    "0" | "false" | "no" | "" => true,
                    _ => false,
                }
            }
            Err(_) => true,
        };

        if colours {
            write!(f, "\x1b[31;1m")?;
        }
        write!(f, "an error occured at: ")?;
        if let Some((file, src, line, col)) = &self.span {
            write!(f, "{}:{}:{}: `{}`: ", file, line, col, src)?;
        }
        write!(f, "{}", self.kind)?;
        if let Some(ref desc) = self.desc {
            write!(f, ": {}", desc)?;
        }
        if let Some(ref cause) = self.cause {
            write!(f, ": {}", cause)?;
        }
        if env::var("RUST_BACKTRACE").is_ok() || env::var("RUST_FAILURE_BACKTRACE").is_ok() {
            write!(f, "\n{}", self.backtrace)?;
        }
        if colours {
            write!(f, "\x1b[0m")
        } else {
            Ok(())
        }
    }
}

impl Fail for Error {
    fn cause(&self) -> Option<&dyn Fail> {
        if let Some(ref cause) = self.cause {
            Some(&**cause)
        } else {
            Some(&self.kind)
        }
    }

    fn backtrace(&self) -> Option<&Backtrace> {
        Some(&self.backtrace)
    }
}

impl PartialEq for Error {
    fn eq(&self, other: &Self) -> bool {
        self.kind == other.kind && self.desc == other.desc && self.span == other.span
    }
}

impl From<Utf8Error> for Error {
    fn from(_err: Utf8Error) -> Error {
        Error::new(ErrorKind::Utf8Error)
    }
}
