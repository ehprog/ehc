#include "test.h"

int foo() {
    return 42;
}

double bar(double baz) {
    return 42 * baz;
}

double qux(double quuz) {
    return quuz + 42;
}
